﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MunicipalityCardPrinting.Controllers
{
    public class CardPrintController : Controller
    {
        // GET: CardPrint
        public ActionResult Index()
        {
            return View();
        }

        // GET: CardPrint/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CardPrint/Create
        public ActionResult AddUpdateView()
        {

            return View("AddUpdateCard",new CardDetailDTO());
        }

        // POST: CardPrint/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUpdateView(IFormCollection collection)
        {
            try
            {
               

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CardPrint/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CardPrint/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CardPrint/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CardPrint/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}