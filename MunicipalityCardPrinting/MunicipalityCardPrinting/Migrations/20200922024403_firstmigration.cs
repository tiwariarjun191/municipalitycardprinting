﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MunicipalityCardPrinting.Migrations
{
    public partial class firstmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Provinces",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProvinceName = table.Column<string>(nullable: true),
                    ProvinceNameNepali = table.Column<string>(nullable: true),
                    Capital = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provinces", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProvinceId = table.Column<long>(nullable: false),
                    DistrictName = table.Column<string>(nullable: true),
                    DistrictNameNepali = table.Column<string>(nullable: true),
                    HeadQuarter = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Districts_Provinces_ProvinceId",
                        column: x => x.ProvinceId,
                        principalTable: "Provinces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MunicipalityVDCs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistrictId = table.Column<long>(nullable: false),
                    VDCName = table.Column<string>(nullable: true),
                    VDCNameNepali = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MunicipalityVDCs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MunicipalityVDCs_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonalDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    FullNameNepali = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    TempAddressId = table.Column<long>(nullable: false),
                    PermanentAddressId = table.Column<long>(nullable: false),
                    TempTole = table.Column<string>(nullable: true),
                    PermanentTole = table.Column<string>(nullable: true),
                    TempWardNumber = table.Column<string>(nullable: true),
                    PermanentWardNumber = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    DOBNepali = table.Column<string>(nullable: true),
                    CitizenshipNumber = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<int>(nullable: true),
                    MarritalStatus = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonalDetails_MunicipalityVDCs_PermanentAddressId",
                        column: x => x.PermanentAddressId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalDetails_MunicipalityVDCs_TempAddressId",
                        column: x => x.TempAddressId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CardDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DisabledPersonId = table.Column<long>(nullable: false),
                    DisabilityTypeByNature = table.Column<int>(nullable: false),
                    DisabilityTypeBySeriousness = table.Column<int>(nullable: false),
                    GuardianId = table.Column<long>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    Qualification = table.Column<string>(nullable: true),
                    CurrentSchoolCollegeName = table.Column<string>(nullable: true),
                    StudyingOrFinished = table.Column<string>(nullable: true),
                    WhatKindOfWorkcanDoInDailyLife = table.Column<string>(nullable: true),
                    WhatKindOfWorkCantDo = table.Column<string>(nullable: true),
                    IsNeededAnyEquipement = table.Column<bool>(nullable: true),
                    AdditionalEquipements = table.Column<string>(nullable: true),
                    HasAnyAdditionalEquipement = table.Column<bool>(nullable: true),
                    OtherFacilities = table.Column<string>(nullable: true),
                    OtherFacilitiesNeeded = table.Column<string>(nullable: true),
                    OfficeToProvideFacility = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardDetails_PersonalDetails_DisabledPersonId",
                        column: x => x.DisabledPersonId,
                        principalTable: "PersonalDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CardDetails_PersonalDetails_GuardianId",
                        column: x => x.GuardianId,
                        principalTable: "PersonalDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CardDetails_DisabledPersonId",
                table: "CardDetails",
                column: "DisabledPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_CardDetails_GuardianId",
                table: "CardDetails",
                column: "GuardianId");

            migrationBuilder.CreateIndex(
                name: "IX_Districts_ProvinceId",
                table: "Districts",
                column: "ProvinceId");

            migrationBuilder.CreateIndex(
                name: "IX_MunicipalityVDCs_DistrictId",
                table: "MunicipalityVDCs",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalDetails_PermanentAddressId",
                table: "PersonalDetails",
                column: "PermanentAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalDetails_TempAddressId",
                table: "PersonalDetails",
                column: "TempAddressId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardDetails");

            migrationBuilder.DropTable(
                name: "PersonalDetails");

            migrationBuilder.DropTable(
                name: "MunicipalityVDCs");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "Provinces");
        }
    }
}
