﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MunicipalityCardPrinting.Migrations
{
    public partial class SecondMigrationFieldAddedinCardDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CitizenshipNumberNepali",
                table: "PersonalDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdCardType",
                table: "CardDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "IdCardnumber",
                table: "CardDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CitizenshipNumberNepali",
                table: "PersonalDetails");

            migrationBuilder.DropColumn(
                name: "IdCardType",
                table: "CardDetails");

            migrationBuilder.DropColumn(
                name: "IdCardnumber",
                table: "CardDetails");
        }
    }
}
