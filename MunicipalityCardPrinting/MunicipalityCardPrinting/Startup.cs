using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Services.Implementation;
using Business.Services.Interface;
using Infrastructure.Database.Context;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Implementation;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MunicipalityCardPrinting
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)

        {
            var connection = @"server=.\\sqlexpress;database=CardPrinting;Enlist= false; trusted_connection=true;";
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseLazyLoadingProxies().UseSqlServer(connection, b => b.MigrationsAssembly("MunicipalityCardPrinting"));
                //  options.ConfigureWarnings(x => x.Ignore(RelationalEventId.AmbientTransactionWarning));
            });

            services.AddControllersWithViews();
            services.AddMvc();
          
           // services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connection));

            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IPersonalDetailService, PersonalDetailService>();
            services.AddScoped<ICardService, CardService>();
            //services.AddScoped<IBaseRepository<CardDetail>, BaseRepository<CardDetail>>();
            //services.AddScoped<IBaseRepository<PersonalDetail>, BaseRepository<PersonalDetail>>();
            //services.AddScoped<IBaseRepository<CardDetail>, BaseRepository<CardDetail>>();
            //services.AddScoped<IBaseRepository<Province>, BaseRepository<Province>>();
            //services.AddScoped<IBaseRepository<District>, BaseRepository<District>>();
            //services.AddScoped<IBaseRepository<MunicipalityVDC>, BaseRepository<MunicipalityVDC>>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
