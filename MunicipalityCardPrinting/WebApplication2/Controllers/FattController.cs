﻿using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Controllers
{
    [Authorize]
    public class FattController : Controller
    {

        private readonly IFattService _fattService;
        private readonly IBaseRepository<Fatt> _fattRepository;

        public FattController(IFattService fattService, IBaseRepository<Fatt> _fattRepository)
        {
            this._fattService = fattService;
            this._fattRepository = _fattRepository;
        }

        public IActionResult Index()
        {
           var data= _fattService.getAllFattDetails();
            return View(data);
        }

        public IActionResult AddUpdateFatt(long id)
        {

            var fatt = new FattDto();
             if (id > 0)
            {
                fatt=_fattService.GetByFattId(id);
            }
            return View(fatt);
        }

        [HttpPost]
        public IActionResult AddUpdateFatt(FattDto fattdto)
        {
            try
            {
                if (fattdto.id > 0)
                {
                    _fattService.UpdateFatt(fattdto);
                }
                else
                {
                    _fattService.AddFatt(fattdto);
                }
                 
                return RedirectToAction("index");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IActionResult DeleteFatt(FattDto fattDto)
        {
            if (fattDto.id > 0)
            {
                _fattService.DeleteFatt(fattDto);
                return RedirectToAction("Index");
            }
            return View();
            
        }
    }
}
