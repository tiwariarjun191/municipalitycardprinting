﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Business.Services.Interface;
using Infrastructure;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MunicipalityCardPrinting.Models;

namespace MunicipalityCardPrinting.Controllers
{
    [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICardService _cardService;

        public HomeController(ILogger<HomeController> logger, ICardService cardService)
        {
            _logger = logger;
            _cardService = cardService;
        }

        [Authorize]

        public IActionResult Index()
        {
            try
            {
                var userId = HttpContext.User.Claims.FirstOrDefault()?.Value;
                var data = _cardService.GetAllDataForHome();
               
                return View(data);
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        [Authorize]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [AllowAnonymous]
        public IActionResult ExpiredPage()
        {
            return View();
        }
    }
}
