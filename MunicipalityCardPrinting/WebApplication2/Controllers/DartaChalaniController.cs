﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using DTO.Enums;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MunicipalityCardPrinting.Utils;

namespace MunicipalityCardPrinting.Controllers
{
    [Authorize]
    public class DartaChalaniController : Controller
    {
        private readonly IBaseRepository<Darta> _dartaRepo;
        private readonly IBaseRepository<Chalani> _chalaniRepo;
        private readonly IAddressService _addressService;
        private readonly IBaseRepository<ApplicationUser> _userRepo;
        private readonly IDartaChalaniService _dartaChalaniService;
        private readonly IFileHelper _fileHelper;
       

        public DartaChalaniController(IBaseRepository<Darta> dartaRepo, IAddressService addressService, IDartaChalaniService dartaChalaniService, IBaseRepository<ApplicationUser> userRepo, IFileHelper fileHelper, IBaseRepository<Chalani> chalaniRepo, ISequenceService sequenceService, IDateConverter dateConverter) 
        {
            _fileHelper = fileHelper;
            _dartaRepo = dartaRepo;
            _userRepo = userRepo;
            _chalaniRepo = chalaniRepo;
            _addressService = addressService;
            _dartaChalaniService = dartaChalaniService;
        }


        // GET: DartaChalani
        public ActionResult DartaList(string Name)
        {
            try
            {
                var allDarta = _dartaRepo.getAll();
                if (!string.IsNullOrEmpty(Name))
                {
                    allDarta = allDarta.Where(a => a.DartaNo.Contains(Name)).ToList();
                }
                return View(allDarta);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult ChalaniList(string Name)
        {
            try
            {

                var allChalani = _chalaniRepo.getAll();
                if (!string.IsNullOrEmpty(Name))
                {
                    allChalani = allChalani.Where(a => a.ChalaniNo.Contains(Name)).ToList();
                }
                return View(allChalani);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        // GET: DartaChalani/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DartaChalani/Create
        public ActionResult CreateDartaDetail(long Id)
        {
            try
            {


                var Fatt = _addressService.GetAllfaatDropdown();
                ViewBag.Fatt = new SelectList(Fatt, "Id", "DisplayName");

                ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = d.GetDisplayName()
                                     };

                var provinces = _addressService.GetAllProvincedropdown();
                ViewBag.provinces = new SelectList(provinces, "Id", "DisplayName");
                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
                var Vdcs = _addressService.GetAllVdcDropdown();
                ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

                var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
                ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");
                var model = new DartaDto();

                if (Id > 0)
                {
                    model = _dartaChalaniService.GetDartaDtoById(Id);
                }
                return View(model);
            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }

        // POST: DartaChalani/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDartaDetail(DartaDto darta)
        {

            try
            {

                var Fatt = _addressService.GetAllfaatDropdown();
                ViewBag.Fatt = new SelectList(Fatt, "Id", "DisplayName");

                ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = d.GetDisplayName()
                                     };

                var provinces = _addressService.GetAllProvincedropdown();
                ViewBag.provinces = new SelectList(provinces, "Id", "DisplayName");
                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
                var Vdcs = _addressService.GetAllVdcDropdown();
                ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

                var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
                ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");

                if (darta.file != null)
                {
                    darta.Photo = _fileHelper.saveImageAndGetFileName(darta.file, darta.Photo);
                }
                _dartaChalaniService.AddUpdateDarta(darta);
                return RedirectToAction("DartaList");
            }
            catch(Exception ex)
            {
                return View();
            }
        }


        // GET: DartaChalani/Create
        public ActionResult CreateChalaniDetail(long Id)
        {

            try
            {
                ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = d.GetDisplayName()
                                     };

                var provinces = _addressService.GetAllProvincedropdown();
                ViewBag.provinces = new SelectList(provinces, "Id", "DisplayName");


               

                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");

                var Vdcs = _addressService.GetAllVdcDropdown();
                ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

                var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
                ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");
                var model = new ChalaniDto();

                if (Id > 0)
                {
                    model = _dartaChalaniService.GetChalaniDtoById(Id);
                }
                return View(model);
            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }

        // POST: DartaChalani/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateChalaniDetail(ChalaniDto chalani)
        {

            try
            {

                ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = d.GetDisplayName()
                                     };

                var provinces = _addressService.GetAllProvincedropdown();
                ViewBag.provinces = new SelectList(provinces, "Id", "DisplayName");




                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");

                var Vdcs = _addressService.GetAllVdcDropdown();
                ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

                var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
                ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");

                if (chalani.file != null)
                {
                    chalani.Photo = _fileHelper.saveImageAndGetFileName(chalani.file, chalani.Photo);
                }
                _dartaChalaniService.AddUpdateChalani(chalani);



                return RedirectToAction("ChalaniList");
            }
            catch
            {

                return View(chalani);
            }
        }

        public ActionResult DeleteDarta(int id)
        {
            _dartaChalaniService.DeleteDarta(id);
            return RedirectToAction("DartaList");
        }

        public ActionResult DeleteChalani(int id)
        {
            _dartaChalaniService.DeleteChalani(id);
            return RedirectToAction("ChalaniList");
        }

        // GET: DartaChalani/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DartaChalani/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: DartaChalani/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DartaChalani/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult getDistrictsFromProvince(long Id)
        {

            try
            {
                var districts = _addressService.GetDistrictDropDownFromProvinceId(Id).ToList();

                return Json(districts);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }

        }
        public IActionResult getVdcsFromDistrict(long Id)
        {
            try
            {
                var vdcs = _addressService.GetMunVdcDropDownFromProvinceId(Id).ToList();

                return Json(vdcs);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }

        }
        public IActionResult GetDartaListOfSevenDays()
        {
            var data = _dartaChalaniService.GetDartaDetailsOfSevenDays();
            return View(data);
        }

        public IActionResult GetChalaniListOfSevenDays()
        {
            var data = _dartaChalaniService.GetChalaniDetailsOfSevenDays();
            return View(data);
        }
    }
}
