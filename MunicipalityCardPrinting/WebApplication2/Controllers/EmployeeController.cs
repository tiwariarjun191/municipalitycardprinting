﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using DTO.Enums;
using Infrastructure.Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Controllers
{

    [Authorize]
    public class EmployeeController : Controller
    {
       
        private readonly IEmployeeService _employeeService;
        private readonly IAddressService _addressService;

        public EmployeeController(IEmployeeService employeeService,IAddressService addressService)
        {
            _employeeService = employeeService;
            _addressService = addressService;
            
        }
        public IActionResult Index(String Name)
        {
            var data = _employeeService.GetAllEmployee().ToList();
            if (!string.IsNullOrEmpty(Name))
            {
                data = data.Where(a => a.Employee_Name_English.ToLower().Contains(Name.ToLower())).ToList();
            }
            return View(data);
        }

        [HttpGet]
        public IActionResult AddUpdateEmployee(long id)
        {

            ViewBag.GenderDropdown = from Gender d in Enum.GetValues(typeof(Gender))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = GenderEnumHandler.ConvertToNepali(d)
                                     };


            ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                 select new
                                 {
                                     Id = ((int)d).ToString(),
                                     DisplayName = d.GetDisplayName()
                                 };

            var districts = _addressService.GetAllDistrictDropdown();
            ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
            var Vdcs = _addressService.GetAllVdcDropdown();
            ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");
            var shredi = _employeeService.GetAllShrediDropdown();
            ViewBag.shrediDropdown = new SelectList(shredi, "Id", "DisplayName");
            var designation = _employeeService.GetAllDesignationDropdown();
            ViewBag.designationDropdown = new SelectList(designation, "Id", "DisplayName");


            var model = new EmployeeDto();
            if (id > 0)
            {
                 model=_employeeService.GetByEmployeeId(id);
            }
            return View(model);
        }


        [HttpPost]

        public IActionResult AddUpdateEmployee(EmployeeDto dto)
        {
            try
            {

                ViewBag.GenderDropdown = from Gender d in Enum.GetValues(typeof(Gender))
                                         select new
                                         {
                                             Id = ((int)d).ToString(),
                                             DisplayName = GenderEnumHandler.ConvertToNepali(d)
                                         };


                ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = d.GetDisplayName()
                                     };

                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
                var Vdcs = _addressService.GetAllProvincedropdown();
                ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");
                var shredi = _employeeService.GetAllShrediDropdown();
                ViewBag.shrediDropdown = new SelectList(shredi, "Id", "DisplayName");
                var designation = _employeeService.GetAllDesignationDropdown();
                ViewBag.designationDropdown = new SelectList(designation, "Id", "DisplayName");

                _employeeService.AddUpdateEmployee(dto);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                throw;
            }
        }


        public IActionResult DeleteEmployee(long id)
        {
            try
            {
                _employeeService.DeleteEmployee(id);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IActionResult getVdcsFromDistrict(long Id)
        {
            try
            {
                var vdcs = _addressService.GetMunVdcDropDownFromProvinceId(Id).ToList();

                return Json(vdcs);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }

        }

    }
}
