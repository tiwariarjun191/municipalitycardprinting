﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Business.Services.Interface;
using DTO;
using DTO.Enums;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MunicipalityCardPrinting.Utils;

namespace MunicipalityCardPrinting.Controllers
{
    public class AccountController : Controller
    {
        // GET: AccountController
        IBaseRepository<Designation> _designationRepo;
        IDesignationService _DesignationService;
        IShrediService _shrediService;
        IBaseRepository<ApplicationUser> _userRepo;
        IBaseRepository<IdentityRole> _roleRepo;
        UserManager<ApplicationUser> _userManager;
        SignInManager<ApplicationUser> _signInManager;
        RoleManager<IdentityRole> _roleManager;
        public AccountController(IBaseRepository<Designation> designationRepo, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IBaseRepository<ApplicationUser> userRepo, RoleManager<IdentityRole> roleManager, IBaseRepository<IdentityRole> roleRepo, IDesignationService DesignationService,IShrediService shrediService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _designationRepo = designationRepo;
            _userRepo = userRepo;
            _roleManager = roleManager;
            _roleRepo = roleRepo;
            _DesignationService = DesignationService;
            _shrediService = shrediService;
        }
        public ActionResult Index()
        {
            return View();
        }

        // GET: AccountController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AccountController/Create
        public ActionResult Register()
        {
            try
            {
                //ViewBag.WardNumberEnum = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                //                             select new
                //                             {
                //                                 Id = ((int)d).ToString(),
                //                                 DisplayName = d.GetDisplayName()
                //                             };

                ViewBag.DesignationDrpDown = _designationRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.NameNepali });
                ViewBag.RoleDropDown= _roleRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.Name });
                return View();
            }
            catch (Exception)
            {

                throw;
            }
           
        }
        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            try
            {
                ViewBag.DesignationDrpDown = _designationRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.NameNepali });
                ViewBag.RoleDropDown = _roleRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.Name });
                if (ModelState.IsValid)
                {
                    var appUser = new ApplicationUser();
                    appUser.FullName = model.FullName;
                    appUser.FullNameNepali = model.NameNepali;
                    appUser.UserName = model.UserName;
                    appUser.PhoneNumber = model.PhoneNumber;
                    appUser.DesignationId = model.DesignationId;
                    appUser.Email = model.Email;
                    var result=await _userManager.CreateAsync(appUser, model.Password);
                    if (result.Succeeded)
                    {
                        var role = await _roleManager.FindByIdAsync(model.RoleId);
                       await _userManager.AddToRoleAsync(appUser, role.Name);
                       
                        AlertHelper.setMessage(this, "User Registered Successfully", ResponseTypes.success);
                        return RedirectToAction("Register");
                    }
                    AddErrors(result);
                }
                return View(model);
                
            }
            catch (Exception)
            {

                AlertHelper.setMessage(this, "Something went wrong please contact administrator", ResponseTypes.error);
                return View(model);
            }

        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(
                    model.UserName, model.Password, model.RememberMe, false);
                var user = await _userManager.FindByNameAsync(model.UserName);
                
                if (result.Succeeded)
                {
                    var claims = new List<Claim>()
                    {
              new Claim(ClaimTypes.NameIdentifier,user.Id.ToString())
                   };
                    var userIdentity = new ClaimsIdentity(claims, "local");
                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                    AuthenticationProperties prop = new AuthenticationProperties();
                    prop.ExpiresUtc = DateTime.UtcNow.AddDays(30);
                    //prop.IsPersistent = model.remember_me;
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, prop);
                    return Redirect("/Home/index");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            }

            return View(model);
        }
        [HttpGet]
        public IActionResult ChangePassword()
        {
            try
            {
                var data = new ChangePasswordViewModel();
                return View(data);
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel change)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   // var userId = HttpContext.User.Claims.FirstOrDefault()?.Value;
                    var user =await _userManager.GetUserAsync(User);
                    var result = await _userManager.ChangePasswordAsync(user,change.OldPassword,change.Password);

                    if (result.Succeeded)
                    {
                        AlertHelper.setMessage(this, "Password Changed Successfully", ResponseTypes.success);
                        return Redirect("/Home/index");
                    }

                    AddErrors(result);
                }
                return View(change);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            await HttpContext.SignOutAsync("Cookies");
            return RedirectToAction("Login");
        }
        public ActionResult AddUpdateDesignation(long Id)
        {
            try
            {
                var data = _designationRepo.getById(Id);
                return View(data);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //shredi



        public ActionResult ShrediIndex()
        {
            var data = _shrediService.GetAllShrediDetails();
            return View(data);
        }

        [HttpGet]
        public ActionResult AddShredi()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddShredi(ShrediDto dto)
        {
            try
            {
                _shrediService.AddShredi(dto);
                return RedirectToAction("ShrediIndex");
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpGet]
        public ActionResult UpdateShredi(long id)
        {
            var data = _shrediService.GetByShrediId(id);
            return View(data);
        }

        [HttpPost]
        public ActionResult UpdateShredi(ShrediDto dto)
        {
            try
            {
                _shrediService.UpdateShredi(dto);
                return RedirectToAction("ShrediIndex");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeleteShredi(long id)
        {
           
             if (id > 0)
                {
                   _shrediService.DeleteShredi(id);
                   return RedirectToAction("ShrediIndex");
                }

            return View();
           
            
        }





        //designation
        [HttpPost]
        public ActionResult AddUpdateDesignation(DesignationDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Id == 0)
                    {
                        _DesignationService.AddDesignation(model);
                    }
                    else
                    {
                        _DesignationService.UpdateDesignation(model);
                    }
                    return RedirectToAction("DesignationList");
                }
                return View(model);
                
                
            }
            catch (Exception)
            {

                return View(model);
            }
        }

        // POST: AccountController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AccountController/Edit/5
        [HttpGet]
        public async Task<IActionResult> EditUser(string Id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(Id);
                ViewBag.DesignationDrpDown = _designationRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.NameNepali });
                ViewBag.RoleDropDown = _roleRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.Name });
                if (user == null)
                {
                    AlertHelper.setMessage(this, "User not found", ResponseTypes.error);
                    return RedirectToAction("UserList");
                }
                var model = new EditUserViewModel();

                model.FullName = user.FullName;
                model.NameNepali = user.FullNameNepali;
                model.UserName = user.UserName;
                model.PhoneNumber = user.PhoneNumber;
                model.DesignationId = user.DesignationId;
                model.Email = user.Email;

                return View(model);
            }
            catch (Exception ex)
            {

                throw;
            }
           
        }

        public ActionResult DesignationList()
        {
            try
            {
                var allDesignation = _designationRepo.getAll();
                return View(allDesignation);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult DeleteDesignation(long Id)
        {
            try
            {
                var des = _designationRepo.getById(Id);
                if (des == null)
                {
                    AlertHelper.setMessage(this, "Designation not found", ResponseTypes.error);
                    return RedirectToAction("DesignationList");
                }
                var user = _userRepo.getQueryable().Where(a => a.DesignationId == Id).FirstOrDefault();
                if (user != null)
                {
                    AlertHelper.setMessage(this, "Can't delete designation already associated to user", ResponseTypes.error);
                    return RedirectToAction("DesignationList");
                }
                _designationRepo.delete(des);
                AlertHelper.setMessage(this, "Deleted Successfully", ResponseTypes.success);
                return RedirectToAction("DesignationList");
            }
            catch (Exception ex)
            {

                AlertHelper.setMessage(this, "Something went wrong please contact administrator", ResponseTypes.error);
                return RedirectToAction("DesignationList");
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel model)
        {
            try
            {
                ViewBag.DesignationDrpDown = _designationRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.NameNepali });
                ViewBag.RoleDropDown = _roleRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.Name });
                var user = await _userManager.FindByIdAsync(model.Id);

                if (user == null)
                {
                    ModelState.AddModelError("", "User Not Found");
                    return View(model);
                }
                else
                {
                    user.FullName = model.FullName;
                    user.FullNameNepali = model.NameNepali;
                    user.UserName = model.UserName;
                    user.PhoneNumber = model.PhoneNumber;
                    user.DesignationId = model.DesignationId;
                    user.Email = model.Email;

                    var result = await _userManager.UpdateAsync(user);

                    if (result.Succeeded)
                    {
                        AlertHelper.setMessage(this, "User Updated Successfully", ResponseTypes.success);
                        return RedirectToAction("UserList");
                    }

                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }

                    return View(model);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
        public ActionResult UserList()
        {
            try
            {
                var AllUser = _userRepo.getQueryable().Select(a => new UserListViewModel { Id = a.Id, Name = a.FullName, FullNameNepali = a.FullNameNepali, Email = a.Email, UserName = a.UserName, Designation = a.DesignationInfo.NameNepali }).ToList();
                return View(AllUser);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AccountController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
        }
    }
}
