﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using DTO.Children;
using DTO.Enums;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MunicipalityCardPrinting.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Controllers
{

     [Authorize]
    public class OldChildCardController : Controller
    {
        private readonly IOldChildCardService _cardService;
        private readonly IAddressService _addressService;
        private readonly IEmployeeService _employeeService;
        private readonly IBaseRepository<ApplicationUser> _userRepo;
        private readonly IBaseRepository<CardDetail> _cardRepo;
        private readonly IBaseRepository<OldChildDetails> _childRepo;
        private readonly IFileHelper _fileHelper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        public OldChildCardController(IOldChildCardService cardService, IAddressService addressService, IBaseRepository<ApplicationUser> userRepo, IFileHelper fileHelper, IBaseRepository<CardDetail> cardRepo, IWebHostEnvironment hostingEnvironment, IBaseRepository<OldChildDetails> childRepo, UserManager<ApplicationUser> userManager, IEmployeeService employeeService)
        {
            _cardService = cardService;
            _addressService = addressService;
            _userRepo = userRepo;
            _fileHelper = fileHelper;
            _cardRepo = cardRepo;
            _hostingEnvironment = hostingEnvironment;
            _childRepo = childRepo;
            _userManager = userManager;
            _employeeService = employeeService;
        }
        // GET: CardPrint
        public ActionResult Index()
        {
            return View();
        }
       

        public ActionResult OldChildCardDetail(string Name,string CardNumber)
        {
            try
            {
                var cards = _cardService.GetAllOldChildernCardDetail();
                if (!string.IsNullOrEmpty(Name))
                {
                    cards = cards.Where(a => a.NameEnglish.ToLower().Contains(Name.ToLower())).ToList();
                }

                if (!string.IsNullOrEmpty(CardNumber))
                {
                    cards = cards.Where(a => a.CardNumber.Contains(CardNumber.ToLower())).ToList();
                }


                return View(cards);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
       
        public ActionResult OldChildCardPrintView(long Id)
        {
            try
            {
                var cardView = _cardService.GetOldChildernCardById(Id);
                return View(cardView);

            }
            catch (Exception ex)
            {

                throw;
            }
        }



       
        public ActionResult Details(int id)
        {
            return View();
        }
      
        public ActionResult AddUpdateOldChildCardDetail(int Id)
        {
            try
            {

                ViewBag.GenderTypeDropdown = from Gender d in Enum.GetValues(typeof(Gender))
                                             select new
                                             {
                                                 Id = ((int)d).ToString(),
                                                 DisplayName = GenderEnumHandler.ConvertToNepali(d)
                                             };


                ViewBag.PerWardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                        select new
                                        {
                                            Id = ((int)d).ToString(),
                                            DisplayName = d.GetDisplayName()
                                        };

                var provinces = _addressService.GetAllProvincedropdown();
                ViewBag.perprovinces = new SelectList(provinces, "Id", "DisplayName");
                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.PerDistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
                var Vdcs = _addressService.GetAllVdcDropdown();
                ViewBag.PervdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

                var employee = _employeeService.GetAllEmployeeDropdown();
                ViewBag.employee = new SelectList(employee, "Id", "DisplayName");

                var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
                ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");
                var model = new OldChildDetailDto();

                if (Id > 0)
                {
                    model = _cardService.GetOldChildCardDtoById(Id);
                }
                return View(model);
            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }
        [HttpPost]
        public ActionResult AddUpdateOldChildCardDetail(OldChildDetailDto model)
        {
            try
            {


                if (model.file != null)
                {
                    model.Photo = _fileHelper.saveImageAndGetFileName(model.file, model.Photo);
                }
                if (model.FatherCitizenFile != null)
                {
                    model.FatherCitizen = _fileHelper.saveImageAndGetFileName(model.FatherCitizenFile, model.FatherCitizen);
                }
                if (model.MotherCitizenFile != null)
                {
                    model.MotherCitizen = _fileHelper.saveImageAndGetFileName(model.MotherCitizenFile, model.MotherCitizen);
                }
                if (model.BirthCertificateFile != null)
                {
                    model.BirthCertificate = _fileHelper.saveImageAndGetFileName(model.BirthCertificateFile, model.BirthCertificate);
                }
                if (model.OtherFile != null)
                {
                    model.other = _fileHelper.saveImageAndGetFileName(model.OtherFile, model.other);
                }
                var user = _userManager.GetUserAsync(User).Result;
                model.UserId = user.Id.ToString();
                var Id = _cardService.AddUpdateOldChildCardDetail(model);

                return RedirectToAction("OldChildCardPrintView", new { Id = Id });
            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }


        [HttpGet]
        public ActionResult GetOldChildPrintPartialView(int Id)
        {
            _cardService.UpdateOldChildCardNumber(Id);
            var cardModel = _cardService.GetOldChildernCardById(Id);

            var htmlView = ControllerExtensions.RenderViewAsync(this, "GetOldChildPrintPartialView", cardModel, true).GetAwaiter().GetResult();
            return Json(new { success = true, dataView = htmlView });
        }
      

        public ActionResult IncreaseChildPrintCount(long Id, string cardNumber)
        {
            try
            {

                _cardService.IncreaseOldChildCardPrintCount(Id);
                return (RedirectToAction("OldChildCardPrintView", new { Id = Id }));
            }
            catch (Exception ex)
            {

                return Json(new { success = false });
            }
        }




        public IActionResult getDistrictsFromProvince(long Id)
        {

            try
            {
                var districts = _addressService.GetDistrictDropDownFromProvinceId(Id).ToList();

                return Json(districts);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }
        }
        
        public IActionResult getVdcsFromDistrict(long Id)
        {
            try
            {
                var vdcs = _addressService.GetMunVdcDropDownFromProvinceId(Id).ToList();

                return Json(vdcs);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }

        }
    }
}
