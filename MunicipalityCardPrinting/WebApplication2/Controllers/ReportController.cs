﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MunicipalityCardPrinting.Utils;

namespace MunicipalityCardPrinting.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {

        private readonly IBaseRepository<Darta> _dartaRepo;
        private readonly IBaseRepository<Chalani> _chalaniRepo;
        private readonly IDateConverter _dateConverter;


        public ReportController(IBaseRepository<Darta> dartaRepo,  IBaseRepository<Chalani> chalaniRepo, IDateConverter dateConverter)
        {
            _dartaRepo = dartaRepo;
            _chalaniRepo = chalaniRepo;
            _dateConverter = dateConverter;
          
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult DartaList(string from_date, string to_date)
        {
            try
            {


                var allDarta = _dartaRepo.getQueryable();


                


                if (from_date != null)
                {
                    //var replacedata = from_date.Replace('-', '/');
                    var english_From_date = _dateConverter.ToAD(from_date);
                    allDarta = allDarta.Where(a => a.DartadateEnglish >= english_From_date);
                }
                if (to_date != null)
                {
                    var english_To_date = _dateConverter.ToAD(to_date);
                    allDarta = allDarta.Where(a => a.DartadateEnglish <= english_To_date);
                }
                return View(allDarta.ToList());
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult ChalaniList(string? from_date, string? to_date)
        {
            try
            {
                var allChalani = _chalaniRepo.getQueryable();

                
                

                if (from_date != null)
                {
                    var english_From_date = _dateConverter.ToAD(from_date);
                    allChalani = allChalani.Where(a => a.Chalanidate >= english_From_date);
                }
                if (to_date != null)
                {
                    var english_To_date = _dateConverter.ToAD(to_date);
                    allChalani = allChalani.Where(a => a.Chalanidate <= english_To_date);
                }
                return View(allChalani.ToList());
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        
    }
}
