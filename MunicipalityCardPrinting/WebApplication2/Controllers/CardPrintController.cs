﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using DTO.Enums;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MunicipalityCardPrinting.Utils;

namespace MunicipalityCardPrinting.Controllers
{
    [Authorize]
    public class CardPrintController : Controller
    {
        private readonly ICardService _cardService;
        private readonly IAddressService _addressService;
        private readonly IEmployeeService _employeeService;
        private readonly IBaseRepository<ApplicationUser> _userRepo;
        private readonly IBaseRepository<CardDetail> _cardRepo;
        private readonly IBaseRepository<ChildDetail> _childRepo;
        private readonly IFileHelper _fileHelper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        public CardPrintController(ICardService cardService, IAddressService addressService, IBaseRepository<ApplicationUser> userRepo, IFileHelper fileHelper, IBaseRepository<CardDetail> cardRepo, IWebHostEnvironment hostingEnvironment, IBaseRepository<ChildDetail> childRepo, UserManager<ApplicationUser> userManager,IEmployeeService employeeService)
        {
            _cardService = cardService;
            _addressService = addressService;
            _userRepo = userRepo;
            _fileHelper = fileHelper;
            _cardRepo = cardRepo;
            _hostingEnvironment = hostingEnvironment;
            _childRepo = childRepo;
            _userManager = userManager;
            _employeeService = employeeService;
        }
        // GET: CardPrint
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DisabilityCardDetail()
        {
            try
            {
                var cards = _cardService.AllDisabledCards();
                return View(cards);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult ElderCardDetail()
        {
            try
            {
                var cards = _cardService.AllElderCards();
                return View(cards);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public ActionResult ChildCardDetail(string Name,string CardNumber)
        {
            try
            {
                
                var cards = _cardService.GetAllChildernCardDetail();
                
                if (!string.IsNullOrEmpty(Name))
                {
                    cards = cards.Where(a => a.NameEnglish.ToLower().Contains(Name.ToLower())).ToList();
                }

                if (!string.IsNullOrEmpty(CardNumber))
                {
                    cards = cards.Where(a => a.CardNumber.Contains(CardNumber)).ToList();
                }
                
                return View(cards);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult PrintedDisabledCardList(int Id)
        {
            try
            {
                ViewBag.Category = Id;
                if (Id == (int)CardCategoryEnum.Disabled)
                {
                    ViewBag.title = "अपाङ्ग";
                }
                else
                {
                    ViewBag.title = "जेष्ठ नागरिक";
                }
                var cards = _cardService.AllPrintedElderCards(Id);
                return View(cards);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult DisableCardPrintView(long Id)
        {
            try
            {
                var cardView = _cardService.GetDisabledPersonCardById(Id);
                return View(cardView);

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult ChildCardPrintView(long Id)
        {
            try
            {
                var cardView = _cardService.GetChildernCardById(Id);
                return View(cardView);

            }
            catch (Exception ex)
            {

                throw;
            }
        }



        public ActionResult ElderPersonCardView(long Id)
        {
            try
            {
                var cardView = _cardService.GetElderPersonCardById(Id);
                return View(cardView);

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult Details(int id)
        {
            return View();
        }
        public ActionResult AddUpdateView(long Id)
        {
            ViewBag.GenderTypeDropdown = from Gender d in Enum.GetValues(typeof(Gender))
                                         select new
                                         {
                                             Id = ((int)d).ToString(),
                                             DisplayName = GenderEnumHandler.ConvertToNepali(d)
                                         };
            ViewBag.IDCardTypeDropdown = from CardType d in Enum.GetValues(typeof(CardType))
                                         select new
                                         {
                                             Id = ((int)d).ToString(),
                                             DisplayName = CardTypeEnumHandler.ConvertToNepali(d)
                                         };
            ViewBag.DisabilityNatureDropDown = from DisbalityTypeOntheBasisOfNature d in Enum.GetValues(typeof(DisbalityTypeOntheBasisOfNature))
                                               select new
                                               {
                                                   Id = ((int)d).ToString(),
                                                   DisplayName = DisabilityNatureEnumHandler.ConvertToNepali(d)
                                               };
            ViewBag.DisabilitySeriousnessDropDown = from DisabilityTypeOnTheBasisOfSeriousness d in Enum.GetValues(typeof(DisabilityTypeOnTheBasisOfSeriousness))
                                                    select new
                                                    {
                                                        Id = ((int)d).ToString(),
                                                        DisplayName = d.GetDisplayName()
                                                    };
            ViewBag.BloodGroupDropDown = from BloodGroupEnum d in Enum.GetValues(typeof(BloodGroupEnum))
                                         select new
                                         {
                                             Id = ((int)d).ToString(),
                                             DisplayName = BloodGroupEnumHandler.ConvertToNepali(d)
                                         };
            ViewBag.MarritalStatusDropDown = from MarritalStatusEnum d in Enum.GetValues(typeof(MarritalStatusEnum))
                                             select new
                                             {
                                                 Id = ((int)d).ToString(),
                                                 DisplayName = d.GetDisplayName()
                                             };
            ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                 select new
                                 {
                                     Id = ((int)d).ToString(),
                                     DisplayName = d.GetDisplayName()
                                 };
            ViewBag.YesNoDropDown = from YesNo d in Enum.GetValues(typeof(YesNo))
                                    select new
                                    {
                                        Id = ((int)d).ToString(),
                                        DisplayName = d.GetDisplayName()
                                    };
            var provinces = _addressService.GetAllProvincedropdown();
            ViewBag.provinces = new SelectList(provinces, "Id", "DisplayName");
            var districts = _addressService.GetAllDistrictDropdown();
            ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
            var Vdcs = _addressService.GetAllProvincedropdown();
            ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

            var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
            ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");
            var model = new CardDetailDTO();

            if (Id > 0)
            {
                model = _cardService.GetCardDetailByCardDetailId(Id);
            }
            model.CardCategory = (int)CardCategoryEnum.Disabled;
            return View("AddUpdateCard", model);
        }

        [HttpPost]
        public ActionResult SaveDisabledBasicDetail(CardBasicDetailDTO model)
        {
            try
            {


                if (model.file != null)
                {
                    model.Photo = _fileHelper.saveImageAndGetFileName(model.file, model.Photo);
                }
                var data = _cardService.AddUpdateBasicDetail(model);
                model.Id = data.CardId;
                //AlertHelper.setMessage(this, "Basic Detail Saved Successfully", ResponseTypes.success);
                return Json(new { success = true, Data = data, Message = "Basic Detail Saved Successfully" });


            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }
        public ActionResult AddUpdateChildCardDetail(int Id)
        {
            try
            {

                ViewBag.GenderTypeDropdown = from Gender d in Enum.GetValues(typeof(Gender))
                                             select new
                                             {
                                                 Id = ((int)d).ToString(),
                                                 DisplayName = GenderEnumHandler.ConvertToNepali(d)
                                             };


                ViewBag.PerWardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                     select new
                                     {
                                         Id = ((int)d).ToString(),
                                         DisplayName = d.GetDisplayName()
                                     };

                var provinces = _addressService.GetAllProvincedropdown();
                ViewBag.perprovinces = new SelectList(provinces, "Id", "DisplayName");
                var districts = _addressService.GetAllDistrictDropdown();
                ViewBag.PerDistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
                var Vdcs = _addressService.GetAllVdcDropdown();
                ViewBag.PervdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");

                var employee = _employeeService.GetAllEmployeeDropdown();
                ViewBag.employee = new SelectList(employee, "Id", "DisplayName");

                var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
                ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");
                var model = new ChildBasicDetailDTO();

                if (Id > 0)
                {
                    model = _cardService.GetChildCardDtoById(Id);
                }
                return View(model);
            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }
        [HttpPost]
        public ActionResult AddUpdateChildCardDetail(ChildBasicDetailDTO model)
        {
            try
            {


                if (model.file != null)
                {
                    model.Photo = _fileHelper.saveImageAndGetFileName(model.file, model.Photo);
                }
                if (model.FatherCitizenFile != null)
                {
                    model.FatherCitizen = _fileHelper.saveImageAndGetFileName(model.FatherCitizenFile, model.FatherCitizen);
                }
                if (model.MotherCitizenFile != null)
                {
                    model.MotherCitizen = _fileHelper.saveImageAndGetFileName(model.MotherCitizenFile, model.MotherCitizen);
                }
                if (model.BirthCertificateFile != null)
                {
                    model.BirthCertificate = _fileHelper.saveImageAndGetFileName(model.BirthCertificateFile, model.BirthCertificate);
                }
                if (model.OtherFile != null)
                {
                    model.other = _fileHelper.saveImageAndGetFileName(model.OtherFile, model.other);
                }
                var user = _userManager.GetUserAsync(User).Result;
                model.UserId = user.Id.ToString();
                var Id = _cardService.AddUpdateChildCardDetail(model);

                return RedirectToAction("ChildCardPrintView", new { Id = Id });
            }
            catch (CustomException ex)
            {
                return Json(new { success = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = "Something went wrong please contact administrator" });
            }
        }
        [HttpPost]
        public ActionResult AddUpdateAdditionalDetail(CardAdditionalDetailDTO model)
        {
            try
            {

                var data = _cardService.AddUpdateCardAdditionalDetail(model);
                AlertHelper.setMessage(this, "Basic Detail Saved Successfully", ResponseTypes.success);
                return Json(new { success = true, Data = data, Message = "Additional Detail Saved Successfully" });

            }
            catch (CustomException ex)
            {
                AlertHelper.setMessage(this, ex.Message, ResponseTypes.success);
                return View("AddUpdateCard", model);
            }
            catch (Exception ex)
            {
                AlertHelper.setMessage(this, "Something went wrong please contact administrator", ResponseTypes.success);
                return View("AddUpdateCard", model);
            }
        }
        public ActionResult DisabledPieChartView()
        {
            try
            {
                var cardPieChart = _cardService.GetDisabledCardQuantityOnTheBasisOfType();
                return View(cardPieChart);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult DisabledBarGraphView()
        {
            try
            {
                var cardBarGraph = _cardService.GetDisabledCardQuantityOnTheBasisOfType();
                return View(cardBarGraph);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult DeleteDisabledCard(long Id)
        {
            try
            {
                var item = _cardRepo.getById(Id);
                if (item == null)
                    throw new CustomException("Item doesnot exist");
                _cardRepo.delete(item);
                AlertHelper.setMessage(this, "Deleted successfully", ResponseTypes.success);
                return RedirectToAction("DisabilityCardDetail");

            }
            catch (CustomException ex)
            {
                AlertHelper.setMessage(this, ex.Message, ResponseTypes.notify);
                return RedirectToAction("DisabilityCardDetail");
            }
            catch (Exception ex)
            {
                AlertHelper.setMessage(this, "Unable to delete please contact administrator", ResponseTypes.error);
                return RedirectToAction("DisabilityCardDetail");
            }
        }
        public ActionResult DeleteElderCard(long Id)
        {
            try
            {
                var item = _cardRepo.getById(Id);
                if (item == null)
                    throw new CustomException("Item doesnot exist");
                _cardRepo.delete(item);
                AlertHelper.setMessage(this, "Deleted successfully", ResponseTypes.success);
                return RedirectToAction("ElderCardDetail");

            }
            catch (CustomException ex)
            {
                AlertHelper.setMessage(this, ex.Message, ResponseTypes.notify);
                return RedirectToAction("DisabilityCardDetail");
            }
            catch (Exception ex)
            {
                AlertHelper.setMessage(this, "Unable to delete please contact administrator", ResponseTypes.error);
                return RedirectToAction("DisabilityCardDetail");
            }
        }
        public ActionResult AddUpdateElderDetail(long Id)
        {
            ViewBag.GenderTypeDropdown = from Gender d in Enum.GetValues(typeof(Gender))
                                         select new
                                         {
                                             Id = ((int)d).ToString(),
                                             DisplayName = GenderEnumHandler.ConvertToNepali(d)
                                         };

            ViewBag.BloodGroupDropDown = from BloodGroupEnum d in Enum.GetValues(typeof(BloodGroupEnum))
                                         select new
                                         {
                                             Id = ((int)d).ToString(),
                                             DisplayName = BloodGroupEnumHandler.ConvertToNepali(d)
                                         };
            ViewBag.CategoryOfEldernessDrpdown = from CategoryOfElderness d in Enum.GetValues(typeof(CategoryOfElderness))
                                                 select new
                                                 {
                                                     Id = ((int)d).ToString(),
                                                     DisplayName = d.GetDisplayName()
                                                 };
            ViewBag.MarritalStatusDropDown = from MarritalStatusEnum d in Enum.GetValues(typeof(MarritalStatusEnum))
                                             select new
                                             {
                                                 Id = ((int)d).ToString(),
                                                 DisplayName = d.GetDisplayName()
                                             };
            ViewBag.WardDrpDwn = from WardEnum d in Enum.GetValues(typeof(WardEnum))
                                 select new
                                 {
                                     Id = ((int)d).ToString(),
                                     DisplayName = d.GetDisplayName()
                                 };
            ViewBag.YesNoDropDown = from YesNo d in Enum.GetValues(typeof(YesNo))
                                    select new
                                    {
                                        Id = ((int)d).ToString(),
                                        DisplayName = d.GetDisplayName()
                                    };
            var provinces = _addressService.GetAllProvincedropdown();
            ViewBag.provinces = new SelectList(provinces, "Id", "DisplayName");
            var districts = _addressService.GetAllDistrictDropdown();
            ViewBag.DistrictsDrpdown = new SelectList(districts, "Id", "DisplayName");
            var Vdcs = _addressService.GetAllProvincedropdown();
            ViewBag.vdcsDrpdown = new SelectList(Vdcs, "Id", "DisplayName");
            var allUsers = _userRepo.getQueryable().Select(a => new DropdownModel { GUID = a.Id, DisplayName = a.FullNameNepali });
            ViewBag.usersDrpDown = new SelectList(allUsers, "GUID", "DisplayName");
            var model = new ElderCardDetailDTO();

            if (Id > 0)
            {
                model = _cardService.GetElderCardDetail(Id);
            }
            model.CardCategory = (int)CardCategoryEnum.OldCitizen;
            return View("AddUpdateOldCitizenCard", model);
        }
        [HttpPost]
        public ActionResult AddUpdateElderDetail(ElderCardSaveModel model)
        {
            try
            {
                if (model.file != null)
                {
                    model.Photo = _fileHelper.saveImageAndGetFileName(model.file, model.Photo);
                }
                var data = _cardService.AddUpdateElderCard(model);
                 return Json(new { success = true, message = "Basic detail saved successfully", Data = data });
                //return RedirectToAction("DisabledIndividualReport", data.CardId);

            }
            catch (CustomException ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Something went wrong please contact administrator" });
            }

        }

        public ActionResult GetPrintPartialView(long Id, string cardNumber)
        {
            _cardService.UpdateCardNumber(Id, cardNumber);
            var cardModel = _cardService.GetDisabledPersonCardById(Id);

            var htmlView = ControllerExtensions.RenderViewAsync(this, "CardPrintWrapper", cardModel, true).GetAwaiter().GetResult();
            return Json(new { success = true, dataView = htmlView });
        }

        [HttpGet]
        public ActionResult GetChildPrintPartialView(int Id)
        {
            _cardService.UpdateChildCardNumber(Id);
            var cardModel = _cardService.GetChildernCardById(Id);

            var htmlView = ControllerExtensions.RenderViewAsync(this, "GetChildPrintPartialView", cardModel, true).GetAwaiter().GetResult();
            return Json(new { success = true, dataView = htmlView });
        }
        public ActionResult IncreasePrintCount(long Id, string cardNumber)
        {
            try
            {

                _cardService.IncreaseCardPrintCount(Id);
                return (RedirectToAction("DisableCardPrintView", new { Id = Id }));
            }
            catch (Exception ex)
            {

                return Json(new { success = false });
            }
        }

        public ActionResult IncreaseChildPrintCount(long Id, string cardNumber)
        {
            try
            {

                _cardService.IncreaseChildCardPrintCount(Id);
                return (RedirectToAction("ChildCardPrintView", new { Id = Id }));
            }
            catch (Exception ex)
            {

                return Json(new { success = false });
            }
        }

        public ActionResult DisabledIndividualReport(long Id)
        {
            try
            {
                var data = _cardService.GetDisabledIndividualReport(Id);
                return View(data);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult DisabledProgressReport()
        {
            try
            {
                var data = _cardService.GetDisabledPersonProgressReport();
                return View(data);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public IActionResult getDistrictsFromProvince(long Id)
        {

            try
            {
                var districts = _addressService.GetDistrictDropDownFromProvinceId(Id).ToList();

                return Json(districts);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }

        }
        public IActionResult getVdcsFromDistrict(long Id)
        {
            try
            {
                var vdcs = _addressService.GetMunVdcDropDownFromProvinceId(Id).ToList();

                return Json(vdcs);
            }
            catch (CustomException ex)
            {
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = System.Net.HttpStatusCode.NotFound;

                return NotFound(Json(new { success = false, responseText = ex.Message }));
            }
            catch (Exception ex)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }

        }
    }
}