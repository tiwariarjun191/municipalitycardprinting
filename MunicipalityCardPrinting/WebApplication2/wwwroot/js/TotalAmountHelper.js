var TotalAmount = function() {
	if(TotalAmount.instance == null) {
		TotalAmount.instance = this;
	}
	else {
		return TotalAmount.instance;
	}
	var element = document.getElementById('bill_total_amount');
	this.updateTotalAmountBy = function($amount) {
		var currentValue = customParseFloat(element.innerText || 0);
		element.innerText = currentValue + $amount;
	};
};