var InputHelper = function() {
	if(InputHelper.instance == null) {
		InputHelper.instance = this;
	}
	else {
		return InputHelper.instance;
	}

	this.replaceEmptyInputWith = function($input, $replacement) {
		if(!$input.val()) $input.val($replacement);
		return $input;
	};

};
