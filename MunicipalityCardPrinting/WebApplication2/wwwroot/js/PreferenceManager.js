var CRYSTAL_PREFERENCE_MANAGER = {};
CRYSTAL_PREFERENCE_MANAGER.PreferenceManager = function() {



	this.shouldConfirmOrder = function() {	
		if(localStorage.getItem('add-order-confirmation-never')) {
			return false;
		}	
		return true;
	};
	this.preferNoConfirmationOnOrder =function() {
		localStorage.setItem('add-order-confirmation-never', 'hola');
	};
	this.preferConfirmationOnOrder =function() {
		localStorage.removeItem('add-order-confirmation-never');
	};

};

CRYSTAL_PREFERENCE_MANAGER.preferenceManager = new CRYSTAL_PREFERENCE_MANAGER.PreferenceManager();