CRYSTAL_TOGGLER = {};

CRYSTAL_TOGGLER.toggle = function(togglerClassName) {
	togglerClassName = togglerClassName || "toggler";
	CRYSTAL_TOGGLER.togglers = document.getElementsByClassName(togglerClassName);

	CRYSTAL_TOGGLER.togglersLength = CRYSTAL_TOGGLER.togglers.length;

	for(var index = 0; index < CRYSTAL_TOGGLER.togglersLength; index++) {
		CRYSTAL_TOGGLER.togglers[index].addEventListener("click", function() {
			closedClass = this.dataset.initial;
			openClass   = this.dataset.final;

			if(this.classList.contains(closedClass)) {
				CRYSTAL_TOGGLER.swapClasses(this , closedClass, openClass);
			}
			else {
				CRYSTAL_TOGGLER.swapClasses(this , openClass, closedClass);
			}
		});
	}
};

CRYSTAL_TOGGLER.swapClasses = function(item, first, second) {
	item.classList.remove(first);
	item.classList.add(second);
}

CRYSTAL_TOGGLER.toggle();