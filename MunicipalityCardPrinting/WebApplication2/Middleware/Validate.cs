﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Middleware
{
    public class Validate
    {
        private readonly RequestDelegate _next;
        public Validate(RequestDelegate next)
        {
            _next = next;
        }
        public Task Invoke(HttpContext httpContext)
        {
            return _next(httpContext);
        }
    }

    public static class ValidateExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<Validate>();
        }
    }
}
