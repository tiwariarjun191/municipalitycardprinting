﻿using DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Utils
{
    public class Alert
    {
        public virtual string message { get; set; }
        public virtual ResponseTypes message_type { get; set; }
    }
}
