using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Services.Implementation;
using Business.Services.Interface;
using Infrastructure.Database.Context;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Implementation;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MunicipalityCardPrinting.Middleware;
using MunicipalityCardPrinting.Utils;
using Newtonsoft.Json.Serialization;

namespace MunicipalityCardPrinting
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }




        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //var connection = @"server=EC2AMAZ-KU4E8VL\FIBONACCI;database=CardPrinting;User ID= sa;Password=Admin@123";
            // var connection = @"server=EC2AMAZ-KU4E8VL\FIBONACCI;database=CardPrinting;User ID= sa;Password=Admin@123";
            //var connection = @"Data Source=EC2AMAZ-KU4E8VL\FIBONACCI;Initial Catalog=CardPrinting;User ID=sa;Password=Admin@123";


           // var connection = @"server =LAPTOP-K603BJ6E; database = CardPrinting; Enlist = false; trusted_connection = true; Connection Timeout=1000"; 
            
            


            




            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseLazyLoadingProxies().UseSqlServer(Configuration.GetConnectionString("myconn"), b => b.MigrationsAssembly("MunicipalityCardPrinting"));
                //  options.ConfigureWarnings(x => x.Ignore(RelationalEventId.AmbientTransactionWarning));
            });

            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<AppDbContext>();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddAuthentication(options =>
            {
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                options.Cookie.HttpOnly = false;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.LoginPath = "/account/login";
                options.LogoutPath = "/account/logout";
                options.AccessDeniedPath = "/error";
            });
            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                options.JsonSerializerOptions.PropertyNamingPolicy = null;

            });


            // services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connection));

            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IPersonalDetailService, PersonalDetailService>();
            services.AddScoped<ICardService, CardService>();
            services.AddScoped<ISequenceService, SequenceService>();
            services.AddScoped<IFattService, FattService>();
            services.AddScoped<IFileHelper, FileHelper>();
            services.AddScoped<IDateConverter, DateConverter>();
            services.AddScoped<IDartaChalaniService, DartaChalaniService>();
            services.AddScoped<IDesignationService, DesignationService>();
            services.AddScoped<IShrediService, ShrediService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IFileHelper1, FileHelper1>();
            services.AddScoped<IOldChildCardService, OldChildCardService>();
            services.AddScoped<IExpiryServices, ExpirydateServices>();

            //services.AddScoped<IBaseRepository<CardDetail>, BaseRepository<CardDetail>>();
            //services.AddScoped<IBaseRepository<PersonalDetail>, BaseRepository<PersonalDetail>>();
            //services.AddScoped<IBaseRepository<CardDetail>, BaseRepository<CardDetail>>();
            //services.AddScoped<IBaseRepository<Province>, BaseRepository<Province>>();
            //services.AddScoped<IBaseRepository<District>, BaseRepository<District>>();
            //services.AddScoped<IBaseRepository<MunicipalityVDC>, BaseRepository<MunicipalityVDC>>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
            });




        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
    IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();

            }
            var service = serviceProvider.GetService<IExpiryServices>().Getexpirydates(1);
            app.Use(async (context,next) =>
            {

                if (context.Request.Path != "/Home/ExpiredPage")
                {
                    if (service.Expirydate_From.AddYears(1) <= DateTime.Now)
                    {
                        context.Response.Redirect("/Home/ExpiredPage");
                    }
                }
                 await next(); 
                
                
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
