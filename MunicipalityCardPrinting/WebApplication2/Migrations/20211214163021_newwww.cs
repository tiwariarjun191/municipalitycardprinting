﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MunicipalityCardPrinting.Migrations
{
    public partial class newwww : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Designation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Designation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Expirydates",
                columns: table => new
                {
                    EXPID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Expirydate_From = table.Column<DateTime>(nullable: false),
                    Expirydate_To = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expirydates", x => x.EXPID);
                });

            migrationBuilder.CreateTable(
                name: "Fatt",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FattName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fatt", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Provinces",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProvinceName = table.Column<string>(nullable: true),
                    ProvinceNameNepali = table.Column<string>(nullable: true),
                    Capital = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provinces", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sequence",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(maxLength: 50, nullable: true),
                    Value = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sequence", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Shredi",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SherdiEnglish = table.Column<string>(nullable: true),
                    SherdiNepali = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shredi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true),
                    WardNumber = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    FullNameNepali = table.Column<string>(nullable: true),
                    DesignationId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Designation_DesignationId",
                        column: x => x.DesignationId,
                        principalTable: "Designation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProvinceId = table.Column<long>(nullable: false),
                    DistrictName = table.Column<string>(nullable: true),
                    DistrictNameNepali = table.Column<string>(nullable: true),
                    HeadQuarter = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Districts_Provinces_ProvinceId",
                        column: x => x.ProvinceId,
                        principalTable: "Provinces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChildernCard",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CardNumber = table.Column<string>(nullable: true),
                    CardTypeCSSClass = table.Column<string>(nullable: true),
                    CardNumberNepali = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true),
                    NameEnglish = table.Column<string>(nullable: true),
                    PermanentProvinceId = table.Column<string>(nullable: false),
                    PermanentNepaliProvinceId = table.Column<string>(nullable: true),
                    PermanentDistrictId = table.Column<string>(nullable: true),
                    PermanentNepaliDistrictId = table.Column<string>(nullable: true),
                    PermanentVDCId = table.Column<string>(nullable: true),
                    PermanentNepaliVDCId = table.Column<string>(nullable: true),
                    PermanentWard = table.Column<string>(nullable: true),
                    TemporaryProvinceId = table.Column<string>(nullable: false),
                    TemporaryNepaliProvinceId = table.Column<string>(nullable: true),
                    TemporaryDistrictId = table.Column<string>(nullable: true),
                    TemporaryNepaliDistrictId = table.Column<string>(nullable: true),
                    TemporaryVDCId = table.Column<string>(nullable: true),
                    TemporaryNepaliVDCId = table.Column<string>(nullable: true),
                    TemporaryWard = table.Column<string>(nullable: true),
                    GenderNepali = table.Column<string>(nullable: true),
                    FatherNameNepali = table.Column<string>(nullable: true),
                    FatherNameEnglish = table.Column<string>(nullable: true),
                    FatherAddressNepali = table.Column<string>(nullable: true),
                    FatherAddressEnglish = table.Column<string>(nullable: true),
                    MotherNameNepali = table.Column<string>(nullable: true),
                    MotherNameEnglish = table.Column<string>(nullable: true),
                    MotherAddressEnglish = table.Column<string>(nullable: true),
                    MotherAddressNepali = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    PrintDate = table.Column<string>(nullable: true),
                    PrintCountNepali = table.Column<int>(nullable: false),
                    PrintCountnpl = table.Column<string>(nullable: true),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedByNepali = table.Column<string>(nullable: true),
                    FatherMotherCitizenShip = table.Column<string>(nullable: true),
                    FatherMotherCitizenShipNepali = table.Column<string>(nullable: true),
                    PositionEnglish = table.Column<string>(nullable: true),
                    PositionNepali = table.Column<string>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    IsCardPrinted = table.Column<bool>(nullable: false),
                    LastPrintDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChildernCard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChildernCard_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OldChildCard",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CardNumber = table.Column<string>(nullable: true),
                    CardTypeCSSClass = table.Column<string>(nullable: true),
                    CardNumberNepali = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true),
                    NameEnglish = table.Column<string>(nullable: true),
                    PermanentProvinceId = table.Column<string>(nullable: false),
                    PermanentNepaliProvinceId = table.Column<string>(nullable: true),
                    PermanentDistrictId = table.Column<string>(nullable: true),
                    PermanentNepaliDistrictId = table.Column<string>(nullable: true),
                    PermanentVDCId = table.Column<string>(nullable: true),
                    PermanentNepaliVDCId = table.Column<string>(nullable: true),
                    PermanentWard = table.Column<string>(nullable: true),
                    TemporaryProvinceId = table.Column<string>(nullable: false),
                    TemporaryNepaliProvinceId = table.Column<string>(nullable: true),
                    TemporaryDistrictId = table.Column<string>(nullable: true),
                    TemporaryNepaliDistrictId = table.Column<string>(nullable: true),
                    TemporaryVDCId = table.Column<string>(nullable: true),
                    TemporaryNepaliVDCId = table.Column<string>(nullable: true),
                    TemporaryWard = table.Column<string>(nullable: true),
                    GenderNepali = table.Column<string>(nullable: true),
                    FatherNameNepali = table.Column<string>(nullable: true),
                    FatherNameEnglish = table.Column<string>(nullable: true),
                    FatherAddressNepali = table.Column<string>(nullable: true),
                    FatherAddressEnglish = table.Column<string>(nullable: true),
                    MotherNameNepali = table.Column<string>(nullable: true),
                    MotherNameEnglish = table.Column<string>(nullable: true),
                    MotherAddressEnglish = table.Column<string>(nullable: true),
                    MotherAddressNepali = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    PrintDate = table.Column<string>(nullable: true),
                    PrintCountNepali = table.Column<int>(nullable: false),
                    PrintCountnpl = table.Column<string>(nullable: true),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedByNepali = table.Column<string>(nullable: true),
                    FatherMotherCitizenShip = table.Column<string>(nullable: true),
                    FatherMotherCitizenShipNepali = table.Column<string>(nullable: true),
                    PositionEnglish = table.Column<string>(nullable: true),
                    PositionNepali = table.Column<string>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    IsCardPrinted = table.Column<bool>(nullable: false),
                    LastPrintDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OldChildCard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OldChildCard_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MunicipalityVDCs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistrictId = table.Column<long>(nullable: false),
                    VDCName = table.Column<string>(nullable: true),
                    VDCNameNepali = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MunicipalityVDCs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MunicipalityVDCs_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Chalanis",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    ChalanidateNepali = table.Column<string>(nullable: true),
                    Chalanidate = table.Column<DateTime>(nullable: false),
                    EnglishDateofReceivedLetter = table.Column<DateTime>(nullable: true),
                    DateofReceivedLetter = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyAddress = table.Column<string>(nullable: true),
                    ChalaniNo = table.Column<string>(nullable: true),
                    ReferenceNo = table.Column<string>(nullable: true),
                    ReceivedBy = table.Column<string>(nullable: true),
                    AddressId = table.Column<long>(nullable: true),
                    Vdc = table.Column<long>(nullable: true),
                    Ward = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chalanis", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chalanis_MunicipalityVDCs_Vdc",
                        column: x => x.Vdc,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Dartas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    DartadateEnglish = table.Column<DateTime>(nullable: true),
                    Dartadate = table.Column<string>(nullable: true),
                    EnglishDateofReceivedLetter = table.Column<DateTime>(nullable: true),
                    DateofReceivedLetter = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyAddress = table.Column<string>(nullable: true),
                    DartaNo = table.Column<string>(nullable: true),
                    ReferenceNo = table.Column<string>(nullable: true),
                    ReceivedBy = table.Column<string>(nullable: true),
                    AddressId = table.Column<long>(nullable: true),
                    Faat = table.Column<long>(nullable: true),
                    Vdc = table.Column<long>(nullable: true),
                    Ward = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dartas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dartas_Fatt_Faat",
                        column: x => x.Faat,
                        principalTable: "Fatt",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dartas_MunicipalityVDCs_Vdc",
                        column: x => x.Vdc,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Employee_Name_English = table.Column<string>(nullable: true),
                    Employee_Name_Nepali = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    DOB = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    District_Province_Vdc_Id = table.Column<long>(nullable: false),
                    Ward = table.Column<string>(nullable: true),
                    Office = table.Column<string>(nullable: true),
                    Shredi_Id = table.Column<long>(nullable: false),
                    Designation_Id = table.Column<long>(nullable: false),
                    Sanket_Number = table.Column<string>(nullable: true),
                    Office_Entry_Date = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employee_Designation_Designation_Id",
                        column: x => x.Designation_Id,
                        principalTable: "Designation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_MunicipalityVDCs_District_Province_Vdc_Id",
                        column: x => x.District_Province_Vdc_Id,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_Shredi_Shredi_Id",
                        column: x => x.Shredi_Id,
                        principalTable: "Shredi",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonalDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    FullNameNepali = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    TempAddressId = table.Column<long>(nullable: true),
                    PermanentAddressId = table.Column<long>(nullable: true),
                    TempTole = table.Column<string>(nullable: true),
                    PermanentTole = table.Column<string>(nullable: true),
                    TempWardNumber = table.Column<string>(nullable: true),
                    PermanentWardNumber = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: true),
                    DOBNepali = table.Column<string>(nullable: true),
                    CitizenshipNumber = table.Column<string>(nullable: true),
                    CitizenshipNumberNepali = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<int>(nullable: true),
                    MarritalStatus = table.Column<int>(nullable: true),
                    SpouseName = table.Column<string>(nullable: true),
                    ContactNumber = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonalDetails_MunicipalityVDCs_PermanentAddressId",
                        column: x => x.PermanentAddressId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalDetails_MunicipalityVDCs_TempAddressId",
                        column: x => x.TempAddressId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ChildDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CardNumber = table.Column<string>(nullable: true),
                    CardNumberNepali = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true),
                    NameEnglish = table.Column<string>(nullable: true),
                    PermanentVDCId = table.Column<long>(nullable: true),
                    PermanentWard = table.Column<string>(nullable: true),
                    TemporaryProvinceId = table.Column<long>(nullable: false),
                    TemporaryVDCId = table.Column<long>(nullable: true),
                    TemporaryWard = table.Column<string>(nullable: true),
                    GenderNepali = table.Column<string>(nullable: true),
                    FatherNameNepali = table.Column<string>(nullable: true),
                    FatherNameEnglish = table.Column<string>(nullable: true),
                    FatherAddressNepali = table.Column<string>(nullable: true),
                    FatherAddressEnglish = table.Column<string>(nullable: true),
                    MotherNameNepali = table.Column<string>(nullable: true),
                    MotherNameEnglish = table.Column<string>(nullable: true),
                    MotherAddressEnglish = table.Column<string>(nullable: true),
                    MotherAddressNepali = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Age = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    PrintDate = table.Column<string>(nullable: true),
                    PrintCountNepali = table.Column<int>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedByNepali = table.Column<string>(nullable: true),
                    FatherMotherCitizenship = table.Column<string>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    IsCardPrinted = table.Column<bool>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    LastPrintDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ApprovedById = table.Column<long>(nullable: false),
                    Employee_Id = table.Column<long>(nullable: false),
                    Employee_Address = table.Column<string>(nullable: true),
                    FatherCitizen = table.Column<string>(nullable: true),
                    MotherCitizen = table.Column<string>(nullable: true),
                    BirthCertificate = table.Column<string>(nullable: true),
                    other = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChildDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChildDetails_Employee_ApprovedById",
                        column: x => x.ApprovedById,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChildDetails_Employee_Employee_Id",
                        column: x => x.Employee_Id,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChildDetails_MunicipalityVDCs_PermanentVDCId",
                        column: x => x.PermanentVDCId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChildDetails_MunicipalityVDCs_TemporaryVDCId",
                        column: x => x.TemporaryVDCId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChildDetails_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OldChildDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CardNumber = table.Column<string>(nullable: true),
                    CardNumberNepali = table.Column<string>(nullable: true),
                    NameNepali = table.Column<string>(nullable: true),
                    NameEnglish = table.Column<string>(nullable: true),
                    PermanentVDCId = table.Column<long>(nullable: true),
                    PermanentWard = table.Column<string>(nullable: true),
                    TemporaryProvinceId = table.Column<long>(nullable: false),
                    TemporaryVDCId = table.Column<long>(nullable: true),
                    TemporaryWard = table.Column<string>(nullable: true),
                    GenderNepali = table.Column<string>(nullable: true),
                    FatherNameNepali = table.Column<string>(nullable: true),
                    FatherNameEnglish = table.Column<string>(nullable: true),
                    FatherAddressNepali = table.Column<string>(nullable: true),
                    FatherAddressEnglish = table.Column<string>(nullable: true),
                    MotherNameNepali = table.Column<string>(nullable: true),
                    MotherNameEnglish = table.Column<string>(nullable: true),
                    MotherAddressEnglish = table.Column<string>(nullable: true),
                    MotherAddressNepali = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Age = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    PrintDate = table.Column<string>(nullable: true),
                    PrintCountNepali = table.Column<int>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedByNepali = table.Column<string>(nullable: true),
                    FatherMotherCitizenship = table.Column<string>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    IsCardPrinted = table.Column<bool>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    LastPrintDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ApprovedById = table.Column<long>(nullable: false),
                    Employee_Id = table.Column<long>(nullable: false),
                    Employee_Address = table.Column<string>(nullable: true),
                    FatherCitizen = table.Column<string>(nullable: true),
                    MotherCitizen = table.Column<string>(nullable: true),
                    BirthCertificate = table.Column<string>(nullable: true),
                    other = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OldChildDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OldChildDetails_Employee_ApprovedById",
                        column: x => x.ApprovedById,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OldChildDetails_Employee_Employee_Id",
                        column: x => x.Employee_Id,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OldChildDetails_MunicipalityVDCs_PermanentVDCId",
                        column: x => x.PermanentVDCId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OldChildDetails_MunicipalityVDCs_TemporaryVDCId",
                        column: x => x.TemporaryVDCId,
                        principalTable: "MunicipalityVDCs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OldChildDetails_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CardDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DisabledPersonId = table.Column<long>(nullable: false),
                    DisabilityTypeByNature = table.Column<int>(nullable: true),
                    IdCardType = table.Column<int>(nullable: true),
                    IdCardnumber = table.Column<string>(nullable: true),
                    DisabilityTypeBySeriousness = table.Column<int>(nullable: true),
                    GuardianId = table.Column<long>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    Qualification = table.Column<string>(nullable: true),
                    CurrentSchoolCollegeName = table.Column<string>(nullable: true),
                    StudyingOrFinished = table.Column<string>(nullable: true),
                    WhatKindOfWorkcanDoInDailyLife = table.Column<string>(nullable: true),
                    WhatKindOfWorkCantDo = table.Column<string>(nullable: true),
                    IsNeededAnyEquipement = table.Column<bool>(nullable: true),
                    AdditionalEquipements = table.Column<string>(nullable: true),
                    HasAnyAdditionalEquipement = table.Column<bool>(nullable: true),
                    OtherFacilities = table.Column<string>(nullable: true),
                    OtherFacilitiesNeeded = table.Column<string>(nullable: true),
                    OfficeToProvideFacility = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    CardCategory = table.Column<int>(nullable: false),
                    CategoryOfElderness = table.Column<int>(nullable: true),
                    DiseaseName = table.Column<string>(nullable: true),
                    MedicineName = table.Column<string>(nullable: true),
                    IsCardPrinted = table.Column<bool>(nullable: false),
                    PrintedCount = table.Column<int>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    FirstCardPrintedDate = table.Column<DateTime>(nullable: true),
                    LastPrintDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    GuardianRelationWithCitizen = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardDetails_AspNetUsers_ApprovedBy",
                        column: x => x.ApprovedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CardDetails_PersonalDetails_DisabledPersonId",
                        column: x => x.DisabledPersonId,
                        principalTable: "PersonalDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CardDetails_PersonalDetails_GuardianId",
                        column: x => x.GuardianId,
                        principalTable: "PersonalDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AddMoreSection",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    childDetailId = table.Column<long>(nullable: false),
                    File = table.Column<string>(nullable: true),
                    TextArea = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddMoreSection", x => x.id);
                    table.ForeignKey(
                        name: "FK_AddMoreSection_ChildDetails_childDetailId",
                        column: x => x.childDetailId,
                        principalTable: "ChildDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OldAddMoreSection",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    childDetailId = table.Column<long>(nullable: false),
                    File = table.Column<string>(nullable: true),
                    TextArea = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OldAddMoreSection", x => x.id);
                    table.ForeignKey(
                        name: "FK_OldAddMoreSection_OldChildDetails_childDetailId",
                        column: x => x.childDetailId,
                        principalTable: "OldChildDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "c58684c1-9127-4688-9a0a-fa44f419041c", "626525df-0a05-4565-a6d5-12e3416e6830", "Admin", "Admin" },
                    { "c6a8d805-6b0b-40e2-a06a-8430bca07108", "7d7635dc-92fa-4faa-80e8-48de315459f9", "Supervisor", "SuperVisor" },
                    { "7535c9a5-a72f-402a-8126-b8986468532a", "2572fbd2-edf2-4fe0-bb1d-9eb67a75e1f6", "Maker", "Maker" },
                    { "06f34af5-280c-4c18-8605-40d7ecdcc0bd", "9e0896d4-39f2-4222-88ff-b693ffe07588", "Approver", "Approver" }
                });

            migrationBuilder.InsertData(
                table: "Designation",
                columns: new[] { "Id", "Name", "NameNepali" },
                values: new object[] { 2L, "Assistant", "सहयोगी" });

            migrationBuilder.InsertData(
                table: "Expirydates",
                columns: new[] { "EXPID", "Expirydate_From", "Expirydate_To" },
                values: new object[] { 1L, new DateTime(2021, 12, 14, 22, 15, 20, 214, DateTimeKind.Local).AddTicks(9990), new DateTime(2022, 12, 14, 22, 15, 20, 215, DateTimeKind.Local).AddTicks(8027) });

            migrationBuilder.InsertData(
                table: "Provinces",
                columns: new[] { "Id", "Capital", "Description", "ProvinceName", "ProvinceNameNepali" },
                values: new object[,]
                {
                    { 1L, null, null, "Province No. 1", "प्रदेश नं. १" },
                    { 2L, null, null, "Province No. 2", "प्रदेश नं. २" },
                    { 3L, null, null, "Province No. 3", "प्रदेश नं. ३" },
                    { 4L, null, null, "Gandaki Pradesh", "गण्डकी प्रदेश" },
                    { 5L, null, null, "Province No. 5", "प्रदेश नं. ५" },
                    { 6L, null, null, "Karnali Pradesh", "कर्णाली प्रदेश" },
                    { 7L, null, null, "Sudurpashchim Pradesh", "सुदूर-पश्चिम प्रदेश" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "DesignationId", "FullName", "FullNameNepali", "IsDeleted", "WardNumber" },
                values: new object[] { "35a3890c-eb70-4f4b-b7d2-71d33c56812a", 0, "8cd7053f-fe27-4759-9175-4aba058ab8d1", "ApplicationUser", "Admin@admin.com", false, false, null, null, "ADMIN", "AQAAAAEAACcQAAAAEE6HJY8HyqieKerkwWzdaZ1MlVVc3Dh2A2ZeJ4z/XbvpRBfExqtGMe4Z/QgtJHcbPg==", null, false, "b50d32bc-6e65-4b27-90b7-762258bb4b8b", false, "admin", 2L, "Admin", "एडमिन", false, null });

            migrationBuilder.InsertData(
                table: "Districts",
                columns: new[] { "Id", "Description", "DistrictName", "DistrictNameNepali", "HeadQuarter", "ProvinceId" },
                values: new object[,]
                {
                    { 55L, null, "रोल्पा", "Rolpa", null, 5L },
                    { 54L, null, "प्युठान", "Pyuthan", null, 5L },
                    { 53L, null, "दाङ देउखुरी", "Dang Deukhuri", null, 5L },
                    { 52L, null, "पाल्पा", "Palpa", null, 5L },
                    { 51L, null, "गुल्मी", "Gulmi", null, 5L },
                    { 50L, null, "अर्घाखाँची", "Arghakhanchi", null, 5L },
                    { 49L, null, "रुपन्देही", "Rupandehi", null, 5L },
                    { 48L, null, "परासी", "Parasi", null, 5L },
                    { 47L, null, "कपिलवस्तु", "Kapilvastu", null, 5L },
                    { 46L, null, "तनहुँ", "Tanahun", null, 4L },
                    { 45L, null, "स्याङग्जा", "Syangja", null, 4L },
                    { 44L, null, "पर्वत", "Parbat", null, 4L },
                    { 43L, null, "नवलपुर", "Nawalpur", null, 4L },
                    { 42L, null, "म्याग्दी", "Myagdi", null, 4L },
                    { 41L, null, "मुस्ताङ", "Mustang", null, 4L },
                    { 56L, null, "पूर्वी रूकुम", "Eastern Rukum", null, 5L },
                    { 57L, null, "बाँके", "Banke", null, 5L },
                    { 58L, null, "बर्दिया", "Bardiya", null, 5L },
                    { 59L, null, "पश्चिमी रूकुम", "Western Rukum", null, 6L },
                    { 75L, null, "डडेलधुरा", "Dadeldhura", null, 7L },
                    { 74L, null, "कंचनपुर", "Kanchanpur", null, 7L },
                    { 73L, null, "बाजुरा", "Bajura", null, 7L },
                    { 72L, null, "बझाङ", "Bajhang", null, 7L },
                    { 71L, null, "डोटी", "Doti", null, 7L },
                    { 70L, null, "अछाम", "Achham", null, 7L },
                    { 69L, null, "कैलाली", "Kailali", null, 7L },
                    { 40L, null, "मनाङ", "Manang", null, 4L },
                    { 68L, null, "जाजरकोट", "Jajarkot", null, 6L },
                    { 66L, null, "सुर्खेत", "Surkhet", null, 6L },
                    { 65L, null, "मुगु", "Mugu", null, 6L },
                    { 64L, null, "कालिकोट", "Kalikot", null, 6L },
                    { 63L, null, "जुम्ला", "Jumla", null, 6L },
                    { 62L, null, "हुम्ला", "Humla", null, 6L },
                    { 61L, null, "डोल्पा", "Dolpa", null, 6L },
                    { 60L, null, "सल्यान", "Salyan", null, 6L },
                    { 67L, null, "दैलेख", "Dailekh", null, 6L },
                    { 39L, null, "लमजुङ", "Lamjung", null, 4L },
                    { 38L, null, "कास्की", "Kaski", null, 4L },
                    { 37L, null, "गोरखा", "Gorkha", null, 4L },
                    { 16L, null, "सिराहा", "Siraha", null, 2L },
                    { 15L, null, "सप्तरी", "Saptari", null, 2L },
                    { 14L, null, "उदयपुर", "Udayapur", null, 1L },
                    { 13L, null, "तेह्रथुम", "Terhathum", null, 1L },
                    { 12L, null, "ताप्लेजुंग", "Taplejung", null, 1L },
                    { 11L, null, "सुनसरी", "Sunsari ", null, 1L },
                    { 10L, null, "सोलुखुम्बू", "Solukhumbu", null, 1L },
                    { 17L, null, "धनुषा", "Dhanusa", null, 2L },
                    { 9L, null, "संखुवासभा", "Sankhuwasabha", null, 1L },
                    { 7L, null, "ओखलढुंगा", "Okhaldhunga", null, 1L },
                    { 6L, null, "मोरंग", "Morang", null, 1L },
                    { 5L, null, "खोटाँग", "Khotang", null, 1L },
                    { 4L, null, "झापा", "Jhapa", null, 1L },
                    { 3L, null, "इलाम", "Ilam", null, 1L },
                    { 2L, null, "धनकुटा", "Dhankuta", null, 1L },
                    { 1L, null, "भोजपुर", "Bhojpur", null, 1L },
                    { 8L, null, "पांचथर", "Panchthar", null, 1L },
                    { 76L, null, "बैतडी", "Baitadi", null, 7L },
                    { 18L, null, "महोत्तरी", "Mahottari", null, 2L },
                    { 20L, null, "बारा", "Bara", null, 2L },
                    { 36L, null, "बागलुङ", "Baglung", null, 4L },
                    { 35L, null, "मकवानपुर", "Makwanpur", null, 3L },
                    { 34L, null, "चितवन", "Chitwan", null, 3L },
                    { 33L, null, "सिन्धुपाल्चोक", "Sindhupalchok", null, 3L },
                    { 32L, null, "रसुवा", "Rasuwa", null, 3L },
                    { 31L, null, "नुवाकोट", "Nuwakot", null, 3L },
                    { 30L, null, "ललितपुर", "Lalitpur", null, 3L },
                    { 19L, null, "सर्लाही", "Sarlahi", null, 2L },
                    { 29L, null, "काभ्रेपलान्चोक", "Kavrepalanchok", null, 3L },
                    { 27L, null, "धादिङ", "Dhading", null, 3L },
                    { 26L, null, "भक्तपुर", "Bhaktapur", null, 3L },
                    { 25L, null, "दोलखा", "Dolakha", null, 3L },
                    { 24L, null, "रामेछाप", "Ramechhap", null, 3L },
                    { 23L, null, "सिन्धुली", "Sindhuli", null, 3L },
                    { 22L, null, "रौतहट", "Rautahat", null, 2L },
                    { 21L, null, "पर्सा", "Parsa", null, 2L },
                    { 28L, null, "काठमाडौँ", "Kathmandu", null, 3L },
                    { 77L, null, "दार्चुला", "Darchula", null, 7L }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "35a3890c-eb70-4f4b-b7d2-71d33c56812a", "c58684c1-9127-4688-9a0a-fa44f419041c" });

            migrationBuilder.InsertData(
                table: "MunicipalityVDCs",
                columns: new[] { "Id", "Description", "DistrictId", "VDCName", "VDCNameNepali" },
                values: new object[,]
                {
                    { 264L, null, 49L, "	Lumbini Sanskritik Municipality", "लुम्बिनी सांस्कृतिक नगरपालिका" },
                    { 265L, null, 49L, "	Siddharthanagar Municipality", "सिद्धार्थनगर नगरपालिका" },
                    { 266L, null, 49L, "	Sainamaina Municipality", "सैनामैना नगरपालिका" },
                    { 267L, null, 49L, "	Devdaha Municipality", "देवदह नगरपालिका" },
                    { 676L, null, 49L, "Gaidhawa Rural Municipality", "गैडहवा गाउँपालिका" },
                    { 677L, null, 49L, "Mayadevi Rural Municipality", "मायादेवी गाउँपालिका" },
                    { 678L, null, 49L, "Kotahimai Rural Municipality", "कोटहीमाई गाउँपालिका" },
                    { 679L, null, 49L, "Marchawarimai Rural Municipality", "मर्चवारीमाई गाउँपालिका" },
                    { 680L, null, 49L, "Siyari Rural Municipality", "सियारी गाउँपालिका" },
                    { 681L, null, 49L, "Sammarimai Rural Municipality", "सम्मरीमाई गाउँपालिका" },
                    { 682L, null, 49L, "Rohini Rural Municipality", "रोहिणी गाउँपालिका" },
                    { 683L, null, 49L, "Shuddhodhan Rural Municipality", "शुद्धोधन गाउँपालिका" },
                    { 684L, null, 49L, "Om Satiya Rural Municipality", "ओमसतीया गाउँपालिका" },
                    { 685L, null, 49L, "Kanchan Rural Municipality", "कञ्चन गाउँपालिका" },
                    { 268L, null, 50L, "	Sitganga Municipality", "सितगंगा नगरपालिका" },
                    { 269L, null, 50L, "	Sandhikharka Municipality", "सन्धिखर्क नगरपालिका" },
                    { 270L, null, 50L, "	Bhumikasthan Municipality", "भूमिकास्थान नगरपालिका" },
                    { 686L, null, 50L, "Malarani Rural Municipality", "मालारानी गाउँपालिका" },
                    { 687L, null, 50L, "Pandini Rural Municipality", "पाणिनी गाउँपालिका" },
                    { 263L, null, 49L, "	Tilottama Municipality", "तिलोत्तमा नगरपालिका" },
                    { 262L, null, 49L, "	Butwal Municipality", "बुटवल उप-महानगरपालिका" },
                    { 675L, null, 48L, "Palhi Nandan Rural Municipality", "पाल्हीनन्दन गाउँपालिका" },
                    { 674L, null, 48L, "Sarawal Rural Municipality", "सरावल गाउँपालिका" },
                    { 663L, null, 46L, "Myagde Rural Municipality", "म्याग्दे	 गाउँपालिका" },
                    { 664L, null, 46L, "Aanbu Khaireni Rural Municipality", "आँबुखैरेनी गाउँपालिका" },
                    { 665L, null, 46L, "Bandipur Rural Municipality", "बन्दिपुर गाउँपालिका" },
                    { 666L, null, 46L, "Ghiring Rural Municipality", "घिरिङ गाउँपालिका" },
                    { 667L, null, 46L, "Devghat	 Rural Municipality", "देवघाट	 गाउँपालिका" },
                    { 254L, null, 47L, "Kapilvastu Municipality", "कपिलवस्तु नगरपालिका" },
                    { 255L, null, 47L, "Banganga Municipality", "बाणगंगा नगरपालिका" },
                    { 25699887L, null, 47L, "Shivaraj Municipality", "शिवराज नगरपालिका" },
                    { 25711223L, null, 47L, "Buddhabhumi Municipality", "बुद्धभूमी नगरपालिका" },
                    { 688L, null, 50L, "Chhatradev Rural Municipality", "छत्रदेव गाउँपालिका" },
                    { 25643243L, null, 47L, "Krishnanagar Municipality", "कृष्णनगर नगरपालिका" },
                    { 668L, null, 47L, "Mayadevi Rural Municipality", "मायादेवी गाउँपालिका" },
                    { 669L, null, 47L, "Shuddhodhan Rural Municipality", "शुद्धोधन गाउँपालिका" },
                    { 670L, null, 47L, "Yasodhara Rural Municipality", "यसोधरा गाउँपालिका" },
                    { 671L, null, 47L, "Bijaynagar Rural Municipality", "विजयनगर गाउँपालिका" },
                    { 258L, null, 48L, "	Ramgram Municipality", "रामग्राम नगरपालिका" },
                    { 259L, null, 48L, "	Sunwal Municipality", "सुनवल नगरपालिका" },
                    { 260L, null, 48L, "	Bardghat Municipality", "बर्दघाट नगरपालिका" },
                    { 672L, null, 48L, "Triveni Susta Rural Municipality", "त्रिवेणी सुस्ता गाउँपालिका" },
                    { 673L, null, 48L, "Pratappur Rural Municipality", "प्रतापपुर गाउँपालिका" },
                    { 2522457L, null, 47L, "Maharajganj Municipality", "महाराजगंज नगरपालिका" },
                    { 662L, null, 46L, "Rishing Rural Municipality", "ऋषिङ्ग गाउँपालिका" },
                    { 271L, null, 51L, "	Musikot Municipality", "मुसिकोट नगरपालिका" },
                    { 689L, null, 51L, "Satyawati Rural Municipality", "सत्यवती गाउँपालिका" },
                    { 709L, null, 53L, "Babai Rural Municipality", "बबई गाउँपालिका" },
                    { 710L, null, 53L, "Shantinagar Rural Municipality", "शान्तिनगर गाउँपालिका" },
                    { 711L, null, 53L, "Rajpur Rural Municipality", "राजपुर गाउँपालिका" },
                    { 712L, null, 53L, "Banglachuli Rural Municipality", "वंगलाचुली	 गाउँपालिका" },
                    { 713L, null, 53L, "Dangisharan Rural Municipality", "दंगीशरण गाउँपालिका" },
                    { 211777409L, null, 54L, "Pyuthan Municipality", "प्युठान नगरपालिका" },
                    { 277L, null, 54L, "Swargadwari", "स्वर्गद्वारी नगरपालिका" },
                    { 714L, null, 54L, "Naubahini Rural Municipality", "नौबहिनी गाउँपालिका" },
                    { 715L, null, 54L, "Jhimaruk Rural Municipality", "झिमरुक गाउँपालिका" },
                    { 716L, null, 54L, "Gaumukhi Rural Municipality", "गौमुखी गाउँपालिका" },
                    { 717L, null, 54L, "Airawati Rural Municipality", "ऐरावती गाउँपालिका" },
                    { 718L, null, 54L, "Sarumarani Rural Municipality", "सरुमारानी गाउँपालिका" },
                    { 719L, null, 54L, "Mallarani Rural Municipality", "मल्लरानी गाउँपालिका" },
                    { 720L, null, 54L, "Mandavi Rural Municipality", "माण्डवी गाउँपालिका" },
                    { 278L, null, 55L, "	Rolpa Municipality", "रोल्पा नगरपालिका" },
                    { 721L, null, 55L, "Sunil Smriti Rural Municipality", "सुनिल स्मृति गाउँपालिका" },
                    { 722L, null, 55L, "Runtigadhi Rural Municipality", "रुन्टीगढी गाउँपालिका" },
                    { 723L, null, 55L, "Lungri Rural Municipality", "लुङ्ग्री गाउँपालिका" },
                    { 724L, null, 55L, "Triveni Rural Municipality", "त्रिवेणी गाउँपालिका" },
                    { 708L, null, 53L, "Gadhawa Rural Municipality", "गढवा गाउँपालिका" },
                    { 707L, null, 53L, "Rapti Rural Municipality", "राप्ती गाउँपालिका" },
                    { 27234323L, null, 53L, "Lamahi Municipality", "लमही नगरपालिका" },
                    { 276L, null, 53L, "Tulsipur Municipality", "तुल्सिपुर उप-महानगरपालिका" },
                    { 690L, null, 51L, "Dhurkot Rural Municipality", "धुर्कोट गाउँपालिका" },
                    { 691L, null, 51L, "Gulmi Durbar Rural Municipality", "गुल्मीदरवार गाउँपालिका" },
                    { 692L, null, 51L, "Madane Rural Municipality", "मदाने गाउँपालिका" },
                    { 693L, null, 51L, "Chandrakot Rural Municipality", "चन्द्रकोट गाउँपालिका" },
                    { 694L, null, 51L, "Malika Rural Municipality", "मालिका गाउँपालिका" },
                    { 695L, null, 51L, "Chhatrakot Rural Municipality", "छत्रकोट गाउँपालिका" },
                    { 696L, null, 51L, "Isma Rural Municipality", "ईस्मा गाउँपालिका" },
                    { 697L, null, 51L, "Kaligandaki Rural Municipality", "कालीगण्डकी गाउँपालिका" },
                    { 698L, null, 51L, "Ruru Rural Municipality", "रुरु गाउँपालिका" },
                    { 272L, null, 51L, "	Resunga Municipality", "रेसुङ्गा नगरपालिका" },
                    { 272343345L, null, 52L, "Tansen Municipality", "तानसेन नगरपालिका" },
                    { 699L, null, 52L, "Rainadevi Chhahara Rural Municipality", "रैनादेवी छहरा गाउँपालिका" },
                    { 700L, null, 52L, "Mathagadhi Rural Municipality", "माथागढी गाउँपालिका" },
                    { 701L, null, 52L, "Nisdi Rural Municipality", "निस्दी गाउँपालिका" },
                    { 702L, null, 52L, "Bagnaskali Rural Municipality", "वगनासकाली गाउँपालिका" },
                    { 703L, null, 52L, "Rambha Rural Municipality", "रम्भा	 गाउँपालिका" },
                    { 704L, null, 52L, "Purbakhola Rural Municipality", "पूर्वखोला गाउँपालिका" },
                    { 705L, null, 52L, "Tinau Rural Municipality", "तिनाउ गाउँपालिका" },
                    { 706L, null, 52L, "Ribdikot Rural Municipality", "रिब्दीकोट गाउँपालिका" },
                    { 275L, null, 53L, "Ghorahi Municipality", "घोराही उप-महानगरपालिका" },
                    { 27324324L, null, 52L, "Rampur Municipality", "रामपुर नगरपालिका" },
                    { 253L, null, 46L, "Bhimad Municipality", "भिमाद नगरपालिका" },
                    { 252L, null, 46L, "Bhanu Municipality", "भानु नगरपालिका" },
                    { 25199008L, null, 46L, "Shuklagandaki Municipality", "शुक्लागण्डकी नगरपालिका" },
                    { 232L, null, 37L, "Gorkha Municipality", "गोरखा नगरपालिका" },
                    { 233L, null, 37L, "Palungtar Municipality", "पालुङटार नगरपालिका" },
                    { 616L, null, 37L, "Shahid Lakhan Rural Municipality", "शहिद लखन गाउँपालिका" },
                    { 617L, null, 37L, "Barpak Sulikot Rural Municipality", "बारपाक सुलीकोट गाउँपालिका" },
                    { 618L, null, 37L, "Aarughat Rural Municipality", "आरूघाट गाउँपालिका" },
                    { 619L, null, 37L, "Siranchok Rural Municipality", "सिरानचोक	 गाउँपालिका" },
                    { 620L, null, 37L, "Gandaki Rural Municipality", "गण्डकी गाउँपालिका" },
                    { 621L, null, 37L, "Bhimsen Thapa Rural Municipality", "भिमसेनथापा गाउँपालिका" },
                    { 622L, null, 37L, "Ajirkot Rural Municipality", "अजिरकोट गाउँपालिका" },
                    { 623L, null, 37L, "Dharche Rural Municipality", "धार्चे गाउँपालिका" },
                    { 624L, null, 37L, "Chum Nubri Rural Municipality", "चुम नुव्री गाउँपालिका" },
                    { 234L, null, 38L, "	Pokhara Municipality", "पोखरा महानगरपालिका" },
                    { 625L, null, 38L, "Annapurna Rural Municipality", "अन्नपुर्ण गाउँपालिका" },
                    { 626L, null, 38L, "Machhapuchhre Rural Municipality", "माछापुछ्रे गाउँपालिका" },
                    { 627L, null, 38L, "Madi Rural Municipality", "मादी	 गाउँपालिका" },
                    { 628L, null, 38L, "Rupa Rural Municipality", "रूपा गाउँपालिका" },
                    { 235L, null, 39L, "	Besisahar Municipality", "बेसीशहर नगरपालिका" },
                    { 236L, null, 39L, "	Sundarbazar Municipality", "सुन्दरबजार नगरपालिका" },
                    { 237L, null, 39L, "	Madhya Nepal Municipality", "मध्यनेपाल नगरपालिका" },
                    { 615L, null, 36L, "Tamankhola Rural Municipality", "तमानखोला गाउँपालिका" },
                    { 614L, null, 36L, "Tarakhola Rural Municipality", "ताराखोला गाउँपालिका" },
                    { 613L, null, 36L, "Bareng Rural Municipality", "वरेङ गाउँपालिका" },
                    { 612L, null, 36L, "Nisikhola Rural Municipality", "निसीखोला गाउँपालिका" },
                    { 224L, null, 34L, "Khairhani Municipality", "खैरहनी नगरपालिका" },
                    { 225L, null, 34L, "Kalika Municipality", "कालिका नगरपालिका" },
                    { 226L, null, 34L, "Madi Municipality", "माडी नगरपालिका" },
                    { 601L, null, 34L, "Ichchhakamana Rural Municipality", "इच्छाकामना गाउँपालिका" },
                    { 240L, null, 35L, "Hetauda Municipality", "हेटौडा उप-महानगरपालिका" },
                    { 227L, null, 35L, "Thaha Municipality", "थाहा नगरपालिका" },
                    { 602L, null, 35L, "Bakaiya Rural Municipality", "बकैया गाउँपालिका" },
                    { 603L, null, 35L, "Manhari Rural Municipality", "मनहरी गाउँपालिका" },
                    { 604L, null, 35L, "Bagmati Rural Municipality", "बाग्मती गाउँपालिका" },
                    { 238L, null, 39L, " Rainas Municipality", "रार्इनास नगरपालिका" },
                    { 605L, null, 35L, "Raksirang Rural Municipality", "राक्सिराङ्ग गाउँपालिका" },
                    { 607L, null, 35L, "Kailash Rural Municipality", "कैलाश गाउँपालिका" },
                    { 608L, null, 35L, "Bhimphedi Rural Municipality", "भीमफेदी	 गाउँपालिका" },
                    { 609L, null, 35L, "Indrasarowar	 Rural Municipality", "ईन्द्र सरोवर गाउँपालिका" },
                    { 228L, null, 36L, "Baglung Municipality", "बागलुङ नगरपालिका" },
                    { 229L, null, 36L, "Galkot Municipality", "गल्कोट नगरपालिका" },
                    { 230L, null, 36L, "Jaimini Municipality", "जैमिनी नगरपालिका" },
                    { 231L, null, 36L, "Dhorpatan Municipality", "ढोरपाटन नगरपालिका" },
                    { 610L, null, 36L, "Badigad Rural Municipality", "वडिगाड गाउँपालिका" },
                    { 611L, null, 36L, "Kathekhola Rural Municipality", "काठेखोला गाउँपालिका" },
                    { 606L, null, 35L, "Makawanpurgadhi Rural Municipality", "मकवानपुरगढी गाउँपालिका" },
                    { 629L, null, 39L, "Marsyangdi Rural Municipality", "मर्स्याङदी गाउँपालिका" },
                    { 630L, null, 39L, "Dordi Rural Municipality", "दोर्दी गाउँपालिका" },
                    { 631L, null, 39L, "Dudhpokhari Rural Municipality", "दूधपोखरी गाउँपालिका" },
                    { 244L, null, 44L, "Kushma Municipality", "कुश्मा नगरपालिका" },
                    { 245L, null, 44L, "Phalewas Municipality", "फलेवास नगरपालिका" },
                    { 651L, null, 44L, "Jaljala Rural Municipality", "जलजला गाउँपालिका" },
                    { 652L, null, 44L, "Modi Rural Municipality", "मोदी गाउँपालिका" },
                    { 653L, null, 44L, "Painyu Rural Municipality", "पैयूं गाउँपालिका" },
                    { 654L, null, 44L, "Bihadi Rural Municipality", "विहादी गाउँपालिका" },
                    { 655L, null, 44L, "Mahashila Rural Municipality", "महाशिला गाउँपालिका" },
                    { 246L, null, 45L, "	Waling Municipality", "वालिङ नगरपालिका" },
                    { 247L, null, 45L, "	Phalewas Municipality", "फलेवास नगरपालिका" },
                    { 650L, null, 43L, "Baudikali Rural Municipality", "बौदीकाली गाउँपालिका" },
                    { 248L, null, 45L, "	Putalibazar Municipality", "पुतलीबजार नगरपालिका" },
                    { 112233L, null, 45L, "	Chapakot Municipality", "चापाकोट नगरपालिका" },
                    { 54346456L, null, 45L, "	Bhirkot Municipality", "भिरकोट नगरपालिका" },
                    { 656L, null, 45L, "Kaligandaki Rural Municipality", "कालीगण्डकी गाउँपालिका" },
                    { 657L, null, 45L, "Biruwa Rural Municipality", "विरुवा गाउँपालिका" },
                    { 658L, null, 45L, "Harinas Rural Municipality", "हरीनास गाउँपालिका" },
                    { 659L, null, 45L, "Aandhikhola	 Rural Municipality", "आँधीखोला गाउँपालिका" },
                    { 660L, null, 45L, "Arjun Chaupari Rural Municipality", "अर्जुन चौपारी गाउँपालिका" },
                    { 661L, null, 45L, "Phedikhola Rural Municipality", "फेदीखोला गाउँपालिका" },
                    { 2244331L, null, 46L, "Vyas Municipality", "व्यास नगरपालिका" },
                    { 249L, null, 45L, "	Galyang Municipality", "गल्याङ नगरपालिका" },
                    { 725L, null, 55L, "Paribartan Rural Municipality", "परिवर्तन गाउँपालिका" },
                    { 649L, null, 43L, "Bulingtar Rural Municipality", "बुलिङटार गाउँपालिका" },
                    { 647L, null, 43L, "Hupsekot Rural Municipality", "हुप्सेकोट गाउँपालिका" },
                    { 632L, null, 39L, "Kwaholasothar Rural Municipality", "क्व्होलासोथार गाउँपालिका" },
                    { 633L, null, 40L, "Manang Disyang Rural Municipality", "मनाङ डिस्याङ गाउँपालिका" },
                    { 634L, null, 40L, "Nason Rural Municipality", "नासोँ गाउँपालिका" },
                    { 635L, null, 40L, "Chame Rural Municipality", "चामे गाउँपालिका" },
                    { 636L, null, 40L, "Narpa Bhumi Rural Municipality", "नार्पा भूमी गाउँपालिका" },
                    { 637L, null, 41L, "Gharapjhong Rural Municipality", "घरपझोङ गाउँपालिका" },
                    { 638L, null, 41L, "Thasang Rural Municipality", "थासाङ गाउँपालिका" },
                    { 639L, null, 41L, "Baragung Muktichhetra Rural Municipality", "बारागुङ मुक्तिक्षेत्र गाउँपालिका" },
                    { 640L, null, 41L, "Lomanthang Rural Municipality", "लोमन्थाङ गाउँपालिका" },
                    { 648L, null, 43L, "Binayi Triveni Rural Municipality", "विनयी त्रिवेणी गाउँपालिका" },
                    { 641L, null, 41L, "Lo-Ghekar Damodarkunda Rural Municipality", "लो-घेकर दामोदरकुण्ड गाउँपालिका" },
                    { 642L, null, 42L, "Malika Rural Municipality", "मालिका गाउँपालिका" },
                    { 643L, null, 42L, "Mangala Rural Municipality", "मंगला गाउँपालिका" },
                    { 644L, null, 42L, "Raghuganga Rural Municipality", "रघुगंगा गाउँपालिका" },
                    { 645L, null, 42L, "Dhaulagiri Rural Municipality", "धवलागिरी गाउँपालिका" },
                    { 646L, null, 42L, "Annapurna Rural Municipality", "अन्नपुर्ण गाउँपालिका" },
                    { 261L, null, 43L, "	Kawasoti Municipality", "कावासोती नगरपालिका" },
                    { 241L, null, 43L, "	Gaindakot Municipality", "गैडाकोट नगरपालिका" },
                    { 242L, null, 43L, "	Madhyabindu Municipality", "मध्यविन्दु नगरपालिका" },
                    { 243L, null, 43L, "	Devchuli Municipality", "देवचुली नगरपालिका" },
                    { 239L, null, 42L, "	Beni Municipality", "बेनी नगरपालिका" },
                    { 223L, null, 34L, "Rapti Municipality", "राप्ती नगरपालिका" },
                    { 726L, null, 55L, "Gangadev Rural Municipality", "गंगादेव गाउँपालिका" },
                    { 728L, null, 55L, "Sunchhahari Rural Municipality", "सुनछहरी गाउँपालिका" },
                    { 805L, null, 70L, "Mellekh Rural Municipality", "मेल्लेख गाउँपालिका" },
                    { 806L, null, 70L, "Dhankari Rural Municipality", "ढँकारी गाउँपालिका" },
                    { 802344567L, null, 70L, "Bannigadi Jayagad Rural Municipality", "बान्नीगडीजैगड गाउँपालिका" },
                    { 323L, null, 71L, "Dipayal Silgadhi Municipality", "दिपायल सिलगढी नगरपालिका" },
                    { 324L, null, 71L, "Shikhar Municipality", "शिखर नगरपालिका" },
                    { 807L, null, 71L, "Aadarsha Rural Municipality", "आदर्श गाउँपालिका" },
                    { 808L, null, 71L, "Purbichauki Rural Municipality", "पूर्वीचौकी गाउँपालिका" },
                    { 809L, null, 71L, "K.I. Singh Rural Municipality", "केआईसिंह गाउँपालिका" },
                    { 810L, null, 71L, "Jorayal Rural Municipality", "जोरायल गाउँपालिका" },
                    { 811L, null, 71L, "Sayal Rural Municipality", "सायल गाउँपालिका" },
                    { 812L, null, 71L, "Bogatan-Phudsil Rural Municipality", "वोगटान–फुड्सिल गाउँपालिका" },
                    { 813L, null, 71L, "Badikedar Rural Municipality", "बड्डी केदार गाउँपालिका" },
                    { 325L, null, 72L, "Bungal Municipality", "बुंगल नगरपालिका" },
                    { 326L, null, 72L, "Jaya Prithvi Municipality", "जयपृथ्वी नगरपालिका" },
                    { 814L, null, 72L, "Kedarsyu Rural Municipality", "केदारस्यु गाउँपालिका" },
                    { 815L, null, 72L, "Thalara Rural Municipality", "थलारा गाउँपालिका" },
                    { 816L, null, 72L, "Bitthadchir Rural Municipality", "बित्थडचिर गाउँपालिका" },
                    { 817L, null, 72L, "Chhabis Pathibhera Rural Municipality", "छब्बीसपाथिभेरा गाउँपालिका" },
                    { 818L, null, 72L, "Chhanna Rural Municipality", "छान्ना गाउँपालिका" },
                    { 804L, null, 70L, "Turmakhand Rural Municipality", "तुर्माखाँद गाउँपालिका" },
                    { 803L, null, 70L, "Chaurpati Rural Municipality", "चौरपाटी गाउँपालिका" },
                    { 802L, null, 70L, "Ramaroshan Rural Municipality", "रामारोशन गाउँपालिका" },
                    { 322L, null, 70L, "Panchadewal Binayak Municipality", "पञ्चदेवल विनायक नगरपालिका" },
                    { 793L, null, 68L, "Kushe Rural Municipality", "कुसे गाउँपालिका" },
                    { 794L, null, 68L, "Barekot Rural Municipality", "बारेकोट गाउँपालिका" },
                    { 795L, null, 68L, "Shivalaya Rural Municipality", "शिवालय गाउँपालिका" },
                    { 311L, null, 69L, "	Dhangadhi Municipality", "धनगढी उप-महानगरपालिका" },
                    { 312L, null, 69L, "	Tikapur Municipality", "टिकापुर नगरपालिका" },
                    { 313L, null, 69L, "	Godawari Municipality", "गोदावरी नगरपालिका" },
                    { 314L, null, 69L, "	Lamki Chuha Municipality", "लम्की चुहा नगरपालिका" },
                    { 315L, null, 69L, "	Tikapur Municipality", "टिकापुर नगरपालिका" },
                    { 316L, null, 69L, "	Ghodaghodi Municipality", "घोडाघोडी नगरपालिका" },
                    { 819L, null, 72L, "Masta Rural Municipality", "मष्टा गाउँपालिका" },
                    { 317L, null, 69L, "	Gauriganga Municipality", "गौरीगंगा नगरपालिका" },
                    { 796L, null, 69L, "Janaki Rural Municipality", "जानकी गाउँपालिका" },
                    { 797L, null, 69L, "Kailari Rural Municipality", "कैलारी गाउँपालिका" },
                    { 798L, null, 69L, "Joshipur Rural Municipality", "जोशीपुर गाउँपालिका" },
                    { 799L, null, 69L, "Bardagoriya Rural Municipality", "बर्गगोरिया गाउँपालिका" },
                    { 800L, null, 69L, "Mohanyal Rural Municipality", "मोहन्याल गाउँपालिका" },
                    { 801L, null, 69L, "Chure Rural Municipality", "चुरे	 गाउँपालिका" },
                    { 319L, null, 70L, "Kamalbazar Municipality", "कमलबजार नगरपालिका" },
                    { 320L, null, 70L, "Sanphebagar Municipality", "साफेबगर नगरपालिका" },
                    { 321L, null, 70L, "Mangalsen Municipality", "मंगलसेन नगरपालिका" },
                    { 318L, null, 69L, "	Bhajani Municipality", "भजनी नगरपालिका" },
                    { 792L, null, 68L, "Junichande Rural Municipality", "जुनीचाँदे गाउँपालिका" },
                    { 820L, null, 72L, "Durgathali Rural Municipality", "दुर्गाथली गाउँपालिका" },
                    { 822L, null, 72L, "Surma Rural Municipality", "सुर्मा गाउँपालिका" },
                    { 834L, null, 75L, "Bhageshwar Rural Municipality", "भागेश्वर गाउँपालिका" },
                    { 835L, null, 75L, "Ajaymeru Rural Municipality", "अजयमेरु गाउँपालिका" },
                    { 340L, null, 76L, "	Melauli Municipality", "मेलौली नगरपालिका" },
                    { 341L, null, 76L, "	Purchaundi Municipality", "पुर्चौडी नगरपालिका" },
                    { 342L, null, 76L, "	Dasharathchand Municipality", "दशरथचन्द नगरपालिका" },
                    { 343L, null, 76L, "	Patan Municipality", "पाटन नगरपालिका" },
                    { 836L, null, 76L, "Dogdakedar Rural Municipality", "दोगडाकेदार गाउँपालिका" },
                    { 837L, null, 76L, "Dilashaini Rural Municipality", "डिलाशैनी गाउँपालिका" },
                    { 838L, null, 76L, "Sigas Rural Municipality", "सिगास गाउँपालिका" },
                    { 839L, null, 76L, "Pancheshwar Rural Municipality", "पञ्चेश्वर गाउँपालिका" },
                    { 840L, null, 76L, "Surnaya Rural Municipality", "सुर्नया गाउँपालिका" },
                    { 841L, null, 76L, "Shivanath Rural Municipality", "शिवनाथ गाउँपालिका" },
                    { 344L, null, 77L, "	Shailyashikhar Municipality", "शैल्यशिखर नगरपालिका" },
                    { 345L, null, 77L, "	Mahakali Municipality", "महाकाली नगरपालिका" },
                    { 842L, null, 77L, "Naugad Rural Municipality", "नौगाड गाउँपालिका" },
                    { 843L, null, 77L, "Malikarjun Rural Municipality", "मालिकार्जुन गाउँपालिका" },
                    { 844L, null, 77L, "Marma Rural Municipality", "मार्मा गाउँपालिका" },
                    { 845L, null, 77L, "Lekam Rural Municipality", "लेकम गाउँपालिका" },
                    { 846L, null, 77L, "Duhu Rural Municipality", "दुहु गाउँपालिका" },
                    { 833L, null, 75L, "Ganyapadhura Rural Municipality", "गन्यापधुरा गाउँपालिका" },
                    { 832L, null, 75L, "Aalitaal Rural Municipality", "आलिताल गाउँपालिका" },
                    { 831L, null, 75L, "Navadurga Rural Municipality", "नवदुर्गा	 गाउँपालिका" },
                    { 339L, null, 75L, "	Amargadhi Municipality", "अमरगढी नगरपालिका" },
                    { 823L, null, 72L, "Saipal Rural Municipality", "सइपाल गाउँपालिका" },
                    { 327L, null, 73L, "Budhiganga Municipality", "बुढीगंगा नगरपालिका" },
                    { 328L, null, 73L, "Budhinanda Municipality", "बुढीनन्दा नगरपालिका" },
                    { 329L, null, 73L, "Tribeni Municipality", "त्रिवेणी नगरपालिका" },
                    { 330L, null, 73L, "Badimalika Municipality", "बडीमालिका नगरपालिका" },
                    { 824L, null, 73L, "Khaptad Chhededaha Rural Municipality", "खप्तड छेडेदह गाउँपालिका" },
                    { 825L, null, 73L, "Swami Kartik Khapar Rural Municipality", "स्वामिकार्तिक खापर गाउँपालिका" },
                    { 826L, null, 73L, "Jagannath Rural Municipality", "जगन्नाथ गाउँपालिका" },
                    { 827L, null, 73L, "Himali Rural Municipality", "हिमाली गाउँपालिका" },
                    { 821L, null, 72L, "Talkot Rural Municipality", "तलकोट गाउँपालिका" },
                    { 828L, null, 73L, "Gaumul Rural Municipality", "गौमुल गाउँपालिका" },
                    { 332L, null, 74L, "	Krishnapur", "कृष्णपुर नगरपालिका" },
                    { 333L, null, 74L, "	Punarbas", "पुनर्वास नगरपालिका" },
                    { 334L, null, 74L, "	Belauri", "बेलौरी नगरपालिका" },
                    { 335L, null, 74L, "	Bedkot", "वेदकोट नगरपालिका" },
                    { 336L, null, 74L, "	Shuklaphanta", "शुक्लाफाँटा नगरपालिका" },
                    { 337L, null, 74L, "	Mahakali", "माहाकाली नगरपालिका" },
                    { 829L, null, 74L, "Laljhadi Rural Municipality", "लालझाँडी गाउँपालिका" },
                    { 830L, null, 74L, "Beldandi Rural Municipality", "बेलडाँडी गाउँपालिका" },
                    { 338L, null, 75L, "	Parashuram Municipality", "परशुराम नगरपालिका" },
                    { 331L, null, 74L, "	Bhimdatta", "भीमदत्त नगरपालिका" },
                    { 310L, null, 68L, "	Nalgad Municipality", "नलगाड नगरपालिका" },
                    { 309L, null, 68L, "	Bheri Municipality", "भेरी नगरपालिका" },
                    { 308L, null, 68L, "	Chhedagad Municipality", "छेडागाड नगरपालिका" },
                    { 743L, null, 59L, "Banphikot Rural Municipality", "बाँफिकोट गाउँपालिका" },
                    { 289L, null, 60L, "	Bangad Kupinde Municipality", "बनगाड कुपिण्डे नगरपालिका" },
                    { 290L, null, 60L, "	Bagchaur Municipality", "बागचौर नगरपालिका" },
                    { 291L, null, 60L, "	Shaarda Municipality", "शारदा नगरपालिका" },
                    { 744L, null, 60L, "Kumakh Rural Municipality", "कुमाख गाउँपालिका" },
                    { 745L, null, 60L, "Kalimati Rural Municipality", "कालीमाटी गाउँपालिका" },
                    { 746L, null, 60L, "Chhatreshwari Rural Municipality", "छत्रेश्वरी गाउँपालिका" },
                    { 747L, null, 60L, "Darma Rural Municipality", "दार्मा गाउँपालिका" },
                    { 748L, null, 60L, "Kapurkot Rural Municipality", "कपुरकोट गाउँपालिका" },
                    { 749L, null, 60L, "Triveni Rural Municipality", "त्रिवेणी गाउँपालिका" },
                    { 750L, null, 60L, "Siddha Kumakh Rural Municipality", "सिद्ध कुमाख गाउँपालिका" },
                    { 293L, null, 61L, "Tripura Sundari Municipality", "त्रिपुरासुन्दरी नगरपालिका" },
                    { 292L, null, 61L, "Thuli Bheri Municipality", "ठूली भेरी नगरपालिका" },
                    { 752L, null, 61L, "Mudkechula Rural Municipality", "मुड्केचुला गाउँपालिका" },
                    { 753L, null, 61L, "Kaike Rural Municipality", "काईके गाउँपालिका" },
                    { 754L, null, 61L, "She Phoksundo Rural Municipality", "शे फोक्सुन्डो गाउँपालिका" },
                    { 755L, null, 61L, "Jagadulla Rural Municipality", "जगदुल्ला गाउँपालिका" },
                    { 756L, null, 61L, "Dolpo Buddha Rural Municipality", "डोल्पो बुद्ध गाउँपालिका" },
                    { 757L, null, 61L, "Chharka Tangsong Rural Municipality", "छार्का ताङसोङ गाउँपालिका" },
                    { 742L, null, 59L, "Triveni Rural Municipality", "त्रिवेणी गाउँपालिका" },
                    { 741L, null, 59L, "Sani Bheri Rural Municipality", "सानीभेरी गाउँपालिका" },
                    { 288L, null, 59L, "	Chaurjahari Municipality", "चौरजहारी नगरपालिका" },
                    { 287L, null, 59L, "	Aathabiskot Municipality", "आठबिसकोट नगरपालिका" },
                    { 729L, null, 55L, "Thawang Rural Municipality", "थवाङ गाउँपालिका" },
                    { 730L, null, 56L, "Bhume Rural Municipality", "भूमे गाउँपालिका" },
                    { 731L, null, 56L, "Putha Uttarganga Rural Municipality", "पुठा उत्तरगंगा गाउँपालिका" },
                    { 732L, null, 56L, "Sisne Rural Municipality", "सिस्ने गाउँपालिका" },
                    { 279L, null, 57L, "	Nepalgunj Municipality", "नेपालगंज उप-महानगरपालिका" },
                    { 280L, null, 57L, "	Kohalpur Municipality", "कोहलपुर नगरपालिका" },
                    { 733L, null, 57L, "Raptisonari Rural Municipality", "राप्ती सोनारी गाउँपालिका" },
                    { 734L, null, 57L, "Baijnath	 Rural Municipality", "वैजनाथ गाउँपालिका" },
                    { 735L, null, 57L, "Khajura Rural Municipality", "खजुरा गाउँपालिका" },
                    { 758L, null, 62L, "Simkot Rural Municipality", "सिमकोट गाउँपालिका" },
                    { 736L, null, 57L, "Janaki Rural Municipality", "जानकी गाउँपालिका" },
                    { 738L, null, 57L, "Narainapur Rural Municipality", "नरैनापुर गाउँपालिका" },
                    { 281L, null, 58L, "	Barbardiya Municipality", "बारबर्दिया नगरपालिका" },
                    { 282L, null, 58L, "	Gulariya Municipality", "गुलरिया नगरपालिका" },
                    { 283L, null, 58L, "	Rajapur Municipality", "राजापुर नगरपालिका" },
                    { 284L, null, 58L, "	Bansgadhi Municipality", "बासगढी नगरपालिका" },
                    { 285L, null, 58L, "	Madhuwan Municipality", "मधुवन नगरपालिका" },
                    { 286L, null, 58L, "	Thakurbaba Municipality", "ठाकुरबाबा नगरपालिका" },
                    { 739L, null, 58L, "Badhaiyatal Rural Municipality", "बढैयाताल	 गाउँपालिका" },
                    { 740L, null, 58L, "Geruwa Rural Municipality", "गेरुवा गाउँपालिका" },
                    { 737L, null, 57L, "Duduwa Rural Municipality", "डुडुवा गाउँपालिका" },
                    { 759L, null, 62L, "Sarkegad Rural Municipality", "सर्केगाड गाउँपालिका" },
                    { 760L, null, 62L, "Adanchuli Rural Municipality", "अदानचुली गाउँपालिका" },
                    { 761L, null, 62L, "Kharpunath Rural Municipality", "खार्पुनाथ गाउँपालिका" },
                    { 299L, null, 66L, "	Birendranagar Municipality", "बीरेन्द्रनगर नगरपालिका" },
                    { 300L, null, 66L, "	Gurbhakot Municipality", "गुर्भाकोट नगरपालिका" },
                    { 301L, null, 66L, "	Panchapuri Municipality", "पञ्चपुरी नगरपालिका" },
                    { 302L, null, 66L, "	Bheriganga Municipality", "भेरीगंगा नगरपालिका" },
                    { 303L, null, 66L, "	Lekbeshi Municipality", "लेकबेशी नगरपालिका" },
                    { 781L, null, 66L, "Barahatal Rural Municipality", "बराहताल गाउँपालिका" },
                    { 782L, null, 66L, "Simta Rural Municipality", "सिम्ता गाउँपालिका" },
                    { 783L, null, 66L, "Chaukune Rural Municipality", "चौकुने गाउँपालिका" },
                    { 784L, null, 66L, "Chingad Rural Municipality", "चिङ्गाड गाउँपालिका" },
                    { 780L, null, 65L, "Mugum Karmarong Rural Municipality", "मुगुम कार्मारोंग गाउँपालिका" },
                    { 304L, null, 67L, "	Dullu Municipality", "दुल्लु नगरपालिका" },
                    { 306L, null, 67L, "	Narayan Municipality", "नारायण नगरपालिका" },
                    { 307L, null, 67L, "	Chamunda Bindrasaini Municipality", "चामुण्डा विन्द्रासैनी नगरपालिका" },
                    { 785L, null, 67L, "Gurans Rural Municipality", "गुराँस गाउँपालिका" },
                    { 786L, null, 67L, "Bhairabi Rural Municipality", "भैरवी गाउँपालिका" },
                    { 787L, null, 67L, "Naumule Rural Municipality", "नौमुले गाउँपालिका" },
                    { 788L, null, 67L, "Mahabu Rural Municipality", "महावु गाउँपालिका" },
                    { 789L, null, 67L, "Thantikandh Rural Municipality", "ठाँटीकाँध गाउँपालिका" },
                    { 790L, null, 67L, "Bhagawatimai Rural Municipality", "भगवतीमाई गाउँपालिका" },
                    { 791L, null, 67L, "Dungeshwar Rural Municipality", "डुंगेश्वर गाउँपालिका" },
                    { 305L, null, 67L, "	Aathabis Municipality", "आठबीस नगरपालिका" },
                    { 727L, null, 55L, "Madi Rural Municipality", "माडी गाउँपालिका" },
                    { 779L, null, 65L, "Soru Rural Municipality", "सोरु गाउँपालिका" },
                    { 298L, null, 65L, "	Chhayanath Rara", "छायाँनाथ रारा नगरपालिका" },
                    { 762L, null, 62L, "Tanjakot Rural Municipality", "ताँजाकोट गाउँपालिका" },
                    { 763L, null, 62L, "Chankheli Rural Municipality", "चंखेली गाउँपालिका" },
                    { 764L, null, 62L, "Namkha Rural Municipality", "नाम्खा गाउँपालिका" },
                    { 294L, null, 63L, "	Chandannath Municipality", "चन्दननाथ नगरपालिका" },
                    { 765L, null, 63L, "Tatopani Rural Municipality", "तातोपानी गाउँपालिका" },
                    { 766L, null, 63L, "Patarasi Rural Municipality", "पातारासी गाउँपालिका" },
                    { 767L, null, 63L, "Tila Rural Municipality", "तिला गाउँपालिका" },
                    { 768L, null, 63L, "Kanaka Sundari Rural Municipality", "कनकासुन्दरी गाउँपालिका" },
                    { 769L, null, 63L, "Sinja Rural Municipality", "सिंजा गाउँपालिका" },
                    { 778L, null, 65L, "Khatyad Rural Municipality", "खत्याड गाउँपालिका" },
                    { 770L, null, 63L, "Hima Rural Municipality", "हिमा गाउँपालिका" },
                    { 295L, null, 64L, "	Khandachakra Municipality", "खाँडाचक्र नगरपालिका" },
                    { 296L, null, 64L, "	Raskot Municipality", "रास्कोट नगरपालिका" },
                    { 297L, null, 64L, "	Tilagupha Municipality", "तिलागुफा नगरपालिका" },
                    { 772L, null, 64L, "Narharinath Rural Municipality", "नरहरिनाथ गाउँपालिका" },
                    { 773L, null, 64L, "Palata Rural Municipality", "पलाता गाउँपालिका" },
                    { 774L, null, 64L, "Shubha Kalika Rural Municipality", "	शुभ कालिका गाउँपालिका" },
                    { 775L, null, 64L, "Sanni Triveni Rural Municipality", "सान्नी त्रिवेणी गाउँपालिका" },
                    { 776L, null, 64L, "Pachaljharana Rural Municipality", "पचालझरना गाउँपालिका" },
                    { 777L, null, 64L, "Mahawai Rural Municipality", "महावै गाउँपालिका" },
                    { 771L, null, 63L, "Guthichaur Rural Municipality", "गुठिचौर गाउँपालिका" },
                    { 847L, null, 77L, "Vyans Rural Municipality", "ब्याँस गाउँपालिका" },
                    { 222L, null, 34L, "Ratnanagar Municipality", "रत्ननगर नगरपालिका" },
                    { 600L, null, 33L, "Tripura Sundari Rural Municipality", "त्रिपुरासुन्दरी गाउँपालिका" },
                    { 375L, null, 12L, "Sidingwa Rural Municipality", "सिदिन्ग्वा गाउँपालिका" },
                    { 376L, null, 12L, "Maiwakhola Rural Municipality", "मैवाखोला गाउँपालिका" },
                    { 377L, null, 12L, "Mikkwakhola Rural Municipality", "मिक्क्वाखोला गाउँपालिका" },
                    { 378L, null, 12L, "Phaktanglung Rural Municipality", "फक्तान्ग्लुंग गाउँपालिका" },
                    { 53L, null, 13L, "Myanglung Municipality", "म्याङलुङ नगर पालिका" },
                    { 54L, null, 13L, "Laligurans Municipality", "लालिगुराँस नगर पालिका" },
                    { 379L, null, 13L, "Aathrai Rural Municipality", "आठराई गाउँपालिका" },
                    { 380L, null, 13L, "Phedap Rural Municipality", "फेदाप गाउँपालिका" },
                    { 381L, null, 13L, "Chhathar Rural Municipality", "	छथर गाउँपालिका" },
                    { 382L, null, 13L, "Menchayayem Rural Municipality", "मेन्छयायेम गाउँपालिका" },
                    { 56L, null, 14L, "Triyuga Municipality", "त्रियुगा नगरपालिका" },
                    { 57L, null, 14L, "Katari Municipality", "कटारी नगरपालिका" },
                    { 58L, null, 14L, "Chaudandigadhi Municipality", "चौदण्डीगढी नगर पालिका" },
                    { 59L, null, 14L, "Belaka Municipality", "वेलका नगर पालिका" },
                    { 383L, null, 14L, "Udayapurgadhi Rural Municipality", "उदयपुरगढी गाउँपालिका" },
                    { 384L, null, 14L, "Rautamai Rural Municipality", "रौतामाई गाउँपालिका" },
                    { 385L, null, 14L, "Tapli Rural Municipality", "	ताप्ली गाउँपालिका" },
                    { 386L, null, 14L, "Limchungbung Rural Municipality", "लिम्चुङबुङ गाउँपालिका" },
                    { 60L, null, 15L, "Rajbiraj Municipality", "राजविराज नगरपालिका" },
                    { 374L, null, 12L, "Meringden Rural Municipality", "मेरिन्ग्देन गाउँपालिका" },
                    { 373L, null, 12L, "Pathibhara Yangwarak Rural Municipality", "पाथीभरा याङवरक गाउँपालिका" },
                    { 372L, null, 12L, "Aathrai Triveni Rural Municipality", "आठराई त्रिवेणी गाउँपालिका" },
                    { 371L, null, 12L, "Sirijangha Rural Municipality", "सिरीजङ्घा गाउँपालिका" },
                    { 358L, null, 10L, "Dudhakaushika Rural Municipality", "दुधकौशिका गाउँपालिका" },
                    { 359L, null, 10L, "Necha Salyan Rural Municipality", "नेचासल्यान गाउँपालिका" },
                    { 360L, null, 10L, "Dudhkoshi Rural Municipality", "दुधकोशी गाउँपालिका" },
                    { 361L, null, 10L, "Maha Kulung Rural Municipality", "महाकुलुङ गाउँपालिका" },
                    { 362L, null, 10L, "Sotang Rural Municipality", "सोताङ गाउँपालिका" },
                    { 363L, null, 10L, "Khumbu Pasang Lhamu Rural Municipality", "खुम्बु पासाङल्हमु गाउँपालिका" },
                    { 364L, null, 10L, "Likhu Pike Rural Municipality", "लिखुपिके गाउँपालिका" },
                    { 50L, null, 11L, "Itahari Municipality", "ईटहरी उप-महानगर पालिका" },
                    { 51L, null, 11L, "Dharan Municipality", "धरान उप-महानगर पालिका" },
                    { 61L, null, 15L, "Hanumannagar Kankalini Municipality", "हनुमाननगर कंकालिनी नगरपालिका" },
                    { 46L, null, 11L, "Barahachhetra Municipality", "बराहक्षेत्र नगर पालिका" },
                    { 48L, null, 11L, "Duhabi Municipality", "दुहवी नगर पालिका" },
                    { 49L, null, 11L, "Ramdhuni Municipality", "रामधुनी नगर पालिका" },
                    { 365L, null, 11L, "Koshi Rural Municipality", "कोशी गाउँपालिका" },
                    { 366L, null, 11L, "Harinagara Rural Municipality", "हरिनगरा गाउँपालिका" },
                    { 367L, null, 11L, "Bhokraha Rural Municipality", "भोक्राहा गाउँपालिका" },
                    { 368L, null, 11L, "Dewanganj Rural Municipality", "देवानगन्ज गाउँपालिका" },
                    { 369L, null, 11L, "Gadhi Rural Municipality", "गढी गाउँपालिका" }
                });

            migrationBuilder.InsertData(
                table: "MunicipalityVDCs",
                columns: new[] { "Id", "Description", "DistrictId", "VDCName", "VDCNameNepali" },
                values: new object[,]
                {
                    { 370L, null, 11L, "Barju Rural Municipality", "बर्जु गाउँपालिका" },
                    { 52L, null, 12L, "Taplejung(Phungling) Municipality", "फुङलिङ नगर पालिका" },
                    { 47L, null, 11L, "Inaruwa Municipality", "ईनरुवा नगर पालिका" },
                    { 45L, null, 10L, "Solu Dudhkunda Municipality", "सोलुदुधकुण्ड नगर पालिका" },
                    { 62L, null, 15L, "Khadak Municipality", "खडक नगरपालिका" },
                    { 64L, null, 15L, "Surunga Municipality", "सुरुङ्‍गा नगरपालिका" },
                    { 399L, null, 16L, "Arnama Rural Municipality", "अर्नमा गाउँपालिका" },
                    { 401L, null, 16L, "Naraha Rural Municipality", "भगवानपुर गाउँपालिका" },
                    { 400L, null, 16L, "Bhagawanpur Rural Municipality", "नरहा गाउँपालिका" },
                    { 402L, null, 16L, "Nawarajpur Rural Municipality", "नवराजपुर गाउँपालिका" },
                    { 403L, null, 16L, "Sakhuwanankarkatti Rural Municipality", "सखुवानान्कारकट्टी गाउँपालिका" },
                    { 404L, null, 16L, "Bishnupur Rural Municipality", "विष्णुपुर गाउँपालिका" },
                    { 77L, null, 17L, "Janakpur Municipality", "जनकपुरधाम उप-महानगरपालिका" },
                    { 78L, null, 17L, "Mithila Bihari Municipality", "मिथिला बिहारी नगर पालिका" },
                    { 79L, null, 17L, "Sabaila Municipality", "सवैला नगर पालिका" },
                    { 80L, null, 17L, "Dhanushadham Municipality", "धनुषाधाम नगर पालिका" },
                    { 81L, null, 17L, "Mithila Municipality", "मिथिला नगर पालिका" },
                    { 82L, null, 17L, "Shahidnagar Municipality", "शहिद नगर पालिका" },
                    { 83L, null, 17L, "Kshireshwarnath Municipality", "क्षिरेश्वरनाथ नगर पालिका" },
                    { 84L, null, 17L, "Hansapur Municipality", "हंसपुर नगर पालिका" },
                    { 85L, null, 17L, "Kamala Municipality", "कमला नगर पालिका" },
                    { 86L, null, 17L, "Ganeshman Charnath Municipality", "गणेशमान चारनाथ नगर पालिका" },
                    { 87L, null, 17L, "Nagarain Municipality", "नगराईन नगर पालिका" },
                    { 88L, null, 17L, "Bideha Municipality", "विदेह नगर पालिका" },
                    { 405L, null, 17L, "Laksminiya Rural Municipality", "लक्ष्मीनिया गाउँपालिका" },
                    { 398L, null, 16L, "Aaurahi	 Rural Municipality", "औरही गाउँपालिका" },
                    { 397L, null, 16L, "Bariyarpatti Rural Municipality", "बरियारपट्टी गाउँपालिका" },
                    { 396L, null, 16L, "Laksmipur Patari Rural Municipality", "लक्ष्मीपुर पतारी गाउँपालिका" },
                    { 76L, null, 16L, "Karjanha Municipality", "कर्जन्हा नगर पालिका" },
                    { 65L, null, 15L, "Bodebarsain Municipality", "बोदेबरसाईन नगरपालिका" },
                    { 66L, null, 15L, "Shambhunath Municipality", "शम्भुनाथ नगरपालिका" },
                    { 67L, null, 15L, "Kanchanrup Municipality", "कञ्चनरूप नगरपालिका" },
                    { 68L, null, 15L, "Saptakoshi Municipality", "सप्तकोशी नगरपालिका" },
                    { 387L, null, 15L, "Tilathi Koiladi Rural Municipality", "तिलाठी कोईलाडी गाउँपालिका" },
                    { 388L, null, 15L, "Belhi Chapena	 Rural Municipality", "बेल्ही चपेना गाउँपालिका" },
                    { 389L, null, 15L, "Chhinnamasta Rural Municipality", "छिन्नमस्ता गाउँपालिका" },
                    { 390L, null, 15L, "	Mahadeva Rural Municipality", "महादेवा गाउँपालिका" },
                    { 391L, null, 15L, "Aagnisaira Krishnasawaran Rural Municipality", "अग्निसाइर कृष्णासवरन	 गाउँपालिका" },
                    { 63L, null, 15L, "Dakneshwari Municipality", "दक्नेश्वारी नगरपालिका" },
                    { 392L, null, 15L, "Rupani Rural Municipality", "रुपनी गाउँपालिका" },
                    { 394L, null, 15L, "Bishnupur Rural Municipality", "बिष्णुपुर गाउँपालिका" },
                    { 395L, null, 15L, "Tirhut Rural Municipality", "तिरहुत गाउँपालिका" },
                    { 69L, null, 16L, "Lahan Municipality", "लहान नगर पालिका" },
                    { 70L, null, 16L, "Siraha Municipality", "सिरहा नगर पालिका" },
                    { 71L, null, 16L, "Golbazar Municipality", "गोलबजार नगर पालिका" },
                    { 72L, null, 16L, "Mirchaiya Municipality", "मिर्चैया नगर पालिका" },
                    { 73L, null, 16L, "Kalyanpur Municipality", "कल्याणपुर नगर पालिका" },
                    { 74L, null, 16L, "Dhangadimai Municipality", "धनगढीमाई नगर पालिका" },
                    { 75L, null, 16L, "Sukhipur Municipality", "सुखीपुर नगर पालिका" },
                    { 393L, null, 15L, "Balan-Bihul Rural Municipality", "बलान-बिहुल गाउँपालिका" },
                    { 357L, null, 9L, "Bhot Khola Rural Municipality", "भोटखोला गाउँपालिका" },
                    { 356L, null, 9L, "Chichila Rural Municipality", "चिचिला गाउँपालिका" },
                    { 355L, null, 9L, "Sabhapokhari Rural Municipality", "सभापोखरी गाउँपालिका" },
                    { 182L, null, 3L, "Mangsebung Rural Municipality", "माङसेबुङ गाउँपालिका" },
                    { 183L, null, 3L, "Sandakpur Rural Municipality", "सन्दकपुर गाउँपालिका" },
                    { 12L, null, 4L, "Mechinagar Municipality", "मेची नगरपालिका" },
                    { 13L, null, 4L, "BirtamodMunicipality", "विर्तामोड नगरपालिका" },
                    { 14L, null, 4L, "Damak Municipality", "दमक नगरपालिका" },
                    { 15L, null, 4L, "Bhadrapur Municipality", "भद्रपुर नगरपालिका" },
                    { 16L, null, 4L, "Shivasatakshi Municipality", "शिवसताक्षि नगरपालिका" },
                    { 17L, null, 4L, "Arjundhara Municipality", "अर्जुनधारा नगरपालिका" },
                    { 18L, null, 4L, "Gauradaha Municipality", "गौरादह नगरपालिका" },
                    { 19L, null, 4L, "Gauriganga Municipality", "कन्काई नगरपालिका" },
                    { 184L, null, 4L, "Kamal Rural Municipality", "कमल गाउँपालिका" },
                    { 185L, null, 4L, "Gaurigunj Rural Municipality", "गौरीगन्ज गाउँपालिका" },
                    { 186L, null, 4L, "Barhadashi Rural Municipality", "बर्हदाशी गाउँपालिका" },
                    { 187L, null, 4L, "Jhapa Rural Municipality", "झापा गाउँपालिका" },
                    { 188L, null, 4L, "Buddhashanti Rural Municipality", "बुद्धशान्ति गाउँपालिका" },
                    { 189L, null, 4L, "Haldibari Rural Municipality", "हल्दिबारी गाउँपालिका" },
                    { 190L, null, 4L, "Kachankawal Rural Municipality", "कचंकवल गाउँपालिका" },
                    { 28L, null, 5L, "Rupakot Majhuwagadhi Municipality", "रुपाकोट–मजुवागढी नगर पालिका" },
                    { 29L, null, 5L, "Halesi Tuwachung Municipality", "हलेसी तुवाचुङ नगर पालिका" },
                    { 181L, null, 3L, "Rong Rural Municipality", "रोङ गाउँपालिका" },
                    { 180L, null, 3L, "Chulachuli Rural Municipality", "चुलाचुली गाउँपालिका" },
                    { 179L, null, 3L, "Mai Jogmai Rural Municipality", "माईजोगमाई गाउँपालिका" },
                    { 178L, null, 3L, "Phakphokthum Rural Municipality", "फाकफोकथुम गाउँपालिका" },
                    { 24L, null, 1L, "Shadanand Municipality", "षडानन्द नगरपालिका" },
                    { 25L, null, 1L, "Bhojpur Municipality", "भोजपुर नगरपालिका" },
                    { 167L, null, 1L, "Hatuwagadhi Rural Municipality", "हतुवागढी गाउँपालिका" },
                    { 168L, null, 1L, "Ramprasad Rai Rural Municipality", "रामप्रसाद राई गाउँपालिका" },
                    { 169L, null, 1L, "Aamchok Rural Municipality", "आमचोक गाउँपालिका" },
                    { 170L, null, 1L, "Tyamke Maiyunm Rural Municipality", "ट्याम्केमैयुम गाउँपालिका" },
                    { 171L, null, 1L, "Arun Gaunpalika Rural Municipality", "अरुण गाउँपालिका" },
                    { 172L, null, 1L, "Pauwadungma Rural Municipality", "पौवादुङमा गाउँपालिका" },
                    { 173L, null, 1L, "Salpasilichho Rural Municipality", "साल्पासिलिछो गाउँपालिका" },
                    { 191L, null, 5L, "Khotehang Rural Municipality", "खोटेहांग गाउँपालिका" },
                    { 26L, null, 2L, "Dhankuta Municipality", "धनकुटा नगरपालिका" },
                    { 115L, null, 2L, "Pakhribas Municipality", "पाख्रिवास नगरपालिका" },
                    { 174L, null, 2L, "Sangurigadhi Rural Municipality", "सागुरीगढी गाउँपालिका" },
                    { 175L, null, 2L, "Chaubise Rural Municipality", "चौविसे गाउँपालिका" },
                    { 176L, null, 2L, "Khalsa Chhintang Sahidbhumi Rural Municipality", "खाल्सा छिन्ताङ सहीदभूमि गाउँपालिका" },
                    { 177L, null, 2L, "Chhathar Jorpati Rural Municipality", "छथर जोरपाटी गाउँपालिका" },
                    { 20L, null, 3L, "Suryodaya Municipality", "सूर्योदय नगरपालिका" },
                    { 21L, null, 3L, "Ilam Municipality", "इलाम नगरपालिका" },
                    { 22L, null, 3L, "Deumai Municipality", "देउमाई नगरपालिका" },
                    { 23L, null, 3L, "Mai Municipality", "माई नगरपालिका" },
                    { 27L, null, 2L, "Mahalaxmi Municipality", "महालक्ष्मी नगरपालिका" },
                    { 192L, null, 5L, "Diprung Rural Municipality", "दिप्रुंग गाउँपालिका" },
                    { 193L, null, 5L, "Aiselukharka Rural Municipality", "ऐसेलुखर्क गाउँपालिका" },
                    { 194L, null, 5L, "Jantedhunga Rural Municipality", "जन्तेदुंगा गाउँपालिका" },
                    { 348L, null, 7L, "Sunkoshi Rural Municipality", "सुनकोशी गाउँपालिका" },
                    { 349L, null, 7L, "Molung Rural Municipality", "मोलुङ गाउँपालिका" },
                    { 350L, null, 7L, "Chisankhugadhi Rural Municipality", "चिसंखुगढी गाउँपालिका" },
                    { 351L, null, 7L, "Khiji Demba Rural Municipality", "खिजिदेम्बा गाउँपालिका" },
                    { 352L, null, 7L, "Likhu Rural Municipality", "लिखु गाउँपालिका" },
                    { 39L, null, 8L, "Phidim Municipality", "फिदिम नगर पालिका" },
                    { 207L, null, 8L, "Hilihang Rural Municipality ", "हिलिहाङ गाउँपालिका" },
                    { 208L, null, 8L, "Kummayak Rural Municipality", "कुम्मायाक गाउँपालिका" },
                    { 209L, null, 8L, "Miklajung Rural Municipality", "मिक्लाजुंग गाउँपालिका" },
                    { 347L, null, 7L, "Champadevi Municipality", "चम्पादेवी गाउँपालिका" },
                    { 210L, null, 8L, "Phalelung Rural Municipality", "फालेलुंग गाउँपालिका" },
                    { 212L, null, 8L, "Tumbewa Rural Municipality", "तुम्वेवा गाउँपालिका" },
                    { 213L, null, 8L, "Yangawarak Rural Municipality", "याङवरक गाउँपालिका" },
                    { 40L, null, 9L, "Khandbari Municipality", "खादँवारी नगर पालिका" },
                    { 41L, null, 9L, "Chainpur Municipality", "चैनपुर नगर पालिका" },
                    { 42L, null, 9L, "Dharmadevi Municipality", "धर्मदेवी नगर पालिका" },
                    { 43L, null, 9L, "Panchkhapan Municipality", "पाँचखपन नगर पालिका" },
                    { 44L, null, 9L, "Madi Municipality", "मादी नगर पालिका" },
                    { 353L, null, 9L, "Makalu Rural Municipality", "मकालु गाउँपालिका" },
                    { 354L, null, 9L, "Silichong Rural Municipality", "सिलीचोङ गाउँपालिका" },
                    { 211L, null, 8L, "Phalgunanda Rural Municipality", "फाल्गुनन्द गाउँपालिका" },
                    { 406L, null, 17L, "Mukhiyapatti Musaharmiya Rural Municipality", "मुखियापट्टी मुसहरमिया गाउँपालिका" },
                    { 346L, null, 7L, "Manebhanjyang Municipality", "मानेभञ्ज्याङ गाउँपालिका" },
                    { 206L, null, 6L, "Jahada Rural Municipality", "जहदा गाउँपालिका" },
                    { 195L, null, 5L, "Kepilasgadhi Rural Municipality", "केपिलास्गढ़ी गाउँपालिका" },
                    { 196L, null, 5L, "Barahpokhari Rural Municipality", "बार्हपोखरी गाउँपालिका" },
                    { 197L, null, 5L, "Lamidanda Rural Municipality", "लमीडाडा गाउँपालिका" },
                    { 198L, null, 5L, "Sakela Rural Municipality", "साकेला गाउँपालिका" },
                    { 55L, null, 6L, "Biratnagar Municipality", "विराटनगर महानगरपालिका" },
                    { 30L, null, 6L, "Sundar Haraincha Municipality", "सुन्दरहरैंचा नगरपालिका" },
                    { 31L, null, 6L, "Belbari Municipality", "बेलवारी नगरपालिका" },
                    { 32L, null, 6L, "Pathari-Shanischare Municipality", "पथरी शनिश्चरे नगरपालिका" },
                    { 33L, null, 6L, "Ratuwamai Municipality", "रतुवामाई नगरपालिका" },
                    { 38L, null, 7L, " 	Siddhicharan Municipality", "सिद्दिचरण नगर पालिका" },
                    { 34L, null, 6L, "Urlabari Municipality", "उर्लावारी नगरपालिका" },
                    { 36L, null, 6L, "Sunawarshi Municipality", "सुनवर्षी नगरपालिका" },
                    { 37L, null, 6L, "Letang Bhogateni Municipality", "लेटाङ नगरपालिका" },
                    { 199L, null, 6L, "Kerabari Rural Municipality", "केराबारी गाउँपालिका" },
                    { 200L, null, 6L, "Miklajung Rural Municipality", "मिक्लाजुंग गाउँपालिका" },
                    { 201L, null, 6L, "Kanepokhari Rural Municipality", "कानेपोखरी गाउँपालिका" },
                    { 202L, null, 6L, "Budiganga Rural Municipality", "बुडिगंगा गाउँपालिका" },
                    { 203L, null, 6L, "Gramthan Rural Municipality", "ग्रम्थान गाउँपालिका" },
                    { 204L, null, 6L, "Katahari Rural Municipality", "कटहरी गाउँपालिका" },
                    { 205L, null, 6L, "Dhampalthan Rural Municipality", "धमपल्थान गाउँपालिका" },
                    { 35L, null, 6L, "Rangeli Municipality", "रंगेली नगरपालिका" },
                    { 221L, null, 34L, "Bharatpur Municipality", "भरतपुर महानगरपालिका" },
                    { 407L, null, 17L, "Janak Nandini Rural Municipality", "जनकनन्दिनी गाउँपालिका" },
                    { 409L, null, 17L, "Bateshwar Rural Municipality", "बटेश्वर गाउँपालिका" },
                    { 559L, null, 27L, "Galchhi Rural Municipality", "गल्छी गाउँपालिका" },
                    { 560L, null, 27L, "Gajuri Rural Municipality", "गजुरी गाउँपालिका" },
                    { 561L, null, 27L, "Jwalamukhi Rural Municipality", "ज्वालामूखी गाउँपालिका" },
                    { 562L, null, 27L, "Siddhalekh Rural Municipality", "सिद्धलेक गाउँपालिका" },
                    { 563L, null, 27L, "Tripura Sundari Rural Municipality", "त्रिपुरासुन्दरी गाउँपालिका" },
                    { 564L, null, 27L, "Gangajamuna Rural Municipality", "गङ्गाजमुना गाउँपालिका" },
                    { 565L, null, 27L, "Netrawati Dabjong Rural Municipality", "नेत्रावती डबजोङ गाउँपालिका" },
                    { 566L, null, 27L, "Khaniyabas Rural Municipality", "खनियाबास गाउँपालिका" },
                    { 1L, null, 28L, "Kathmandu Metropolitan", "काठमाण्डौ महानगर पालिका" },
                    { 2L, null, 28L, "Budhanilkantha Municipality", "बुढानिलकण्ठ नगर पालिका" },
                    { 3L, null, 28L, "Gokarneshwar Municipality", "गोकर्णेश्वर नगर पालिका" },
                    { 4L, null, 28L, "Tokha Municipality", "टोखा नगर पालिका" },
                    { 5L, null, 28L, "Chandragiri Municipality", "चन्द्रागिरी नगर पालिका" },
                    { 6L, null, 28L, "Tarakeshwar Municipality", "तारकेश्वर नगर पालिका" },
                    { 7L, null, 28L, "Kirtipur Municipality", "किर्तिपुर नगर पालिका" },
                    { 8L, null, 28L, "Nagarjun Municipality", "नागार्जुन नगर पालिका" },
                    { 9L, null, 28L, "Kageshwari-Manohara Municipality", "कागेश्वरी मनोहरा नगर पालिका" },
                    { 10L, null, 28L, "Shankharapur Municipality", "शङ्खरापुर नगर पालिका" },
                    { 11L, null, 28L, "Dakshinkali Municipality", "दक्षिणकाली नगर पालिका" },
                    { 558L, null, 27L, "Benighat Rorang Rural Municipality", "बेनीघाट रोराङ्ग गाउँपालिका" },
                    { 557L, null, 27L, "Thakre Rural Municipality", "थाक्रे गाउँपालिका" },
                    { 556L, null, 27L, "Ruby Valley Rural Municipality", "रुवी भ्याली गाउँपालिका" },
                    { 149L, null, 27L, "Nilkantha Municipality", "निलकण्ठ नगरपालिका" },
                    { 543L, null, 24L, "Khandadevi Rural Municipality", " गाउँपालिका" },
                    { 544L, null, 24L, "Likhu Tamakoshi Rural Municipality", "लिखु तामाकोशी खाँडादेवी गाउँपालिका" },
                    { 545L, null, 24L, "Doramba Rural Municipality", "दोरम्बा गाउँपालिका" },
                    { 546L, null, 24L, "Gokulganga Rural Municipality", "गोकुलगङ्गा	 गाउँपालिका" },
                    { 547L, null, 24L, "Sunapati Rural Municipality", "सुनापती गाउँपालिका" },
                    { 548L, null, 24L, "Umakunda Rural Municipality", "उमाकुण्ड गाउँपालिका" },
                    { 142L, null, 25L, "Bhimeswor Municipality", "भिमेस्वोर नगरपालिका" },
                    { 143L, null, 25L, "Jiri Municipality", "जिरी नगरपालिका" },
                    { 549L, null, 25L, "Kalinchok Rural Municipality", "कालिन्चोक गाउँपालिका" },
                    { 161L, null, 29L, "Dhulikhel Municipality", " नगरपालिका" },
                    { 550L, null, 25L, "Melung Rural Municipality", "मेलुङ गाउँपालिका" },
                    { 552L, null, 25L, "Baiteshwar Rural Municipality", "वैतेश्वर गाउँपालिका" },
                    { 553L, null, 25L, "Tamakoshi Rural Municipality", "तामाकोशी गाउँपालिका" },
                    { 554L, null, 25L, "Bigu Rural Municipality", "विगु गाउँपालिका" },
                    { 555L, null, 25L, "Gaurishankar Rural Municipality", "गौरिशंकर गाउँपालिका" },
                    { 144L, null, 26L, "Bhaktapur Municipality", "भक्तपुर नगरपालिका" },
                    { 145L, null, 26L, "Changunarayan Municipality", "चाँगुनारायण नगरपालिका" },
                    { 146L, null, 26L, "Madhyapur Thimi Municipality", "मध्यपुर थिमी नगरपालिका" },
                    { 147L, null, 26L, "Suryabinayak Municipality", "सुर्यविनायक नगरपालिका" },
                    { 148L, null, 27L, "Dhunibeshi Municipality", "धुनिबेशी नगरपालिका" },
                    { 551L, null, 25L, "Shailung Rural Municipality", "शैलुङ गाउँपालिका" },
                    { 141L, null, 24L, "Ramechhap Municipality", "रामेछाप नगरपालिका" },
                    { 162L, null, 29L, "Banepa Municipality", " नगरपालिका" },
                    { 164L, null, 29L, "Panchkhal Municipality", " नगरपालिका" },
                    { 584L, null, 31L, "Tarkeshwar Rural Municipality", "तारकेश्वर गाउँपालिका" },
                    { 585L, null, 31L, "Kispang Rural Municipality", "किस्पाङ गाउँपालिका" },
                    { 586L, null, 31L, "Myagang	 Rural Municipality", "म्यागङ गाउँपालिका" },
                    { 587L, null, 32L, "Naukunda Rural Municipality", "नौकुण्ड गाउँपालिका" },
                    { 588L, null, 32L, "Kalika Rural Municipality", "कालिका गाउँपालिका" },
                    { 589L, null, 32L, "Uttargaya Rural Municipality", "उत्तरगया गाउँपालिका" },
                    { 590L, null, 32L, "Gosaikund Rural Municipality", "गोसाईकुण्ड गाउँपालिका" },
                    { 591L, null, 32L, "Aamachodingmo Rural Municipality", "आमाछोदिङमो गाउँपालिका" },
                    { 218L, null, 33L, "Chautara Sangachokgadhi Municipality", "चौतारा साँगाचोकगढी नगरपालिका" },
                    { 219L, null, 33L, "Melamchi Municipality", "मेलम्ची नगरपालिका" },
                    { 220L, null, 33L, "Barhabise Municipality", "बाह्रविसे नगरपालिका" },
                    { 592L, null, 33L, "Indrawati Rural Municipality", "र्इन्द्रावती गाउँपालिका" },
                    { 593L, null, 33L, "Panchpokhari Thangpal Rural Municipality", "पाँचपोखरी थाङपाल गाउँपालिका" },
                    { 594L, null, 33L, "Jugal Rural Municipality", "जुगल गाउँपालिका" },
                    { 595L, null, 33L, "Balephi	 Rural Municipality", "बलेफी गाउँपालिका" },
                    { 596L, null, 33L, "Helambu Rural Municipality", "हेलम्बु गाउँपालिका" },
                    { 597L, null, 33L, "Bhotekoshi Rural Municipality", "भोटेकोशी गाउँपालिका" },
                    { 598L, null, 33L, "Sunkoshi Rural Municipality", "सुनकोशी गाउँपालिका" },
                    { 599L, null, 33L, "Lisankhu Pakhar Rural Municipality", "लिसंखु पाखर गाउँपालिका" },
                    { 583L, null, 31L, "Panchakanya Rural Municipality", "पञ्चकन्या गाउँपालिका" },
                    { 582L, null, 31L, "Suryagadhi Rural Municipality", "सुर्यगढी गाउँपालिका" },
                    { 581L, null, 31L, "Likhu Rural Municipality", "लिखु गाउँपालिका" },
                    { 580L, null, 31L, "Tadi Rural Municipality", "तादी गाउँपालिका" },
                    { 165L, null, 29L, "Namobuddha Municipality", " नगरपालिका" },
                    { 45899L, null, 29L, "Mandandeupur Municipality", " नगरपालिका" },
                    { 567L, null, 29L, "Roshi Rural Municipality", "रोशी गाउँपालिका" },
                    { 568L, null, 29L, "Temal Rural Municipality", "तेमाल गाउँपालिका" },
                    { 569L, null, 29L, "Chaunri Deurali Rural Municipality", "चौंरी देउराली गाउँपालिका" },
                    { 570L, null, 29L, "Bhumlu Rural Municipality", "भुम्लु गाउँपालिका" },
                    { 571L, null, 29L, "Mahabharat Rural Municipality", "महाभारत गाउँपालिका" },
                    { 572L, null, 29L, "Bethanchok Rural Municipality", "बेथानचोक गाउँपालिका" },
                    { 573L, null, 29L, "Khanikhola Rural Municipality", "खानीखोला गाउँपालिका" },
                    { 163L, null, 29L, "Panauti Municipality", " नगरपालिका" },
                    { 987777L, null, 30L, "Lalitpur Metropolitan", "ललितपुर महानगरपालिका" },
                    { 215L, null, 30L, "Mahalaxmi Municipality", "महालक्ष्मी नगरपालिका" },
                    { 574L, null, 30L, "Bagmati Rural Municipality", "बाग्मती गाउँपालिका" },
                    { 575L, null, 30L, "Konjyosom Rural Municipality", "कोन्ज्योसोम गाउँपालिका" },
                    { 576L, null, 30L, "Mahankal Rural Municipality", "महाङ्काल गाउँपालिका" },
                    { 216L, null, 31L, "Bidur Municipality", "विदुर नगरपालिका" },
                    { 217L, null, 31L, "Belkotgadhi Municipality", "बेलकोटगढी नगरपालिका" },
                    { 577L, null, 31L, "Kakani Rural Municipality", "ककनी गाउँपालिका" },
                    { 578L, null, 31L, "Dupcheshwar Rural Municipality", "दुप्चेश्वर गाउँपालिका" },
                    { 579L, null, 31L, "Shivapuri Rural Municipality", "शिवपुरी गाउँपालिका" },
                    { 214L, null, 30L, "Godawari Municipality", "गोदावरी नगरपालिका" },
                    { 140L, null, 24L, "Manthali Municipality", "मन्थली नगरपालिका" },
                    { 542L, null, 23L, "Ghyanglekh Rural Municipality", "घ्याङलेख गाउँपालिका" },
                    { 541L, null, 23L, "Phikkal Rural Municipality", "फिक्कल गाउँपालिका" },
                    { 107L, null, 19L, "Bagmati Municipality", "बागमती नगर पालिका" },
                    { 108L, null, 19L, "Haripur Municipality", "हरिपुर नगर पालिका" },
                    { 109L, null, 19L, "Haripurwa Municipality", "हरिपुर्वा नगर पालिका" },
                    { 506L, null, 19L, "Chandranagar Rural Municipality", "चन्द्रनगर गाउँपालिका" },
                    { 507L, null, 19L, "Bramhapuri Rural Municipality", "ब्रह्मपुरी गाउँपालिका" },
                    { 508L, null, 19L, "Ramnagar Rural Municipality", "रामनगर गाउँपालिका" },
                    { 509L, null, 19L, "Chakraghatta Rural Municipality", "चक्रघट्टा गाउँपालिका" },
                    { 510L, null, 19L, "Kaudena Rural Municipality", "कौडेना गाउँपालिका" },
                    { 511L, null, 19L, "Dhankaul Rural Municipality", "धनकौल गाउँपालिका" },
                    { 512L, null, 19L, "Bishnu Rural Municipality", "विष्णु गाउँपालिका" },
                    { 513L, null, 19L, "Basbariya Rural Municipality", "बसबरिया गाउँपालिका" },
                    { 514L, null, 19L, "Parsa Rural Municipality", "पर्सा गाउँपालिका" },
                    { 110L, null, 20L, "Kalaiya Municipality", "कलैया उप-महानगरपालिका" },
                    { 111L, null, 20L, "Jitpur Simara Municipality", "जीतपुरसिमरा उप-महानगरपालिका" },
                    { 112L, null, 20L, "Mahagadhimai Municipality", "महागढीमाई नगर पालिका" },
                    { 113L, null, 20L, "Simraungadh Municipality", "सिम्रोनगढ नगर पालिका" },
                    { 114L, null, 20L, "Kolhabi Municipality", "कोल्हवी नगर पालिका" },
                    { 116L, null, 20L, "Nijgadh Municipality", "निजगढ नगर पालिका" },
                    { 117L, null, 20L, "Pachrauta Municipality", "पचरौता नगर पालिका" },
                    { 106L, null, 19L, "Kabilasi Municipality", "कविलासी  नगर पालिका" },
                    { 105L, null, 19L, "Hariwan Municipality", "हरिवन नगर पालिका" },
                    { 104L, null, 19L, "Balara Municipality", "बलरा नगर पालिका" },
                    { 103L, null, 19L, "Malangwa Municipality", "मलङ्गवा  नगर पालिका" },
                    { 500L, null, 17L, "Dhanauji Rural Municipality", "धनौजी गाउँपालिका" },
                    { 89L, null, 18L, "Gaushala Municipality", "गौशाला नगर पालिका" },
                    { 90L, null, 18L, "Bardibas Municipality", "बर्दिबास नगर पालिका" },
                    { 91L, null, 18L, "Jaleshwar Municipality", "जलेश्वर नगर पालिका" },
                    { 92L, null, 18L, "Manara Shisawa Municipality", "मनरा शिसवा नगर पालिका" },
                    { 93L, null, 18L, "Bhangaha Municipality", "भँगाहा नगर पालिका" },
                    { 94L, null, 18L, "Balawa Municipality", "बलवा नगर पालिका" },
                    { 95L, null, 18L, "Loharpatti Municipality", "लोहरपट्टी नगर पालिका" },
                    { 96L, null, 18L, "Aurahi Municipality", "औरही नगर पालिका" },
                    { 515L, null, 20L, "Subarna Rural Municipality", "सुवर्ण गाउँपालिका" },
                    { 97L, null, 18L, "Matihani Municipality", "मटिहानी नगर पालिका" },
                    { 501L, null, 18L, "Sonama Rural Municipality", " गाउँपालिका" },
                    { 502L, null, 18L, "Pipara Rural Municipality", "पिपरा सोनमा गाउँपालिका" },
                    { 503L, null, 18L, "Samsi Rural Municipality", "साम्सी गाउँपालिका" },
                    { 504L, null, 18L, "Ekdara Rural Municipality", "एकडारा गाउँपालिका" },
                    { 505L, null, 18L, "Mahottari Rural Municipality", "महोत्तरी गाउँपालिका" },
                    { 99L, null, 19L, "Barahathwa Municipality", "बरहथवा नगर पालिका" },
                    { 100L, null, 19L, "Ishwarpur Municipality", "ईश्वरपूर नगर पालिका" },
                    { 101L, null, 19L, "Lalbandi Municipality", "लालबन्दी नगर पालिका" },
                    { 102L, null, 19L, "Godaita Municipality", "गोडैता नगर पालिका" },
                    { 98L, null, 18L, "Ramgopalpur Municipality", "रामगोपालपुर नगर पालिका" },
                    { 516L, null, 20L, "Adarsha Kotwal Rural Municipality", "आदर्श कोतवाल	 गाउँपालिका" },
                    { 517L, null, 20L, "Baragadhi Rural Municipality", "बारागढी गाउँपालिका" },
                    { 518L, null, 20L, "Pheta Rural Municipality", "फेटा गाउँपालिका" },
                    { 127L, null, 22L, "Garuda Municipality", "गरुडा नगरपालिका" },
                    { 128L, null, 22L, "Gaur Municipality", "गौर नगरपालिका" },
                    { 129L, null, 22L, "Gujara Municipality", "गुजारा नगरपालिका" },
                    { 130L, null, 22L, "Ishanath Municipality", "इशाबाथ नगरपालिका" },
                    { 131L, null, 22L, "Katahariya Municipality", "कटहरिया नगरपालिका" },
                    { 132L, null, 22L, "Madhav Narayan Municipality", "माधव नारायण नगरपालिका" },
                    { 133L, null, 22L, "Maulapur Municipality", "मौलापुर नगरपालिका" },
                    { 134L, null, 22L, "Paroha Municipality", "परोहा नगरपालिका" },
                    { 135L, null, 22L, "Phatuwa Bijayapur Municipality", "फतुवा विजयपुर नगरपालिका" },
                    { 126L, null, 22L, "Dewahi Gonahi Municipality", "देवाही गोनाही नगरपालिका" },
                    { 136L, null, 22L, "Rajdevi Municipality", "राजदेवी नगरपालिका" },
                    { 534L, null, 22L, "	Durga Bhagawati Rural Municipality", "दुर्गा भगवती गाउँपालिका" },
                    { 535L, null, 22L, "	Yamunamai Rural Municipality", "यमुनामाई गाउँपालिका" },
                    { 138L, null, 23L, "Kamalamai Municipality", "कमलामाई नगरपालिका" },
                    { 139L, null, 23L, "Dudhauli Municipality", "दुधौली नगरपालिका" },
                    { 536L, null, 23L, "Tinpatan	Rural Municipality", "तिनपाटन गाउँपालिका" },
                    { 537L, null, 23L, "Marin Rural Municipality", "मरिण गाउँपालिका" },
                    { 538L, null, 23L, "Hariharpurgadhi Rural Municipality", "हरिहरपुरगढी गाउँपालिका" },
                    { 539L, null, 23L, "Sunkoshi Rural Municipality", "सुनकोशी गाउँपालिका" },
                    { 540L, null, 23L, "Golanjor Rural Municipality", "गोलन्जोर गाउँपालिका" },
                    { 137L, null, 22L, "Rajpur Municipality", "राजपुर नगरपालिका" },
                    { 408L, null, 17L, "Aaurahi Rural Municipality", "औरही गाउँपालिका" },
                    { 125L, null, 22L, "Chandrapur Municipality", "चंद्रपुर नगरपालिका" },
                    { 123L, null, 22L, "Buadhimai Municipality", "बौधिमाई नगरपालिका" },
                    { 519L, null, 20L, "Karaiyamai Rural Municipality", "करैयामाई गाउँपालिका" },
                    { 520L, null, 20L, "Prasauni Rural Municipality", "प्रसौनी गाउँपालिका" },
                    { 521L, null, 20L, "Bishrampur Rural Municipality", "विश्रामपुर गाउँपालिका" },
                    { 522L, null, 20L, "Devtal Rural Municipality", "देवताल गाउँपालिका" },
                    { 523L, null, 20L, "Parawanipur Rural Municipality", "परवानीपुर गाउँपालिका" },
                    { 118L, null, 21L, "Birganj Municipality", "बिरगंज महानगरपालिका" },
                    { 119L, null, 21L, "Bahudarmai Municipality", "बहुदरमाई नगरपालिका" },
                    { 120L, null, 21L, "Mahagadhimai Municipality", "पर्सागढी नगर पालिका" },
                    { 121L, null, 21L, "Parsagadhi Municipality", "पर्सागढी नगर पालिका" },
                    { 124L, null, 22L, "Brindaban Municipality", "पचरौता नगरपालिका" },
                    { 122L, null, 21L, "Pokhariya Municipality", "पोखरिया नगर पालिका" },
                    { 525L, null, 21L, "Jagarnathpur Rural Municipality", "जगरनाथपुर गाउँपालिका" },
                    { 526L, null, 21L, "Chhipaharmai Rural Municipality", "छिपहरमाई गाउँपालिका" },
                    { 527L, null, 21L, "Bindabasini Rural Municipality", "बिन्दबासिनी गाउँपालिका" },
                    { 528L, null, 21L, "Paterwa Sugauli Rural Municipality", "पटेर्वा सुगौली गाउँपालिका" },
                    { 529L, null, 21L, "Jira Bhavani Rural Municipality", "जिरा भवानी गाउँपालिका" },
                    { 530L, null, 21L, "Kalikamai Rural Municipality", "कालिकामाई गाउँपालिका" },
                    { 531L, null, 21L, "Pakaha Mainpur Rural Municipality", "पकाहा मैनपुर गाउँपालिका" },
                    { 532L, null, 21L, "Thori Rural Municipality", "ठोरी गाउँपालिका" },
                    { 533L, null, 21L, "	Dhobini Rural Municipality", "धोबीनी गाउँपालिका" },
                    { 524L, null, 21L, "Sakhuwa Prasauni Rural Municipality", "सखुवा प्रसौनी गाउँपालिका" },
                    { 848L, null, 77L, "Api Himal Rural Municipality", "अपि हिमाल गाउँपालिका" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AddMoreSection_childDetailId",
                table: "AddMoreSection",
                column: "childDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_DesignationId",
                table: "AspNetUsers",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CardDetails_ApprovedBy",
                table: "CardDetails",
                column: "ApprovedBy");

            migrationBuilder.CreateIndex(
                name: "IX_CardDetails_DisabledPersonId",
                table: "CardDetails",
                column: "DisabledPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_CardDetails_GuardianId",
                table: "CardDetails",
                column: "GuardianId");

            migrationBuilder.CreateIndex(
                name: "IX_Chalanis_Vdc",
                table: "Chalanis",
                column: "Vdc");

            migrationBuilder.CreateIndex(
                name: "IX_ChildDetails_ApprovedById",
                table: "ChildDetails",
                column: "ApprovedById");

            migrationBuilder.CreateIndex(
                name: "IX_ChildDetails_Employee_Id",
                table: "ChildDetails",
                column: "Employee_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ChildDetails_PermanentVDCId",
                table: "ChildDetails",
                column: "PermanentVDCId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildDetails_TemporaryVDCId",
                table: "ChildDetails",
                column: "TemporaryVDCId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildDetails_UserId",
                table: "ChildDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildernCard_ApplicationUserId",
                table: "ChildernCard",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Dartas_Faat",
                table: "Dartas",
                column: "Faat");

            migrationBuilder.CreateIndex(
                name: "IX_Dartas_Vdc",
                table: "Dartas",
                column: "Vdc");

            migrationBuilder.CreateIndex(
                name: "IX_Districts_ProvinceId",
                table: "Districts",
                column: "ProvinceId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_Designation_Id",
                table: "Employee",
                column: "Designation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_District_Province_Vdc_Id",
                table: "Employee",
                column: "District_Province_Vdc_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_Shredi_Id",
                table: "Employee",
                column: "Shredi_Id");

            migrationBuilder.CreateIndex(
                name: "IX_MunicipalityVDCs_DistrictId",
                table: "MunicipalityVDCs",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_OldAddMoreSection_childDetailId",
                table: "OldAddMoreSection",
                column: "childDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_OldChildCard_ApplicationUserId",
                table: "OldChildCard",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_OldChildDetails_ApprovedById",
                table: "OldChildDetails",
                column: "ApprovedById");

            migrationBuilder.CreateIndex(
                name: "IX_OldChildDetails_Employee_Id",
                table: "OldChildDetails",
                column: "Employee_Id");

            migrationBuilder.CreateIndex(
                name: "IX_OldChildDetails_PermanentVDCId",
                table: "OldChildDetails",
                column: "PermanentVDCId");

            migrationBuilder.CreateIndex(
                name: "IX_OldChildDetails_TemporaryVDCId",
                table: "OldChildDetails",
                column: "TemporaryVDCId");

            migrationBuilder.CreateIndex(
                name: "IX_OldChildDetails_UserId",
                table: "OldChildDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalDetails_PermanentAddressId",
                table: "PersonalDetails",
                column: "PermanentAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalDetails_TempAddressId",
                table: "PersonalDetails",
                column: "TempAddressId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AddMoreSection");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CardDetails");

            migrationBuilder.DropTable(
                name: "Chalanis");

            migrationBuilder.DropTable(
                name: "ChildernCard");

            migrationBuilder.DropTable(
                name: "Dartas");

            migrationBuilder.DropTable(
                name: "Expirydates");

            migrationBuilder.DropTable(
                name: "OldAddMoreSection");

            migrationBuilder.DropTable(
                name: "OldChildCard");

            migrationBuilder.DropTable(
                name: "Sequence");

            migrationBuilder.DropTable(
                name: "ChildDetails");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "PersonalDetails");

            migrationBuilder.DropTable(
                name: "Fatt");

            migrationBuilder.DropTable(
                name: "OldChildDetails");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "MunicipalityVDCs");

            migrationBuilder.DropTable(
                name: "Shredi");

            migrationBuilder.DropTable(
                name: "Designation");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "Provinces");
        }
    }
}
