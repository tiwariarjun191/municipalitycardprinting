﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MunicipalityCardPrinting.Migrations
{
    public partial class hello : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "06f34af5-280c-4c18-8605-40d7ecdcc0bd");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7535c9a5-a72f-402a-8126-b8986468532a");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c6a8d805-6b0b-40e2-a06a-8430bca07108");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "35a3890c-eb70-4f4b-b7d2-71d33c56812a", "c58684c1-9127-4688-9a0a-fa44f419041c" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c58684c1-9127-4688-9a0a-fa44f419041c");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "35a3890c-eb70-4f4b-b7d2-71d33c56812a");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "7e0d63ac-8b38-4aed-9a2f-576c38e6fdeb", "d6011fcb-de58-48b4-8db8-623fca2ef321", "Admin", "Admin" },
                    { "4dab5839-bf34-451f-92a4-87dc31f8039d", "02fefc00-33ed-4bbf-84a7-118c4f1d35be", "Supervisor", "SuperVisor" },
                    { "d3c1aa93-8a77-4e71-902c-56bcceaeafb4", "71f92fd0-bc58-40fd-aa04-235a99bb46e1", "Maker", "Maker" },
                    { "d898c8d6-f6ee-4231-bc38-c4608e42fc57", "246bd512-7701-4761-87cf-c89dfa57a091", "Approver", "Approver" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "DesignationId", "FullName", "FullNameNepali", "IsDeleted", "WardNumber" },
                values: new object[] { "62a36547-5e7a-4938-9759-7169211de6a8", 0, "88f82135-8811-49df-8b7d-a7d7b77cbd65", "ApplicationUser", "Admin@admin.com", false, false, null, null, "ADMIN", "AQAAAAEAACcQAAAAELh8wnOu6Ixx4ZQfHv3Ubi/elVCFeZtzHC9M0Q2uM3Dqv0Hn3wL8HBX81Kn0akl2MA==", null, false, "67f35b36-52d4-4cc9-a1d1-f9d2b255affb", false, "admin", 2L, "Admin", "एडमिन", false, null });

            migrationBuilder.UpdateData(
                table: "Expirydates",
                keyColumn: "EXPID",
                keyValue: 1L,
                columns: new[] { "Expirydate_From", "Expirydate_To" },
                values: new object[] { new DateTime(2021, 12, 15, 8, 28, 32, 460, DateTimeKind.Local).AddTicks(8420), new DateTime(2022, 12, 15, 8, 28, 32, 463, DateTimeKind.Local).AddTicks(2153) });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "62a36547-5e7a-4938-9759-7169211de6a8", "7e0d63ac-8b38-4aed-9a2f-576c38e6fdeb" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4dab5839-bf34-451f-92a4-87dc31f8039d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d3c1aa93-8a77-4e71-902c-56bcceaeafb4");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d898c8d6-f6ee-4231-bc38-c4608e42fc57");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "62a36547-5e7a-4938-9759-7169211de6a8", "7e0d63ac-8b38-4aed-9a2f-576c38e6fdeb" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7e0d63ac-8b38-4aed-9a2f-576c38e6fdeb");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "62a36547-5e7a-4938-9759-7169211de6a8");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "c58684c1-9127-4688-9a0a-fa44f419041c", "626525df-0a05-4565-a6d5-12e3416e6830", "Admin", "Admin" },
                    { "c6a8d805-6b0b-40e2-a06a-8430bca07108", "7d7635dc-92fa-4faa-80e8-48de315459f9", "Supervisor", "SuperVisor" },
                    { "7535c9a5-a72f-402a-8126-b8986468532a", "2572fbd2-edf2-4fe0-bb1d-9eb67a75e1f6", "Maker", "Maker" },
                    { "06f34af5-280c-4c18-8605-40d7ecdcc0bd", "9e0896d4-39f2-4222-88ff-b693ffe07588", "Approver", "Approver" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "DesignationId", "FullName", "FullNameNepali", "IsDeleted", "WardNumber" },
                values: new object[] { "35a3890c-eb70-4f4b-b7d2-71d33c56812a", 0, "8cd7053f-fe27-4759-9175-4aba058ab8d1", "ApplicationUser", "Admin@admin.com", false, false, null, null, "ADMIN", "AQAAAAEAACcQAAAAEE6HJY8HyqieKerkwWzdaZ1MlVVc3Dh2A2ZeJ4z/XbvpRBfExqtGMe4Z/QgtJHcbPg==", null, false, "b50d32bc-6e65-4b27-90b7-762258bb4b8b", false, "admin", 2L, "Admin", "एडमिन", false, null });

            migrationBuilder.UpdateData(
                table: "Expirydates",
                keyColumn: "EXPID",
                keyValue: 1L,
                columns: new[] { "Expirydate_From", "Expirydate_To" },
                values: new object[] { new DateTime(2021, 12, 14, 22, 15, 20, 214, DateTimeKind.Local).AddTicks(9990), new DateTime(2022, 12, 14, 22, 15, 20, 215, DateTimeKind.Local).AddTicks(8027) });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "35a3890c-eb70-4f4b-b7d2-71d33c56812a", "c58684c1-9127-4688-9a0a-fa44f419041c" });
        }
    }
}
