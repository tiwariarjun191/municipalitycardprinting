﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Exceptions
{
    public class CustomException : Exception
    {
        public CustomException(string Message) : base(Message)
        {

        }
    }

    public class NameAlreadyExistException : CustomException
    {
        public NameAlreadyExistException(string Message = "Name Already Exist") : base(Message)
        {

        }
    }

    public class ItemNotFoundException : CustomException
    {
        public ItemNotFoundException(string Message = "Item Not found") : base(Message)
        {

        }
    }
}
