﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Implementation
{
    public class PersonalDetailService:IPersonalDetailService
    {
        private readonly IBaseRepository<PersonalDetail> _personalDetailRepo;

        public  PersonalDetailService(IBaseRepository<PersonalDetail> personalDetailRepo)
        {
            _personalDetailRepo = personalDetailRepo;
        }

        public long AddPersonalDetail(PersonalDetailDTO entity)
        {
            try
            {
                var dbEntity = MapDTOToDbEntity(entity, new PersonalDetail());
                _personalDetailRepo.insert(dbEntity);
                return dbEntity.Id;
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void UpdatePersonalDetail(PersonalDetailDTO entity)
        {
            try
            {
               
                var ActualDbEntity = _personalDetailRepo.getById(entity.Id);
                var dbEntity = MapDTOToDbEntity(entity, ActualDbEntity);
                _personalDetailRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void DeletePersonalDetail(int Id)
        {
            try
            {
                var dbEntity = _personalDetailRepo.getById(Id);
                if (dbEntity == null)
                    throw new ItemNotFoundException();
                dbEntity.IsDeleted = true;
                _personalDetailRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public PersonalDetail FindById(long Id)
        {
            try
            {
                var objectToBeReturned = _personalDetailRepo.getById(Id);
                return objectToBeReturned;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
         PersonalDetail MapDTOToDbEntity(PersonalDetailDTO dto, PersonalDetail dbEntity)
        {
            dbEntity.Id = dto.Id;
            dbEntity.BloodGroup = dto.BloodGroup;
            dbEntity.CitizenshipNumber = dto.CitizenshipNumber;
            dbEntity.CitizenshipNumberNepali = dto.CitizenshipNumberNepali;
            dbEntity.DOB = dto.DOB;
            dbEntity.DOBNepali = dto.DOBNepali;
            dbEntity.FullName = dto.FullName;
            dbEntity.FullNameNepali = dto.FullNameNepali;
            dbEntity.Gender = dto.Gender;
            dbEntity.IsDeleted = dto.IsDeleted;
            dbEntity.MarritalStatus = dto.MarritalStatus;
            dbEntity.PermanentAddressId = dto.PermanentAddressId;
            dbEntity.PermanentTole = dto.PermanentTole;
            dbEntity.SpouseName = dto.SpouseName;
            dbEntity.ContactNumber = dto.ContactNumber;
            dbEntity.PermanentWardNumber = dto.PermanentWardNumber;
            dbEntity.TempAddressId = dto.TempAddressId;
            dbEntity.TempTole = dto.TempTole;
            dbEntity.TempWardNumber = dto.TempWardNumber;

            return dbEntity;
        }
        PersonalDetail MapDbEntityToDTO(PersonalDetailDTO dto, PersonalDetail dbEntity)
        {
            dto.Id = dbEntity.Id;
            dto.BloodGroup = dbEntity.BloodGroup;
            dto.CitizenshipNumber = dbEntity.CitizenshipNumber;
            dto.CitizenshipNumberNepali = dbEntity.CitizenshipNumberNepali;
            if(dbEntity.DOB!=null)
                dto.DOB = dbEntity.DOB.Value;

            dto.DOBNepali = dbEntity.DOBNepali;
            dto.FullName = dbEntity.FullName;
            dto.FullNameNepali = dbEntity.FullNameNepali;
            dto.Gender = dbEntity.Gender;
            dto.IsDeleted = dbEntity.IsDeleted;
            dto.MarritalStatus = dbEntity.MarritalStatus;
            dto.PermanentAddressId = dbEntity.PermanentAddressId.GetValueOrDefault();
            dto.PermanentTole = dbEntity.PermanentTole;
            dto.PermanentWardNumber = dbEntity.PermanentWardNumber;
            dto.TempAddressId = dbEntity.TempAddressId.GetValueOrDefault();
            dto.TempTole = dbEntity.TempTole;
            dto.TempWardNumber = dbEntity.TempWardNumber;

            return dbEntity;
        }
    }
}
