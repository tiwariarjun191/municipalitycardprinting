﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Implementation
{
    public class DesignationService: IDesignationService
    {
        private readonly IBaseRepository<Designation> _designationRepository;
        public DesignationService(IBaseRepository<Designation> designationRepository)
        {
            this._designationRepository = designationRepository;
        }
        public void AddDesignation(DesignationDto fattDto)
        {
            try
            {
                var data = MapDtoToEntity(fattDto, new Designation());
                _designationRepository.insert(data);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public void UpdateDesignation(DesignationDto fattDto)
        {
            try
            {
                var entity = _designationRepository.getById(fattDto.Id);
                var data = MapDtoToEntity(fattDto, entity);
                _designationRepository.update(data);
            }
            catch (Exception)
            {

                throw;
            }

        }


        //public void DeleteFatt(FattDto fattDto)
        //{
        //    try
        //    {

        //        var entity = _fattRepository.getById(fattDto.id);
        //        if (entity == null)
        //            throw new ItemNotFoundException();
        //        _fattRepository.delete(entity);

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        //public FattDto GetByFattId(long id)
        //{
        //    var data = _fattRepository.getById(id);
        //    var fattdto = new FattDto();
        //    MapEntityToDto(fattdto, data);
        //    return fattdto;
        //}

        public Designation MapDtoToEntity(DesignationDto dto, Designation entity)
        {
            entity.Id = dto.Id;
            entity.Name = dto.Name;
            entity.NameNepali = dto.NameNepali;
            return entity;
        }

        public DesignationDto MapEntityToDto(DesignationDto dto, Designation entity)
        {
            dto.Id = entity.Id;
            dto.Name = entity.Name;
            dto.NameNepali = entity.NameNepali;
            return dto;
        }

        //public List<FattDto> getAllFattDetails()
        //{
        //    var data = _fattRepository.getQueryable().ToList();
        //    var Details = new List<FattDto>();
        //    foreach (var Item in data)
        //    {
        //        var mapdata = MapEntityToDto(new FattDto(), Item);
        //        Details.Add(mapdata);
        //    }

        //    return Details;

        


    }
}
