﻿using Business.Services.Interface;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Services.Implementation
{
     public class SequenceService:ISequenceService
    {
        private readonly IBaseRepository<Sequence> SequenceRepository;

        public SequenceService(IBaseRepository<Sequence> SequenceRepository)
        {
            this.SequenceRepository = SequenceRepository;
        }
      
        public long GetCurrentSequence(string Type)
        {
            var sequence = SequenceRepository.getQueryable().Where(a => a.Type.ToLower() == Type.ToLower()).SingleOrDefault();
            if (sequence == null)
            {
                Sequence sequence1 = new Sequence();
                sequence1.Type = Type;
                sequence1.Value = 1;
                this.SequenceRepository.insert(sequence1);
                return sequence1.Value;
            }
            sequence.Value++;
            SequenceRepository.update(sequence);
            return sequence.Value;

        }

    }
}
