﻿using Business.Services.Interface;
using DTO;
using DTO.Children;
using DTO.Enums;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Identity;
using MunicipalityCardPrinting.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Statics;
using static DTO.Children.OldChildDetailDto;
using static Infrastructure.Database.Models.ChildDetail;
using static Infrastructure.Database.Models.OldChildDetails;

namespace Business.Services.Implementation
{
   public class OldChildCardService: IOldChildCardService
    {

        private readonly IPersonalDetailService _personalDetailService;
        private readonly IBaseRepository<CardDetail> _cardDetailRepo;
        private readonly IBaseRepository<OldChildDetails> _childDetailRepo;
        private readonly IDateConverter _dateConverter;
        private readonly IFileHelper1 _fileHelper1;
        private readonly IBaseRepository<District> _districtRepo;
        private readonly IBaseRepository<OldAddMoreSection> _addmoreSection;
        private readonly IBaseRepository<Employee> _employeeRepo;
        private readonly IBaseRepository<Province> _provienceRepo;
        private readonly IBaseRepository<MunicipalityVDC> _vdcRepo;
        private readonly IBaseRepository<Darta> _dartaRepo;
        private readonly IBaseRepository<Chalani> _chalaniRepo;
        private readonly IBaseRepository<Designation> _designationRepo;
        private readonly ISequenceService _sequenceService;
        private readonly UserManager<ApplicationUser> _userManager;
        public OldChildCardService(IPersonalDetailService personalDetailService, IBaseRepository<CardDetail> cardDetailRepo, IDateConverter dateCoverter, IBaseRepository<OldChildDetails> childDetailRepo, ISequenceService sequenceService, IBaseRepository<MunicipalityVDC> vdcRepo, IBaseRepository<Darta> dartaRepo, IBaseRepository<Chalani> chalaniRepo, IBaseRepository<Designation> designationRepo, UserManager<ApplicationUser> userManager, IFileHelper1 fileHelper1, IBaseRepository<OldAddMoreSection> addmoreSection, IBaseRepository<Employee> employeerepo)
        {
            _vdcRepo = vdcRepo;
            _personalDetailService = personalDetailService;
            _cardDetailRepo = cardDetailRepo;
            _dateConverter = dateCoverter;
            _childDetailRepo = childDetailRepo;
            _sequenceService = sequenceService;
            _dartaRepo = dartaRepo;
            _chalaniRepo = chalaniRepo;
            _designationRepo = designationRepo;
            _userManager = userManager;
            _fileHelper1 = fileHelper1;
            _addmoreSection = addmoreSection;
            _employeeRepo = employeerepo;
        }



        private void SaveAddMoreSection(OldChildDetailDto dto, long id)
        {
            List<OldAddMoreSection> addMoreSections = new List<OldAddMoreSection>();

            foreach (var item in dto.AddMoreSections)
            {
                if (item.TextArea != null || item.File != null)
                {
                    OldAddMoreSection addMoreSection = new OldAddMoreSection();
                    addMoreSection.childDetailId = id;
                    addMoreSection.TextArea = item.TextArea;
                    if (item.fileinput != null)
                    {
                        addMoreSection.File = _fileHelper1.saveImageAndGetFileName(item.fileinput, item.File);
                    }

                    addMoreSections.Add(addMoreSection);
                }

            }
            _addmoreSection.insertRange(addMoreSections);
        }

        private void DeleteAddMoreSection(long id)
        {
            var data = _addmoreSection.getQueryable().Where(a => a.childDetailId == id).ToList();
            _addmoreSection.deleteRange(data);
        }
        public long AddUpdateOldChildCardDetail(OldChildDetailDto entity)
        {
            try
            {
                OldChildDetails dbEntity = new OldChildDetails();
                if (entity.Id == 0)
                {
                    SetChildDetail(dbEntity, entity);
                    dbEntity.Photo = entity.Photo;
                    dbEntity.FatherCitizen = entity.FatherCitizen;
                    dbEntity.MotherCitizen = entity.MotherCitizen;
                    dbEntity.BirthCertificate = entity.BirthCertificate;
                    dbEntity.other = entity.other;
                   
                    _childDetailRepo.insert(dbEntity);
                    SaveAddMoreSection(entity, dbEntity.Id);
                }
                else
                {
                    dbEntity = _childDetailRepo.getById(entity.Id);

                    SetChildDetail(dbEntity, entity);

                    if (entity.Photo != null)
                    {
                        dbEntity.Photo = entity.Photo;
                    }

                    if (entity.FatherCitizen != null)
                    {
                        dbEntity.FatherCitizen = entity.FatherCitizen;
                    }

                    if (entity.MotherCitizen != null)
                    {
                        dbEntity.MotherCitizen = entity.MotherCitizen;
                    }
                    if (entity.BirthCertificate != null)
                    {
                        dbEntity.BirthCertificate = entity.BirthCertificate;
                    }
                    if (entity.other != null)
                    {
                        dbEntity.other = entity.other;
                    }
                    _childDetailRepo.update(dbEntity);
                    DeleteAddMoreSection(dbEntity.Id);
                    SaveAddMoreSection(entity, dbEntity.Id);
                }
                return dbEntity.Id;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool UpdateOldChildCardNumber(long CardId)
        {
            try
            {
                var card = _cardDetailRepo.getById(CardId);
                if (card == null)
                    return false;

                _cardDetailRepo.update(card);
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public OldChildDetailDto GetOldChildCardDtoById(long id)
        {
            var dbEntity = _childDetailRepo.getById(id);
            OldChildDetailDto dto = new OldChildDetailDto();
            SetChildDetailDto(dbEntity, dto);
            return dto;


        }


        public OldChildCard GetOldChildernCardById(long Id)
        {
            try
            {
                var cardDetail = _childDetailRepo.getQueryable().Where(a => a.Id == Id).FirstOrDefault();
                OldChildCard model = new OldChildCard();
                model.Id = cardDetail.Id;

                if (string.IsNullOrEmpty(cardDetail.CardNumber))
                {
                    model.CardNumber = $"{DateTime.Now.Year}{cardDetail.Id}";
                }
                else
                {
                    model.CardNumber = cardDetail.CardNumber;
                }


                model.NameEnglish = cardDetail.NameEnglish;

                model.TemporaryDistrictId = cardDetail.TempAddressInfo.DistrictInfo.DistrictNameNepali;
                model.TemporaryProvinceId = cardDetail.TempAddressInfo.DistrictInfo.ProvinceInfo.ProvinceName;
                model.TemporaryVDCId = cardDetail.TempAddressInfo.VDCName;
                model.TemporaryWard = cardDetail.TemporaryWard;
                model.PermanentDistrictId = cardDetail.PermanentAddressInfo.DistrictInfo.DistrictNameNepali;
                model.PermanentProvinceId = cardDetail.PermanentAddressInfo.DistrictInfo.ProvinceInfo.ProvinceName;
                model.PermanentVDCId = cardDetail.PermanentAddressInfo.VDCName;
                model.PermanentWard = cardDetail.PermanentWard;
                model.FatherNameEnglish = cardDetail.FatherNameEnglish;
                model.FatherAddressEnglish = cardDetail.FatherAddressEnglish;
                model.MotherNameEnglish = cardDetail.MotherNameEnglish;
                model.MotherAddressEnglish = cardDetail.MotherAddressEnglish;
                var gendersEnum = (Gender)cardDetail.Gender;
                model.Gender = gendersEnum.ToString();
                model.Photo = cardDetail.Photo;
                model.FatherMotherCitizenShip = cardDetail.FatherMotherCitizenship;
                model.PrintCountNepali = cardDetail.PrintCountNepali;
                model.PrintDate = _dateConverter.ToBS(DateTime.Now);
                model.ApprovedBy = cardDetail.Approved_By_Details.Employee_Name_English;

                model.PositionEnglish = cardDetail.UserInfo.DesignationInfo.Name;





                //Nepali
                model.CardNumberNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.CardNumber);
                model.PrintCountnpl = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.PrintCountNepali.ToString());
                model.NameNepali = cardDetail.NameNepali;

                model.TemporaryNepaliDistrictId = cardDetail.TempAddressInfo.DistrictInfo.DistrictName;
                model.TemporaryNepaliProvinceId = cardDetail.TempAddressInfo.DistrictInfo.ProvinceInfo.ProvinceNameNepali;
                model.TemporaryNepaliVDCId = cardDetail.TempAddressInfo.VDCNameNepali;
                model.TemporaryWard = cardDetail.TemporaryWard;
                model.PermanentNepaliDistrictId = cardDetail.PermanentAddressInfo.DistrictInfo.DistrictName;
                model.PermanentNepaliProvinceId = cardDetail.PermanentAddressInfo.DistrictInfo.ProvinceInfo.ProvinceNameNepali;
                model.PermanentNepaliVDCId = cardDetail.PermanentAddressInfo.VDCNameNepali;
                model.PermanentWard = cardDetail.PermanentWard;
                model.FatherNameNepali = cardDetail.FatherNameNepali;
                model.FatherAddressNepali = cardDetail.FatherAddressNepali;
                model.MotherNameNepali = cardDetail.MotherNameNepali;
                model.MotherAddressNepali = cardDetail.MotherAddressNepali;
                model.GenderNepali = GenderEnumHandler.ConvertToNepali(gendersEnum);
                model.Photo = cardDetail.Photo;
                model.PrintDate = _dateConverter.ToBS(DateTime.Now);
                model.ApprovedByNepali = cardDetail.Approved_By_Details.Employee_Name_Nepali;
                model.FatherMotherCitizenShipNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.FatherMotherCitizenShip);
                model.PositionNepali = cardDetail.UserInfo?.DesignationInfo?.NameNepali;

                return model;

            }
            catch (Exception ex)
            {

                throw;
            }
        }



        public List<OldChildDetailDto> GetAllOldChildernCardDetail()
        {
            var carddetail = _childDetailRepo.getQueryable().ToList();
            var detail = new List<OldChildDetailDto>();
            foreach (var item in carddetail)
            {
                var mapdata = SetChildDetailDto(item, new OldChildDetailDto());
                detail.Add(mapdata);
            }

            return detail;
        }

        public void IncreaseOldChildCardPrintCount(long Id)
        {
            try
            {
                var card = _childDetailRepo.getById(Id);
                if (!card.IsCardPrinted)
                {
                    card.EntryDate = DateTime.Now;
                }
                card.LastPrintDate = DateTime.Now;
                card.IsCardPrinted = true;
                card.PrintCountNepali++;
                _childDetailRepo.update(card);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        void SetChildDetail(OldChildDetails entity, OldChildDetailDto childDto)
        {
            try
            {
                entity.Id = childDto.Id;
                entity.CardNumber = childDto.CardNumber;
                entity.Gender = childDto.Gender;
                entity.ApprovedBy = childDto.ApprovedBy;
                entity.ApprovedByNepali = childDto.ApprovedByNepali;
                entity.CardNumberNepali = childDto.CardNumberNepali;
                entity.EntryDate = childDto.EntryDate;
                entity.FatherAddressEnglish = childDto.FatherAddressEnglish;
                entity.FatherAddressNepali = childDto.FatherAddressNepali;
                entity.FatherNameEnglish = childDto.FatherNameEnglish;
                entity.FatherNameNepali = childDto.FatherNameNepali;
                entity.IsCardPrinted = childDto.IsCardPrinted;
                entity.LastPrintDate = childDto.LastPrintDate;
                entity.ModifiedDate = childDto.ModifiedDate;
                entity.MotherAddressEnglish = childDto.MotherAddressEnglish;
                entity.MotherAddressNepali = childDto.MotherAddressNepali;
                entity.MotherNameEnglish = childDto.MotherNameEnglish;
                entity.MotherNameNepali = childDto.MotherNameNepali;
                entity.NameEnglish = childDto.NameEnglish;
                entity.NameNepali = childDto.NameNepali;

                entity.UserId = childDto.UserId;
                entity.FatherMotherCitizenship = childDto.FatherMotherCitizenship;
                // entity.PermanentProvinceId = childDto.PermanentProvinceId;
                entity.PermanentVDCId = childDto.PermanentVDCId;
                entity.PermanentWard = childDto.PermanentWard;

                entity.PrintCountNepali = childDto.PrintCountNepali;
                entity.PrintDate = childDto.PrintDate;
                entity.TemporaryProvinceId = childDto.TemporaryProvinceId;
                entity.TemporaryVDCId = childDto.TemporaryVDCId;
                entity.TemporaryWard = childDto.TemporaryWard;
                entity.Employee_Id = childDto.Employee_Id;
                entity.ApprovedById = childDto.ApprovedById;
                entity.Employee_Address = childDto.Employee_Address;
                entity.Employee = _employeeRepo.getById(childDto.Employee_Id);

                if (childDto.PermanentVDCId.HasValue)
                {
                    entity.PermanentAddressInfo = _vdcRepo.getById(Convert.ToInt64(childDto.PermanentVDCId));
                }

                //if (childDto.TemporaryVDCId.HasValue)
                //{
                //    entity.TempAddressInfo = _vdcRepo.getById(Convert.ToInt64(childDto.TemporaryVDCId));
                //}
            }
            catch (Exception)
            {

                throw;
            }

        }
        OldChildDetailDto SetChildDetailDto(OldChildDetails entity, OldChildDetailDto childDto)
        {
            try
            {
                childDto.Id = entity.Id;
                childDto.CardNumber = entity.CardNumber;
                childDto.Gender = entity.Gender;
                childDto.ApprovedBy = entity.ApprovedBy;
                childDto.ApprovedByNepali = entity.ApprovedByNepali;

                childDto.CardNumber = entity.CardNumber;
                childDto.CardNumberNepali = entity.CardNumberNepali;
                childDto.EntryDate = entity.EntryDate;
                childDto.FatherAddressEnglish = entity.FatherAddressEnglish;
                childDto.FatherAddressNepali = entity.FatherAddressNepali;
                childDto.FatherNameEnglish = entity.FatherNameEnglish;
                childDto.FatherNameNepali = entity.FatherNameNepali;
                childDto.IsCardPrinted = entity.IsCardPrinted;
                childDto.LastPrintDate = entity.LastPrintDate;
                childDto.ModifiedDate = entity.ModifiedDate;
                childDto.MotherAddressEnglish = entity.MotherAddressEnglish;
                childDto.MotherAddressNepali = entity.MotherAddressNepali;
                childDto.MotherNameEnglish = entity.MotherNameEnglish;
                childDto.MotherNameNepali = entity.MotherNameNepali;
                childDto.NameEnglish = entity.NameEnglish;
                childDto.NameNepali = entity.NameNepali;
                childDto.FatherMotherCitizenship = entity.FatherMotherCitizenship;
                childDto.Employee_Id = entity.Employee_Id;
                childDto.Employee_Address = entity.Employee_Address;

                //childDto.PermanentProvinceId = entity.PermanentProvinceId;
                childDto.PermanentVDCId = entity.PermanentVDCId;
                childDto.PermanentWard = entity.PermanentWard;
                childDto.Photo = entity.Photo;
                childDto.FatherCitizen = entity.FatherCitizen;
                childDto.MotherCitizen = entity.MotherCitizen;
                childDto.BirthCertificate = entity.BirthCertificate;
                childDto.other = entity.other;
                childDto.PrintCountNepali = entity.PrintCountNepali;
                childDto.PrintDate = entity.PrintDate;
                childDto.TemporaryProvinceId = entity.TemporaryProvinceId;
                childDto.TemporaryVDCId = entity.TemporaryVDCId;
                childDto.TemporaryWard = entity.TemporaryWard;
                childDto.ApprovedById = entity.ApprovedById;




                foreach (var item in entity.AddMoreSections)
                {
                    childDto.AddMoreSections.Add(new OldAddMoreSectionDto()
                    {
                        File = item.File,
                        TextArea = item.TextArea
                    });
                }

                if (entity.PermanentAddressInfo != null)
                {
                    childDto.PermanentDistrictId = entity.PermanentAddressInfo.DistrictId;
                    childDto.PermanentProvinceId = entity.PermanentAddressInfo.DistrictInfo.ProvinceId;

                }

                if (entity.TempAddressInfo != null)
                {
                    childDto.TemporaryDistrictId = entity.TempAddressInfo.DistrictId;
                    childDto.PermanentProvinceId = entity.TempAddressInfo.DistrictInfo.ProvinceId;
                }

                return childDto;

            }
            catch (Exception)
            {

                throw;
            }

        }







    }
}
