﻿using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using MunicipalityCardPrinting.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Business.Services.Implementation
{
    public class DartaChalaniService : IDartaChalaniService
    {

        private readonly IBaseRepository<Darta> _dartaRepo;
        private readonly IBaseRepository<Chalani> _chalaniRepo;
        private readonly ISequenceService _sequenceService;
        private readonly IDateConverter _dateConverter;

        public DartaChalaniService(IBaseRepository<Darta> dartaRepo, IBaseRepository<Chalani> chalaniRepo, ISequenceService sequenceService, IDateConverter dateConverter)
        {
            _dateConverter = dateConverter;
            _sequenceService = sequenceService;
            _dartaRepo = dartaRepo;
            _chalaniRepo = chalaniRepo;
        }
        public bool AddUpdateChalani(ChalaniDto entity)
        {


            try
            {
              
                    if (entity.Id == 0)
                    {
                        Chalani dbEntity = new Chalani();
                        SetChalaniDetail(dbEntity, entity);
                        dbEntity.Photo = entity.Photo;
                        var ChalaniNumber = _sequenceService.GetCurrentSequence("chalani");
                        var fiscalyear = _dateConverter.getFiscalYear(DateTime.Now);
                        var data = fiscalyear.ToString().Substring(1);

                        var newdata = fiscalyear + 1;
                        var newfiscal = newdata.ToString().Substring(1);
                        // var paddedChalaniNumber = ChalaniNumber.ToString("D5");
                        var ChalaniNumberandFiscalYear = $"{data}/{newfiscal}/{ChalaniNumber:0}";
                        dbEntity.ChalaniNo = ChalaniNumberandFiscalYear;
                        _chalaniRepo.insert(dbEntity);
                    }
                    else
                    {
                        Chalani dbEntity = _chalaniRepo.getById(entity.Id);
                        SetChalaniDetail(dbEntity, entity);
                        if (entity.Photo != null)
                        {
                            dbEntity.Photo = entity.Photo;
                        }
                        _chalaniRepo.update(dbEntity);
                    }
                   
                    return true;
                
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DartaDto GetDartaDtoById(long id)
        {
            var dbEntity = _dartaRepo.getById(id);
            DartaDto dto = new DartaDto();
            var data = SetDartaDetailFromEntity(dbEntity, dto);
            return data;


        }

        public void DeleteDarta(long id)
        {
            var entity = _dartaRepo.getById(id);
            _dartaRepo.delete(entity);
        }

        public void DeleteChalani(long id)
        {
            var entity = _chalaniRepo.getById(id);
            _chalaniRepo.delete(entity);
        }

        public ChalaniDto GetChalaniDtoById(long id)
        {
            var dbEntity = _chalaniRepo.getById(id);
            ChalaniDto dto = new ChalaniDto();
            var data = SetChalaniDetailFromEntity(dbEntity, dto);
            return data;


        }
        public bool AddUpdateDarta(DartaDto entity)
        {

            try
            {
                if (entity.Id == 0)
                {
                    Darta dbEntity = new Darta();
                    dbEntity.Photo = entity.Photo;
                    var DartaNumber = _sequenceService.GetCurrentSequence("darta");
                    var fiscalyear = _dateConverter.getFiscalYear(DateTime.Now);

                    var data = fiscalyear.ToString().Substring(1);

                    var newdata = fiscalyear + 1;
                    var newfiscal = newdata.ToString().Substring(1);

                    var DartaNumberandFiscalYear = $"{data}/{newfiscal}/{DartaNumber:0}";
                    SetDartaDetail(dbEntity, entity);
                    dbEntity.DartaNo = DartaNumberandFiscalYear;
                    _dartaRepo.insert(dbEntity);
                }
                else
                {
                    Darta dbEntity = _dartaRepo.getById(entity.Id);
                    SetDartaDetail(dbEntity, entity);
                    if (entity.Photo != null)
                    {
                        dbEntity.Photo = entity.Photo;
                    }
                    _dartaRepo.update(dbEntity);
                }
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        void SetDartaDetail(Darta entity, DartaDto dartaDto)
        {
            try
            {
                entity.Id = dartaDto.Id;
                entity.CompanyAddress = dartaDto.CompanyAddress;
                entity.CompanyName = dartaDto.CompanyName;
                entity.Dartadate = dartaDto.Dartadate;
                var englishdate = _dateConverter.ToAD(dartaDto.Dartadate);
                entity.DartadateEnglish = englishdate;
                entity.Name = dartaDto.Name;
                entity.NameNepali = dartaDto.NameNepali;
                //entity.Photo = dartaDto.Photo;

                entity.ReceivedBy = dartaDto.ReceivedBy;
                entity.ReferenceNo = dartaDto.ReferenceNo;
                entity.Subject = dartaDto.Subject;
                entity.Ward = dartaDto.Ward;
                entity.AddressId = dartaDto.AddressId;
                entity.Faat = dartaDto.Faat;
                entity.Vdc = dartaDto.VDCId;
                entity.Remarks = dartaDto.Remarks;
                entity.EnglishDateofReceivedLetter = dartaDto.EnglishDateofReceivedLetter;
                entity.DateofReceivedLetter = dartaDto.DateofReceivedLetter;

            }
            catch (Exception)
            {

                throw;
            }

        }

        DartaDto SetDartaDetailFromEntity(Darta entity, DartaDto dartaDto)
        {
            try
            {
                dartaDto.Id = entity.Id;
                dartaDto.CompanyAddress = entity.CompanyAddress;
                dartaDto.CompanyName = entity.CompanyName;
                dartaDto.Dartadate = entity.Dartadate;
                dartaDto.DartadateEnglish = entity.DartadateEnglish;
                dartaDto.Name = entity.Name;
                dartaDto.NameNepali = entity.NameNepali;
                dartaDto.Photo = entity.Photo;
                dartaDto.ReceivedBy = entity.ReceivedBy;
                dartaDto.ReferenceNo = entity.ReferenceNo;
                dartaDto.Subject = entity.Subject;
                dartaDto.Ward = entity.Ward;
                dartaDto.AddressId = entity.AddressId;
                dartaDto.DistrictId = entity.AdressInfo.DistrictId;

                dartaDto.Faat = entity.Faat;
                dartaDto.VDCId = entity.Vdc;
                dartaDto.Remarks = entity.Remarks;
                dartaDto.EnglishDateofReceivedLetter = entity.EnglishDateofReceivedLetter;
                dartaDto.DateofReceivedLetter = entity.DateofReceivedLetter;


                return dartaDto;

            }

            catch (Exception)
            {

                throw;
            }
        }


        void SetChalaniDetail(Chalani entity, ChalaniDto chalaniDto)
        {
            try
            {
                entity.Id = chalaniDto.Id;
                //entity.Chalanidate = chalaniDto.Chalanidate;
                entity.ChalanidateNepali = chalaniDto.ChalanidateNepali;
                var englishdate = _dateConverter.ToAD(chalaniDto.ChalanidateNepali);
                entity.Chalanidate = englishdate;
                entity.CompanyAddress = chalaniDto.CompanyAddress;
                entity.CompanyName = chalaniDto.CompanyName;
                entity.Name = chalaniDto.Name;
                entity.NameNepali = chalaniDto.NameNepali;
                //entity.Photo = chalaniDto.Photo;
                entity.ReceivedBy = chalaniDto.ReceivedBy;
                entity.ReferenceNo = chalaniDto.ReferenceNo;
                entity.Subject = chalaniDto.Subject;
                entity.AddressId = chalaniDto.AddressId;
                entity.Ward = chalaniDto.Ward;
                entity.Vdc = chalaniDto.VDCId;



                entity.Remarks = chalaniDto.Remarks;
                entity.EnglishDateofReceivedLetter = chalaniDto.EnglishDateofReceivedLetter;
                entity.DateofReceivedLetter = chalaniDto.DateofReceivedLetter;
            }
            catch (Exception)
            {

                throw;
            }

        }


        public ChalaniDto SetChalaniDetailFromEntity(Chalani entity, ChalaniDto chalaniDto)
        {
            try
            {
                chalaniDto.Id = entity.Id;
                chalaniDto.Chalanidate = entity.Chalanidate;
                chalaniDto.ChalanidateNepali = entity.ChalanidateNepali;
                chalaniDto.CompanyAddress = entity.CompanyAddress;
                chalaniDto.CompanyName = entity.CompanyName;
                chalaniDto.Name = entity.Name;
                chalaniDto.NameNepali = entity.NameNepali;
                chalaniDto.Photo = entity.Photo;
                chalaniDto.ReceivedBy = entity.ReceivedBy;
                chalaniDto.ReferenceNo = entity.ReferenceNo;
                chalaniDto.Subject = entity.Subject;
                chalaniDto.Ward = entity.Ward;
                chalaniDto.VDCId = entity.Vdc;
                chalaniDto.DistrictId = entity.AdressInfo.DistrictId;
                chalaniDto.AddressId = entity.AddressId;
                chalaniDto.Remarks = entity.Remarks;
                chalaniDto.EnglishDateofReceivedLetter = entity.EnglishDateofReceivedLetter;
                chalaniDto.DateofReceivedLetter = entity.DateofReceivedLetter;

                return chalaniDto;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<Darta> GetDartaDetailsOfSevenDays()
        {
            var dataofSevendays = DateTime.Now.AddDays(-7);

            var detail = _dartaRepo.getQueryable().Where(a => a.DartadateEnglish > dataofSevendays).ToList();


            return detail;
        }


        public List<Chalani> GetChalaniDetailsOfSevenDays()
        {
            var dataofSevendays = DateTime.Now.AddDays(-7);

            var detail = _chalaniRepo.getQueryable().Where(a => a.Chalanidate > dataofSevendays).ToList();


            return detail;
        }

    }
}
