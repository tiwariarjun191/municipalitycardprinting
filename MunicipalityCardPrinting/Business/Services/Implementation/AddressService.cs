﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Services.Implementation
{
    public class AddressService:IAddressService
    {
        private readonly IBaseRepository<Province> _provinceRepo;
        private readonly IBaseRepository<District> _districtRepo;
        private readonly IBaseRepository<MunicipalityVDC> _vdcRepo;
        private readonly IBaseRepository<Fatt> _fattRepo;

        public AddressService(IBaseRepository<Province> provinceRepo, IBaseRepository<District> districtRepo, IBaseRepository<MunicipalityVDC> vdcRepo, IBaseRepository<Fatt> fattRepo)
        {
            _provinceRepo = provinceRepo;
            _districtRepo = districtRepo;
            _vdcRepo = vdcRepo;
            _fattRepo = fattRepo;
        }

       
        public void AddProvince(ProvinceDTO entity)
        {
            try
            {
                if (ProvinceNameAlreadyExist(entity.ProvinceName, entity.Id))
                    throw new NameAlreadyExistException();
                var dbEntity = MapProvinceDTOToDbEntity(entity, new Province());
                _provinceRepo.insert(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void UpdateProvince(ProvinceDTO entity)
        {
            try
            {
                if (ProvinceNameAlreadyExist(entity.ProvinceName, entity.Id))
                    throw new NameAlreadyExistException();
                var ActualDbEntity = _provinceRepo.getById(entity.Id);
                var dbEntity = MapProvinceDTOToDbEntity(entity, ActualDbEntity);
                _provinceRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void DeleteProvince(int Id)
        {
            try
            {
                var dbEntity = _provinceRepo.getById(Id);
                if (dbEntity == null)
                    throw new ItemNotFoundException();
                _provinceRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public bool ProvinceNameAlreadyExist(string Name, long Id)
        {
            var requiredEntity = _provinceRepo.getQueryable().FirstOrDefault(a => a.ProvinceName == Name);
            if (requiredEntity != null)
            {
                if (Id != 0)
                {
                    if (requiredEntity.Id == Id)
                        return false;
                    return true;
                }
                return true;

            }
            return false;
        }
        public void AddDistrict(DistrictDTO entity)
        {
            try
            {
                if (DistrictNameAlreadyExist(entity.DistrictName, entity.Id))
                    throw new NameAlreadyExistException();
                var dbEntity = MapDistrictDTOToDbEntity(entity, new District());
                _districtRepo.insert(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void UpdateDistrict(DistrictDTO entity)
        {
            try
            {
                if (DistrictNameAlreadyExist(entity.DistrictName, entity.Id))
                    throw new NameAlreadyExistException();
                var ActualDbEntity = _districtRepo.getById(entity.Id);
                var dbEntity = MapDistrictDTOToDbEntity(entity, ActualDbEntity);
                _districtRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void DeleteDistrict(int Id)
        {
            try
            {
                var dbEntity = _districtRepo.getById(Id);
                if (dbEntity == null)
                    throw new ItemNotFoundException();
                _districtRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public bool DistrictNameAlreadyExist(string Name, long Id)
        {
            var requiredEntity = _districtRepo.getQueryable().FirstOrDefault(a => a.DistrictName == Name);
            if (requiredEntity != null)
            {
                if (Id != 0)
                {
                    if (requiredEntity.Id == Id)
                        return false;
                    return true;
                }
                return true;

            }
            return false;
        }
        public void AddVdc(MunicipalityVDCDTO entity)
        {
            try
            {
                
                var dbEntity = MapMunicipalityDTOToDbEntity(entity, new MunicipalityVDC());
                _vdcRepo.insert(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void UpdateVdc(MunicipalityVDCDTO entity)
        {
            try
            {
               
                var ActualDbEntity = _vdcRepo.getById(entity.Id);
                var dbEntity = MapMunicipalityDTOToDbEntity(entity, ActualDbEntity);
                _vdcRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        public void DeleteVdc(int Id)
        {
            try
            {
                var dbEntity = _vdcRepo.getById(Id);
                if (dbEntity == null)
                    throw new ItemNotFoundException();
                _vdcRepo.update(dbEntity);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public List<DropdownModel> GetAllProvincedropdown()
        {
            try
            {
                var AllProvinceDropdown = _provinceRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.ProvinceName }).ToList();
                return AllProvinceDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
        public List<DropdownModel> GetAllDistrictDropdown ()
        {
            try
            {
                var AllProvinceDropdown = _districtRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.DistrictNameNepali }).ToList();
                return AllProvinceDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public List<DropdownModel> GetAllVdcDropdown()
        {
            try
            {
                var AllProvinceDropdown = _vdcRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.VDCName }).ToList();
                return AllProvinceDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public List<DropdownModel> GetAllfaatDropdown()
        {
            try
            {
                var AllFattDropdown = _fattRepo.getQueryable().Select(a => new DropdownModel { Id = a.id, DisplayName = a.FattName }).ToList();
                return AllFattDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public List<DropdownModel> GetDistrictDropDownFromProvinceId(long ProvinceId)
        {
            try
            {
                var Districts = _districtRepo.getQueryable().Where(a => a.ProvinceId == ProvinceId).Select(a => new DropdownModel { Id = a.Id, DisplayName = a.DistrictNameNepali }).ToList();
                return Districts;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public List<DropdownModel> GetMunVdcDropDownFromProvinceId(long DistrictId)
        {
            try
            {
                var Districts = _vdcRepo.getQueryable().Where(a => a.DistrictId == DistrictId).Select(a => new DropdownModel { Id = a.Id, DisplayName = a.VDCName }).ToList();
                return Districts;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        Province MapProvinceDTOToDbEntity(ProvinceDTO dto, Province dbEntity)
        {
            dbEntity.Id = dto.Id;
            dbEntity.ProvinceName = dto.ProvinceName;
            dbEntity.ProvinceNameNepali = dto.ProvinceNameNepali;
            dbEntity.Capital = dto.Capital;
            dbEntity.Description = dto.Description;

            return dbEntity;
        }
        District MapDistrictDTOToDbEntity(DistrictDTO dto, District dbEntity)
        {
            dbEntity.Id = dto.Id;
            dbEntity.Description = dto.Description;
            dbEntity.DistrictName = dto.DistrictName;
            dbEntity.DistrictNameNepali = dto.DistrictNameNepali;
            dbEntity.HeadQuarter = dto.HeadQuarter;
            dbEntity.ProvinceId = dto.ProvinceId;

            return dbEntity;
        }
        MunicipalityVDC MapMunicipalityDTOToDbEntity(MunicipalityVDCDTO dto, MunicipalityVDC dbEntity)
        {
            dbEntity.Id = dto.Id;
            dbEntity.Description = dto.Description;
            dbEntity.DistrictId = dto.DistrictId;
            dbEntity.VDCNameNepali = dto.VDCNameNepali;
           
            return dbEntity;
        }
    }
}
