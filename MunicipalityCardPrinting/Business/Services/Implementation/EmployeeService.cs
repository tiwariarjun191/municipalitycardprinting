﻿using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MunicipalityCardPrinting.Utils;

namespace Business.Services.Implementation
{
    public class EmployeeService:IEmployeeService
    {

        private readonly IBaseRepository<MunicipalityVDC> _vdcRepo;
        private readonly IBaseRepository<Designation> _designationRepo;
        private readonly IBaseRepository<Shredi> _shrediRepo;
        private readonly IFileHelper1 _fileHelper;
        private readonly IBaseRepository<Employee> _employeeRepo;
        public EmployeeService(IBaseRepository<MunicipalityVDC> vdcRepo,IBaseRepository<Designation> designationRepo,IBaseRepository<Shredi> shrediRepo,IBaseRepository<Employee> employeeRepo ,IFileHelper1 fileHelper)
        {
            _vdcRepo = vdcRepo;
            _shrediRepo = shrediRepo;
            _designationRepo = designationRepo;
            _employeeRepo = employeeRepo;
            _fileHelper = fileHelper;
        }



        public void AddUpdateEmployee(EmployeeDto dto)
        {
            if (dto.Id == 0)
            {
                var data = MapFromDtoToEntity(new Employee(), dto);
                _employeeRepo.insert(data);
            }
            else
            {
                var entity = _employeeRepo.getById(dto.Id);
                var data = MapFromDtoToEntity(entity, dto);
                _employeeRepo.update(data);
            }
        }

        public void DeleteEmployee(long id)
        {
            if (id>0) 
            {
                var data = _employeeRepo.getById(id);
                _employeeRepo.delete(data);
            }
        }

        public EmployeeDto GetByEmployeeId(long id)
        {
            var data = _employeeRepo.getById(id);
            var mappeddata = MapFormEntityToDto(data, new EmployeeDto());
            return mappeddata;
        }

        public List<EmployeeDto> GetAllEmployee()
        {
            var list = _employeeRepo.getQueryable().ToList();
            var returnList = new List<EmployeeDto>();
            foreach(var item in list)
            {
                var data=MapFormEntityToDto(item, new EmployeeDto());
                returnList.Add(data);
            }
            return returnList;
        }

        public List<DropdownModel> GetAllShrediDropdown()
        {
            try
            {
                var AllProvinceDropdown = _shrediRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.SherdiNepali }).ToList();
                return AllProvinceDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public List<DropdownModel> GetAllDesignationDropdown()
        {
            try
            {
                var AllProvinceDropdown = _designationRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.NameNepali }).ToList();
                return AllProvinceDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public Employee MapFromDtoToEntity(Employee entity,EmployeeDto dto)
        {
            entity.Id =dto.Id;
            entity.Employee_Name_English = dto.Employee_Name_English;
            entity.Employee_Name_Nepali = dto.Employee_Name_Nepali;
            entity.Designation_Id = dto.Designation_Id;
            entity.DOB = dto.DOB;
            entity.Gender = dto.Gender;
            if (dto.PhotoFile != null)
            {
                entity.Photo = _fileHelper.saveImageAndGetFileName(dto.PhotoFile, dto.Photo);
            }
            entity.Ward = dto.Ward;
            entity.Sanket_Number = dto.Sanket_Number;
            entity.Office = dto.Office;
            entity.Shredi_Id = dto.Shredi_Id;
            entity.Office_Entry_Date = dto.Office_Entry_Date;
            entity.District_Province_Vdc_Id = dto.Vdc_Id;
            entity.AddressInfo = _vdcRepo.getById(dto.Vdc_Id);
            entity.DesignationInfo = _designationRepo.getById(dto.Designation_Id);
            entity.ShrediInfo = _shrediRepo.getById(dto.Shredi_Id);
            return entity;
        }

        public EmployeeDto MapFormEntityToDto(Employee entity,EmployeeDto dto)
        {
            dto.Id = entity.Id;
            dto.Employee_Name_English = entity.Employee_Name_English;
            dto.Employee_Name_Nepali = entity.Employee_Name_Nepali;
            dto.Designation_Id = entity.Designation_Id;
            dto.Sanket_Number = entity.Sanket_Number;
            dto.DOB = entity.DOB;
            dto.Gender = entity.Gender;
            dto.Photo = entity.Photo;
            dto.Ward = entity.Ward;
            dto.Office = entity.Office;
            dto.Shredi_Id = entity.Shredi_Id;
            dto.Office_Entry_Date = entity.Office_Entry_Date;
            dto.Vdc_Id = entity.District_Province_Vdc_Id;
            dto.District_Id = Convert.ToInt32(entity.AddressInfo.DistrictId);
            return dto;
        }

        public List<DropdownModel> GetAllEmployeeDropdown()
        {
            try
            {
                var AllProvinceDropdown = _employeeRepo.getQueryable().Select(a => new DropdownModel { Id = a.Id, DisplayName = a.Employee_Name_Nepali }).ToList();
                return AllProvinceDropdown;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

    }
}
