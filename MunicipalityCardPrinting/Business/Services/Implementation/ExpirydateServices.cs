﻿using Business.Services.Interface;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Services.Implementation
{
    public class ExpirydateServices:IExpiryServices
    {
        private readonly IBaseRepository<Expirydate> _ExpiryRepo; 
        public ExpirydateServices(IBaseRepository<Expirydate> ExpiryRepo)
        {
            _ExpiryRepo= ExpiryRepo;
        }

        public Expirydate Getexpirydates(long id)
        {
            var data = _ExpiryRepo.getById(id);

            return data;
        }


    }
}
