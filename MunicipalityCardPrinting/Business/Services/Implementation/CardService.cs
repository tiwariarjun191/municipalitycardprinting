﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using DTO.Children;
using DTO.Enums;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MunicipalityCardPrinting.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Utils;
using Utils.Statics;
using static DTO.ChildBasicDetailDTO;
using static Infrastructure.Database.Models.ChildDetail;

namespace Business.Services.Implementation
{
    public class CardService : ICardService
    {
        private readonly IPersonalDetailService _personalDetailService;
        private readonly IBaseRepository<CardDetail> _cardDetailRepo;
        private readonly IBaseRepository<ChildDetail> _childDetailRepo;
        private readonly IDateConverter _dateConverter;
        private readonly IFileHelper1 _fileHelper1;
        private readonly IBaseRepository<District> _districtRepo;
        private readonly IBaseRepository<AddMoreSection> _addmoreSection;
        private readonly IBaseRepository<Employee> _employeeRepo;
        private readonly IBaseRepository<Province> _provienceRepo;
        private readonly IBaseRepository<MunicipalityVDC> _vdcRepo;
        private readonly IBaseRepository<Darta> _dartaRepo;
        private readonly IBaseRepository<Chalani> _chalaniRepo;
        private readonly IBaseRepository<Designation> _designationRepo;
        private readonly ISequenceService _sequenceService;
        private readonly UserManager<ApplicationUser> _userManager;
        public CardService(IPersonalDetailService personalDetailService, IBaseRepository<CardDetail> cardDetailRepo, IDateConverter dateCoverter, IBaseRepository<ChildDetail> childDetailRepo, ISequenceService sequenceService, IBaseRepository<MunicipalityVDC> vdcRepo, IBaseRepository<Darta> dartaRepo, IBaseRepository<Chalani> chalaniRepo, IBaseRepository<Designation> designationRepo, UserManager<ApplicationUser> userManager,IFileHelper1 fileHelper1,IBaseRepository<AddMoreSection> addmoreSection,IBaseRepository<Employee> employeerepo)
        {
            _vdcRepo = vdcRepo;
            _personalDetailService = personalDetailService;
            _cardDetailRepo = cardDetailRepo;
            _dateConverter = dateCoverter;
            _childDetailRepo = childDetailRepo;
            _sequenceService = sequenceService;
            _dartaRepo = dartaRepo;
            _chalaniRepo = chalaniRepo;
            _designationRepo = designationRepo;
            _userManager = userManager;
            _fileHelper1 = fileHelper1;
            _addmoreSection = addmoreSection;
            _employeeRepo = employeerepo;
        }
       


        private void SaveAddMoreSection(ChildBasicDetailDTO dto,long id)
        {
            List<AddMoreSection> addMoreSections = new List<AddMoreSection>();

            foreach(var item in dto.AddMoreSections)
            {
                if (item.TextArea != null || item.File != null)
                {
                    AddMoreSection addMoreSection = new AddMoreSection();
                    addMoreSection.childDetailId = id;
                    addMoreSection.TextArea = item.TextArea;
                    if (item.fileinput != null)
                    {
                        addMoreSection.File = _fileHelper1.saveImageAndGetFileName(item.fileinput, item.File);
                    }

                    addMoreSections.Add(addMoreSection);
                }
               
            }
            _addmoreSection.insertRange(addMoreSections);
        }

        private void DeleteAddMoreSection(long id)
        {
               var data=_addmoreSection.getQueryable().Where(a => a.childDetailId == id).ToList();
            _addmoreSection.deleteRange(data);
        }
        public  long AddUpdateChildCardDetail(ChildBasicDetailDTO entity)
        {
            try
            {
                ChildDetail dbEntity = new ChildDetail();
                if (entity.Id == 0)
                {
                    SetChildDetail(dbEntity, entity);
                    dbEntity.Photo = entity.Photo;
                    dbEntity.FatherCitizen = entity.FatherCitizen;
                    dbEntity.MotherCitizen = entity.MotherCitizen;
                    dbEntity.BirthCertificate = entity.BirthCertificate;
                    dbEntity.other = entity.other;
                    var ChildCardNumber = _sequenceService.GetCurrentSequence("ChildCardNumber");

                    var data2 = "23";
                    var data3 = "01";
                    var fiscalyear = _dateConverter.getFiscalYear(DateTime.Now);
                    var year = fiscalyear.ToString().Substring(2);
                    var ChildCardNumberfinal = $"{data2}/{data3}/{year}/{ChildCardNumber:0000}";
                    dbEntity.CardNumber = ChildCardNumberfinal;
                    _childDetailRepo.insert(dbEntity);
                    SaveAddMoreSection(entity, dbEntity.Id);
                }
                else
                {
                    dbEntity = _childDetailRepo.getById(entity.Id);

                    SetChildDetail(dbEntity, entity);

                    if (entity.Photo != null)
                    {
                        dbEntity.Photo = entity.Photo;
                    }

                    if (entity.FatherCitizen != null)
                    {
                        dbEntity.FatherCitizen = entity.FatherCitizen;
                    }

                    if (entity.MotherCitizen != null)
                    {
                        dbEntity.MotherCitizen = entity.MotherCitizen;
                    }
                    if (entity.BirthCertificate != null)
                    {
                        dbEntity.BirthCertificate = entity.BirthCertificate;
                    }
                    if (entity.other != null)
                    {
                        dbEntity.other = entity.other;
                    }
                    _childDetailRepo.update(dbEntity);
                    DeleteAddMoreSection(dbEntity.Id);
                    SaveAddMoreSection(entity, dbEntity.Id);
                }
                return dbEntity.Id;
            
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool UpdateChildCardNumber(long CardId)
        {
            try
            {
                var card = _cardDetailRepo.getById(CardId);
                if (card == null)
                    return false;

                _cardDetailRepo.update(card);
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ChildBasicDetailDTO GetChildCardDtoById(long id)
        {
            var dbEntity = _childDetailRepo.getById(id);
            ChildBasicDetailDTO dto = new ChildBasicDetailDTO();
            SetChildDetailDto(dbEntity, dto);
            return dto;


        }


        public ChildernCard GetChildernCardById(long Id)
        {
            try
            {
                var cardDetail = _childDetailRepo.getQueryable().Where(a => a.Id == Id).FirstOrDefault();
                ChildernCard model = new ChildernCard();
                model.Id = cardDetail.Id;

                if (string.IsNullOrEmpty(cardDetail.CardNumber))
                {
                    model.CardNumber = $"{DateTime.Now.Year}{cardDetail.Id}";
                }
                else
                {
                    model.CardNumber = cardDetail.CardNumber;
                }


                model.NameEnglish = cardDetail.NameEnglish;

                model.TemporaryDistrictId = cardDetail.TempAddressInfo.DistrictInfo.DistrictNameNepali;
                model.TemporaryProvinceId = cardDetail.TempAddressInfo.DistrictInfo.ProvinceInfo.ProvinceName;
                model.TemporaryVDCId = cardDetail.TempAddressInfo.VDCName;
                model.TemporaryWard = cardDetail.TemporaryWard;
                model.PermanentDistrictId = cardDetail.PermanentAddressInfo.DistrictInfo.DistrictNameNepali;
                model.PermanentProvinceId = cardDetail.PermanentAddressInfo.DistrictInfo.ProvinceInfo.ProvinceName;
                model.PermanentVDCId = cardDetail.PermanentAddressInfo.VDCName;
                model.PermanentWard = cardDetail.PermanentWard;
                model.FatherNameEnglish = cardDetail.FatherNameEnglish;
                model.FatherAddressEnglish = cardDetail.FatherAddressEnglish;
                model.MotherNameEnglish = cardDetail.MotherNameEnglish;
                model.MotherAddressEnglish = cardDetail.MotherAddressEnglish;
                var gendersEnum = (Gender)cardDetail.Gender;
                model.Gender = gendersEnum.ToString();
                model.Photo = cardDetail.Photo;
                model.FatherMotherCitizenShip = cardDetail.FatherMotherCitizenship;
                model.PrintCountNepali = cardDetail.PrintCountNepali;
                model.PrintDate = _dateConverter.ToBS(DateTime.Now);
                model.ApprovedBy = cardDetail.Approved_By_Details.Employee_Name_English;

                model.PositionEnglish = cardDetail.UserInfo.DesignationInfo.Name;





                //Nepali
                model.CardNumberNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.CardNumber);
                model.PrintCountnpl = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.PrintCountNepali.ToString());
                model.NameNepali = cardDetail.NameNepali;

                model.TemporaryNepaliDistrictId = cardDetail.TempAddressInfo.DistrictInfo.DistrictName;
                model.TemporaryNepaliProvinceId = cardDetail.TempAddressInfo.DistrictInfo.ProvinceInfo.ProvinceNameNepali;
                model.TemporaryNepaliVDCId = cardDetail.TempAddressInfo.VDCNameNepali;
                model.TemporaryWard = cardDetail.TemporaryWard;
                model.PermanentNepaliDistrictId = cardDetail.PermanentAddressInfo.DistrictInfo.DistrictName;
                model.PermanentNepaliProvinceId = cardDetail.PermanentAddressInfo.DistrictInfo.ProvinceInfo.ProvinceNameNepali;
                model.PermanentNepaliVDCId = cardDetail.PermanentAddressInfo.VDCNameNepali;
                model.PermanentWard = cardDetail.PermanentWard;
                model.FatherNameNepali = cardDetail.FatherNameNepali;
                model.FatherAddressNepali = cardDetail.FatherAddressNepali;
                model.MotherNameNepali = cardDetail.MotherNameNepali;
                model.MotherAddressNepali = cardDetail.MotherAddressNepali;
                model.GenderNepali = GenderEnumHandler.ConvertToNepali(gendersEnum);
                model.Photo = cardDetail.Photo;
                model.PrintDate = _dateConverter.ToBS(DateTime.Now);
                model.ApprovedByNepali = cardDetail.Approved_By_Details.Employee_Name_Nepali;
                model.FatherMotherCitizenShipNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.FatherMotherCitizenShip);
                model.PositionNepali = cardDetail.UserInfo?.DesignationInfo?.NameNepali;

                return model;

            }
            catch (Exception ex)
            {

                throw;
            }
        }



        public List<ChildBasicDetailDTO> GetAllChildernCardDetail()
        {
            var carddetail = _childDetailRepo.getQueryable().ToList();
            var detail = new List<ChildBasicDetailDTO>();
            foreach (var item in carddetail)
            {
                var mapdata = SetChildDetailDto(item, new ChildBasicDetailDTO());
                detail.Add(mapdata);
            }

            return detail;
        }

        public void IncreaseChildCardPrintCount(long Id)
        {
            try
            {
                var card = _childDetailRepo.getById(Id);
                if (!card.IsCardPrinted)
                {
                    card.EntryDate = DateTime.Now;
                }
                card.LastPrintDate = DateTime.Now;
                card.IsCardPrinted = true;
                card.PrintCountNepali++;
                _childDetailRepo.update(card);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        void SetChildDetail(ChildDetail entity, ChildBasicDetailDTO childDto)
        {
            try
            {
                entity.Id = childDto.Id;
                entity.Gender = childDto.Gender;
                entity.ApprovedBy = childDto.ApprovedBy;
                entity.ApprovedByNepali = childDto.ApprovedByNepali;
                entity.CardNumberNepali = childDto.CardNumberNepali;
                entity.EntryDate = childDto.EntryDate;
                entity.FatherAddressEnglish = childDto.FatherAddressEnglish;
                entity.FatherAddressNepali = childDto.FatherAddressNepali;
                entity.FatherNameEnglish = childDto.FatherNameEnglish;
                entity.FatherNameNepali = childDto.FatherNameNepali;
                entity.IsCardPrinted = childDto.IsCardPrinted;
                entity.LastPrintDate = childDto.LastPrintDate;
                entity.ModifiedDate = childDto.ModifiedDate;
                entity.MotherAddressEnglish = childDto.MotherAddressEnglish;
                entity.MotherAddressNepali = childDto.MotherAddressNepali;
                entity.MotherNameEnglish = childDto.MotherNameEnglish;
                entity.MotherNameNepali = childDto.MotherNameNepali;
                entity.NameEnglish = childDto.NameEnglish;
                entity.NameNepali = childDto.NameNepali;

                entity.UserId = childDto.UserId;
                entity.FatherMotherCitizenship = childDto.FatherMotherCitizenship;
                // entity.PermanentProvinceId = childDto.PermanentProvinceId;
                entity.PermanentVDCId = childDto.PermanentVDCId;
                entity.PermanentWard = childDto.PermanentWard;

                entity.PrintCountNepali = childDto.PrintCountNepali;
                entity.PrintDate = childDto.PrintDate;
                entity.TemporaryProvinceId = childDto.TemporaryProvinceId;
                entity.TemporaryVDCId = childDto.TemporaryVDCId;
                entity.TemporaryWard = childDto.TemporaryWard;
                entity.Employee_Id = childDto.Employee_Id;
                entity.ApprovedById = childDto.ApprovedById;
                entity.Employee_Address = childDto.Employee_Address;
                entity.Employee = _employeeRepo.getById(childDto.Employee_Id);
                //entity.Approved_By_Details = _employeeRepo.getById(childDto.ApprovedById);

                if (childDto.PermanentVDCId.HasValue)
                {
                    entity.PermanentAddressInfo = _vdcRepo.getById(Convert.ToInt64(childDto.PermanentVDCId));
                }

                //if (childDto.TemporaryVDCId.HasValue)
                //{
                //    entity.TempAddressInfo = _vdcRepo.getById(Convert.ToInt64(childDto.TemporaryVDCId));
                //}
            }
            catch (Exception)
            {

                throw;
            }

        }
        ChildBasicDetailDTO SetChildDetailDto(ChildDetail entity, ChildBasicDetailDTO childDto)
        {
            try
            {
                childDto.Id = entity.Id;
                childDto.Gender = entity.Gender;
                childDto.ApprovedBy = entity.ApprovedBy;
                childDto.ApprovedByNepali = entity.ApprovedByNepali;

                childDto.CardNumber = entity.CardNumber;
                childDto.CardNumberNepali = entity.CardNumberNepali;
                childDto.EntryDate = entity.EntryDate;
                childDto.FatherAddressEnglish = entity.FatherAddressEnglish;
                childDto.FatherAddressNepali = entity.FatherAddressNepali;
                childDto.FatherNameEnglish = entity.FatherNameEnglish;
                childDto.FatherNameNepali = entity.FatherNameNepali;
                childDto.IsCardPrinted = entity.IsCardPrinted;
                childDto.LastPrintDate = entity.LastPrintDate;
                childDto.ModifiedDate = entity.ModifiedDate;
                childDto.MotherAddressEnglish = entity.MotherAddressEnglish;
                childDto.MotherAddressNepali = entity.MotherAddressNepali;
                childDto.MotherNameEnglish = entity.MotherNameEnglish;
                childDto.MotherNameNepali = entity.MotherNameNepali;
                childDto.NameEnglish = entity.NameEnglish;
                childDto.NameNepali = entity.NameNepali;
                childDto.FatherMotherCitizenship = entity.FatherMotherCitizenship;
                childDto.Employee_Id = entity.Employee_Id;
                childDto.ApprovedById = entity.ApprovedById;
                childDto.Employee_Address = entity.Employee_Address;

                //childDto.PermanentProvinceId = entity.PermanentProvinceId;
                childDto.PermanentVDCId = entity.PermanentVDCId;
                childDto.PermanentWard = entity.PermanentWard;
                childDto.Photo = entity.Photo;
                childDto.FatherCitizen = entity.FatherCitizen;
                childDto.MotherCitizen = entity.MotherCitizen;
                childDto.BirthCertificate = entity.BirthCertificate;
                childDto.other = entity.other;
                childDto.PrintCountNepali = entity.PrintCountNepali;
                childDto.PrintDate = entity.PrintDate;
                childDto.TemporaryProvinceId = entity.TemporaryProvinceId;
                childDto.TemporaryVDCId = entity.TemporaryVDCId;
                childDto.TemporaryWard = entity.TemporaryWard;



                foreach (var item in entity.AddMoreSections)
                {
                    childDto.AddMoreSections.Add(new AddMoreSectionDto()
                    {
                        File = item.File,
                        TextArea = item.TextArea
                    });
                }

                if (entity.PermanentAddressInfo != null) 
                {
                    childDto.PermanentDistrictId = entity.PermanentAddressInfo.DistrictId;
                    childDto.PermanentProvinceId = entity.PermanentAddressInfo.DistrictInfo.ProvinceId;

                }

                if (entity.TempAddressInfo != null)
                {
                    childDto.TemporaryDistrictId = entity.TempAddressInfo.DistrictId;
                    childDto.PermanentProvinceId = entity.TempAddressInfo.DistrictInfo.ProvinceId;
                }

                return childDto;

            }
            catch (Exception)
            {

                throw;
            }

        }












        public CardDetailReturnModel AddUpdateCardAdditionalDetail(CardAdditionalDetailDTO entity)
        {
            try
            {
                CardDetailReturnModel dataToBeReturned = new CardDetailReturnModel();
                CardDetail dbEntity = _cardDetailRepo.getById(entity.CardDetailId);
                MapCardAdditionalDTOToEntity(entity, dbEntity);
                _cardDetailRepo.update(dbEntity);
                dataToBeReturned.CardId = dbEntity.Id;
                return dataToBeReturned;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public CardDetailDTO GetCardDetailByCardDetailId(long CardDetailId)
        {
            try
            {
                var cardDetail = _cardDetailRepo.getQueryable().Include(a => a.DisabledPersonInfo).Include(a => a.GuardianInfo).Include(a => a.DisabledPersonInfo.TempAddressInfo).Include(a => a.DisabledPersonInfo.TempAddressInfo.DistrictInfo).Include(a => a.DisabledPersonInfo.PermanentAddressInfo).Include(a => a.DisabledPersonInfo.TempAddressInfo).Include(a => a.GuardianInfo.TempAddressInfo).Include(a => a.GuardianInfo.TempAddressInfo.DistrictInfo).Where(x => x.Id == CardDetailId).FirstOrDefault();
                if (cardDetail == null)
                    throw new CustomException("Card detail doesnot exist anymore");
                CardDetailDTO model = new CardDetailDTO();
                MapCardDetailDbEntityToDTO(cardDetail, model);
                return model;


            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ElderCardDetailDTO GetElderCardDetail(long CardDetailId)
        {
            try
            {
                var cardDetail = _cardDetailRepo.getQueryable().Include(a => a.DisabledPersonInfo).Include(a => a.GuardianInfo).Include(a => a.DisabledPersonInfo.TempAddressInfo).Include(a => a.DisabledPersonInfo.TempAddressInfo.DistrictInfo).Include(a => a.DisabledPersonInfo.PermanentAddressInfo).Include(a => a.DisabledPersonInfo.TempAddressInfo).Include(a => a.GuardianInfo.TempAddressInfo).Include(a => a.GuardianInfo.TempAddressInfo.DistrictInfo).Where(x => x.Id == CardDetailId).FirstOrDefault();
                if (cardDetail == null)
                    throw new CustomException("Card detail doesnot exist anymore");
                ElderCardDetailDTO model = new ElderCardDetailDTO();
                MapCardDetailDbEntityToElderCardDTO(cardDetail, model);
                return model;


            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<DisabledCardViewModel> AllDisabledCards()
        {
            try
            {
                var Cards = _cardDetailRepo.getQueryable().Where(x => x.CardCategory == (int)CardCategoryEnum.Disabled).OrderByDescending(z => z.Id).Select(a => new { a.Id, a.IdCardType, a.IdCardnumber, a.DisabledPersonInfo.FullNameNepali, a.DisabilityTypeByNature, a.DisabilityTypeBySeriousness }).ToList();
                List<DisabledCardViewModel> viewModels = new List<DisabledCardViewModel>();
                foreach (var data in Cards)
                {
                    DisabledCardViewModel model = new DisabledCardViewModel();
                    model.CardId = data.Id;
                    model.CardHolder = data.FullNameNepali;
                    model.CardNumber = NepaliNumberConverter.ConvertDigitsToNepaliDigits(data.IdCardnumber);
                    if (data.IdCardType != null)
                    {
                        var cardTypeEnum = (CardType)data.IdCardType;
                        model.CardType = CardTypeEnumHandler.ConvertToNepali(cardTypeEnum);
                    }
                    if (data.DisabilityTypeByNature != null)
                    {
                        var disablityNautre = (DisbalityTypeOntheBasisOfNature)data.DisabilityTypeByNature;
                        model.DisablityTypeByNature = DisabilityNatureEnumHandler.ConvertToNepali(disablityNautre);
                    }
                    if (data.DisabilityTypeBySeriousness != null)
                    {
                        var disablitySeriousness = (DisabilityTypeOnTheBasisOfSeriousness)data.DisabilityTypeBySeriousness;
                        model.DisablityTypeBySeriousness = disablitySeriousness.GetDisplayName();
                    }

                    viewModels.Add(model);
                }
                return viewModels;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public List<ElderCardViewModel> AllElderCards()
        {
            try
            {
                var Cards = _cardDetailRepo.getQueryable().Where(x => x.CardCategory == (int)CardCategoryEnum.OldCitizen).OrderByDescending(c => c.Id).Select(a => new { a.Id, a.IdCardnumber, a.DisabledPersonInfo.FullNameNepali, a.CategoryOfElderness }).ToList();
                List<ElderCardViewModel> viewModels = new List<ElderCardViewModel>();
                foreach (var data in Cards)
                {
                    ElderCardViewModel model = new ElderCardViewModel();
                    model.CardId = data.Id;
                    model.CardHolderName = data.FullNameNepali;
                    model.CardNumber = data.IdCardnumber;
                    if (data.CategoryOfElderness != null)
                    {
                        var categoryEnum = (CategoryOfElderness)data.CategoryOfElderness;
                        model.CategoryOfElder = categoryEnum.GetDisplayName();
                    }


                    viewModels.Add(model);
                }
                return viewModels;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<PrintedDisabledList> AllPrintedElderCards(int Category)
        {
            try
            {
                var Cards = _cardDetailRepo.getQueryable().Where(x => x.CardCategory == Category && x.IsCardPrinted).OrderByDescending(y => y.Id).Select(a => new { a.Id, a.FirstCardPrintedDate, a.DisabledPersonInfo.DOB, a.IdCardnumber, a.DisabledPersonInfo.DOBNepali, a.DisabledPersonInfo.FullNameNepali, a.DisabledPersonInfo.CitizenshipNumberNepali, a.DisabledPersonInfo.TempWardNumber }).ToList();
                List<PrintedDisabledList> viewModels = new List<PrintedDisabledList>();
                foreach (var data in Cards)
                {
                    PrintedDisabledList model = new PrintedDisabledList();
                    model.CardId = data.Id;
                    model.FullName = data.FullNameNepali;

                    model.CardNumber = NepaliNumberConverter.ConvertDigitsToNepaliDigits(data.IdCardnumber);
                    if (data.TempWardNumber != null)
                    {
                        model.Address = $"भद्रपुर न.पा., वाड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(data.TempWardNumber)}";
                    }
                    if (!string.IsNullOrEmpty(data.DOBNepali))
                    {
                        model.DateOfBirth = NepaliNumberConverter.ConvertDigitsToNepaliDigits(data.DOBNepali);
                        if (data.DOB != null)
                            model.Age = NepaliNumberConverter.ConvertDigitsToNepaliDigits(GetAge(data.DOB.Value).ToString());

                    }
                    model.CitizenshipNumber = NepaliNumberConverter.ConvertDigitsToNepaliDigits(data.CitizenshipNumberNepali);
                    if (data.FirstCardPrintedDate != null)
                    {
                        model.PrintedDate = NepaliNumberConverter.ConvertDigitsToNepaliDigits(_dateConverter.ToBS(data.FirstCardPrintedDate.Value));
                    }
                    viewModels.Add(model);
                }
                return viewModels;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public List<DisabledCardViewModel> AllElderCard()
        {
            try
            {
                var Cards = _cardDetailRepo.getQueryable().Where(x => x.CardCategory == (int)CardCategoryEnum.OldCitizen).Select(a => new { a.Id, a.IdCardType, a.IdCardnumber, a.DisabledPersonInfo.FullName, a.DisabilityTypeByNature, a.DisabilityTypeBySeriousness }).ToList();
                List<DisabledCardViewModel> viewModels = new List<DisabledCardViewModel>();
                foreach (var data in Cards)
                {
                    DisabledCardViewModel model = new DisabledCardViewModel();
                    model.CardId = data.Id;
                    model.CardHolder = data.FullName;
                    model.CardNumber = data.IdCardnumber;
                    var cardTypeEnum = (CardType)data.IdCardType;
                    model.CardType = cardTypeEnum.GetDisplayName();
                    var disablityNautre = (DisbalityTypeOntheBasisOfNature)data.DisabilityTypeByNature;
                    model.DisablityTypeByNature = disablityNautre.GetDisplayName();
                    var disablitySeriousness = (DisbalityTypeOntheBasisOfNature)data.DisabilityTypeBySeriousness;
                    model.DisablityTypeBySeriousness = disablitySeriousness.GetDisplayName();
                    viewModels.Add(model);
                }
                return viewModels;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public HomeViewModel GetAllDataForHome()
        {
            try
            {
                HomeViewModel model = new HomeViewModel();
                var totalDisabledCard = _cardDetailRepo.getQueryable().Where(a => a.CardCategory == (int)CardCategoryEnum.Disabled).ToList();
                var allElderCard = _cardDetailRepo.getQueryable().Where(a => a.CardCategory == (int)CardCategoryEnum.OldCitizen).ToList();

                var totalEntryOfDisabledCard = totalDisabledCard.Count;
                model.TotalEntryOfDisabledCard = NepaliNumberConverter.ConvertDigitsToNepaliDigits(totalEntryOfDisabledCard.ToString());
                var TotalPrintedDisabledCard = totalDisabledCard.Where(a => a.IsCardPrinted).ToList().Count();
                model.TotalPrintedDisabledCard = NepaliNumberConverter.ConvertDigitsToNepaliDigits(TotalPrintedDisabledCard.ToString());
                var NotPrintedDisabledCard = totalEntryOfDisabledCard - TotalPrintedDisabledCard;
                model.TotalYetToPrintDisabledCard = NepaliNumberConverter.ConvertDigitsToNepaliDigits(NotPrintedDisabledCard.ToString());
                var TotalExtremeDisabled = totalDisabledCard.Where(x => x.DisabilityTypeBySeriousness == (int)DisabilityTypeOnTheBasisOfSeriousness.Extreme).ToList().Count;
                model.TotalExtremeDisabled = NepaliNumberConverter.ConvertDigitsToNepaliDigits(TotalExtremeDisabled.ToString());
                var NormalDisable = totalEntryOfDisabledCard - TotalExtremeDisabled;
                model.TotalNormalDisabled = NepaliNumberConverter.ConvertDigitsToNepaliDigits(NormalDisable.ToString());



                var totalElderCard = allElderCard.Count;
                model.TotalElderPersonCard = NepaliNumberConverter.ConvertDigitsToNepaliDigits(totalElderCard.ToString());
                var allPrintedElderCard = allElderCard.Where(a => a.IsCardPrinted).ToList().Count;
                model.TotalPrintedElderCard = NepaliNumberConverter.ConvertDigitsToNepaliDigits(totalElderCard.ToString());
                var NotPrintedElderCard = totalElderCard - allPrintedElderCard;
                model.TotalYetToPrintElderCard = NepaliNumberConverter.ConvertDigitsToNepaliDigits(NotPrintedElderCard.ToString());

                model.TotalBodyDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.BodyDisability).ToList().Count.ToString().ToNepali();
                model.TotalHearingDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.HearingDisability).ToList().Count.ToString().ToNepali();
                model.TotalSpeakingDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.SpeakingDisability).ToList().Count.ToString().ToNepali();
                model.TotalMentalDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.MentalDisability).ToList().Count.ToString().ToNepali();
                model.TotalSocialDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.SocialDisability).ToList().Count.ToString().ToNepali();
                model.TotalHemopholia = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.Hemopholia).ToList().Count.ToString().ToNepali();
                model.TotalOtism = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.Otism).ToList().Count.ToString().ToNepali();
                model.TotalMultipleDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.MultipleDisability).ToList().Count.ToString().ToNepali();
                model.TotalVisionAndHearingDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.VisionAndHearingDisability).ToList().Count.ToString().ToNepali();
                model.TotalVisionDisability = allElderCard.Where(a => a.DisabilityTypeByNature == (int)DisbalityTypeOntheBasisOfNature.VisionDisability).ToList().Count.ToString().ToNepali();


                var darta = _dartaRepo.getQueryable().Count();
                model.Totaldarta = darta.ToString();


                var chalani = _chalaniRepo.getQueryable().Count();
                model.TotalChalanai = chalani.ToString();


                var child = _childDetailRepo.getQueryable().Count();
                model.TotalChild = child.ToString();
                return model;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DisabledIndividualReport GetDisabledIndividualReport(long Id)
        {
            try
            {
                DisabledIndividualReport model = new DisabledIndividualReport();
                var cardDetail = _cardDetailRepo.getQueryable().Include(a => a.GuardianInfo).Include(a => a.UserInfo).Include(a => a.DisabledPersonInfo).Where(a => a.Id == Id).FirstOrDefault();
                var DisabledPerson = cardDetail.DisabledPersonInfo;
                var Guardian = cardDetail.GuardianInfo;
                var Staff = cardDetail.UserInfo;
                model.Id = cardDetail.Id;
                model.CardNumberNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(cardDetail.IdCardnumber);
                if (cardDetail.IdCardType != null)
                {
                    var cardTypeEnum = (CardType)cardDetail.IdCardType;
                    model.CardTypeNepali = CardTypeEnumHandler.ConvertToNepali(cardTypeEnum);
                }
                var genderEnum = (Gender)DisabledPerson.Gender;
                model.NameNepali = DisabledPerson.FullNameNepali;
                model.AddressNepali = $"प्रदेश नं. १ जिल्ला झापा, भद्रपुर न.पा., वाड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.TempWardNumber)}";
                model.DateNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.DOBNepali.Replace("-", "/"));
                model.CitizenshipNumberNepali = DisabledPerson.CitizenshipNumberNepali;
                model.RelationWithCitizen = cardDetail.GuardianRelationWithCitizen;
                model.GenderNepali = GenderEnumHandler.ConvertToNepali(genderEnum);
                if (DisabledPerson.BloodGroup != null)
                {
                    var bloodGrpEnum = (BloodGroupEnum)DisabledPerson.BloodGroup;
                    model.BloodGroupNepali = BloodGroupEnumHandler.ConvertToNepali(bloodGrpEnum);
                }
                if (cardDetail.DisabilityTypeByNature != null)
                {
                    var disabilityNatureEnum = (DisbalityTypeOntheBasisOfNature)cardDetail.DisabilityTypeByNature;
                    model.OnTheBasisOfNatureNepali = DisabilityNatureEnumHandler.ConvertToNepali(disabilityNatureEnum);
                }
                if (cardDetail.DisabilityTypeBySeriousness != null)
                {
                    var disablitySeriousness = (DisabilityTypeOnTheBasisOfSeriousness)cardDetail.DisabilityTypeBySeriousness;
                    model.OnTheBasisOfSeriousnessNepali = disablitySeriousness.GetDisplayName();
                }
                if (DisabledPerson.MarritalStatus != null)
                {
                    var bloodGrpEnum = (MarritalStatusEnum)DisabledPerson.MarritalStatus;
                    model.MarritalStatusNepali = bloodGrpEnum.GetDisplayName();
                }
                if (DisabledPerson.PermanentAddressId != null)
                {
                    model.PermanentAddress = $"प्रदेश नं. {DisabledPerson.PermanentAddressInfo.DistrictInfo.ProvinceInfo.ProvinceNameNepali} जिल्ला {DisabledPerson.PermanentAddressInfo.DistrictInfo.DistrictNameNepali}, {DisabledPerson.PermanentAddressInfo.VDCNameNepali}., वाड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.PermanentWardNumber)}";
                }
                if (Guardian.PermanentAddressId != null)
                {
                    model.GuardianAddressNepali = $"प्रदेश नं. {Guardian.PermanentAddressInfo.DistrictInfo.ProvinceInfo.ProvinceNameNepali} जिल्ला {Guardian.PermanentAddressInfo.DistrictInfo.DistrictNameNepali}, {DisabledPerson.PermanentAddressInfo.VDCNameNepali}., वाड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(Guardian.TempWardNumber)}";

                }

                model.GuardianNameNepali = Guardian.FullNameNepali;
                model.ApproverNameNepali = Staff.FullNameNepali; ;
                model.DesignationNepali = Staff.DesignationInfo.NameNepali;
                model.PrintCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(cardDetail.PrintedCount.ToString());
                model.PrintDateNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(_dateConverter.ToBS(DateTime.Now));

                model.Occupation = cardDetail.Occupation;
                model.Qualification = cardDetail.Qualification;
                model.CurrentSchoolCollegeName = cardDetail.CurrentSchoolCollegeName;
                model.StudyingOrFinished = cardDetail.StudyingOrFinished;
                model.WhatKindOfWorkcanDoInDailyLife = cardDetail.WhatKindOfWorkcanDoInDailyLife;
                model.WhatKindOfWorkCantDo = cardDetail.WhatKindOfWorkCantDo;
                if (cardDetail.IsNeededAnyEquipement.GetValueOrDefault())
                {
                    model.IsNeededAnyEquipement = "पर्ने";
                }
                else
                {
                    model.IsNeededAnyEquipement = "नपर्ने";
                }

                model.AdditionalEquipements = cardDetail.AdditionalEquipements;
                if (cardDetail.HasAnyAdditionalEquipement.GetValueOrDefault())
                {
                    model.HasAnyAdditionalEquipement = "पाएको";
                }
                else
                {
                    model.HasAnyAdditionalEquipement = "नपाएको";
                }
                model.OtherFacilities = cardDetail.OtherFacilities;
                model.OtherFacilitiesNeeded = cardDetail.OtherFacilitiesNeeded;
                model.OfficeToProvideFacility = cardDetail.OfficeToProvideFacility;
                model.Remarks = cardDetail.Remarks;

                return model;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DisabledProgressReportWrapper GetDisabledPersonProgressReport()
        {
            try
            {
                var cardDetailGroupByType = _cardDetailRepo.getQueryable().Where(a => a.DisabilityTypeByNature != null).ToList().GroupBy(x => x.DisabilityTypeByNature).OrderBy(z => z.Key).ToList();

                DisabledProgressReportWrapper model = new DisabledProgressReportWrapper();
                model.reports = new List<DisabledPersonsCardProgressReport>();
                model.Total = new ProgressReportTotalModel();
                model.DateNepali = _dateConverter.ToBS(DateTime.Now);
                model.DistrictNepali = "झापा";
                model.Municipality = "भद्रपुर नगरपालिका";
                var allCardType = cardDetailGroupByType.Select(a => a.Key).ToList();

                foreach (var data in cardDetailGroupByType)
                {
                    DisabledPersonsCardProgressReport individual = new DisabledPersonsCardProgressReport();
                    individual.DisabilityTypeEnumValue = data.Key.GetValueOrDefault();
                    var Typeenum = (DisbalityTypeOntheBasisOfNature)individual.DisabilityTypeEnumValue;
                    individual.DisabilityTypeNepali = DisabilityNatureEnumHandler.ConvertToNepali(Typeenum);
                    individual.TypeA = new ProgressReportTypeWiseModel();
                    individual.TypeB = new ProgressReportTypeWiseModel();
                    individual.TypeC = new ProgressReportTypeWiseModel();
                    individual.TypeD = new ProgressReportTypeWiseModel();
                    individual.TotalMale = data.Where(a => a.DisabledPersonInfo.Gender == (int)Gender.Male).ToList().Count;
                    individual.TotalFemale = data.Where(a => a.DisabledPersonInfo.Gender == (int)Gender.Female).ToList().Count;
                    individual.TotalOther = data.Where(a => a.DisabledPersonInfo.Gender == (int)Gender.Other).ToList().Count;
                    individual.Total = data.Count();

                    individual.TotalMaleNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TotalMale.ToString());
                    individual.TotalFemaleNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TotalFemale.ToString());
                    individual.TotalOtherNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TotalOther.ToString());
                    individual.TotalNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.Total.ToString());

                    var CardAMale = data.Where(a => a.IdCardType == (int)CardType.A && a.DisabledPersonInfo.Gender == (int)Gender.Male).ToList();
                    var CardAFemale = data.Where(a => a.IdCardType == (int)CardType.A && a.DisabledPersonInfo.Gender == (int)Gender.Female).ToList();
                    var CardAOther = data.Where(a => a.IdCardType == (int)CardType.A && a.DisabledPersonInfo.Gender == (int)Gender.Other).ToList();

                    var CardBMale = data.Where(a => a.IdCardType == (int)CardType.B && a.DisabledPersonInfo.Gender == (int)Gender.Male).ToList();
                    var CardBFemale = data.Where(a => a.IdCardType == (int)CardType.B && a.DisabledPersonInfo.Gender == (int)Gender.Female).ToList();
                    var CardBOther = data.Where(a => a.IdCardType == (int)CardType.B && a.DisabledPersonInfo.Gender == (int)Gender.Other).ToList();

                    var CardCMale = data.Where(a => a.IdCardType == (int)CardType.C && a.DisabledPersonInfo.Gender == (int)Gender.Male).ToList();
                    var CardCFemale = data.Where(a => a.IdCardType == (int)CardType.C && a.DisabledPersonInfo.Gender == (int)Gender.Female).ToList();
                    var CardCOther = data.Where(a => a.IdCardType == (int)CardType.C && a.DisabledPersonInfo.Gender == (int)Gender.Other).ToList();

                    var CardDMale = data.Where(a => a.IdCardType == (int)CardType.D && a.DisabledPersonInfo.Gender == (int)Gender.Male).ToList();
                    var CardDFemale = data.Where(a => a.IdCardType == (int)CardType.D && a.DisabledPersonInfo.Gender == (int)Gender.Female).ToList();
                    var CardDOther = data.Where(a => a.IdCardType == (int)CardType.D && a.DisabledPersonInfo.Gender == (int)Gender.Other).ToList();

                    individual.TypeA.MaleCount = CardAMale.Count;
                    individual.TypeA.MaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeA.MaleCount}");

                    individual.TypeA.FemaleCount = CardAFemale.Count;
                    individual.TypeA.FemaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeA.FemaleCount}");

                    individual.TypeA.OtherCount = CardAOther.Count;
                    individual.TypeA.OtherCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeA.OtherCount}");
                    individual.TypeA.TotalCount = CardAMale.Count + CardAFemale.Count + CardAOther.Count;
                    individual.TypeA.TotalCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TypeA.TotalCount.ToString());

                    individual.TypeB.MaleCount = CardBMale.Count;
                    individual.TypeB.MaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeB.MaleCount}");

                    individual.TypeB.FemaleCount = CardBFemale.Count;
                    individual.TypeB.FemaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeB.FemaleCount}");

                    individual.TypeB.OtherCount = CardBOther.Count;
                    individual.TypeB.OtherCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeB.OtherCount}");
                    individual.TypeB.TotalCount = CardBMale.Count + CardBFemale.Count + CardBOther.Count;
                    individual.TypeB.TotalCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TypeB.TotalCount.ToString());

                    individual.TypeC.MaleCount = CardCMale.Count;
                    individual.TypeC.MaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeC.MaleCount}");

                    individual.TypeC.FemaleCount = CardCFemale.Count;
                    individual.TypeC.FemaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeC.FemaleCount}");

                    individual.TypeC.OtherCount = CardCOther.Count;
                    individual.TypeC.OtherCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeC.OtherCount}");
                    individual.TypeC.TotalCount = CardCMale.Count + CardCFemale.Count + CardCOther.Count;
                    individual.TypeC.TotalCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TypeC.TotalCount.ToString());

                    individual.TypeD.MaleCount = CardDMale.Count;
                    individual.TypeD.MaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeD.MaleCount}");

                    individual.TypeD.FemaleCount = CardDFemale.Count;
                    individual.TypeD.FemaleCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeD.FemaleCount}");

                    individual.TypeD.OtherCount = CardDOther.Count;
                    individual.TypeD.OtherCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits($"{individual.TypeD.OtherCount}");
                    individual.TypeD.TotalCount = CardDMale.Count + CardDFemale.Count + CardDOther.Count;
                    individual.TypeD.TotalCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(individual.TypeD.TotalCount.ToString());

                    model.reports.Add(individual);

                }

                var AllDisabilityType = from DisbalityTypeOntheBasisOfNature d in Enum.GetValues(typeof(DisbalityTypeOntheBasisOfNature))
                                        select new
                                        {
                                            Id = (int)d
                                        };
                var AllRemainingTypes = AllDisabilityType.Where(a => !allCardType.Contains(a.Id)).ToList();
                foreach (var val in AllRemainingTypes)
                {
                    DisabledPersonsCardProgressReport individual = new DisabledPersonsCardProgressReport();
                    individual.DisabilityTypeEnumValue = val.Id;
                    var Typeenum = (DisbalityTypeOntheBasisOfNature)individual.DisabilityTypeEnumValue;
                    individual.DisabilityTypeNepali = DisabilityNatureEnumHandler.ConvertToNepali(Typeenum);
                    individual.TypeA = new ProgressReportTypeWiseModel();
                    individual.TypeB = new ProgressReportTypeWiseModel();
                    individual.TypeC = new ProgressReportTypeWiseModel();
                    individual.TypeD = new ProgressReportTypeWiseModel();
                    model.reports.Add(individual);
                }

                model.Total.TypeAMale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeA.MaleCount).ToString());
                model.Total.TypeAFemale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeA.FemaleCount).ToString());
                model.Total.TypeAOther = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeA.OtherCount).ToString());
                model.Total.TypeATotal = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeA.TotalCount).ToString());

                model.Total.TypeBMale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeB.MaleCount).ToString());
                model.Total.TypeBFemale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeB.FemaleCount).ToString());
                model.Total.TypeBOther = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeB.OtherCount).ToString());
                model.Total.TypeBTotal = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeB.TotalCount).ToString());

                model.Total.TypeCMale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeC.MaleCount).ToString());
                model.Total.TypeCFemale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeC.FemaleCount).ToString());
                model.Total.TypeCOther = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeC.OtherCount).ToString());
                model.Total.TypeCTotal = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeC.TotalCount).ToString());

                model.Total.TypeDMale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeD.MaleCount).ToString());
                model.Total.TypeDFemale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeD.FemaleCount).ToString());
                model.Total.TypeDOther = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeD.OtherCount).ToString());
                model.Total.TypeDTotal = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TypeD.TotalCount).ToString());

                model.Total.TotalMale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TotalMale).ToString());
                model.Total.TotalFemale = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TotalFemale).ToString());
                model.Total.TotalOther = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.TotalOther).ToString());
                model.Total.Total = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.reports.Sum(a => a.Total).ToString());

                return model;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DisabledPersonCard GetDisabledPersonCardById(long Id)
        {
            try
            {
                var cardDetail = _cardDetailRepo.getQueryable().Include(a => a.GuardianInfo).Include(a => a.UserInfo).Include(a => a.DisabledPersonInfo).Where(a => a.Id == Id).FirstOrDefault();
                var DisabledPerson = cardDetail.DisabledPersonInfo;
                var Guardian = cardDetail.GuardianInfo;
                var Staff = cardDetail.UserInfo;
                DisabledPersonCard model = new DisabledPersonCard();
                model.Id = cardDetail.Id;

                if (string.IsNullOrEmpty(cardDetail.IdCardnumber))
                {
                    model.CardNumber = $"{DateTime.Now.Year}{cardDetail.Id}";
                }
                else
                {
                    model.CardNumber = cardDetail.IdCardnumber;
                }

                if (cardDetail.IdCardType != null)
                {
                    var cardTypeEnum = (CardType)cardDetail.IdCardType;
                    model.CardTypeCSSClass = cardTypeEnum.GetDisplayName();
                    model.CardType = cardTypeEnum.ToString();
                }
                model.Name = DisabledPerson.FullName;
                model.Address = $"Province No. 1, District Jhapa, Bhadrapur Municipality Ward No:{DisabledPerson.TempWardNumber}";
                if (DisabledPerson.DOBNepali != null)
                {
                    model.Date = DisabledPerson.DOBNepali.Replace("-", "/");
                }

                model.CitizenshipNumber = DisabledPerson.CitizenshipNumber;
                var genderEnum = (Gender)DisabledPerson.Gender;
                model.Gender = genderEnum.GetDisplayName();
                if (DisabledPerson.BloodGroup != null)
                {
                    var bloodGrpEnum = (BloodGroupEnum)DisabledPerson.BloodGroup;
                    model.BloodGroup = bloodGrpEnum.GetDisplayName();
                }
                if (cardDetail.DisabilityTypeByNature != null)
                {
                    var disabilityNatureEnum = (DisbalityTypeOntheBasisOfNature)cardDetail.DisabilityTypeByNature;
                    model.OnTheBasisOfNature = disabilityNatureEnum.GetDisplayName();
                }
                if (cardDetail.DisabilityTypeBySeriousness != null)
                {
                    var disablitySeriousness = (DisabilityTypeOnTheBasisOfSeriousness)cardDetail.DisabilityTypeBySeriousness;
                    model.OnTheBasisOfSeriousness = disablitySeriousness.ToString();
                }
                model.GuardianName = Guardian.FullName;

                model.ApproverName = Staff.FullName;
                model.Designation = Staff.DesignationInfo.Name;
                model.Photo = cardDetail.Photo;
                model.PrintCount = cardDetail.PrintedCount;

                model.PrintDate = _dateConverter.ToBS(DateTime.Now);


                //Nepali
                model.CardNumberNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.CardNumber);
                if (cardDetail.IdCardType != null)
                {
                    var cardTypeEnum = (CardType)cardDetail.IdCardType;
                    model.CardTypeNepali = CardTypeEnumHandler.ConvertToNepali(cardTypeEnum);
                }
                model.NameNepali = DisabledPerson.FullNameNepali;
                model.AddressNepali = $"प्रदेश नं. १ जिल्ला झापा, भद्रपुर न.पा., वाड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.TempWardNumber)}";
                model.DateNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.DOBNepali);
                model.CitizenshipNumberNepali = DisabledPerson.CitizenshipNumberNepali;

                model.GenderNepali = GenderEnumHandler.ConvertToNepali(genderEnum);
                if (DisabledPerson.BloodGroup != null)
                {
                    var bloodGrpEnum = (BloodGroupEnum)DisabledPerson.BloodGroup;
                    model.BloodGroupNepali = BloodGroupEnumHandler.ConvertToNepali(bloodGrpEnum);
                }
                if (cardDetail.DisabilityTypeByNature != null)
                {
                    var disabilityNatureEnum = (DisbalityTypeOntheBasisOfNature)cardDetail.DisabilityTypeByNature;
                    model.OnTheBasisOfNatureNepali = DisabilityNatureEnumHandler.ConvertToNepali(disabilityNatureEnum);
                }
                if (cardDetail.DisabilityTypeBySeriousness != null)
                {
                    var disablitySeriousness = (DisabilityTypeOnTheBasisOfSeriousness)cardDetail.DisabilityTypeBySeriousness;
                    model.OnTheBasisOfSeriousnessNepali = disablitySeriousness.GetDisplayName();
                }
                model.GuardianNameNepali = Guardian.FullNameNepali;
                model.ApproverNameNepali = Staff.FullNameNepali; ;
                model.DesignationNepali = Staff.DesignationInfo.NameNepali;
                model.PrintCountNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(cardDetail.PrintedCount.ToString());
                model.PrintDateNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.PrintDateNepali);

                return model;

            }
            catch (Exception ex)
            {

                throw;
            }
        }




        public bool UpdateCardNumber(long CardId, string CardNumber)
        {
            try
            {
                var card = _cardDetailRepo.getById(CardId);
                if (card == null)
                    return false;
                card.IdCardnumber = CardNumber;
                _cardDetailRepo.update(card);
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }




        public ElderPersonCard GetElderPersonCardById(long Id)
        {
            try
            {
                var cardDetail = _cardDetailRepo.getQueryable().Include(a => a.GuardianInfo).Include(a => a.UserInfo).Include(a => a.DisabledPersonInfo).Where(a => a.Id == Id).FirstOrDefault();
                var DisabledPerson = cardDetail.DisabledPersonInfo;
                var Guardian = cardDetail.GuardianInfo;
                var Staff = cardDetail.UserInfo;
                ElderPersonCard model = new ElderPersonCard();
                model.Id = cardDetail.Id;
                if (string.IsNullOrEmpty(cardDetail.IdCardnumber))
                {
                    model.CardNumber = $"{DateTime.Now.Year}{cardDetail.Id}";
                }
                else
                {
                    model.CardNumber = cardDetail.IdCardnumber;
                }

                model.PrintDate = NepaliNumberConverter.ConvertDigitsToNepaliDigits(_dateConverter.ToBS(DateTime.Now));
                var genderEnum = (Gender)DisabledPerson.Gender;


                model.Photo = cardDetail.Photo;


                //Nepali
                model.CardNumberNepali = NepaliNumberConverter.ConvertDigitsToNepaliDigits(model.CardNumber);

                model.NameNepali = DisabledPerson.FullNameNepali;
                model.AddressNepali = $"प्रदेश नं. १ जिल्ला झापा, भद्रपुर न.पा., वाड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.TempWardNumber)}";
                model.CitizenshipNumberNepali = DisabledPerson.CitizenshipNumberNepali;

                model.GenderNepali = GenderEnumHandler.ConvertToNepali(genderEnum);

                if (cardDetail.CategoryOfElderness != null)
                {
                    var disabilityNatureEnum = (CategoryOfElderness)cardDetail.CategoryOfElderness;
                    model.CategoryOfElderness = disabilityNatureEnum.GetDisplayName();
                }
                if (DisabledPerson.ContactNumber != null)
                {
                    model.PhoneNumber = NepaliNumberConverter.ConvertDigitsToNepaliDigits(DisabledPerson.ContactNumber);
                }

                model.GuardianNameAddress = $"{Guardian.FullNameNepali}, {Guardian.TempAddressInfo.DistrictInfo.DistrictNameNepali} जिल्ला, वार्ड नं {NepaliNumberConverter.ConvertDigitsToNepaliDigits(Guardian.TempWardNumber)}";
                model.ApproverNameNepali = Staff.FullNameNepali;
                model.DesignationNepali = Staff.DesignationInfo.NameNepali;
                model.Facilities = cardDetail.OtherFacilities;
                model.SpouseName = DisabledPerson.SpouseName;
                var BloodGroupNepali = "";
                if (DisabledPerson.BloodGroup != null)
                {
                    var bloodGrpEnum = (BloodGroupEnum)DisabledPerson.BloodGroup;
                    BloodGroupNepali = BloodGroupEnumHandler.ConvertToNepali(bloodGrpEnum);
                }
                model.BloodGroupDiseaseMedicine = $"{BloodGroupNepali} {cardDetail.DiseaseName} {cardDetail.MedicineName}";

                return model;

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public void IncreaseCardPrintCount(long Id)
        {
            try
            {
                var card = _cardDetailRepo.getById(Id);
                if (!card.IsCardPrinted)
                {
                    card.FirstCardPrintedDate = DateTime.Now;
                }
                card.LastPrintDate = DateTime.Now;
                card.IsCardPrinted = true;
                card.PrintedCount++;
                _cardDetailRepo.update(card);
            }
            catch (Exception ex)
            {

                throw;
            }
        }





        public List<PieChartViewModel> GetDisabledCardQuantityOnTheBasisOfType()
        {
            try
            {
                List<PieChartViewModel> models = new List<PieChartViewModel>();
                var AllCardGroupByType = _cardDetailRepo.getQueryable().Where(x => x.CardCategory == (int)CardCategoryEnum.Disabled && x.IdCardType != null).Select(a => new { a.IdCardType, a.DisabledPersonId }).ToList().GroupBy(y => y.IdCardType).ToList();

                foreach (var data in AllCardGroupByType)
                {
                    PieChartViewModel model = new PieChartViewModel();
                    var cardTypeEnum = (CardType)data.Key;
                    model.DimensionOne = $"{CardTypeEnumHandler.ConvertToNepali(cardTypeEnum)} वर्ग";
                    model.Quantity = data.Count();
                    models.Add(model);
                }
                return models;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        //Private functions
        void SetPersonalDetail(CardDetailDTO entity, PersonalDetailDTO personDTO)
        {
            try
            {
                personDTO.Id = entity.Id;
                personDTO.CitizenshipNumber = entity.CitizenshipNumber;
                personDTO.CitizenshipNumberNepali = entity.CitizenshipNumberNepali;
                personDTO.DOB = entity.DOB;
                if (entity.DOB != null)
                    personDTO.DOBNepali = _dateConverter.ToBS(entity.DOB.Value);
                personDTO.FullName = entity.FullName;
                personDTO.FullNameNepali = entity.FullNameNepali;
                personDTO.Gender = entity.Gender;
                personDTO.PermanentAddressId = entity.PermanentAddressId;
                personDTO.PermanentTole = entity.PermanentTole;
                personDTO.PermanentWardNumber = entity.PermanentWardNumber;
                personDTO.TempAddressId = entity.TempAddressId;
                personDTO.TempTole = entity.TempTole;
                personDTO.TempWardNumber = entity.TempWardNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }

        void SetPersonalDetailFromCardBasicDetail(CardBasicDetailDTO entity, PersonalDetailDTO personDTO)
        {
            try
            {
                personDTO.Id = entity.DisabledPersonId;
                personDTO.CitizenshipNumber = entity.CitizenshipNumber;
                personDTO.CitizenshipNumberNepali = entity.CitizenshipNumberNepali;
                personDTO.DOBNepali = entity.DOBNepali;
                if (!string.IsNullOrEmpty(entity.DOBNepali))
                {
                    var dateWisthSlash = entity.DOBNepali.Replace("-", "-");
                    personDTO.DOB = _dateConverter.ToAD(dateWisthSlash);
                }

                personDTO.FullName = entity.FullName;
                personDTO.FullNameNepali = entity.FullNameNepali;
                personDTO.BloodGroup = entity.BloodGroup;
                personDTO.Gender = entity.Gender;
                personDTO.PermanentAddressId = entity.PermanentAddressId;
                personDTO.PermanentTole = entity.PermanentTole;
                personDTO.PermanentWardNumber = entity.PermanentWardNumber;
                personDTO.TempAddressId = entity.TempAddressId;
                personDTO.TempTole = entity.TempTole;
                personDTO.MarritalStatus = entity.MarritalStatus;
                personDTO.TempWardNumber = entity.TempWardNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }
        void SetPersonalDetailFromElderCardDetail(ElderCardSaveModel entity, PersonalDetailDTO personDTO)
        {
            try
            {
                personDTO.Id = entity.DisabledPersonId;
                personDTO.CitizenshipNumberNepali = entity.CitizenshipNumberNepali;
                personDTO.DOBNepali = entity.DOBNepali;
                if (entity.DOBNepali != null)
                {
                    var formattedDate = entity.DOBNepali.Replace("-", "-");
                    personDTO.DOB = _dateConverter.ToAD(formattedDate);
                }

                personDTO.FullNameNepali = entity.FullNameNepali;
                personDTO.Gender = entity.Gender;
                personDTO.BloodGroup = entity.BloodGroup;
                personDTO.ContactNumber = entity.ContactNumber;
                personDTO.MarritalStatus = entity.MarritalStatus;
                personDTO.PermanentAddressId = entity.TempAddressId;
                personDTO.SpouseName = entity.SpouseName;
                personDTO.TempAddressId = entity.TempAddressId;
                personDTO.TempTole = entity.TempTole;
                personDTO.TempWardNumber = entity.TempWardNumber;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        void SetGuardianDetail(CardDetailDTO entity, PersonalDetailDTO personDTO)
        {
            try
            {
                personDTO.Id = entity.GuardianId;
                personDTO.CitizenshipNumber = entity.GuardianCitizenshipNumber;
                personDTO.CitizenshipNumberNepali = entity.GuardianCitizenshipNumberNepali;
                personDTO.DOB = entity.GuardianDOB;
                if (entity.GuardianDOB != null)
                    personDTO.DOBNepali = _dateConverter.ToBS(entity.GuardianDOB.Value);
                personDTO.FullName = entity.GuardianFullName;
                personDTO.FullNameNepali = entity.GuardianFullNameNepali;
                personDTO.Gender = entity.GuardianGender;

                personDTO.TempAddressId = entity.GuardianTempAddressId;
                personDTO.TempTole = entity.GuardianTempTole;
                personDTO.TempWardNumber = entity.GuardianTempWardNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }
        void SetGuardianDetailFromCardBasicDetail(CardBasicDetailDTO entity, PersonalDetailDTO personDTO)
        {
            try
            {
                personDTO.Id = entity.GuardianId;
                personDTO.DOB = entity.GuardianDOB;
                if (entity.GuardianDOB != null)
                    personDTO.DOBNepali = _dateConverter.ToBS(entity.GuardianDOB.Value);
                personDTO.FullName = entity.GuardianFullName;
                personDTO.FullNameNepali = entity.GuardianFullNameNepali;
                personDTO.Gender = entity.GuardianGender;
                personDTO.TempAddressId = entity.GuardianTempAddressId;
                personDTO.PermanentAddressId = entity.GuardianTempAddressId;
                personDTO.TempTole = entity.GuardianTempTole;
                personDTO.TempWardNumber = entity.GuardianTempWardNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }
        void SetGuardianDetailFromElderDetail(ElderCardSaveModel entity, PersonalDetailDTO personDTO)
        {
            try
            {
                personDTO.Id = entity.GuardianId;
                personDTO.FullNameNepali = entity.GuardianFullNameNepali;
                personDTO.TempAddressId = entity.GuardianTempAddressId;
                personDTO.PermanentAddressId = personDTO.TempAddressId;
                personDTO.TempTole = entity.GuardianTempTole;
                personDTO.TempWardNumber = entity.GuardianTempWardNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }
        CardDetail MapCardBasicDetailDTOToEntity(CardBasicDetailDTO dto, CardDetail dbEntity)
        {
            try
            {
                dbEntity.Id = dto.Id;
                dbEntity.DisabledPersonId = dto.DisabledPersonId;
                dbEntity.GuardianId = dto.GuardianId;
                dbEntity.GuardianRelationWithCitizen = dto.GuardianRelationWithCitizen;
                dbEntity.Photo = dto.Photo;
                dbEntity.CardCategory = dto.CardCategory;
                dbEntity.DisabilityTypeByNature = dto.DisabilityTypeByNature;
                dbEntity.DisabilityTypeBySeriousness = dto.DisabilityTypeBySeriousness;
                dbEntity.IdCardType = dto.IdCardType.GetValueOrDefault();
                dbEntity.ApprovedBy = dto.ApprovedBy;
                return dbEntity;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        CardDetail MapElderCardDetailDTOToEntity(ElderCardSaveModel dto, CardDetail dbEntity)
        {
            try
            {
                dbEntity.Id = dto.Id;
                dbEntity.DisabledPersonId = dto.DisabledPersonId;
                dbEntity.GuardianId = dto.GuardianId;
                dbEntity.OtherFacilities = dto.OtherFacilities;
                dbEntity.CardCategory = dto.CardCategory;
                dbEntity.CategoryOfElderness = dto.CategoryOfElderness;
                dbEntity.MedicineName = dto.MedicineName;
                dbEntity.DiseaseName = dto.DiseaseName;
                dbEntity.ApprovedBy = dto.ApprovedBy;
                dbEntity.Photo = dto.Photo;
                return dbEntity;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        CardDetail MapCardAdditionalDTOToEntity(CardAdditionalDetailDTO dto, CardDetail dbEntity)
        {
            try
            {
                dbEntity.Id = dto.CardDetailId;
                dbEntity.Occupation = dto.Occupation;
                dbEntity.Qualification = dto.Qualification;
                dbEntity.CurrentSchoolCollegeName = dto.CurrentSchoolCollegeName;
                dbEntity.StudyingOrFinished = dto.StudyingOrFinished;
                dbEntity.WhatKindOfWorkcanDoInDailyLife = dto.WhatKindOfWorkcanDoInDailyLife;
                dbEntity.WhatKindOfWorkCantDo = dto.WhatKindOfWorkCantDo;
                if (dto.IsNeededAnyEquipement == (int)YesNo.Yes)
                {
                    dbEntity.IsNeededAnyEquipement = true;
                }
                else if (dto.IsNeededAnyEquipement == (int)YesNo.No)
                {
                    dbEntity.IsNeededAnyEquipement = false;
                }

                dbEntity.AdditionalEquipements = dto.AdditionalEquipements;
                if (dto.HasAnyAdditionalEquipement == (int)YesNo.Yes)
                {
                    dbEntity.HasAnyAdditionalEquipement = true;
                }
                else if (dto.HasAnyAdditionalEquipement == (int)YesNo.No)
                {
                    dbEntity.HasAnyAdditionalEquipement = false;
                }
                dbEntity.OtherFacilities = dto.OtherFacilities;
                dbEntity.OtherFacilitiesNeeded = dto.OtherFacilitiesNeeded;
                dbEntity.OfficeToProvideFacility = dto.OfficeToProvideFacility;
                dbEntity.Remarks = dto.Remarks;
                return dbEntity;


            }
            catch (Exception ex)
            {

                throw;
            }
        }
        CardDetail MapCardDetailDTOToEntity(CardDetailDTO dto, CardDetail dbEntity)
        {
            try
            {
                dbEntity.Id = dto.Id;
                dbEntity.DisabledPersonId = dto.DisabledPersonId;
                dbEntity.DisabilityTypeByNature = dto.DisabilityTypeByNature;
                dbEntity.DisabilityTypeBySeriousness = dto.DisabilityTypeBySeriousness;
                dbEntity.IdCardType = dto.IdCardType.GetValueOrDefault();
                dbEntity.Occupation = dto.Occupation;
                dbEntity.Qualification = dto.Qualification;
                dbEntity.CurrentSchoolCollegeName = dto.CurrentSchoolCollegeName;
                dbEntity.StudyingOrFinished = dto.StudyingOrFinished;
                dbEntity.WhatKindOfWorkcanDoInDailyLife = dto.WhatKindOfWorkcanDoInDailyLife;
                dbEntity.WhatKindOfWorkCantDo = dto.WhatKindOfWorkCantDo;
                if (dto.IsNeededAnyEquipement == (int)YesNo.Yes)
                {
                    dbEntity.IsNeededAnyEquipement = true;
                }
                else if (dto.IsNeededAnyEquipement == (int)YesNo.No)
                {
                    dbEntity.IsNeededAnyEquipement = false;
                }
                dbEntity.AdditionalEquipements = dto.AdditionalEquipements;
                if (dto.HasAnyAdditionalEquipement == (int)YesNo.Yes)
                {
                    dbEntity.HasAnyAdditionalEquipement = true;
                }
                else if (dto.HasAnyAdditionalEquipement == (int)YesNo.No)
                {
                    dbEntity.HasAnyAdditionalEquipement = false;
                }
                dbEntity.OtherFacilities = dto.OtherFacilities;
                dbEntity.OtherFacilitiesNeeded = dto.OtherFacilitiesNeeded;
                dbEntity.OfficeToProvideFacility = dto.OfficeToProvideFacility;
                dbEntity.Remarks = dto.Remarks;

                return dbEntity;


            }
            catch (Exception ex)
            {

                throw;
            }
        }
        CardDetailDTO MapCardDetailDbEntityToDTO(CardDetail dbEntity, CardDetailDTO dto)
        {
            try
            {
                dto.Id = dbEntity.Id;
                dto.CardDetailId = dbEntity.Id;
                dto.DisabledPersonId = dbEntity.DisabledPersonId;
                dto.GuardianId = dbEntity.GuardianId.GetValueOrDefault();
                dto.DisabilityTypeByNature = dbEntity.DisabilityTypeByNature.GetValueOrDefault();
                dto.DisabilityTypeBySeriousness = dbEntity.DisabilityTypeBySeriousness.GetValueOrDefault();
                dto.IdCardType = dbEntity.IdCardType;
                dto.Photo = dbEntity.Photo;
                dto.ApprovedBy = dbEntity.ApprovedBy;
                dto.Occupation = dbEntity.Occupation;
                dto.Qualification = dbEntity.Qualification;
                dto.GuardianRelationWithCitizen = dbEntity.GuardianRelationWithCitizen;
                dto.CurrentSchoolCollegeName = dbEntity.CurrentSchoolCollegeName;
                dto.StudyingOrFinished = dbEntity.StudyingOrFinished;
                dto.WhatKindOfWorkcanDoInDailyLife = dbEntity.WhatKindOfWorkcanDoInDailyLife;
                dto.WhatKindOfWorkCantDo = dbEntity.WhatKindOfWorkCantDo;
                if (dbEntity.IsNeededAnyEquipement == true)
                {
                    dto.IsNeededAnyEquipement = (int)YesNo.Yes;
                }
                else if (dbEntity.IsNeededAnyEquipement == false)
                {
                    dto.IsNeededAnyEquipement = (int)YesNo.No;
                }
                if (dbEntity.HasAnyAdditionalEquipement == true)
                {
                    dto.HasAnyAdditionalEquipement = (int)YesNo.Yes;
                }
                else if (dbEntity.HasAnyAdditionalEquipement == false)
                {
                    dto.HasAnyAdditionalEquipement = (int)YesNo.No;
                }
                dto.AdditionalEquipements = dbEntity.AdditionalEquipements;
                dto.OtherFacilities = dbEntity.OtherFacilities;
                dto.OtherFacilitiesNeeded = dbEntity.OtherFacilitiesNeeded;
                dto.OfficeToProvideFacility = dbEntity.OfficeToProvideFacility;
                dto.Remarks = dbEntity.Remarks;

                dto.FullName = dbEntity.DisabledPersonInfo.FullName;
                dto.FullNameNepali = dbEntity.DisabledPersonInfo.FullNameNepali;
                dto.Gender = dbEntity.DisabledPersonInfo.Gender;
                if (dbEntity.DisabledPersonInfo.TempAddressId != null)
                {
                    dto.TempAddressId = dbEntity.DisabledPersonInfo.TempAddressId.GetValueOrDefault();
                    dto.TempDistrictId = dbEntity.DisabledPersonInfo.TempAddressInfo.DistrictId;
                    dto.TempProvinceId = dbEntity.DisabledPersonInfo.TempAddressInfo.DistrictInfo.ProvinceId;
                }
                if (dbEntity.DisabledPersonInfo.PermanentAddressId != null)
                {
                    dto.PermanentAddressId = dbEntity.DisabledPersonInfo.PermanentAddressId.GetValueOrDefault();
                    dto.PermanentDistrictId = dbEntity.DisabledPersonInfo.PermanentAddressInfo.DistrictId;
                    dto.PermanentProvinceId = dbEntity.DisabledPersonInfo.PermanentAddressInfo.DistrictInfo.ProvinceId;
                }
                dto.TempTole = dbEntity.DisabledPersonInfo.TempTole;
                dto.PermanentTole = dbEntity.DisabledPersonInfo.PermanentTole;
                dto.TempWardNumber = dbEntity.DisabledPersonInfo.TempWardNumber;
                dto.PermanentWardNumber = dbEntity.DisabledPersonInfo.PermanentWardNumber;
                dto.DOBNepali = dbEntity.DisabledPersonInfo.DOBNepali;
                dto.CitizenshipNumber = dbEntity.DisabledPersonInfo.CitizenshipNumber;
                dto.CitizenshipNumberNepali = dbEntity.DisabledPersonInfo.CitizenshipNumberNepali;
                dto.BloodGroup = dbEntity.DisabledPersonInfo.BloodGroup;
                dto.MarritalStatus = dbEntity.DisabledPersonInfo.MarritalStatus;
                dto.GuardianFullName = dbEntity.DisabledPersonInfo.FullName;
                dto.GuardianFullNameNepali = dbEntity.DisabledPersonInfo.FullNameNepali;
                dto.GuardianGender = dbEntity.DisabledPersonInfo.Gender;
                if (dbEntity.GuardianInfo.TempAddressId != null)
                {
                    dto.GuardianTempAddressId = dbEntity.GuardianInfo.TempAddressId.GetValueOrDefault();
                    dto.GuardianTempDistrictId = dbEntity.GuardianInfo.TempAddressInfo.DistrictId;
                    dto.GuardianTempProvinceId = dbEntity.GuardianInfo.TempAddressInfo.DistrictInfo.ProvinceId;
                }
                dto.GuardianTempTole = dbEntity.DisabledPersonInfo.TempTole;
                dto.GuardianTempWardNumber = dbEntity.DisabledPersonInfo.TempWardNumber;
                dto.GuardianDOB = dbEntity.DisabledPersonInfo.DOB;
                dto.GuardianCitizenshipNumber = dbEntity.DisabledPersonInfo.CitizenshipNumber;
                dto.GuardianCitizenshipNumberNepali = dbEntity.DisabledPersonInfo.CitizenshipNumberNepali;
                dto.GuardianBloodGroup = dbEntity.DisabledPersonInfo.BloodGroup;
                dto.GuardianMarritalStatus = dbEntity.DisabledPersonInfo.MarritalStatus;
                return dto;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        ElderCardDetailDTO MapCardDetailDbEntityToElderCardDTO(CardDetail dbEntity, ElderCardDetailDTO dto)
        {
            try
            {
                dto.Id = dbEntity.Id;
                dto.DisabledPersonId = dbEntity.DisabledPersonId;
                dto.GuardianId = dbEntity.GuardianId.GetValueOrDefault();
                dto.CardCategory = dbEntity.CardCategory;
                dto.CategoryOfElderness
                    = dbEntity.CategoryOfElderness;

                dto.ApprovedBy = dbEntity.ApprovedBy;
                dto.Photo = dbEntity.Photo;
                dto.OtherFacilities = dbEntity.OtherFacilities;
                dto.DiseaseName = dbEntity.DiseaseName;
                dto.MedicineName = dbEntity.MedicineName;
                dto.FullNameNepali = dbEntity.DisabledPersonInfo.FullNameNepali;
                dto.Gender = dbEntity.DisabledPersonInfo.Gender;
                dto.SpouseName = dbEntity.DisabledPersonInfo.SpouseName;
                dto.ContactNumber = dbEntity.DisabledPersonInfo.ContactNumber;
                if (dbEntity.DisabledPersonInfo.TempAddressId != null)
                {
                    dto.TempAddressId = dbEntity.DisabledPersonInfo.TempAddressId.GetValueOrDefault();
                    dto.TempDistrictId = dbEntity.DisabledPersonInfo.TempAddressInfo.DistrictId;
                    dto.TempProvinceId = dbEntity.DisabledPersonInfo.TempAddressInfo.DistrictInfo.ProvinceId;
                }

                dto.TempTole = dbEntity.DisabledPersonInfo.TempTole;
                dto.TempWardNumber = dbEntity.DisabledPersonInfo.TempWardNumber;
                dto.DOBNepali = dbEntity.DisabledPersonInfo.DOBNepali;
                dto.CitizenshipNumberNepali = dbEntity.DisabledPersonInfo.CitizenshipNumberNepali;
                dto.BloodGroup = dbEntity.DisabledPersonInfo.BloodGroup;
                dto.MarritalStatus = dbEntity.DisabledPersonInfo.MarritalStatus;

                dto.GuardianFullNameNepali = dbEntity.GuardianInfo.FullNameNepali;
                if (dbEntity.GuardianInfo.TempAddressId != null)
                {
                    dto.GuardianTempAddressId = dbEntity.GuardianInfo.TempAddressId.GetValueOrDefault();
                    dto.GuardianTempDistrictId = dbEntity.GuardianInfo.TempAddressInfo.DistrictId;
                    dto.GuardianTempProvinceId = dbEntity.GuardianInfo.TempAddressInfo.DistrictInfo.ProvinceId;
                }
                dto.GuardianTempTole = dbEntity.GuardianInfo.TempTole;
                dto.GuardianTempWardNumber = dbEntity.GuardianInfo.TempWardNumber;

                return dto;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        int GetAge(DateTime DOB)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - DOB.Year;
            if (now < DOB.AddYears(age)) age--;
            return age;
        }


        public CardDetailReturnModel AddUpdateCardDetail(CardDetailDTO entity)
        {
            try
            {
                PersonalDetailDTO personalDetail = new PersonalDetailDTO();
                PersonalDetailDTO Guardiandetail = new PersonalDetailDTO();
                CardDetailReturnModel dataToBeReturned = new CardDetailReturnModel();
                if (entity.Id == 0)
                {

                    SetPersonalDetail(entity, personalDetail);

                    entity.DisabledPersonId = _personalDetailService.AddPersonalDetail(personalDetail);
                    CardDetail dbEntity = new CardDetail();

                    SetPersonalDetail(entity, Guardiandetail);
                    Guardiandetail.PermanentAddressId = Guardiandetail.TempAddressId;
                    entity.GuardianId = _personalDetailService.AddPersonalDetail(Guardiandetail);
                    SetGuardianDetail(entity, Guardiandetail);
                    entity.PermanentAddressId = Guardiandetail.TempAddressId;
                    dbEntity = MapCardDetailDTOToEntity(entity, dbEntity);
                    _cardDetailRepo.insert(dbEntity);
                    dataToBeReturned.CardId = dbEntity.Id;

                }
                else
                {
                    var disabledperson = _personalDetailService.FindById(entity.DisabledPersonId);
                    var guardian = _personalDetailService.FindById(entity.DisabledPersonId);
                    if (disabledperson == null || guardian == null)
                        throw new ItemNotFoundException();
                    SetPersonalDetail(entity, personalDetail);
                    SetPersonalDetail(entity, Guardiandetail);
                    entity.PermanentAddressId = Guardiandetail.TempAddressId;
                    _personalDetailService.UpdatePersonalDetail(personalDetail);
                    _personalDetailService.UpdatePersonalDetail(Guardiandetail);
                    CardDetail dbEntity = _cardDetailRepo.getById(entity.Id);
                    MapCardDetailDTOToEntity(entity, dbEntity);
                    _cardDetailRepo.update(dbEntity);
                    dataToBeReturned.CardId = dbEntity.Id;
                }
                return dataToBeReturned;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public CardDetailReturnModel AddUpdateBasicDetail(CardBasicDetailDTO entity)
        {
            try
            {
                PersonalDetailDTO personalDetail = new PersonalDetailDTO();
                PersonalDetailDTO Guardiandetail = new PersonalDetailDTO();
                CardDetailReturnModel dataToBeReturned = new CardDetailReturnModel();
                if (entity.Id == 0)
                {
                    SetPersonalDetailFromCardBasicDetail(entity, personalDetail);
                    entity.DisabledPersonId = _personalDetailService.AddPersonalDetail(personalDetail);
                    CardDetail dbEntity = new CardDetail();
                    SetGuardianDetailFromCardBasicDetail(entity, Guardiandetail);
                    Guardiandetail.PermanentAddressId = Guardiandetail.TempAddressId;
                    entity.GuardianId = _personalDetailService.AddPersonalDetail(Guardiandetail);
                    entity.PermanentAddressId = Guardiandetail.TempAddressId;
                    dbEntity = MapCardBasicDetailDTOToEntity(entity, dbEntity);
                    dbEntity.EntryDate = DateTime.Now;
                    _cardDetailRepo.insert(dbEntity);
                    dataToBeReturned.CardId = dbEntity.Id;
                    dataToBeReturned.DisabledPersonId = dbEntity.DisabledPersonId;
                    dataToBeReturned.GuardianId = dbEntity.GuardianId.GetValueOrDefault();
                }
                else
                {
                    var disabledperson = _personalDetailService.FindById(entity.DisabledPersonId);
                    var guardian = _personalDetailService.FindById(entity.DisabledPersonId);
                    if (disabledperson == null || guardian == null)
                        throw new ItemNotFoundException();
                    SetPersonalDetailFromCardBasicDetail(entity, personalDetail);
                    SetGuardianDetailFromCardBasicDetail(entity, Guardiandetail);
                    _personalDetailService.UpdatePersonalDetail(personalDetail);
                    _personalDetailService.UpdatePersonalDetail(Guardiandetail);
                    CardDetail dbEntity = _cardDetailRepo.getById(entity.Id);
                    MapCardBasicDetailDTOToEntity(entity, dbEntity);
                    _cardDetailRepo.update(dbEntity);
                    dataToBeReturned.CardId = dbEntity.Id;
                    dataToBeReturned.DisabledPersonId = dbEntity.DisabledPersonId;
                    dataToBeReturned.GuardianId = dbEntity.GuardianId.GetValueOrDefault();
                }
                return dataToBeReturned;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public CardDetailReturnModel AddUpdateElderCard(ElderCardSaveModel entity)
        {
            try
            {
                PersonalDetailDTO personalDetail = new PersonalDetailDTO();
                PersonalDetailDTO Guardiandetail = new PersonalDetailDTO();
                CardDetailReturnModel dataToBeReturned = new CardDetailReturnModel();
                if (entity.Id == 0)
                {

                    SetPersonalDetailFromElderCardDetail(entity, personalDetail);
                    entity.DisabledPersonId = _personalDetailService.AddPersonalDetail(personalDetail);
                    CardDetail dbEntity = new CardDetail();
                    SetGuardianDetailFromElderDetail(entity, Guardiandetail);
                    Guardiandetail.PermanentAddressId = Guardiandetail.TempAddressId;
                    entity.GuardianId = _personalDetailService.AddPersonalDetail(Guardiandetail);
                    dbEntity = MapElderCardDetailDTOToEntity(entity, dbEntity);
                    dbEntity.EntryDate = DateTime.Now;
                    _cardDetailRepo.insert(dbEntity);
                    dataToBeReturned.CardId = dbEntity.Id;
                    dataToBeReturned.DisabledPersonId = dbEntity.DisabledPersonId;
                    dataToBeReturned.GuardianId = dbEntity.GuardianId.GetValueOrDefault();

                }
                else
                {
                    var disabledperson = _personalDetailService.FindById(entity.DisabledPersonId);
                    var guardian = _personalDetailService.FindById(entity.DisabledPersonId);
                    if (disabledperson == null || guardian == null)
                        throw new ItemNotFoundException();
                    SetPersonalDetailFromElderCardDetail(entity, personalDetail);
                    SetGuardianDetailFromElderDetail(entity, Guardiandetail);
                    _personalDetailService.UpdatePersonalDetail(personalDetail);
                    _personalDetailService.UpdatePersonalDetail(Guardiandetail);
                    CardDetail dbEntity = _cardDetailRepo.getById(entity.Id);
                    MapElderCardDetailDTOToEntity(entity, dbEntity);
                    _cardDetailRepo.update(dbEntity);
                    dataToBeReturned.CardId = dbEntity.Id;
                    dataToBeReturned.DisabledPersonId = dbEntity.DisabledPersonId;
                    dataToBeReturned.GuardianId = dbEntity.GuardianId.GetValueOrDefault();
                }
                return dataToBeReturned;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
