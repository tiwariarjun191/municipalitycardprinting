﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Services.Implementation
{
    public class FattService: IFattService
    {
        private readonly IBaseRepository<Fatt> _fattRepository;
        public FattService(IBaseRepository<Fatt> _fattRepository)
        {
            this._fattRepository = _fattRepository;
        }
        public void AddFatt(FattDto fattDto)
        {
            try
            {
                var data = MapDtoToEntity(fattDto, new Fatt());
                _fattRepository.insert(data);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public void UpdateFatt(FattDto fattDto)
        {
            try
            {
                var entity = _fattRepository.getById(fattDto.id);
                var data = MapDtoToEntity(fattDto, entity);
                _fattRepository.update(data);
            }
            catch (Exception)
            {

                throw;
            }
            
        }


        public void DeleteFatt(FattDto fattDto)
        {
            try
            {
               
                var entity = _fattRepository.getById(fattDto.id);
                if (entity == null)
                    throw new ItemNotFoundException();
                _fattRepository.delete(entity);
                
            }
            catch (Exception)
            {

                throw;
            }
        }


        public FattDto GetByFattId(long id)
        {
            var data = _fattRepository.getById(id);
            var fattdto = new FattDto();
            MapEntityToDto(fattdto,data);
            return fattdto;
        }

        public Fatt MapDtoToEntity(FattDto fattDto,Fatt fatt)
        {
            fatt.id = fattDto.id; 
            fatt.FattName = fattDto.FattName;
            return fatt;
        }

        public FattDto MapEntityToDto(FattDto fattDto, Fatt fatt)
        {
            fattDto.id= fatt.id;
            fattDto.FattName = fatt.FattName;
            return fattDto;
        }

        public List<FattDto> getAllFattDetails()  
        {
            var data = _fattRepository.getQueryable().ToList();
            var Details = new List<FattDto>();
            foreach(var Item in data)
            {
                var mapdata = MapEntityToDto(new FattDto(),Item);
                Details.Add(mapdata);
            }

            return Details;

        }



    }
}
