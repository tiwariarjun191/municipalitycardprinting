﻿using Business.Exceptions;
using Business.Services.Interface;
using DTO;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Services.Implementation
{

    
    public class ShrediService:IShrediService
    {
        private readonly IBaseRepository<Shredi> _shrediRepository;

        public ShrediService(IBaseRepository<Shredi> shrediRepository)
        {
            _shrediRepository = shrediRepository;
        }
        public void AddShredi(ShrediDto dto)
        {

            try
            {
                var data = MapFormDtoToEntity(new Shredi(), dto);
                _shrediRepository.insert(data);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        public void UpdateShredi(ShrediDto dto)
        {
            try
            {
                var data = _shrediRepository.getById(dto.Id);
                var mappeddata = MapFormDtoToEntity(data, dto);
                _shrediRepository.update(mappeddata);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void DeleteShredi(long id)
        {
            var data = _shrediRepository.getById(id);
            if (data == null)
                throw new ItemNotFoundException();
            _shrediRepository.delete(data);
        }

        public ShrediDto GetByShrediId(long id)
        {
            var entity = _shrediRepository.getById(id);
           var data= MapFromEntityToDto(entity, new ShrediDto());
            return data;
        }

        public List<ShrediDto> GetAllShrediDetails()
        {
            var data = _shrediRepository.getQueryable().ToList();
            var list = new List<ShrediDto>();
            foreach(var item in data)
            {
                var mappeddata = MapFromEntityToDto(item, new ShrediDto());
                list.Add(mappeddata);
            }
            return list;
        }




        public Shredi MapFormDtoToEntity(Shredi entity,ShrediDto dto)
        {
            entity.Id = dto.Id;
            entity.SherdiEnglish = dto.SherdiEnglish;
            entity.SherdiNepali = dto.SherdiNepali;
            return entity;
        } 

        public ShrediDto MapFromEntityToDto(Shredi entity,ShrediDto dto)
        {
            dto.Id = entity.Id;
            dto.SherdiEnglish = entity.SherdiEnglish;
            dto.SherdiNepali = entity.SherdiNepali;
            return dto;
        }

    }
}
