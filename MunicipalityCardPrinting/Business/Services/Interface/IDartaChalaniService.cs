﻿using DTO;
using Infrastructure.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IDartaChalaniService
    {
        bool AddUpdateDarta(DartaDto model);

        bool AddUpdateChalani(ChalaniDto model);

        DartaDto GetDartaDtoById(long id);
        ChalaniDto GetChalaniDtoById(long id);

        void DeleteDarta(long id);
        void DeleteChalani(long id);

        List<Darta> GetDartaDetailsOfSevenDays();
        List<Chalani> GetChalaniDetailsOfSevenDays();

    }
}
