﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
   public  interface IShrediService
    {
        void AddShredi(ShrediDto dto);
        void UpdateShredi(ShrediDto dto);
        void DeleteShredi(long id);
        ShrediDto GetByShrediId(long id);
        List<ShrediDto> GetAllShrediDetails();
    }
}
