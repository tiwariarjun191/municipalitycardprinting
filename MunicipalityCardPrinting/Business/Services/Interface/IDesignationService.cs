﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
   public interface IDesignationService
    {

        void AddDesignation(DesignationDto fattDto);

        void UpdateDesignation(DesignationDto fattDto);
    }
}
