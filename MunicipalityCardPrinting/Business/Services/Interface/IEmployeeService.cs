﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IEmployeeService
    {
        void AddUpdateEmployee(EmployeeDto dto);

        void DeleteEmployee(long id);

        EmployeeDto GetByEmployeeId(long id);
        List<EmployeeDto> GetAllEmployee();
        List<DropdownModel> GetAllShrediDropdown();
        List<DropdownModel> GetAllEmployeeDropdown();

        List<DropdownModel> GetAllDesignationDropdown();
    }
}
