﻿using DTO.Children;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IOldChildCardService
    {

        long AddUpdateOldChildCardDetail(OldChildDetailDto entity);
        OldChildDetailDto GetOldChildCardDtoById(long id);
        OldChildCard GetOldChildernCardById(long Id);

        List<OldChildDetailDto> GetAllOldChildernCardDetail();
        void IncreaseOldChildCardPrintCount(long Id);

        bool UpdateOldChildCardNumber(long CardId);

    }
}
