﻿using DTO;
using Infrastructure.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IPersonalDetailService
    {
        long AddPersonalDetail(PersonalDetailDTO entity);
        void UpdatePersonalDetail(PersonalDetailDTO entity);
        PersonalDetail FindById(long Id);
        void DeletePersonalDetail(int Id);
    }
}
