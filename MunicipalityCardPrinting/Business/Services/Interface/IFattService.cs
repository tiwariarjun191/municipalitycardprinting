﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IFattService
    {

        void AddFatt(FattDto fattDto);
        void UpdateFatt(FattDto fattDto);
        void DeleteFatt(FattDto fattDto);

        FattDto GetByFattId(long id);

        List<FattDto> getAllFattDetails();
    }
}


