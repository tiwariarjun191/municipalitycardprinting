﻿using Infrastructure.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IExpiryServices
    {
        Expirydate Getexpirydates(long id);
    }
}
