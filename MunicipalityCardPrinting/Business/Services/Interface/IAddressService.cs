﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface IAddressService
    {
        public void AddProvince(ProvinceDTO entity);
        public void UpdateProvince(ProvinceDTO entity);
        public void DeleteProvince(int Id);
        public bool ProvinceNameAlreadyExist(string Name, long Id);
        public void AddDistrict(DistrictDTO entity);
        public void UpdateDistrict(DistrictDTO entity);
        public void DeleteDistrict(int Id);
        List<DropdownModel> GetAllProvincedropdown();
        List<DropdownModel> GetAllDistrictDropdown();
        List<DropdownModel> GetAllVdcDropdown();
        List<DropdownModel> GetDistrictDropDownFromProvinceId(long ProvinceId);
        List<DropdownModel> GetMunVdcDropDownFromProvinceId(long DistrictId);
        public bool DistrictNameAlreadyExist(string Name, long Id);
        public void AddVdc(MunicipalityVDCDTO entity);
        public void UpdateVdc(MunicipalityVDCDTO entity);
        public void DeleteVdc(int Id);

        List<DropdownModel> GetAllfaatDropdown();
    }
}
