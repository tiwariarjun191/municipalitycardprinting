﻿using DTO;
using DTO.Children;
using Infrastructure.Database.Models;
using Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services.Interface
{
    public interface ICardService
    {
        

        ChildBasicDetailDTO GetChildCardDtoById(long id);
        
        HomeViewModel GetAllDataForHome();
       
        long AddUpdateChildCardDetail(ChildBasicDetailDTO entity);

        ChildernCard GetChildernCardById(long Id);

        List<ChildBasicDetailDTO> GetAllChildernCardDetail();

        void IncreaseChildCardPrintCount(long Id);

        bool UpdateChildCardNumber(long CardId);



        CardDetailReturnModel AddUpdateCardDetail(CardDetailDTO entity);
        CardDetailReturnModel AddUpdateBasicDetail(CardBasicDetailDTO entity);
        CardDetailReturnModel AddUpdateCardAdditionalDetail(CardAdditionalDetailDTO entity);
        CardDetailDTO GetCardDetailByCardDetailId(long CardDetailId);
        List<DisabledCardViewModel> AllDisabledCards();
        CardDetailReturnModel AddUpdateElderCard(ElderCardSaveModel entity);
        ElderCardDetailDTO GetElderCardDetail(long CardDetailId);
        List<ElderCardViewModel> AllElderCards();
        
        DisabledPersonCard GetDisabledPersonCardById(long Id);
        ElderPersonCard GetElderPersonCardById(long Id);
        DisabledIndividualReport GetDisabledIndividualReport(long Id);
        void IncreaseCardPrintCount(long Id);
        DisabledProgressReportWrapper GetDisabledPersonProgressReport();
        List<PrintedDisabledList> AllPrintedElderCards(int Category);
        List<PieChartViewModel> GetDisabledCardQuantityOnTheBasisOfType();

        bool UpdateCardNumber(long CardId, string CardNumber);
    }
}
