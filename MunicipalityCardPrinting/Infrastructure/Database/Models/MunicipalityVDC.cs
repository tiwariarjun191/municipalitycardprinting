﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class MunicipalityVDC
    {
        public long Id { get; set; }
        [ForeignKey("DistrictInfo")]
        [Required]
        public long DistrictId { get; set; }
        public string VDCName { get; set; }
        public string VDCNameNepali { get; set; }
        public string Description { get; set; }
        public District DistrictInfo { get; set; }
    }
}
