﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class PersonalDetail
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string FullNameNepali { get; set; }
        public int Gender { get; set; }
        [ForeignKey("TempAddressInfo")]
        [Required]
        public long TempAddressId { get; set; }
        [ForeignKey("PermanentAddressInfo")]
        [Required]
        public long PermanentAddressId { get; set; }
        public string TempTole { get; set; }
        public string PermanentTole { get; set; }
        public string TempWardNumber { get; set; }
        public string PermanentWardNumber { get; set; }
        public DateTime DOB { get; set; }
        public string DOBNepali { get; set; }
        public string CitizenshipNumber { get; set; }
        public int? BloodGroup { get; set; }
        public int? MarritalStatus { get; set; }
        
        public bool IsDeleted { get; set; }
        public MunicipalityVDC TempAddressInfo { get; set; }
        public MunicipalityVDC PermanentAddressInfo { get; set; }
    }
}
