﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class District
    {
        public long Id { get; set; }
        [ForeignKey("ProvinceInfo")]
        [Required]
        public long ProvinceId { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameNepali { get; set; }
        public string HeadQuarter { get; set; }
        public string Description { get; set; }
        public Province ProvinceInfo { get; set; }
        public virtual List<MunicipalityVDC> Municipalities { get; set; }
    }
}
