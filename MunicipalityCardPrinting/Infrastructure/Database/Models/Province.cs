﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Province
    {
        public long Id { get; set; }
        public virtual string ProvinceName { get; set; }
        public virtual string ProvinceNameNepali { get; set; }
        public virtual string Capital { get; set; }
        public virtual string Description { get; set; }

        public virtual List<District> Districts { get; set; }
    }
}
