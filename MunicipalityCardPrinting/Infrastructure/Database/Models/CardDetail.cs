﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class CardDetail
    {
        public long Id { get; set; }
        public long DisabledPersonId { get; set; }
        public int DisabilityTypeByNature { get; set; }
        public int DisabilityTypeBySeriousness { get; set; }
        public long? GuardianId { get; set; }
        public string Occupation { get; set; }
        public string Qualification { get; set; }
        public string CurrentSchoolCollegeName { get; set; }
        public string StudyingOrFinished { get; set; }
        public string WhatKindOfWorkcanDoInDailyLife{get; set;}
        public string WhatKindOfWorkCantDo{get; set;}
        public bool? IsNeededAnyEquipement{get; set;}
        public string AdditionalEquipements{get; set;}
        public bool? HasAnyAdditionalEquipement{get; set;}
        public string OtherFacilities{get; set;}
        public string OtherFacilitiesNeeded{get; set;}
        public string OfficeToProvideFacility{get; set;}
        public string Remarks{get; set;}
    }
}
