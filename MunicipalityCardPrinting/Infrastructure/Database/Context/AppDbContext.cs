﻿using Infrastructure.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Database.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<CardDetail> CardDetails { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<MunicipalityVDC> MunicipalityVDCs { get; set; }
        public DbSet<PersonalDetail> PersonalDetails { get; set; }
        public DbSet<Province> Provinces { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=DESKTOP-GLHFE5N\SQLEXPRESS; initial catalog=HotelManagement; integrated security=true", b => b.MigrationsAssembly("Context"));
        }
    }
}
