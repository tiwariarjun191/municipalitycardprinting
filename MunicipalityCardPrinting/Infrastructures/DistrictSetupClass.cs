﻿using Infrastructure.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public static  class DistrictSetupClass
    {
        public static List<District> setup()
        {
            List<District> DistrictList = new List<District>();

            DistrictList.Add(new District() { Id = 1, DistrictName = "भोजपुर", DistrictNameNepali = "Bhojpur", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 2, DistrictName = "धनकुटा", DistrictNameNepali = "Dhankuta", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 3, DistrictName = "इलाम", DistrictNameNepali = "Ilam", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 4, DistrictName = "झापा", DistrictNameNepali = "Jhapa", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 5, DistrictName = "खोटाँग", DistrictNameNepali = "Khotang", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 6, DistrictName = "मोरंग", DistrictNameNepali = "Morang", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 7, DistrictName = "ओखलढुंगा", DistrictNameNepali = "Okhaldhunga", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 8, DistrictName = "पांचथर", DistrictNameNepali = "Panchthar", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 9, DistrictName = "संखुवासभा", DistrictNameNepali = "Sankhuwasabha", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 10, DistrictName = "सोलुखुम्बू", DistrictNameNepali = "Solukhumbu", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 11, DistrictName = "सुनसरी", DistrictNameNepali = "Sunsari ", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 12, DistrictName = "ताप्लेजुंग", DistrictNameNepali = "Taplejung", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 13, DistrictName = "तेह्रथुम", DistrictNameNepali = "Terhathum", ProvinceId = 1 });
            DistrictList.Add(new District() { Id = 14, DistrictName = "उदयपुर", DistrictNameNepali = "Udayapur", ProvinceId = 1 });


            DistrictList.Add(new District() { Id = 15, DistrictName = "सप्तरी", DistrictNameNepali = "Saptari", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 16, DistrictName = "सिराहा", DistrictNameNepali = "Siraha", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 17, DistrictName = "धनुषा", DistrictNameNepali = "Dhanusa", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 18, DistrictName = "महोत्तरी", DistrictNameNepali = "Mahottari", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 19, DistrictName = "सर्लाही", DistrictNameNepali = "Sarlahi", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 20, DistrictName = "बारा", DistrictNameNepali = "Bara", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 21, DistrictName = "पर्सा", DistrictNameNepali = "Parsa", ProvinceId = 2 });
            DistrictList.Add(new District() { Id = 22, DistrictName = "रौतहट", DistrictNameNepali = "Rautahat", ProvinceId = 2 });


            DistrictList.Add(new District() { Id = 23, DistrictName = "सिन्धुली", DistrictNameNepali = "Sindhuli", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 24, DistrictName = "रामेछाप", DistrictNameNepali = "Ramechhap", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 25, DistrictName = "दोलखा", DistrictNameNepali = "Dolakha", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 26, DistrictName = "भक्तपुर", DistrictNameNepali = "Bhaktapur", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 27, DistrictName = "धादिङ", DistrictNameNepali = "Dhading", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 28, DistrictName = "काठमाडौँ", DistrictNameNepali = "Kathmandu", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 29, DistrictName = "काभ्रेपलान्चोक", DistrictNameNepali = "Kavrepalanchok", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 30, DistrictName = "ललितपुर", DistrictNameNepali = "Lalitpur", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 31, DistrictName = "नुवाकोट", DistrictNameNepali = "Nuwakot", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 32, DistrictName = "रसुवा", DistrictNameNepali = "Rasuwa", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 33, DistrictName = "सिन्धुपाल्चोक", DistrictNameNepali = "Sindhupalchok", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 34, DistrictName = "चितवन", DistrictNameNepali = "Chitwan", ProvinceId = 3 });
            DistrictList.Add(new District() { Id = 35, DistrictName = "मकवानपुर", DistrictNameNepali = "Makwanpur", ProvinceId = 3 });


            DistrictList.Add(new District() { Id = 36, DistrictName = "बागलुङ", DistrictNameNepali = "Baglung", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 37, DistrictName = "गोरखा", DistrictNameNepali = "Gorkha", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 38, DistrictName = "कास्की", DistrictNameNepali = "Kaski", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 39, DistrictName = "लमजुङ", DistrictNameNepali = "Lamjung", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 40, DistrictName = "मनाङ", DistrictNameNepali = "Manang", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 41, DistrictName = "मुस्ताङ", DistrictNameNepali = "Mustang", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 42, DistrictName = "म्याग्दी", DistrictNameNepali = "Myagdi", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 43, DistrictName = "नवलपुर", DistrictNameNepali = "Nawalpur", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 44, DistrictName = "पर्वत", DistrictNameNepali = "Parbat", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 45, DistrictName = "स्याङग्जा", DistrictNameNepali = "Syangja", ProvinceId = 4 });
            DistrictList.Add(new District() { Id = 46, DistrictName = "तनहुँ", DistrictNameNepali = "Tanahun", ProvinceId = 4 });


            DistrictList.Add(new District() { Id = 47, DistrictName = "कपिलवस्तु", DistrictNameNepali = "Kapilvastu", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 48, DistrictName = "परासी", DistrictNameNepali = "Parasi", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 49, DistrictName = "रुपन्देही", DistrictNameNepali = "Rupandehi", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 50, DistrictName = "अर्घाखाँची", DistrictNameNepali = "Arghakhanchi", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 51, DistrictName = "गुल्मी", DistrictNameNepali = "Gulmi", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 52, DistrictName = "पाल्पा", DistrictNameNepali = "Palpa", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 53, DistrictName = "दाङ देउखुरी", DistrictNameNepali = "Dang Deukhuri", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 54, DistrictName = "प्युठान", DistrictNameNepali = "Pyuthan", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 55, DistrictName = "रोल्पा", DistrictNameNepali = "Rolpa", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 56, DistrictName = "पूर्वी रूकुम", DistrictNameNepali = "Eastern Rukum", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 57, DistrictName = "बाँके", DistrictNameNepali = "Banke", ProvinceId = 5 });
            DistrictList.Add(new District() { Id = 58, DistrictName = "बर्दिया", DistrictNameNepali = "Bardiya", ProvinceId = 5 });


            DistrictList.Add(new District() { Id = 59, DistrictName = "पश्चिमी रूकुम", DistrictNameNepali = "Western Rukum", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 60, DistrictName = "सल्यान", DistrictNameNepali = "Salyan", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 61, DistrictName = "डोल्पा", DistrictNameNepali = "Dolpa", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 62, DistrictName = "हुम्ला", DistrictNameNepali = "Humla", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 63, DistrictName = "जुम्ला", DistrictNameNepali = "Jumla", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 64, DistrictName = "कालिकोट", DistrictNameNepali = "Kalikot", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 65, DistrictName = "मुगु", DistrictNameNepali = "Mugu", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 66, DistrictName = "सुर्खेत", DistrictNameNepali = "Surkhet", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 67, DistrictName = "दैलेख", DistrictNameNepali = "Dailekh", ProvinceId = 6 });
            DistrictList.Add(new District() { Id = 68, DistrictName = "जाजरकोट", DistrictNameNepali = "Jajarkot", ProvinceId = 6 });


            DistrictList.Add(new District() { Id = 69, DistrictName = "कैलाली", DistrictNameNepali = "Kailali", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 70, DistrictName = "अछाम", DistrictNameNepali = "Achham", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 71, DistrictName = "डोटी", DistrictNameNepali = "Doti", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 72, DistrictName = "बझाङ", DistrictNameNepali = "Bajhang", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 73, DistrictName = "बाजुरा", DistrictNameNepali = "Bajura", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 74, DistrictName = "कंचनपुर", DistrictNameNepali = "Kanchanpur", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 75, DistrictName = "डडेलधुरा", DistrictNameNepali = "Dadeldhura", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 76, DistrictName = "बैतडी", DistrictNameNepali = "Baitadi", ProvinceId = 7 });
            DistrictList.Add(new District() { Id = 77, DistrictName = "दार्चुला", DistrictNameNepali = "Darchula", ProvinceId = 7 });

            return DistrictList;
        }
        
    }
}
