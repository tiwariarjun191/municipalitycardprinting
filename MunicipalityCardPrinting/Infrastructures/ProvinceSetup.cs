﻿using Infrastructure.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public static class ProvinceSetup
    {

        public static List<Province> setup()
        {
            List<Province> ProvinceList = new List<Province>();

            ProvinceList.Add(new Province() { Id = 1, ProvinceName = "Province No. 1", ProvinceNameNepali = "प्रदेश नं. १" });
            ProvinceList.Add(new Province() { Id = 2, ProvinceName = "Province No. 2", ProvinceNameNepali = "प्रदेश नं. २" });
            ProvinceList.Add(new Province() { Id = 3, ProvinceName = "Province No. 3", ProvinceNameNepali = "प्रदेश नं. ३" });
            ProvinceList.Add(new Province() { Id = 4, ProvinceName = "Gandaki Pradesh", ProvinceNameNepali = "गण्डकी प्रदेश" });
            ProvinceList.Add(new Province() { Id = 5, ProvinceName = "Province No. 5", ProvinceNameNepali = "प्रदेश नं. ५" });
            ProvinceList.Add(new Province() { Id = 6, ProvinceName = "Karnali Pradesh", ProvinceNameNepali = "कर्णाली प्रदेश" });
            ProvinceList.Add(new Province() { Id = 7, ProvinceName = "Sudurpashchim Pradesh", ProvinceNameNepali = "सुदूर-पश्चिम प्रदेश" });

            return ProvinceList;
        }
       
    }
}
