﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Infrastructure.Database.Models
{
    public class Darta
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameNepali { get; set; }
        public string Subject { get; set; }
        public DateTime? DartadateEnglish { get; set; } = DateTime.Now;
        public string Dartadate { get; set; }
        public DateTime? EnglishDateofReceivedLetter { get; set; }
        public string DateofReceivedLetter { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }//internationl address
        public string DartaNo { get; set; }
        public string ReferenceNo { get; set; }        
        public string ReceivedBy { get; set; }

        [ForeignKey("ProvinceInfo")]
        public long? AddressId { get; set; }

        [ForeignKey("Fatt")]
        public long? Faat { get; set; }
        [ForeignKey("AdressInfo")]
        public long? Vdc { get; set; }
        public string Ward { get; set; }
        public string Photo { get; set; }
        public string Remarks { get; set; }


        public virtual Fatt Fatt { get; set; }
        public virtual MunicipalityVDC AdressInfo { get; set; }


    }
}
