﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Employee
    {
        public long Id { get; set; }
        public string Employee_Name_English { get; set; }
        public string Employee_Name_Nepali { get; set; }
        public int Gender { get; set; }
        public string DOB { get; set; }
        public string Photo { get; set; }
        public long District_Province_Vdc_Id { get; set; }
        public string Ward { get; set; }
       public string Office { get; set; }

        public long Shredi_Id { get; set; }
        public long Designation_Id { get; set; }
        public string Sanket_Number { get; set; }

        public string Office_Entry_Date { get; set; }

        [ForeignKey(nameof(Shredi_Id))]
        public virtual Shredi ShrediInfo { get; set; }


        [ForeignKey(nameof(Designation_Id))]
        public virtual Designation DesignationInfo { get; set; }

        [ForeignKey(nameof(District_Province_Vdc_Id))]
        public virtual MunicipalityVDC AddressInfo { get; set; }

        public virtual List<ChildDetail> Employees { get; set; }
        public virtual List<ChildDetail> ApprovedBy { get; set; }

        public virtual List<OldChildDetails> OldEmployees { get; set; }
        public virtual List<OldChildDetails> OldApprovedBy { get; set; }
    }
}
