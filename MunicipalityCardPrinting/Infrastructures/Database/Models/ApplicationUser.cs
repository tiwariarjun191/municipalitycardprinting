﻿using DTO.Children;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class ApplicationUser:IdentityUser
    {
        public bool IsDeleted { get; set; }
        public string WardNumber { get; set; }
        public string FullName { get; set; }
        public string FullNameNepali { get; set; }
        [ForeignKey("DesignationInfo")]
        [Required]
        public long DesignationId { get; set; }
        public virtual Designation DesignationInfo { get; set; }

        public virtual List<CardDetail> CardsApproved { get; set; }
        public virtual List<ChildernCard> ChildernCards { get; set; }
        public virtual List<OldChildCard> OldChildernCards { get; set; }
    }
}
