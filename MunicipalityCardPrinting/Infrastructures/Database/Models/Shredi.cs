﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Shredi
    {
        public long Id { get; set; }
        public string SherdiEnglish { get; set; }
        public string SherdiNepali { get; set; }
    }
}
