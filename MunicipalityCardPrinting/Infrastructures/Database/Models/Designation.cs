﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Designation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameNepali { get; set; }
        public virtual List<ApplicationUser> ApplicationUsers { get; set; }
        
       
    }
}
