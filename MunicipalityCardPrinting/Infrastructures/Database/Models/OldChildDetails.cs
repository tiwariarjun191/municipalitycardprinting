﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class OldChildDetails
    {

        public long Id { get; set; }
        public string CardNumber { get; set; }
        public string CardNumberNepali { get; set; }
        public string NameNepali { get; set; }
        public string NameEnglish { get; set; }

        [Display(Name = "स्थाई नगर/गाउँपालिका")]
        public long? PermanentVDCId { get; set; }
        [Display(Name = "स्थाई वार्ड")]
        public string PermanentWard { get; set; }

        [Display(Name = "अस्थाई प्रदेश")]
        [Required]
        public long? TemporaryProvinceId { get; set; }
        [Display(Name = "अस्थाई नगर/गाउँपालिका")]
        public long? TemporaryVDCId { get; set; }
        [Display(Name = "अस्थाई वार्ड")]
        public string TemporaryWard { get; set; }
        public string GenderNepali { get; set; }
        public string FatherNameNepali { get; set; }
        public string FatherNameEnglish { get; set; }
        public string FatherAddressNepali { get; set; }
        public string FatherAddressEnglish { get; set; }
        public string MotherNameNepali { get; set; }
        public string MotherNameEnglish { get; set; }
        public string MotherAddressEnglish { get; set; }
        public string MotherAddressNepali { get; set; }
        public int Gender { get; set; }
        public string Age { get; set; }
        public string Photo { get; set; }
        public string PrintDate { get; set; }
        public int PrintCountNepali { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedByNepali { get; set; }
        public string FatherMotherCitizenship { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IsCardPrinted { get; set; }

        public string UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual ApplicationUser UserInfo { get; set; }
        public DateTime? LastPrintDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey(nameof(TemporaryVDCId))]
        public virtual MunicipalityVDC TempAddressInfo { get; set; }
        [ForeignKey(nameof(PermanentVDCId))]
        public virtual MunicipalityVDC PermanentAddressInfo { get; set; }
        public long ApprovedById { get; set; }

        [ForeignKey(nameof(ApprovedById))]
        public virtual Employee Approved_By_Details { get; set; }

        [ForeignKey(nameof(Employee_Id))]
        public virtual Employee Employee { get; set; }

        public long Employee_Id { get; set; }
        public string Employee_Address { get; set; }


        public string FatherCitizen { get; set; }
        public string MotherCitizen { get; set; }
        public string BirthCertificate { get; set; }
        public string other { get; set; }

        public virtual List<OldAddMoreSection> AddMoreSections { get; set; } = new List<OldAddMoreSection>();

        public class OldAddMoreSection
        {
            public long id { get; set; }
            public long childDetailId { get; set; }
            public string File { get; set; }
            public string TextArea { get; set; }

            [ForeignKey(nameof(childDetailId))]
            public virtual OldChildDetails ChildDetail { get; set; }
        }
    }

}

