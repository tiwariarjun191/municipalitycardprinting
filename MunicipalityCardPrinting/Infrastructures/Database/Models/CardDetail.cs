﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class CardDetail
    {
        public long Id { get; set; }
        [Required]
        public long DisabledPersonId { get; set; }
        public int? DisabilityTypeByNature { get; set; }
        public int? IdCardType { get; set; }
        public string IdCardnumber { get; set; }
        public int? DisabilityTypeBySeriousness { get; set; }
        public long? GuardianId { get; set; }
        public string Occupation { get; set; }
        public string Qualification { get; set; }
        public string CurrentSchoolCollegeName { get; set; }
        public string StudyingOrFinished { get; set; }
        public string WhatKindOfWorkcanDoInDailyLife { get; set; }
        public string WhatKindOfWorkCantDo { get; set; }
        public bool? IsNeededAnyEquipement { get; set; }
        public string AdditionalEquipements { get; set; }
        public bool? HasAnyAdditionalEquipement { get; set; }
        public string OtherFacilities { get; set; }
        public string OtherFacilitiesNeeded { get; set; }
        public string OfficeToProvideFacility { get; set; }
        public string Remarks { get; set; }
        public int CardCategory { get; set; }
        public int? CategoryOfElderness { get; set; }
        public string DiseaseName { get; set; }
        public string MedicineName { get; set; }
        public bool IsCardPrinted { get; set; }
        public int PrintedCount { get; set; }
        public string ApprovedBy { get; set; }
        public string Photo { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? FirstCardPrintedDate { get; set; }
        public virtual PersonalDetail DisabledPersonInfo { get; set; }
        public virtual PersonalDetail GuardianInfo { get; set; }
        public virtual ApplicationUser UserInfo { get; set; }
        public DateTime? LastPrintDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string GuardianRelationWithCitizen { get; set; }
    }
}
