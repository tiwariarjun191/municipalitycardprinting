﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Chalani
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameNepali { get; set; }
        public string Subject { get; set; }
        public string ChalanidateNepali { get; set; }
        public DateTime Chalanidate { get; set; } = DateTime.Now;
        public DateTime? EnglishDateofReceivedLetter { get; set; }
        public string DateofReceivedLetter { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string ChalaniNo { get; set; }
        public string ReferenceNo { get; set; }
        public string ReceivedBy { get; set; }

        [ForeignKey("ProvinceInfo")]
        public long? AddressId { get; set; }
        [ForeignKey("AdressInfo")]
        public long? Vdc { get; set; }
        public string Ward { get; set; }
        public string Photo { get; set; }
        public string Remarks { get; set; }
        public virtual MunicipalityVDC AdressInfo { get; set; }


    }
}
