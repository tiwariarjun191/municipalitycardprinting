﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Sequence
    {
        [Key]
        public long id { get; set; }
        [MaxLength(50)]
        public string Type { get; set; }
        public long Value { get; set; }
    }
}
