﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class MunicipalityVDC
    {
        public long Id { get; set; }
        [Required]
        public long DistrictId { get; set; }
        public string VDCName { get; set; }
        public string VDCNameNepali { get; set; }
        public string Description { get; set; }
        [ForeignKey(nameof(DistrictId))]
        public virtual District DistrictInfo { get; set; }
        public virtual List<PersonalDetail> PersonTempAdress { get; set; }
        public virtual List<PersonalDetail> PersonPermanentAddress { get; set; }
        public virtual List<ChildDetail> ChildTempAddress { get; set; }
       
        public virtual List<ChildDetail> ChildPermanentAddress { get; set; }

        public virtual List<OldChildDetails> OldChildTempAddress { get; set; }
        public virtual List<OldChildDetails> OldChildPermanentAddress { get; set; }
        public virtual List<Darta> DartaDetailAddress { get; set; }
        public virtual List<Chalani> ChalaniDetailAddress { get; set; }
    }
}
