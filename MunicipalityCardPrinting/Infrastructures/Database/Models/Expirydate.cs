﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Database.Models
{
    public class Expirydate
    {
        [Key]
        public long EXPID { get; set; }
        public DateTime Expirydate_From {get; set;}
        public DateTime Expirydate_To {get; set;}
    }
}
