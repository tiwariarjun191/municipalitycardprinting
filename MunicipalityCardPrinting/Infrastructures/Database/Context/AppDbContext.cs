﻿using Infrastructure.Database.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Database.Context
{
    public class AppDbContext : IdentityDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<ApplicationUser> AspNetUsers { get; set; }
        public DbSet<CardDetail> CardDetails { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Darta> Dartas { get; set; }
        public DbSet<Chalani> Chalanis { get; set; }
        public DbSet<Sequence> Sequence { get; set; }
        public DbSet<Fatt> Fatt { get; set; }
        public DbSet<Shredi> Shredi { get; set; }
        public DbSet<Employee> Employee { get; set; }

        public DbSet<MunicipalityVDC> MunicipalityVDCs { get; set; }
        public DbSet<PersonalDetail> PersonalDetails { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<ChildDetail> ChildDetails { get; set; }
        public DbSet<OldChildDetails> OldChildDetails { get; set; }
        public DbSet<Expirydate> Expirydates { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //  optionsBuilder.UseSqlServer(@"data source=DESKTOP-GLHFE5N\SQLEXPRESS; initial catalog=CardPrinting; integrated security=true", b => b.MigrationsAssembly("Context"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<PersonalDetail>()
                .HasOne(m => m.TempAddressInfo)
                .WithMany(t => t.PersonTempAdress)
                .HasForeignKey(m => m.TempAddressId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<PersonalDetail>()
              .HasOne(m => m.PermanentAddressInfo)
              .WithMany(t => t.PersonPermanentAddress)
              .HasForeignKey(m => m.PermanentAddressId)
              .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<CardDetail>()
                .HasOne(m => m.DisabledPersonInfo)
                .WithMany(t => t.DisabledPersons)
                .HasForeignKey(m => m.DisabledPersonId)
                .OnDelete(DeleteBehavior.Restrict);

           
            modelBuilder.Entity<ChildDetail>()
               .HasOne(m => m.TempAddressInfo)
               .WithMany(t => t.ChildTempAddress)
               .HasForeignKey(m => m.TemporaryVDCId)
               .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<ChildDetail>()
              .HasOne(m => m.Employee)
              .WithMany(t => t.Employees)
              .HasForeignKey(m => m.Employee_Id)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ChildDetail>()
             .HasOne(m => m.Approved_By_Details)
             .WithMany(t => t.ApprovedBy)
             .HasForeignKey(m => m.ApprovedById)
             .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OldChildDetails>()
            .HasOne(m => m.Employee)
            .WithMany(t => t.OldEmployees)
            .HasForeignKey(m => m.Employee_Id)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OldChildDetails>()
             .HasOne(m => m.Approved_By_Details)
             .WithMany(t => t.OldApprovedBy)
             .HasForeignKey(m => m.ApprovedById)
             .OnDelete(DeleteBehavior.Restrict);




            modelBuilder.Entity<ChildDetail>()
              .HasOne(m => m.PermanentAddressInfo)
              .WithMany(t => t.ChildPermanentAddress)
              .HasForeignKey(m => m.PermanentVDCId)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OldChildDetails>()
              .HasOne(m => m.TempAddressInfo)
              .WithMany(t => t.OldChildTempAddress)
              .HasForeignKey(m => m.TemporaryVDCId)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OldChildDetails>()
              .HasOne(m => m.PermanentAddressInfo)
              .WithMany(t => t.OldChildPermanentAddress)
              .HasForeignKey(m => m.PermanentVDCId)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CardDetail>()
                .HasOne(m => m.DisabledPersonInfo)
                .WithMany(t => t.DisabledPersons)
                .HasForeignKey(m => m.DisabledPersonId)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<CardDetail>()
              .HasOne(m => m.GuardianInfo)
              .WithMany(t => t.Gauardians)
              .HasForeignKey(m => m.GuardianId)
              .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<CardDetail>()
              .HasOne(m => m.UserInfo)
              .WithMany(t => t.CardsApproved)
              .HasForeignKey(m => m.ApprovedBy)
              .OnDelete(DeleteBehavior.Restrict);
            var AllProvince = ProvinceSetup.setup();
            var AllDistrict = DistrictSetupClass.setup();
            var AllVdcs = VdcSetup.Setup();
            modelBuilder.Entity<Province>().HasData(AllProvince);
            modelBuilder.Entity<District>().HasData(AllDistrict);
            modelBuilder.Entity<MunicipalityVDC>().HasData(AllVdcs);

            var expirydate = new Expirydate()
            {
                EXPID=1,
                Expirydate_From = DateTime.Now,
                Expirydate_To = DateTime.Now.AddYears(1)
            };

            var designation = new Designation()
            {
                Id = 2,
                Name = "Assistant",
                NameNepali = "सहयोगी"
            };
            var role1 = new IdentityRole
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Admin",
                NormalizedName = "Admin"

            };
            var role2 = new IdentityRole
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Supervisor",
                NormalizedName = "SuperVisor"

            };
            var role3 = new IdentityRole
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Maker",
                NormalizedName = "Maker"

            };
            var role4 = new IdentityRole
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Approver",
                NormalizedName = "Approver"

            };
            modelBuilder.Entity<IdentityRole>().HasData(new List<IdentityRole>
            {
             role1,role2,role3,role4
            });
            var hasher = new PasswordHasher<ApplicationUser>();
            modelBuilder.Entity<Designation>().HasData(
             designation
            );

            modelBuilder.Entity<Expirydate>().HasData(expirydate);

            var user1 = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(), // primary key
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                PasswordHash = hasher.HashPassword(null, "Pass@word1"),
                IsDeleted = false,
                Email = "Admin@admin.com",
                DesignationId = designation.Id,
                FullName = "Admin",
                FullNameNepali = "एडमिन"
            };
            modelBuilder.Entity<ApplicationUser>().HasData(
            user1
            );
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    UserId = user1.Id,
                    RoleId = role1.Id
                }
                );
            base.OnModelCreating(modelBuilder);


        }
    }
}
