﻿using Infrastructure.Database.Context;
using Infrastructure.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementation
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly AppDbContext _context;
        public BaseRepository(AppDbContext appDbContext)
        {
            _context = appDbContext;
        }

        public void delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public void deleteRange(List<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
            _context.SaveChanges();
        }

        //public void bulkInsert(List<T> entities, Action<BulkOperation<T>> options)
        //{
        //    _context.BulkInsert(entities, options);
        //    _context.SaveChanges();
        //}

        public List<T> getAll()
        {
            return _context.Set<T>().ToList();
        }

        public T getById(long id)
        {
            return _context.Set<T>().Find(id);
        }

        public void insert(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
        }

        public void insertRange(List<T> entities)
        {
            _context.Set<T>().AddRange(entities);
            _context.SaveChanges();
        }

        public void update(T entity)
        {
            _context.SaveChanges();
        }

        public void updateRange(List<T> entities)
        {
            _context.SaveChanges();
        }

        public List<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? _context.Set<T>().AsNoTracking().ToList() : _context.Set<T>().ToList();
        }

        public List<T> Get(Expression<Func<T, bool>> filterExpression = null, Expression<Func<T, object>> orderExpression = null)
        {
            IQueryable<T> query = _context.Set<T>();
            if (filterExpression != null)
                query = query.Where(filterExpression);
            if (orderExpression != null)
                query = query.OrderBy(orderExpression);
            return query.ToList();
        }
        public List<T> GetWithMultipleInclude(Expression<Func<T, bool>> filterExpression = null, Expression<Func<T, object>> orderExpression = null, bool orderDescending = false, string includeProperties = null, int? skip = null,
            int? take = null, bool asNoTracking = false)
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<T> query = asNoTracking ? _context.Set<T>().AsNoTracking() : _context.Set<T>();

            if (filterExpression != null)
                query = asNoTracking ? query.Where(filterExpression).AsNoTracking() : query.Where(filterExpression);
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = asNoTracking ? query.Include(includeProperty).AsNoTracking() : query.Include(includeProperty);
            }
            if (orderExpression != null)
            {
                if (orderDescending)
                {
                    query = query.OrderByDescending(orderExpression);
                }
                else
                {
                    query = query.OrderBy(orderExpression);
                }
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }
            return query.ToList();
        }

        public Task<T> FirstOrDefaultAsync()
        {
            return _context.Set<T>().FirstOrDefaultAsync();
        }

        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().FirstOrDefaultAsync(expression);
        }

        public Task<List<T>> GetAllAsync(bool asNoTracking = false)
        {
            return asNoTracking ? _context.Set<T>().AsNoTracking().ToListAsync() : _context.Set<T>().ToListAsync();
        }

        public Task<List<T>> GetAsync(Expression<Func<T, bool>> filterExpression = null, Expression<Func<T, object>> orderExpression = null)
        {
            IQueryable<T> query = _context.Set<T>();

            if (filterExpression != null)
                query = query.Where(filterExpression);

            if (orderExpression != null)
                query = query.OrderBy(orderExpression);
            return query.ToListAsync();
        }

        public Task<List<T>> GetWithMultipleIncludeAsync(Expression<Func<T, bool>> filterExpression = null, Expression<Func<T, object>> orderExpression = null, bool orderDescending = false,
            string includeProperties = null, int? skip = null, int? take = null, bool asNoTracking = false)
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<T> query = _context.Set<T>();
            if (filterExpression != null)
                query = query.Where(filterExpression);
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }


            if (asNoTracking)
                query = query.AsNoTracking();
            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }
            if (orderExpression != null)
            {
                if (orderDescending)
                {
                    query = query.OrderByDescending(orderExpression);
                }
                else
                {
                    query = query.OrderBy(orderExpression);
                }
            }
            return query.ToListAsync();
        }

        public IQueryable<T> getQueryable()
        {
            return _context.Set<T>();
        }

        public IQueryable<T> getQueryableDownTheHierarchy(long organisation_id, long? company_id = null, long? branch_id = null)
        {
            IQueryable<T> queryable = _context.Set<T>();

            ParameterExpression pe = Expression.Parameter(typeof(T), "a");

            string organisationIdKey = "ORGANISATION_ID";
            string companyKey = "COMPANY_ID";
            string branchKey = "BRANCH_ID";

            Expression predicateBody = Expression.Equal(Expression.Constant(1), Expression.Constant(1));

            foreach (var prop in typeof(T).GetProperties())
            {
                if (branch_id.HasValue)
                {
                    if (prop.Name.Equals(branchKey))
                    {
                        var left = Expression.Property(pe, typeof(T).GetProperty(branchKey));
                        var right = Expression.Convert(Expression.Constant(branch_id), prop.PropertyType);
                        predicateBody = Expression.And(Expression.Equal(left, right), predicateBody);
                    }
                }
                else if (company_id.HasValue)
                {
                    if (prop.Name.Equals(companyKey))
                    {
                        var left = Expression.Property(pe, typeof(T).GetProperty(companyKey));
                        var right = Expression.Convert(Expression.Constant(company_id), prop.PropertyType);
                        predicateBody = Expression.And(Expression.Equal(left, right), predicateBody);

                    }
                }
                else if (prop.Name.Equals(organisationIdKey))
                {
                    var left = Expression.Property(pe, typeof(T).GetProperty(organisationIdKey));
                    var right = Expression.Convert(Expression.Constant(organisation_id), prop.PropertyType);
                    predicateBody = Expression.And(Expression.Equal(left, right), predicateBody);
                }
            }

            MethodCallExpression whereCallExpression = Expression.Call(
                typeof(Queryable),
                "Where",
                new Type[] { queryable.ElementType },
                queryable.Expression,
                Expression.Lambda<Func<T, bool>>(predicateBody, new ParameterExpression[] { pe }));

            return queryable.Provider.CreateQuery<T>(whereCallExpression);
        }
    }
}
