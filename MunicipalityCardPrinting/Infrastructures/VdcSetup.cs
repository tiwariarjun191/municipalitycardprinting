﻿using Infrastructure.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public static class VdcSetup
    {
        public static List<MunicipalityVDC> Setup()
        {
            List<MunicipalityVDC> vdcmunlist = new List<MunicipalityVDC>();
            //bhojpur
            vdcmunlist.Add(new MunicipalityVDC() { Id = 24, VDCName = "Shadanand Municipality", VDCNameNepali = "षडानन्द नगरपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 25, VDCName = "Bhojpur Municipality", VDCNameNepali = "भोजपुर नगरपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 167, VDCName = "Hatuwagadhi Rural Municipality", VDCNameNepali = "हतुवागढी गाउँपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 168, VDCName = "Ramprasad Rai Rural Municipality", VDCNameNepali = "रामप्रसाद राई गाउँपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 169, VDCName = "Aamchok Rural Municipality", VDCNameNepali = "आमचोक गाउँपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 170, VDCName = "Tyamke Maiyunm Rural Municipality", VDCNameNepali = "ट्याम्केमैयुम गाउँपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 171, VDCName = "Arun Gaunpalika Rural Municipality", VDCNameNepali = "अरुण गाउँपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 172, VDCName = "Pauwadungma Rural Municipality", VDCNameNepali = "पौवादुङमा गाउँपालिका", DistrictId = 1 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 173, VDCName = "Salpasilichho Rural Municipality", VDCNameNepali = "साल्पासिलिछो गाउँपालिका", DistrictId = 1 });

            //dhankuta
            vdcmunlist.Add(new MunicipalityVDC() { Id = 26, VDCName = "Dhankuta Municipality", VDCNameNepali = "धनकुटा नगरपालिका", DistrictId = 2 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 27, VDCName = "Mahalaxmi Municipality", VDCNameNepali = "महालक्ष्मी नगरपालिका", DistrictId = 2 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 115, VDCName = "Pakhribas Municipality", VDCNameNepali = "पाख्रिवास नगरपालिका", DistrictId = 2 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 174, VDCName = "Sangurigadhi Rural Municipality", VDCNameNepali = "सागुरीगढी गाउँपालिका", DistrictId = 2 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 175, VDCName = "Chaubise Rural Municipality", VDCNameNepali = "चौविसे गाउँपालिका", DistrictId = 2 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 176, VDCName = "Khalsa Chhintang Sahidbhumi Rural Municipality", VDCNameNepali = "खाल्सा छिन्ताङ सहीदभूमि गाउँपालिका", DistrictId = 2 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 177, VDCName = "Chhathar Jorpati Rural Municipality", VDCNameNepali = "छथर जोरपाटी गाउँपालिका", DistrictId = 2 });


            //ilam
            vdcmunlist.Add(new MunicipalityVDC() { Id = 20, VDCName = "Suryodaya Municipality", VDCNameNepali = "सूर्योदय नगरपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 21, VDCName = "Ilam Municipality", VDCNameNepali = "इलाम नगरपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 22, VDCName = "Deumai Municipality", VDCNameNepali = "देउमाई नगरपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 23, VDCName = "Mai Municipality", VDCNameNepali = "माई नगरपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 178, VDCName = "Phakphokthum Rural Municipality", VDCNameNepali = "फाकफोकथुम गाउँपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 179, VDCName = "Mai Jogmai Rural Municipality", VDCNameNepali = "माईजोगमाई गाउँपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 180, VDCName = "Chulachuli Rural Municipality", VDCNameNepali = "चुलाचुली गाउँपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 181, VDCName = "Rong Rural Municipality", VDCNameNepali = "रोङ गाउँपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 182, VDCName = "Mangsebung Rural Municipality", VDCNameNepali = "माङसेबुङ गाउँपालिका", DistrictId = 3 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 183, VDCName = "Sandakpur Rural Municipality", VDCNameNepali = "सन्दकपुर गाउँपालिका", DistrictId = 3 });


            //jhapa
            vdcmunlist.Add(new MunicipalityVDC() { Id = 12, VDCName = "Mechinagar Municipality", VDCNameNepali = "मेची नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 13, VDCName = "BirtamodMunicipality", VDCNameNepali = "विर्तामोड नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 14, VDCName = "Damak Municipality", VDCNameNepali = "दमक नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 15, VDCName = "Bhadrapur Municipality", VDCNameNepali = "भद्रपुर नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 16, VDCName = "Shivasatakshi Municipality", VDCNameNepali = "शिवसताक्षि नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 17, VDCName = "Arjundhara Municipality", VDCNameNepali = "अर्जुनधारा नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 18, VDCName = "Gauradaha Municipality", VDCNameNepali = "गौरादह नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 19, VDCName = "Gauriganga Municipality", VDCNameNepali = "कन्काई नगरपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 184, VDCName = "Kamal Rural Municipality", VDCNameNepali = "कमल गाउँपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 185, VDCName = "Gaurigunj Rural Municipality", VDCNameNepali = "गौरीगन्ज गाउँपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 186, VDCName = "Barhadashi Rural Municipality", VDCNameNepali = "बर्हदाशी गाउँपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 187, VDCName = "Jhapa Rural Municipality", VDCNameNepali = "झापा गाउँपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 188, VDCName = "Buddhashanti Rural Municipality", VDCNameNepali = "बुद्धशान्ति गाउँपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 189, VDCName = "Haldibari Rural Municipality", VDCNameNepali = "हल्दिबारी गाउँपालिका", DistrictId = 4 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 190, VDCName = "Kachankawal Rural Municipality", VDCNameNepali = "कचंकवल गाउँपालिका", DistrictId = 4 });


            //khotang
            vdcmunlist.Add(new MunicipalityVDC() { Id = 28, VDCName = "Rupakot Majhuwagadhi Municipality", VDCNameNepali = "रुपाकोट–मजुवागढी नगर पालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 29, VDCName = "Halesi Tuwachung Municipality", VDCNameNepali = "हलेसी तुवाचुङ नगर पालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 191, VDCName = "Khotehang Rural Municipality", VDCNameNepali = "खोटेहांग गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 192, VDCName = "Diprung Rural Municipality", VDCNameNepali = "दिप्रुंग गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 193, VDCName = "Aiselukharka Rural Municipality", VDCNameNepali = "ऐसेलुखर्क गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 194, VDCName = "Jantedhunga Rural Municipality", VDCNameNepali = "जन्तेदुंगा गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 195, VDCName = "Kepilasgadhi Rural Municipality", VDCNameNepali = "केपिलास्गढ़ी गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 196, VDCName = "Barahpokhari Rural Municipality", VDCNameNepali = "बार्हपोखरी गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 197, VDCName = "Lamidanda Rural Municipality", VDCNameNepali = "लमीडाडा गाउँपालिका", DistrictId = 5 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 198, VDCName = "Sakela Rural Municipality", VDCNameNepali = "साकेला गाउँपालिका", DistrictId = 5 });


            //Morang
            vdcmunlist.Add(new MunicipalityVDC() { Id = 55, VDCName = "Biratnagar Municipality", VDCNameNepali = "विराटनगर महानगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 30, VDCName = "Sundar Haraincha Municipality", VDCNameNepali = "सुन्दरहरैंचा नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 31, VDCName = "Belbari Municipality", VDCNameNepali = "बेलवारी नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 32, VDCName = "Pathari-Shanischare Municipality", VDCNameNepali = "पथरी शनिश्चरे नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 33, VDCName = "Ratuwamai Municipality", VDCNameNepali = "रतुवामाई नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 34, VDCName = "Urlabari Municipality", VDCNameNepali = "उर्लावारी नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 35, VDCName = "Rangeli Municipality", VDCNameNepali = "रंगेली नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 36, VDCName = "Sunawarshi Municipality", VDCNameNepali = "सुनवर्षी नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 37, VDCName = "Letang Bhogateni Municipality", VDCNameNepali = "लेटाङ नगरपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 199, VDCName = "Kerabari Rural Municipality", VDCNameNepali = "केराबारी गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 200, VDCName = "Miklajung Rural Municipality", VDCNameNepali = "मिक्लाजुंग गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 201, VDCName = "Kanepokhari Rural Municipality", VDCNameNepali = "कानेपोखरी गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 202, VDCName = "Budiganga Rural Municipality", VDCNameNepali = "बुडिगंगा गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 203, VDCName = "Gramthan Rural Municipality", VDCNameNepali = "ग्रम्थान गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 204, VDCName = "Katahari Rural Municipality", VDCNameNepali = "कटहरी गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 205, VDCName = "Dhampalthan Rural Municipality", VDCNameNepali = "धमपल्थान गाउँपालिका", DistrictId = 6 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 206, VDCName = "Jahada Rural Municipality", VDCNameNepali = "जहदा गाउँपालिका", DistrictId = 6 });


            //okhaldhunga
            vdcmunlist.Add(new MunicipalityVDC() { Id = 38, VDCName = " 	Siddhicharan Municipality", VDCNameNepali = "सिद्दिचरण नगर पालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 346, VDCName = "Manebhanjyang Municipality", VDCNameNepali = "मानेभञ्ज्याङ गाउँपालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 347, VDCName = "Champadevi Municipality", VDCNameNepali = "चम्पादेवी गाउँपालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 348, VDCName = "Sunkoshi Rural Municipality", VDCNameNepali = "सुनकोशी गाउँपालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 349, VDCName = "Molung Rural Municipality", VDCNameNepali = "मोलुङ गाउँपालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 350, VDCName = "Chisankhugadhi Rural Municipality", VDCNameNepali = "चिसंखुगढी गाउँपालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 351, VDCName = "Khiji Demba Rural Municipality", VDCNameNepali = "खिजिदेम्बा गाउँपालिका", DistrictId = 7 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 352, VDCName = "Likhu Rural Municipality", VDCNameNepali = "लिखु गाउँपालिका", DistrictId = 7 });

            //paanchthar
            vdcmunlist.Add(new MunicipalityVDC() { Id = 39, VDCName = "Phidim Municipality", VDCNameNepali = "फिदिम नगर पालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 207, VDCName = "Hilihang Rural Municipality ", VDCNameNepali = "हिलिहाङ गाउँपालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 208, VDCName = "Kummayak Rural Municipality", VDCNameNepali = "कुम्मायाक गाउँपालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 209, VDCName = "Miklajung Rural Municipality", VDCNameNepali = "मिक्लाजुंग गाउँपालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 210, VDCName = "Phalelung Rural Municipality", VDCNameNepali = "फालेलुंग गाउँपालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 211, VDCName = "Phalgunanda Rural Municipality", VDCNameNepali = "फाल्गुनन्द गाउँपालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 212, VDCName = "Tumbewa Rural Municipality", VDCNameNepali = "तुम्वेवा गाउँपालिका", DistrictId = 8 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 213, VDCName = "Yangawarak Rural Municipality", VDCNameNepali = "याङवरक गाउँपालिका", DistrictId = 8 });

            //Sankhuwasabha
            vdcmunlist.Add(new MunicipalityVDC() { Id = 40, VDCName = "Khandbari Municipality", VDCNameNepali = "खादँवारी नगर पालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 41, VDCName = "Chainpur Municipality", VDCNameNepali = "चैनपुर नगर पालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 42, VDCName = "Dharmadevi Municipality", VDCNameNepali = "धर्मदेवी नगर पालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 43, VDCName = "Panchkhapan Municipality", VDCNameNepali = "पाँचखपन नगर पालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 44, VDCName = "Madi Municipality", VDCNameNepali = "मादी नगर पालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 353, VDCName = "Makalu Rural Municipality", VDCNameNepali = "मकालु गाउँपालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 354, VDCName = "Silichong Rural Municipality", VDCNameNepali = "सिलीचोङ गाउँपालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 355, VDCName = "Sabhapokhari Rural Municipality", VDCNameNepali = "सभापोखरी गाउँपालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 356, VDCName = "Chichila Rural Municipality", VDCNameNepali = "चिचिला गाउँपालिका", DistrictId = 9 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 357, VDCName = "Bhot Khola Rural Municipality", VDCNameNepali = "भोटखोला गाउँपालिका", DistrictId = 9 });

            //Solukhumbu
            vdcmunlist.Add(new MunicipalityVDC() { Id = 45, VDCName = "Solu Dudhkunda Municipality", VDCNameNepali = "सोलुदुधकुण्ड नगर पालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 358, VDCName = "Dudhakaushika Rural Municipality", VDCNameNepali = "दुधकौशिका गाउँपालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 359, VDCName = "Necha Salyan Rural Municipality", VDCNameNepali = "नेचासल्यान गाउँपालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 360, VDCName = "Dudhkoshi Rural Municipality", VDCNameNepali = "दुधकोशी गाउँपालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 361, VDCName = "Maha Kulung Rural Municipality", VDCNameNepali = "महाकुलुङ गाउँपालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 362, VDCName = "Sotang Rural Municipality", VDCNameNepali = "सोताङ गाउँपालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 363, VDCName = "Khumbu Pasang Lhamu Rural Municipality", VDCNameNepali = "खुम्बु पासाङल्हमु गाउँपालिका", DistrictId = 10 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 364, VDCName = "Likhu Pike Rural Municipality", VDCNameNepali = "लिखुपिके गाउँपालिका", DistrictId = 10 });

            //Sunsari
            vdcmunlist.Add(new MunicipalityVDC() { Id = 50, VDCName = "Itahari Municipality", VDCNameNepali = "ईटहरी उप-महानगर पालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 51, VDCName = "Dharan Municipality", VDCNameNepali = "धरान उप-महानगर पालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 46, VDCName = "Barahachhetra Municipality", VDCNameNepali = "बराहक्षेत्र नगर पालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 47, VDCName = "Inaruwa Municipality", VDCNameNepali = "ईनरुवा नगर पालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 48, VDCName = "Duhabi Municipality", VDCNameNepali = "दुहवी नगर पालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 49, VDCName = "Ramdhuni Municipality", VDCNameNepali = "रामधुनी नगर पालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 365, VDCName = "Koshi Rural Municipality", VDCNameNepali = "कोशी गाउँपालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 366, VDCName = "Harinagara Rural Municipality", VDCNameNepali = "हरिनगरा गाउँपालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 367, VDCName = "Bhokraha Rural Municipality", VDCNameNepali = "भोक्राहा गाउँपालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 368, VDCName = "Dewanganj Rural Municipality", VDCNameNepali = "देवानगन्ज गाउँपालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 369, VDCName = "Gadhi Rural Municipality", VDCNameNepali = "गढी गाउँपालिका", DistrictId = 11 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 370, VDCName = "Barju Rural Municipality", VDCNameNepali = "बर्जु गाउँपालिका", DistrictId = 11 });

            //Taplejung
            vdcmunlist.Add(new MunicipalityVDC() { Id = 52, VDCName = "Taplejung(Phungling) Municipality", VDCNameNepali = "फुङलिङ नगर पालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 371, VDCName = "Sirijangha Rural Municipality", VDCNameNepali = "सिरीजङ्घा गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 372, VDCName = "Aathrai Triveni Rural Municipality", VDCNameNepali = "आठराई त्रिवेणी गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 373, VDCName = "Pathibhara Yangwarak Rural Municipality", VDCNameNepali = "पाथीभरा याङवरक गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 374, VDCName = "Meringden Rural Municipality", VDCNameNepali = "मेरिन्ग्देन गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 375, VDCName = "Sidingwa Rural Municipality", VDCNameNepali = "सिदिन्ग्वा गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 376, VDCName = "Maiwakhola Rural Municipality", VDCNameNepali = "मैवाखोला गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 377, VDCName = "Mikkwakhola Rural Municipality", VDCNameNepali = "मिक्क्वाखोला गाउँपालिका", DistrictId = 12 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 378, VDCName = "Phaktanglung Rural Municipality", VDCNameNepali = "फक्तान्ग्लुंग गाउँपालिका", DistrictId = 12 });



            //Terhathum
            vdcmunlist.Add(new MunicipalityVDC() { Id = 53, VDCName = "Myanglung Municipality", VDCNameNepali = "म्याङलुङ नगर पालिका", DistrictId = 13 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 54, VDCName = "Laligurans Municipality", VDCNameNepali = "लालिगुराँस नगर पालिका", DistrictId = 13 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 379, VDCName = "Aathrai Rural Municipality", VDCNameNepali = "आठराई गाउँपालिका", DistrictId = 13 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 380, VDCName = "Phedap Rural Municipality", VDCNameNepali = "फेदाप गाउँपालिका", DistrictId = 13 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 381, VDCName = "Chhathar Rural Municipality", VDCNameNepali = "	छथर गाउँपालिका", DistrictId = 13 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 382, VDCName = "Menchayayem Rural Municipality", VDCNameNepali = "मेन्छयायेम गाउँपालिका", DistrictId = 13 });

            //Udayapur
            vdcmunlist.Add(new MunicipalityVDC() { Id = 56, VDCName = "Triyuga Municipality", VDCNameNepali = "त्रियुगा नगरपालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 57, VDCName = "Katari Municipality", VDCNameNepali = "कटारी नगरपालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 58, VDCName = "Chaudandigadhi Municipality", VDCNameNepali = "चौदण्डीगढी नगर पालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 59, VDCName = "Belaka Municipality", VDCNameNepali = "वेलका नगर पालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 383, VDCName = "Udayapurgadhi Rural Municipality", VDCNameNepali = "उदयपुरगढी गाउँपालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 384, VDCName = "Rautamai Rural Municipality", VDCNameNepali = "रौतामाई गाउँपालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 385, VDCName = "Tapli Rural Municipality", VDCNameNepali = "	ताप्ली गाउँपालिका", DistrictId = 14 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 386, VDCName = "Limchungbung Rural Municipality", VDCNameNepali = "लिम्चुङबुङ गाउँपालिका", DistrictId = 14 });

            //Saptari
            vdcmunlist.Add(new MunicipalityVDC() { Id = 60, VDCName = "Rajbiraj Municipality", VDCNameNepali = "राजविराज नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 61, VDCName = "Hanumannagar Kankalini Municipality", VDCNameNepali = "हनुमाननगर कंकालिनी नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 62, VDCName = "Khadak Municipality", VDCNameNepali = "खडक नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 63, VDCName = "Dakneshwari Municipality", VDCNameNepali = "दक्नेश्वारी नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 64, VDCName = "Surunga Municipality", VDCNameNepali = "सुरुङ्‍गा नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 65, VDCName = "Bodebarsain Municipality", VDCNameNepali = "बोदेबरसाईन नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 66, VDCName = "Shambhunath Municipality", VDCNameNepali = "शम्भुनाथ नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 67, VDCName = "Kanchanrup Municipality", VDCNameNepali = "कञ्चनरूप नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 68, VDCName = "Saptakoshi Municipality", VDCNameNepali = "सप्तकोशी नगरपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 387, VDCName = "Tilathi Koiladi Rural Municipality", VDCNameNepali = "तिलाठी कोईलाडी गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 388, VDCName = "Belhi Chapena	 Rural Municipality", VDCNameNepali = "बेल्ही चपेना गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 389, VDCName = "Chhinnamasta Rural Municipality", VDCNameNepali = "छिन्नमस्ता गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 390, VDCName = "	Mahadeva Rural Municipality", VDCNameNepali = "महादेवा गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 391, VDCName = "Aagnisaira Krishnasawaran Rural Municipality", VDCNameNepali = "अग्निसाइर कृष्णासवरन	 गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 392, VDCName = "Rupani Rural Municipality", VDCNameNepali = "रुपनी गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 393, VDCName = "Balan-Bihul Rural Municipality", VDCNameNepali = "बलान-बिहुल गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 394, VDCName = "Bishnupur Rural Municipality", VDCNameNepali = "बिष्णुपुर गाउँपालिका", DistrictId = 15 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 395, VDCName = "Tirhut Rural Municipality", VDCNameNepali = "तिरहुत गाउँपालिका", DistrictId = 15 });


            //Siraha
            vdcmunlist.Add(new MunicipalityVDC() { Id = 69, VDCName = "Lahan Municipality", VDCNameNepali = "लहान नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 70, VDCName = "Siraha Municipality", VDCNameNepali = "सिरहा नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 71, VDCName = "Golbazar Municipality", VDCNameNepali = "गोलबजार नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 72, VDCName = "Mirchaiya Municipality", VDCNameNepali = "मिर्चैया नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 73, VDCName = "Kalyanpur Municipality", VDCNameNepali = "कल्याणपुर नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 74, VDCName = "Dhangadimai Municipality", VDCNameNepali = "धनगढीमाई नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 75, VDCName = "Sukhipur Municipality", VDCNameNepali = "सुखीपुर नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 76, VDCName = "Karjanha Municipality", VDCNameNepali = "कर्जन्हा नगर पालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 396, VDCName = "Laksmipur Patari Rural Municipality", VDCNameNepali = "लक्ष्मीपुर पतारी गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 397, VDCName = "Bariyarpatti Rural Municipality", VDCNameNepali = "बरियारपट्टी गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 398, VDCName = "Aaurahi	 Rural Municipality", VDCNameNepali = "औरही गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 399, VDCName = "Arnama Rural Municipality", VDCNameNepali = "अर्नमा गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 401, VDCName = "Naraha Rural Municipality", VDCNameNepali = "भगवानपुर गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 400, VDCName = "Bhagawanpur Rural Municipality", VDCNameNepali = "नरहा गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 402, VDCName = "Nawarajpur Rural Municipality", VDCNameNepali = "नवराजपुर गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 403, VDCName = "Sakhuwanankarkatti Rural Municipality", VDCNameNepali = "सखुवानान्कारकट्टी गाउँपालिका", DistrictId = 16 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 404, VDCName = "Bishnupur Rural Municipality", VDCNameNepali = "विष्णुपुर गाउँपालिका", DistrictId = 16 });


            //Dhanusa
            vdcmunlist.Add(new MunicipalityVDC() { Id = 77, VDCName = "Janakpur Municipality", VDCNameNepali = "जनकपुरधाम उप-महानगरपालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 78, VDCName = "Mithila Bihari Municipality", VDCNameNepali = "मिथिला बिहारी नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 79, VDCName = "Sabaila Municipality", VDCNameNepali = "सवैला नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 80, VDCName = "Dhanushadham Municipality", VDCNameNepali = "धनुषाधाम नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 81, VDCName = "Mithila Municipality", VDCNameNepali = "मिथिला नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 82, VDCName = "Shahidnagar Municipality", VDCNameNepali = "शहिद नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 83, VDCName = "Kshireshwarnath Municipality", VDCNameNepali = "क्षिरेश्वरनाथ नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 84, VDCName = "Hansapur Municipality", VDCNameNepali = "हंसपुर नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 85, VDCName = "Kamala Municipality", VDCNameNepali = "कमला नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 86, VDCName = "Ganeshman Charnath Municipality", VDCNameNepali = "गणेशमान चारनाथ नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 87, VDCName = "Nagarain Municipality", VDCNameNepali = "नगराईन नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 88, VDCName = "Bideha Municipality", VDCNameNepali = "विदेह नगर पालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 405, VDCName = "Laksminiya Rural Municipality", VDCNameNepali = "लक्ष्मीनिया गाउँपालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 406, VDCName = "Mukhiyapatti Musaharmiya Rural Municipality", VDCNameNepali = "मुखियापट्टी मुसहरमिया गाउँपालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 407, VDCName = "Janak Nandini Rural Municipality", VDCNameNepali = "जनकनन्दिनी गाउँपालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 408, VDCName = "Aaurahi Rural Municipality", VDCNameNepali = "औरही गाउँपालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 409, VDCName = "Bateshwar Rural Municipality", VDCNameNepali = "बटेश्वर गाउँपालिका", DistrictId = 17 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 500, VDCName = "Dhanauji Rural Municipality", VDCNameNepali = "धनौजी गाउँपालिका", DistrictId = 17 });



            //Mahottari
            vdcmunlist.Add(new MunicipalityVDC() { Id = 89, VDCName = "Gaushala Municipality", VDCNameNepali = "गौशाला नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 90, VDCName = "Bardibas Municipality", VDCNameNepali = "बर्दिबास नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 91, VDCName = "Jaleshwar Municipality", VDCNameNepali = "जलेश्वर नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 92, VDCName = "Manara Shisawa Municipality", VDCNameNepali = "मनरा शिसवा नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 93, VDCName = "Bhangaha Municipality", VDCNameNepali = "भँगाहा नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 94, VDCName = "Balawa Municipality", VDCNameNepali = "बलवा नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 95, VDCName = "Loharpatti Municipality", VDCNameNepali = "लोहरपट्टी नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 96, VDCName = "Aurahi Municipality", VDCNameNepali = "औरही नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 97, VDCName = "Matihani Municipality", VDCNameNepali = "मटिहानी नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 98, VDCName = "Ramgopalpur Municipality", VDCNameNepali = "रामगोपालपुर नगर पालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 501, VDCName = "Sonama Rural Municipality", VDCNameNepali = " गाउँपालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 502, VDCName = "Pipara Rural Municipality", VDCNameNepali = "पिपरा सोनमा गाउँपालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 503, VDCName = "Samsi Rural Municipality", VDCNameNepali = "साम्सी गाउँपालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 504, VDCName = "Ekdara Rural Municipality", VDCNameNepali = "एकडारा गाउँपालिका", DistrictId = 18 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 505, VDCName = "Mahottari Rural Municipality", VDCNameNepali = "महोत्तरी गाउँपालिका", DistrictId = 18 });


            //Sarlahi
            vdcmunlist.Add(new MunicipalityVDC() { Id = 99, VDCName = "Barahathwa Municipality", VDCNameNepali = "बरहथवा नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 100, VDCName = "Ishwarpur Municipality", VDCNameNepali = "ईश्वरपूर नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 101, VDCName = "Lalbandi Municipality", VDCNameNepali = "लालबन्दी नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 102, VDCName = "Godaita Municipality", VDCNameNepali = "गोडैता नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 103, VDCName = "Malangwa Municipality", VDCNameNepali = "मलङ्गवा  नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 104, VDCName = "Balara Municipality", VDCNameNepali = "बलरा नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 105, VDCName = "Hariwan Municipality", VDCNameNepali = "हरिवन नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 106, VDCName = "Kabilasi Municipality", VDCNameNepali = "कविलासी  नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 107, VDCName = "Bagmati Municipality", VDCNameNepali = "बागमती नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 108, VDCName = "Haripur Municipality", VDCNameNepali = "हरिपुर नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 109, VDCName = "Haripurwa Municipality", VDCNameNepali = "हरिपुर्वा नगर पालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 506, VDCName = "Chandranagar Rural Municipality", VDCNameNepali = "चन्द्रनगर गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 507, VDCName = "Bramhapuri Rural Municipality", VDCNameNepali = "ब्रह्मपुरी गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 508, VDCName = "Ramnagar Rural Municipality", VDCNameNepali = "रामनगर गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 509, VDCName = "Chakraghatta Rural Municipality", VDCNameNepali = "चक्रघट्टा गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 510, VDCName = "Kaudena Rural Municipality", VDCNameNepali = "कौडेना गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 511, VDCName = "Dhankaul Rural Municipality", VDCNameNepali = "धनकौल गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 512, VDCName = "Bishnu Rural Municipality", VDCNameNepali = "विष्णु गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 513, VDCName = "Basbariya Rural Municipality", VDCNameNepali = "बसबरिया गाउँपालिका", DistrictId = 19 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 514, VDCName = "Parsa Rural Municipality", VDCNameNepali = "पर्सा गाउँपालिका", DistrictId = 19 });


            //Bara
            vdcmunlist.Add(new MunicipalityVDC() { Id = 110, VDCName = "Kalaiya Municipality", VDCNameNepali = "कलैया उप-महानगरपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 111, VDCName = "Jitpur Simara Municipality", VDCNameNepali = "जीतपुरसिमरा उप-महानगरपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 112, VDCName = "Mahagadhimai Municipality", VDCNameNepali = "महागढीमाई नगर पालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 113, VDCName = "Simraungadh Municipality", VDCNameNepali = "सिम्रोनगढ नगर पालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 114, VDCName = "Kolhabi Municipality", VDCNameNepali = "कोल्हवी नगर पालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 116, VDCName = "Nijgadh Municipality", VDCNameNepali = "निजगढ नगर पालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 117, VDCName = "Pachrauta Municipality", VDCNameNepali = "पचरौता नगर पालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 515, VDCName = "Subarna Rural Municipality", VDCNameNepali = "सुवर्ण गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 516, VDCName = "Adarsha Kotwal Rural Municipality", VDCNameNepali = "आदर्श कोतवाल	 गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 517, VDCName = "Baragadhi Rural Municipality", VDCNameNepali = "बारागढी गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 518, VDCName = "Pheta Rural Municipality", VDCNameNepali = "फेटा गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 519, VDCName = "Karaiyamai Rural Municipality", VDCNameNepali = "करैयामाई गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 520, VDCName = "Prasauni Rural Municipality", VDCNameNepali = "प्रसौनी गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 521, VDCName = "Bishrampur Rural Municipality", VDCNameNepali = "विश्रामपुर गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 522, VDCName = "Devtal Rural Municipality", VDCNameNepali = "देवताल गाउँपालिका", DistrictId = 20 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 523, VDCName = "Parawanipur Rural Municipality", VDCNameNepali = "परवानीपुर गाउँपालिका", DistrictId = 20 });


            //Parsa
            vdcmunlist.Add(new MunicipalityVDC() { Id = 118, VDCName = "Birganj Municipality", VDCNameNepali = "बिरगंज महानगरपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 119, VDCName = "Bahudarmai Municipality", VDCNameNepali = "बहुदरमाई नगरपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 120, VDCName = "Mahagadhimai Municipality", VDCNameNepali = "पर्सागढी नगर पालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 121, VDCName = "Parsagadhi Municipality", VDCNameNepali = "पर्सागढी नगर पालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 122, VDCName = "Pokhariya Municipality", VDCNameNepali = "पोखरिया नगर पालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 524, VDCName = "Sakhuwa Prasauni Rural Municipality", VDCNameNepali = "सखुवा प्रसौनी गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 525, VDCName = "Jagarnathpur Rural Municipality", VDCNameNepali = "जगरनाथपुर गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 526, VDCName = "Chhipaharmai Rural Municipality", VDCNameNepali = "छिपहरमाई गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 527, VDCName = "Bindabasini Rural Municipality", VDCNameNepali = "बिन्दबासिनी गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 528, VDCName = "Paterwa Sugauli Rural Municipality", VDCNameNepali = "पटेर्वा सुगौली गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 529, VDCName = "Jira Bhavani Rural Municipality", VDCNameNepali = "जिरा भवानी गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 530, VDCName = "Kalikamai Rural Municipality", VDCNameNepali = "कालिकामाई गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 531, VDCName = "Pakaha Mainpur Rural Municipality", VDCNameNepali = "पकाहा मैनपुर गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 532, VDCName = "Thori Rural Municipality", VDCNameNepali = "ठोरी गाउँपालिका", DistrictId = 21 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 533, VDCName = "	Dhobini Rural Municipality", VDCNameNepali = "धोबीनी गाउँपालिका", DistrictId = 21 });



            //Rautahat
            vdcmunlist.Add(new MunicipalityVDC() { Id = 123, VDCName = "Buadhimai Municipality", VDCNameNepali = "बौधिमाई नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 124, VDCName = "Brindaban Municipality", VDCNameNepali = "पचरौता नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 125, VDCName = "Chandrapur Municipality", VDCNameNepali = "चंद्रपुर नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 126, VDCName = "Dewahi Gonahi Municipality", VDCNameNepali = "देवाही गोनाही नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 127, VDCName = "Garuda Municipality", VDCNameNepali = "गरुडा नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 128, VDCName = "Gaur Municipality", VDCNameNepali = "गौर नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 129, VDCName = "Gujara Municipality", VDCNameNepali = "गुजारा नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 130, VDCName = "Ishanath Municipality", VDCNameNepali = "इशाबाथ नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 131, VDCName = "Katahariya Municipality", VDCNameNepali = "कटहरिया नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 132, VDCName = "Madhav Narayan Municipality", VDCNameNepali = "माधव नारायण नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 133, VDCName = "Maulapur Municipality", VDCNameNepali = "मौलापुर नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 134, VDCName = "Paroha Municipality", VDCNameNepali = "परोहा नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 135, VDCName = "Phatuwa Bijayapur Municipality", VDCNameNepali = "फतुवा विजयपुर नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 136, VDCName = "Rajdevi Municipality", VDCNameNepali = "राजदेवी नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 137, VDCName = "Rajpur Municipality", VDCNameNepali = "राजपुर नगरपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 534, VDCName = "	Durga Bhagawati Rural Municipality", VDCNameNepali = "दुर्गा भगवती गाउँपालिका", DistrictId = 22 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 535, VDCName = "	Yamunamai Rural Municipality", VDCNameNepali = "यमुनामाई गाउँपालिका", DistrictId = 22 });

            //सिन्धुली 
            vdcmunlist.Add(new MunicipalityVDC() { Id = 138, VDCName = "Kamalamai Municipality", VDCNameNepali = "कमलामाई नगरपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 139, VDCName = "Dudhauli Municipality", VDCNameNepali = "दुधौली नगरपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 536, VDCName = "Tinpatan	Rural Municipality", VDCNameNepali = "तिनपाटन गाउँपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 537, VDCName = "Marin Rural Municipality", VDCNameNepali = "मरिण गाउँपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 538, VDCName = "Hariharpurgadhi Rural Municipality", VDCNameNepali = "हरिहरपुरगढी गाउँपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 539, VDCName = "Sunkoshi Rural Municipality", VDCNameNepali = "सुनकोशी गाउँपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 540, VDCName = "Golanjor Rural Municipality", VDCNameNepali = "गोलन्जोर गाउँपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 541, VDCName = "Phikkal Rural Municipality", VDCNameNepali = "फिक्कल गाउँपालिका", DistrictId = 23 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 542, VDCName = "Ghyanglekh Rural Municipality", VDCNameNepali = "घ्याङलेख गाउँपालिका", DistrictId = 23 });



            //रामेछाप
            vdcmunlist.Add(new MunicipalityVDC() { Id = 140, VDCName = "Manthali Municipality", VDCNameNepali = "मन्थली नगरपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 141, VDCName = "Ramechhap Municipality", VDCNameNepali = "रामेछाप नगरपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 543, VDCName = "Khandadevi Rural Municipality", VDCNameNepali = " गाउँपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 544, VDCName = "Likhu Tamakoshi Rural Municipality", VDCNameNepali = "लिखु तामाकोशी खाँडादेवी गाउँपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 545, VDCName = "Doramba Rural Municipality", VDCNameNepali = "दोरम्बा गाउँपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 546, VDCName = "Gokulganga Rural Municipality", VDCNameNepali = "गोकुलगङ्गा	 गाउँपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 547, VDCName = "Sunapati Rural Municipality", VDCNameNepali = "सुनापती गाउँपालिका", DistrictId = 24 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 548, VDCName = "Umakunda Rural Municipality", VDCNameNepali = "उमाकुण्ड गाउँपालिका", DistrictId = 24 });


            //दोलखा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 142, VDCName = "Bhimeswor Municipality", VDCNameNepali = "भिमेस्वोर नगरपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 143, VDCName = "Jiri Municipality", VDCNameNepali = "जिरी नगरपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 549, VDCName = "Kalinchok Rural Municipality", VDCNameNepali = "कालिन्चोक गाउँपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 550, VDCName = "Melung Rural Municipality", VDCNameNepali = "मेलुङ गाउँपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 551, VDCName = "Shailung Rural Municipality", VDCNameNepali = "शैलुङ गाउँपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 552, VDCName = "Baiteshwar Rural Municipality", VDCNameNepali = "वैतेश्वर गाउँपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 553, VDCName = "Tamakoshi Rural Municipality", VDCNameNepali = "तामाकोशी गाउँपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 554, VDCName = "Bigu Rural Municipality", VDCNameNepali = "विगु गाउँपालिका", DistrictId = 25 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 555, VDCName = "Gaurishankar Rural Municipality", VDCNameNepali = "गौरिशंकर गाउँपालिका", DistrictId = 25 });


            //भक्तपुर
            vdcmunlist.Add(new MunicipalityVDC() { Id = 144, VDCName = "Bhaktapur Municipality", VDCNameNepali = "भक्तपुर नगरपालिका", DistrictId = 26 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 145, VDCName = "Changunarayan Municipality", VDCNameNepali = "चाँगुनारायण नगरपालिका", DistrictId = 26 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 146, VDCName = "Madhyapur Thimi Municipality", VDCNameNepali = "मध्यपुर थिमी नगरपालिका", DistrictId = 26 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 147, VDCName = "Suryabinayak Municipality", VDCNameNepali = "सुर्यविनायक नगरपालिका", DistrictId = 26 });

            //धादिङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 148, VDCName = "Dhunibeshi Municipality", VDCNameNepali = "धुनिबेशी नगरपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 149, VDCName = "Nilkantha Municipality", VDCNameNepali = "निलकण्ठ नगरपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 556, VDCName = "Ruby Valley Rural Municipality", VDCNameNepali = "रुवी भ्याली गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 557, VDCName = "Thakre Rural Municipality", VDCNameNepali = "थाक्रे गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 558, VDCName = "Benighat Rorang Rural Municipality", VDCNameNepali = "बेनीघाट रोराङ्ग गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 559, VDCName = "Galchhi Rural Municipality", VDCNameNepali = "गल्छी गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 560, VDCName = "Gajuri Rural Municipality", VDCNameNepali = "गजुरी गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 561, VDCName = "Jwalamukhi Rural Municipality", VDCNameNepali = "ज्वालामूखी गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 562, VDCName = "Siddhalekh Rural Municipality", VDCNameNepali = "सिद्धलेक गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 563, VDCName = "Tripura Sundari Rural Municipality", VDCNameNepali = "त्रिपुरासुन्दरी गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 564, VDCName = "Gangajamuna Rural Municipality", VDCNameNepali = "गङ्गाजमुना गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 565, VDCName = "Netrawati Dabjong Rural Municipality", VDCNameNepali = "नेत्रावती डबजोङ गाउँपालिका", DistrictId = 27 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 566, VDCName = "Khaniyabas Rural Municipality", VDCNameNepali = "खनियाबास गाउँपालिका", DistrictId = 27 });



            //kathmandu
            vdcmunlist.Add(new MunicipalityVDC() { Id = 1, VDCName = "Kathmandu Metropolitan", VDCNameNepali = "काठमाण्डौ महानगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 2, VDCName = "Budhanilkantha Municipality", VDCNameNepali = "बुढानिलकण्ठ नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 3, VDCName = "Gokarneshwar Municipality", VDCNameNepali = "गोकर्णेश्वर नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 4, VDCName = "Tokha Municipality", VDCNameNepali = "टोखा नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 5, VDCName = "Chandragiri Municipality", VDCNameNepali = "चन्द्रागिरी नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 6, VDCName = "Tarakeshwar Municipality", VDCNameNepali = "तारकेश्वर नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 7, VDCName = "Kirtipur Municipality", VDCNameNepali = "किर्तिपुर नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 8, VDCName = "Nagarjun Municipality", VDCNameNepali = "नागार्जुन नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 9, VDCName = "Kageshwari-Manohara Municipality", VDCNameNepali = "कागेश्वरी मनोहरा नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 10, VDCName = "Shankharapur Municipality", VDCNameNepali = "शङ्खरापुर नगर पालिका", DistrictId = 28 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 11, VDCName = "Dakshinkali Municipality", VDCNameNepali = "दक्षिणकाली नगर पालिका", DistrictId = 28 });


            //काभ्रेपलान्चोक
            vdcmunlist.Add(new MunicipalityVDC() { Id = 161, VDCName = "Dhulikhel Municipality", VDCNameNepali = " नगरपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 162, VDCName = "Banepa Municipality", VDCNameNepali = " नगरपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 163, VDCName = "Panauti Municipality", VDCNameNepali = " नगरपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 164, VDCName = "Panchkhal Municipality", VDCNameNepali = " नगरपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 165, VDCName = "Namobuddha Municipality", VDCNameNepali = " नगरपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 45899, VDCName = "Mandandeupur Municipality", VDCNameNepali = " नगरपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 567, VDCName = "Roshi Rural Municipality", VDCNameNepali = "रोशी गाउँपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 568, VDCName = "Temal Rural Municipality", VDCNameNepali = "तेमाल गाउँपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 569, VDCName = "Chaunri Deurali Rural Municipality", VDCNameNepali = "चौंरी देउराली गाउँपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 570, VDCName = "Bhumlu Rural Municipality", VDCNameNepali = "भुम्लु गाउँपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 571, VDCName = "Mahabharat Rural Municipality", VDCNameNepali = "महाभारत गाउँपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 572, VDCName = "Bethanchok Rural Municipality", VDCNameNepali = "बेथानचोक गाउँपालिका", DistrictId = 29 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 573, VDCName = "Khanikhola Rural Municipality", VDCNameNepali = "खानीखोला गाउँपालिका", DistrictId = 29 });



            //ललितपुर
            vdcmunlist.Add(new MunicipalityVDC() { Id = 987777, VDCName = "Lalitpur Metropolitan", VDCNameNepali = "ललितपुर महानगरपालिका", DistrictId = 30 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 214, VDCName = "Godawari Municipality", VDCNameNepali = "गोदावरी नगरपालिका", DistrictId = 30 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 215, VDCName = "Mahalaxmi Municipality", VDCNameNepali = "महालक्ष्मी नगरपालिका", DistrictId = 30 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 574, VDCName = "Bagmati Rural Municipality", VDCNameNepali = "बाग्मती गाउँपालिका", DistrictId = 30 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 575, VDCName = "Konjyosom Rural Municipality", VDCNameNepali = "कोन्ज्योसोम गाउँपालिका", DistrictId = 30 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 576, VDCName = "Mahankal Rural Municipality", VDCNameNepali = "महाङ्काल गाउँपालिका", DistrictId = 30 });

            //नुवाकोट 
            vdcmunlist.Add(new MunicipalityVDC() { Id = 216, VDCName = "Bidur Municipality", VDCNameNepali = "विदुर नगरपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 217, VDCName = "Belkotgadhi Municipality", VDCNameNepali = "बेलकोटगढी नगरपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 577, VDCName = "Kakani Rural Municipality", VDCNameNepali = "ककनी गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 578, VDCName = "Dupcheshwar Rural Municipality", VDCNameNepali = "दुप्चेश्वर गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 579, VDCName = "Shivapuri Rural Municipality", VDCNameNepali = "शिवपुरी गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 580, VDCName = "Tadi Rural Municipality", VDCNameNepali = "तादी गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 581, VDCName = "Likhu Rural Municipality", VDCNameNepali = "लिखु गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 582, VDCName = "Suryagadhi Rural Municipality", VDCNameNepali = "सुर्यगढी गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 583, VDCName = "Panchakanya Rural Municipality", VDCNameNepali = "पञ्चकन्या गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 584, VDCName = "Tarkeshwar Rural Municipality", VDCNameNepali = "तारकेश्वर गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 585, VDCName = "Kispang Rural Municipality", VDCNameNepali = "किस्पाङ गाउँपालिका", DistrictId = 31 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 586, VDCName = "Myagang	 Rural Municipality", VDCNameNepali = "म्यागङ गाउँपालिका", DistrictId = 31 });

            //रसुवा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 587, VDCName = "Naukunda Rural Municipality", VDCNameNepali = "नौकुण्ड गाउँपालिका", DistrictId = 32 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 588, VDCName = "Kalika Rural Municipality", VDCNameNepali = "कालिका गाउँपालिका", DistrictId = 32 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 589, VDCName = "Uttargaya Rural Municipality", VDCNameNepali = "उत्तरगया गाउँपालिका", DistrictId = 32 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 590, VDCName = "Gosaikund Rural Municipality", VDCNameNepali = "गोसाईकुण्ड गाउँपालिका", DistrictId = 32 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 591, VDCName = "Aamachodingmo Rural Municipality", VDCNameNepali = "आमाछोदिङमो गाउँपालिका", DistrictId = 32 });

            //सिन्धुपाल्चोक
            vdcmunlist.Add(new MunicipalityVDC() { Id = 218, VDCName = "Chautara Sangachokgadhi Municipality", VDCNameNepali = "चौतारा साँगाचोकगढी नगरपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 219, VDCName = "Melamchi Municipality", VDCNameNepali = "मेलम्ची नगरपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 220, VDCName = "Barhabise Municipality", VDCNameNepali = "बाह्रविसे नगरपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 592, VDCName = "Indrawati Rural Municipality", VDCNameNepali = "र्इन्द्रावती गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 593, VDCName = "Panchpokhari Thangpal Rural Municipality", VDCNameNepali = "पाँचपोखरी थाङपाल गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 594, VDCName = "Jugal Rural Municipality", VDCNameNepali = "जुगल गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 595, VDCName = "Balephi	 Rural Municipality", VDCNameNepali = "बलेफी गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 596, VDCName = "Helambu Rural Municipality", VDCNameNepali = "हेलम्बु गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 597, VDCName = "Bhotekoshi Rural Municipality", VDCNameNepali = "भोटेकोशी गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 598, VDCName = "Sunkoshi Rural Municipality", VDCNameNepali = "सुनकोशी गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 599, VDCName = "Lisankhu Pakhar Rural Municipality", VDCNameNepali = "लिसंखु पाखर गाउँपालिका", DistrictId = 33 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 600, VDCName = "Tripura Sundari Rural Municipality", VDCNameNepali = "त्रिपुरासुन्दरी गाउँपालिका", DistrictId = 33 });



            //चितवन
            vdcmunlist.Add(new MunicipalityVDC() { Id = 221, VDCName = "Bharatpur Municipality", VDCNameNepali = "भरतपुर महानगरपालिका", DistrictId = 34 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 222, VDCName = "Ratnanagar Municipality", VDCNameNepali = "रत्ननगर नगरपालिका", DistrictId = 34 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 223, VDCName = "Rapti Municipality", VDCNameNepali = "राप्ती नगरपालिका", DistrictId = 34 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 224, VDCName = "Khairhani Municipality", VDCNameNepali = "खैरहनी नगरपालिका", DistrictId = 34 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 225, VDCName = "Kalika Municipality", VDCNameNepali = "कालिका नगरपालिका", DistrictId = 34 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 226, VDCName = "Madi Municipality", VDCNameNepali = "माडी नगरपालिका", DistrictId = 34 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 601, VDCName = "Ichchhakamana Rural Municipality", VDCNameNepali = "इच्छाकामना गाउँपालिका", DistrictId = 34 });


            //मकवानपुर
            vdcmunlist.Add(new MunicipalityVDC() { Id = 240, VDCName = "Hetauda Municipality", VDCNameNepali = "हेटौडा उप-महानगरपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 227, VDCName = "Thaha Municipality", VDCNameNepali = "थाहा नगरपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 602, VDCName = "Bakaiya Rural Municipality", VDCNameNepali = "बकैया गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 603, VDCName = "Manhari Rural Municipality", VDCNameNepali = "मनहरी गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 604, VDCName = "Bagmati Rural Municipality", VDCNameNepali = "बाग्मती गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 605, VDCName = "Raksirang Rural Municipality", VDCNameNepali = "राक्सिराङ्ग गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 606, VDCName = "Makawanpurgadhi Rural Municipality", VDCNameNepali = "मकवानपुरगढी गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 607, VDCName = "Kailash Rural Municipality", VDCNameNepali = "कैलाश गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 608, VDCName = "Bhimphedi Rural Municipality", VDCNameNepali = "भीमफेदी	 गाउँपालिका", DistrictId = 35 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 609, VDCName = "Indrasarowar	 Rural Municipality", VDCNameNepali = "ईन्द्र सरोवर गाउँपालिका", DistrictId = 35 });


            //बागलुङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 228, VDCName = "Baglung Municipality", VDCNameNepali = "बागलुङ नगरपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 229, VDCName = "Galkot Municipality", VDCNameNepali = "गल्कोट नगरपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 230, VDCName = "Jaimini Municipality", VDCNameNepali = "जैमिनी नगरपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 231, VDCName = "Dhorpatan Municipality", VDCNameNepali = "ढोरपाटन नगरपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 610, VDCName = "Badigad Rural Municipality", VDCNameNepali = "वडिगाड गाउँपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 611, VDCName = "Kathekhola Rural Municipality", VDCNameNepali = "काठेखोला गाउँपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 612, VDCName = "Nisikhola Rural Municipality", VDCNameNepali = "निसीखोला गाउँपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 613, VDCName = "Bareng Rural Municipality", VDCNameNepali = "वरेङ गाउँपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 614, VDCName = "Tarakhola Rural Municipality", VDCNameNepali = "ताराखोला गाउँपालिका", DistrictId = 36 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 615, VDCName = "Tamankhola Rural Municipality", VDCNameNepali = "तमानखोला गाउँपालिका", DistrictId = 36 });


            //गोरखा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 232, VDCName = "Gorkha Municipality", VDCNameNepali = "गोरखा नगरपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 233, VDCName = "Palungtar Municipality", VDCNameNepali = "पालुङटार नगरपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 616, VDCName = "Shahid Lakhan Rural Municipality", VDCNameNepali = "शहिद लखन गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 617, VDCName = "Barpak Sulikot Rural Municipality", VDCNameNepali = "बारपाक सुलीकोट गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 618, VDCName = "Aarughat Rural Municipality", VDCNameNepali = "आरूघाट गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 619, VDCName = "Siranchok Rural Municipality", VDCNameNepali = "सिरानचोक	 गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 620, VDCName = "Gandaki Rural Municipality", VDCNameNepali = "गण्डकी गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 621, VDCName = "Bhimsen Thapa Rural Municipality", VDCNameNepali = "भिमसेनथापा गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 622, VDCName = "Ajirkot Rural Municipality", VDCNameNepali = "अजिरकोट गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 623, VDCName = "Dharche Rural Municipality", VDCNameNepali = "धार्चे गाउँपालिका", DistrictId = 37 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 624, VDCName = "Chum Nubri Rural Municipality", VDCNameNepali = "चुम नुव्री गाउँपालिका", DistrictId = 37 });


            //कास्की
            vdcmunlist.Add(new MunicipalityVDC() { Id = 234, VDCName = "	Pokhara Municipality", VDCNameNepali = "पोखरा महानगरपालिका", DistrictId = 38 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 625, VDCName = "Annapurna Rural Municipality", VDCNameNepali = "अन्नपुर्ण गाउँपालिका", DistrictId = 38 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 626, VDCName = "Machhapuchhre Rural Municipality", VDCNameNepali = "माछापुछ्रे गाउँपालिका", DistrictId = 38 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 627, VDCName = "Madi Rural Municipality", VDCNameNepali = "मादी	 गाउँपालिका", DistrictId = 38 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 628, VDCName = "Rupa Rural Municipality", VDCNameNepali = "रूपा गाउँपालिका", DistrictId = 38 });


            //लमजुङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 235, VDCName = "	Besisahar Municipality", VDCNameNepali = "बेसीशहर नगरपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 236, VDCName = "	Sundarbazar Municipality", VDCNameNepali = "सुन्दरबजार नगरपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 237, VDCName = "	Madhya Nepal Municipality", VDCNameNepali = "मध्यनेपाल नगरपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 238, VDCName = " Rainas Municipality", VDCNameNepali = "रार्इनास नगरपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 629, VDCName = "Marsyangdi Rural Municipality", VDCNameNepali = "मर्स्याङदी गाउँपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 630, VDCName = "Dordi Rural Municipality", VDCNameNepali = "दोर्दी गाउँपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 631, VDCName = "Dudhpokhari Rural Municipality", VDCNameNepali = "दूधपोखरी गाउँपालिका", DistrictId = 39 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 632, VDCName = "Kwaholasothar Rural Municipality", VDCNameNepali = "क्व्होलासोथार गाउँपालिका", DistrictId = 39 });

            //मनाङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 633, VDCName = "Manang Disyang Rural Municipality", VDCNameNepali = "मनाङ डिस्याङ गाउँपालिका", DistrictId = 40 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 634, VDCName = "Nason Rural Municipality", VDCNameNepali = "नासोँ गाउँपालिका", DistrictId = 40 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 635, VDCName = "Chame Rural Municipality", VDCNameNepali = "चामे गाउँपालिका", DistrictId = 40 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 636, VDCName = "Narpa Bhumi Rural Municipality", VDCNameNepali = "नार्पा भूमी गाउँपालिका", DistrictId = 40 });

            //मुस्ताङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 637, VDCName = "Gharapjhong Rural Municipality", VDCNameNepali = "घरपझोङ गाउँपालिका", DistrictId = 41 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 638, VDCName = "Thasang Rural Municipality", VDCNameNepali = "थासाङ गाउँपालिका", DistrictId = 41 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 639, VDCName = "Baragung Muktichhetra Rural Municipality", VDCNameNepali = "बारागुङ मुक्तिक्षेत्र गाउँपालिका", DistrictId = 41 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 640, VDCName = "Lomanthang Rural Municipality", VDCNameNepali = "लोमन्थाङ गाउँपालिका", DistrictId = 41 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 641, VDCName = "Lo-Ghekar Damodarkunda Rural Municipality", VDCNameNepali = "लो-घेकर दामोदरकुण्ड गाउँपालिका", DistrictId = 41 });

            //म्याग्दी
            vdcmunlist.Add(new MunicipalityVDC() { Id = 239, VDCName = "	Beni Municipality", VDCNameNepali = "बेनी नगरपालिका", DistrictId = 42 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 642, VDCName = "Malika Rural Municipality", VDCNameNepali = "मालिका गाउँपालिका", DistrictId = 42 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 643, VDCName = "Mangala Rural Municipality", VDCNameNepali = "मंगला गाउँपालिका", DistrictId = 42 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 644, VDCName = "Raghuganga Rural Municipality", VDCNameNepali = "रघुगंगा गाउँपालिका", DistrictId = 42 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 645, VDCName = "Dhaulagiri Rural Municipality", VDCNameNepali = "धवलागिरी गाउँपालिका", DistrictId = 42 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 646, VDCName = "Annapurna Rural Municipality", VDCNameNepali = "अन्नपुर्ण गाउँपालिका", DistrictId = 42 });


            //नवलपुर
            vdcmunlist.Add(new MunicipalityVDC() { Id = 261, VDCName = "	Kawasoti Municipality", VDCNameNepali = "कावासोती नगरपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 241, VDCName = "	Gaindakot Municipality", VDCNameNepali = "गैडाकोट नगरपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 242, VDCName = "	Madhyabindu Municipality", VDCNameNepali = "मध्यविन्दु नगरपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 243, VDCName = "	Devchuli Municipality", VDCNameNepali = "देवचुली नगरपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 647, VDCName = "Hupsekot Rural Municipality", VDCNameNepali = "हुप्सेकोट गाउँपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 648, VDCName = "Binayi Triveni Rural Municipality", VDCNameNepali = "विनयी त्रिवेणी गाउँपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 649, VDCName = "Bulingtar Rural Municipality", VDCNameNepali = "बुलिङटार गाउँपालिका", DistrictId = 43 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 650, VDCName = "Baudikali Rural Municipality", VDCNameNepali = "बौदीकाली गाउँपालिका", DistrictId = 43 });



            //पर्वत
            vdcmunlist.Add(new MunicipalityVDC() { Id = 244, VDCName = "Kushma Municipality", VDCNameNepali = "कुश्मा नगरपालिका", DistrictId = 44 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 245, VDCName = "Phalewas Municipality", VDCNameNepali = "फलेवास नगरपालिका", DistrictId = 44 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 651, VDCName = "Jaljala Rural Municipality", VDCNameNepali = "जलजला गाउँपालिका", DistrictId = 44 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 652, VDCName = "Modi Rural Municipality", VDCNameNepali = "मोदी गाउँपालिका", DistrictId = 44 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 653, VDCName = "Painyu Rural Municipality", VDCNameNepali = "पैयूं गाउँपालिका", DistrictId = 44 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 654, VDCName = "Bihadi Rural Municipality", VDCNameNepali = "विहादी गाउँपालिका", DistrictId = 44 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 655, VDCName = "Mahashila Rural Municipality", VDCNameNepali = "महाशिला गाउँपालिका", DistrictId = 44 });


            //स्याङग्जा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 246, VDCName = "	Waling Municipality", VDCNameNepali = "वालिङ नगरपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 247, VDCName = "	Phalewas Municipality", VDCNameNepali = "फलेवास नगरपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 248, VDCName = "	Putalibazar Municipality", VDCNameNepali = "पुतलीबजार नगरपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 249, VDCName = "	Galyang Municipality", VDCNameNepali = "गल्याङ नगरपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 112233, VDCName = "	Chapakot Municipality", VDCNameNepali = "चापाकोट नगरपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 54346456, VDCName = "	Bhirkot Municipality", VDCNameNepali = "भिरकोट नगरपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 656, VDCName = "Kaligandaki Rural Municipality", VDCNameNepali = "कालीगण्डकी गाउँपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 657, VDCName = "Biruwa Rural Municipality", VDCNameNepali = "विरुवा गाउँपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 658, VDCName = "Harinas Rural Municipality", VDCNameNepali = "हरीनास गाउँपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 659, VDCName = "Aandhikhola	 Rural Municipality", VDCNameNepali = "आँधीखोला गाउँपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 660, VDCName = "Arjun Chaupari Rural Municipality", VDCNameNepali = "अर्जुन चौपारी गाउँपालिका", DistrictId = 45 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 661, VDCName = "Phedikhola Rural Municipality", VDCNameNepali = "फेदीखोला गाउँपालिका", DistrictId = 45 });

            //तनहुँ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 2244331, VDCName = "Vyas Municipality", VDCNameNepali = "व्यास नगरपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 25199008, VDCName = "Shuklagandaki Municipality", VDCNameNepali = "शुक्लागण्डकी नगरपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 252, VDCName = "Bhanu Municipality", VDCNameNepali = "भानु नगरपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 253, VDCName = "Bhimad Municipality", VDCNameNepali = "भिमाद नगरपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 662, VDCName = "Rishing Rural Municipality", VDCNameNepali = "ऋषिङ्ग गाउँपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 663, VDCName = "Myagde Rural Municipality", VDCNameNepali = "म्याग्दे	 गाउँपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 664, VDCName = "Aanbu Khaireni Rural Municipality", VDCNameNepali = "आँबुखैरेनी गाउँपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 665, VDCName = "Bandipur Rural Municipality", VDCNameNepali = "बन्दिपुर गाउँपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 666, VDCName = "Ghiring Rural Municipality", VDCNameNepali = "घिरिङ गाउँपालिका", DistrictId = 46 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 667, VDCName = "Devghat	 Rural Municipality", VDCNameNepali = "देवघाट	 गाउँपालिका", DistrictId = 46 });

            //कपिलवस्तु
            vdcmunlist.Add(new MunicipalityVDC() { Id = 254, VDCName = "Kapilvastu Municipality", VDCNameNepali = "कपिलवस्तु नगरपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 255, VDCName = "Banganga Municipality", VDCNameNepali = "बाणगंगा नगरपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 25699887, VDCName = "Shivaraj Municipality", VDCNameNepali = "शिवराज नगरपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 25711223, VDCName = "Buddhabhumi Municipality", VDCNameNepali = "बुद्धभूमी नगरपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 25643243, VDCName = "Krishnanagar Municipality", VDCNameNepali = "कृष्णनगर नगरपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 2522457, VDCName = "Maharajganj Municipality", VDCNameNepali = "महाराजगंज नगरपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 668, VDCName = "Mayadevi Rural Municipality", VDCNameNepali = "मायादेवी गाउँपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 669, VDCName = "Shuddhodhan Rural Municipality", VDCNameNepali = "शुद्धोधन गाउँपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 670, VDCName = "Yasodhara Rural Municipality", VDCNameNepali = "यसोधरा गाउँपालिका", DistrictId = 47 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 671, VDCName = "Bijaynagar Rural Municipality", VDCNameNepali = "विजयनगर गाउँपालिका", DistrictId = 47 });

            //परासी
            vdcmunlist.Add(new MunicipalityVDC() { Id = 258, VDCName = "	Ramgram Municipality", VDCNameNepali = "रामग्राम नगरपालिका", DistrictId = 48 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 259, VDCName = "	Sunwal Municipality", VDCNameNepali = "सुनवल नगरपालिका", DistrictId = 48 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 260, VDCName = "	Bardghat Municipality", VDCNameNepali = "बर्दघाट नगरपालिका", DistrictId = 48 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 672, VDCName = "Triveni Susta Rural Municipality", VDCNameNepali = "त्रिवेणी सुस्ता गाउँपालिका", DistrictId = 48 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 673, VDCName = "Pratappur Rural Municipality", VDCNameNepali = "प्रतापपुर गाउँपालिका", DistrictId = 48 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 674, VDCName = "Sarawal Rural Municipality", VDCNameNepali = "सरावल गाउँपालिका", DistrictId = 48 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 675, VDCName = "Palhi Nandan Rural Municipality", VDCNameNepali = "पाल्हीनन्दन गाउँपालिका", DistrictId = 48 });

            //रुपन्देही
            vdcmunlist.Add(new MunicipalityVDC() { Id = 262, VDCName = "	Butwal Municipality", VDCNameNepali = "बुटवल उप-महानगरपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 263, VDCName = "	Tilottama Municipality", VDCNameNepali = "तिलोत्तमा नगरपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 264, VDCName = "	Lumbini Sanskritik Municipality", VDCNameNepali = "लुम्बिनी सांस्कृतिक नगरपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 265, VDCName = "	Siddharthanagar Municipality", VDCNameNepali = "सिद्धार्थनगर नगरपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 266, VDCName = "	Sainamaina Municipality", VDCNameNepali = "सैनामैना नगरपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 267, VDCName = "	Devdaha Municipality", VDCNameNepali = "देवदह नगरपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 676, VDCName = "Gaidhawa Rural Municipality", VDCNameNepali = "गैडहवा गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 677, VDCName = "Mayadevi Rural Municipality", VDCNameNepali = "मायादेवी गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 678, VDCName = "Kotahimai Rural Municipality", VDCNameNepali = "कोटहीमाई गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 679, VDCName = "Marchawarimai Rural Municipality", VDCNameNepali = "मर्चवारीमाई गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 680, VDCName = "Siyari Rural Municipality", VDCNameNepali = "सियारी गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 681, VDCName = "Sammarimai Rural Municipality", VDCNameNepali = "सम्मरीमाई गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 682, VDCName = "Rohini Rural Municipality", VDCNameNepali = "रोहिणी गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 683, VDCName = "Shuddhodhan Rural Municipality", VDCNameNepali = "शुद्धोधन गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 684, VDCName = "Om Satiya Rural Municipality", VDCNameNepali = "ओमसतीया गाउँपालिका", DistrictId = 49 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 685, VDCName = "Kanchan Rural Municipality", VDCNameNepali = "कञ्चन गाउँपालिका", DistrictId = 49 });


            //अर्घाखाँची
            vdcmunlist.Add(new MunicipalityVDC() { Id = 268, VDCName = "	Sitganga Municipality", VDCNameNepali = "सितगंगा नगरपालिका", DistrictId = 50 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 269, VDCName = "	Sandhikharka Municipality", VDCNameNepali = "सन्धिखर्क नगरपालिका", DistrictId = 50 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 270, VDCName = "	Bhumikasthan Municipality", VDCNameNepali = "भूमिकास्थान नगरपालिका", DistrictId = 50 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 686, VDCName = "Malarani Rural Municipality", VDCNameNepali = "मालारानी गाउँपालिका", DistrictId = 50 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 687, VDCName = "Pandini Rural Municipality", VDCNameNepali = "पाणिनी गाउँपालिका", DistrictId = 50 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 688, VDCName = "Chhatradev Rural Municipality", VDCNameNepali = "छत्रदेव गाउँपालिका", DistrictId = 50 });


            //गुल्मी
            vdcmunlist.Add(new MunicipalityVDC() { Id = 271, VDCName = "	Musikot Municipality", VDCNameNepali = "मुसिकोट नगरपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 272, VDCName = "	Resunga Municipality", VDCNameNepali = "रेसुङ्गा नगरपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 689, VDCName = "Satyawati Rural Municipality", VDCNameNepali = "सत्यवती गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 690, VDCName = "Dhurkot Rural Municipality", VDCNameNepali = "धुर्कोट गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 691, VDCName = "Gulmi Durbar Rural Municipality", VDCNameNepali = "गुल्मीदरवार गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 692, VDCName = "Madane Rural Municipality", VDCNameNepali = "मदाने गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 693, VDCName = "Chandrakot Rural Municipality", VDCNameNepali = "चन्द्रकोट गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 694, VDCName = "Malika Rural Municipality", VDCNameNepali = "मालिका गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 695, VDCName = "Chhatrakot Rural Municipality", VDCNameNepali = "छत्रकोट गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 696, VDCName = "Isma Rural Municipality", VDCNameNepali = "ईस्मा गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 697, VDCName = "Kaligandaki Rural Municipality", VDCNameNepali = "कालीगण्डकी गाउँपालिका", DistrictId = 51 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 698, VDCName = "Ruru Rural Municipality", VDCNameNepali = "रुरु गाउँपालिका", DistrictId = 51 });

            //पाल्पा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 272343345, VDCName = "Tansen Municipality", VDCNameNepali = "तानसेन नगरपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 27324324, VDCName = "Rampur Municipality", VDCNameNepali = "रामपुर नगरपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 699, VDCName = "Rainadevi Chhahara Rural Municipality", VDCNameNepali = "रैनादेवी छहरा गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 700, VDCName = "Mathagadhi Rural Municipality", VDCNameNepali = "माथागढी गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 701, VDCName = "Nisdi Rural Municipality", VDCNameNepali = "निस्दी गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 702, VDCName = "Bagnaskali Rural Municipality", VDCNameNepali = "वगनासकाली गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 703, VDCName = "Rambha Rural Municipality", VDCNameNepali = "रम्भा	 गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 704, VDCName = "Purbakhola Rural Municipality", VDCNameNepali = "पूर्वखोला गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 705, VDCName = "Tinau Rural Municipality", VDCNameNepali = "तिनाउ गाउँपालिका", DistrictId = 52 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 706, VDCName = "Ribdikot Rural Municipality", VDCNameNepali = "रिब्दीकोट गाउँपालिका", DistrictId = 52 });

            //दाङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 275, VDCName = "Ghorahi Municipality", VDCNameNepali = "घोराही उप-महानगरपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 276, VDCName = "Tulsipur Municipality", VDCNameNepali = "तुल्सिपुर उप-महानगरपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 27234323, VDCName = "Lamahi Municipality", VDCNameNepali = "लमही नगरपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 707, VDCName = "Rapti Rural Municipality", VDCNameNepali = "राप्ती गाउँपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 708, VDCName = "Gadhawa Rural Municipality", VDCNameNepali = "गढवा गाउँपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 709, VDCName = "Babai Rural Municipality", VDCNameNepali = "बबई गाउँपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 710, VDCName = "Shantinagar Rural Municipality", VDCNameNepali = "शान्तिनगर गाउँपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 711, VDCName = "Rajpur Rural Municipality", VDCNameNepali = "राजपुर गाउँपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 712, VDCName = "Banglachuli Rural Municipality", VDCNameNepali = "वंगलाचुली	 गाउँपालिका", DistrictId = 53 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 713, VDCName = "Dangisharan Rural Municipality", VDCNameNepali = "दंगीशरण गाउँपालिका", DistrictId = 53 });


            //प्युठान
            vdcmunlist.Add(new MunicipalityVDC() { Id = 211777409, VDCName = "Pyuthan Municipality", VDCNameNepali = "प्युठान नगरपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 277, VDCName = "Swargadwari", VDCNameNepali = "स्वर्गद्वारी नगरपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 714, VDCName = "Naubahini Rural Municipality", VDCNameNepali = "नौबहिनी गाउँपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 715, VDCName = "Jhimaruk Rural Municipality", VDCNameNepali = "झिमरुक गाउँपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 716, VDCName = "Gaumukhi Rural Municipality", VDCNameNepali = "गौमुखी गाउँपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 717, VDCName = "Airawati Rural Municipality", VDCNameNepali = "ऐरावती गाउँपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 718, VDCName = "Sarumarani Rural Municipality", VDCNameNepali = "सरुमारानी गाउँपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 719, VDCName = "Mallarani Rural Municipality", VDCNameNepali = "मल्लरानी गाउँपालिका", DistrictId = 54 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 720, VDCName = "Mandavi Rural Municipality", VDCNameNepali = "माण्डवी गाउँपालिका", DistrictId = 54 });

            //रोल्पा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 278, VDCName = "	Rolpa Municipality", VDCNameNepali = "रोल्पा नगरपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 721, VDCName = "Sunil Smriti Rural Municipality", VDCNameNepali = "सुनिल स्मृति गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 722, VDCName = "Runtigadhi Rural Municipality", VDCNameNepali = "रुन्टीगढी गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 723, VDCName = "Lungri Rural Municipality", VDCNameNepali = "लुङ्ग्री गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 724, VDCName = "Triveni Rural Municipality", VDCNameNepali = "त्रिवेणी गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 725, VDCName = "Paribartan Rural Municipality", VDCNameNepali = "परिवर्तन गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 726, VDCName = "Gangadev Rural Municipality", VDCNameNepali = "गंगादेव गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 727, VDCName = "Madi Rural Municipality", VDCNameNepali = "माडी गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 728, VDCName = "Sunchhahari Rural Municipality", VDCNameNepali = "सुनछहरी गाउँपालिका", DistrictId = 55 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 729, VDCName = "Thawang Rural Municipality", VDCNameNepali = "थवाङ गाउँपालिका", DistrictId = 55 });



            //पूर्वी रूकुम
            vdcmunlist.Add(new MunicipalityVDC() { Id = 730, VDCName = "Bhume Rural Municipality", VDCNameNepali = "भूमे गाउँपालिका", DistrictId = 56 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 731, VDCName = "Putha Uttarganga Rural Municipality", VDCNameNepali = "पुठा उत्तरगंगा गाउँपालिका", DistrictId = 56 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 732, VDCName = "Sisne Rural Municipality", VDCNameNepali = "सिस्ने गाउँपालिका", DistrictId = 56 });


            //बाँके
            vdcmunlist.Add(new MunicipalityVDC() { Id = 279, VDCName = "	Nepalgunj Municipality", VDCNameNepali = "नेपालगंज उप-महानगरपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 280, VDCName = "	Kohalpur Municipality", VDCNameNepali = "कोहलपुर नगरपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 733, VDCName = "Raptisonari Rural Municipality", VDCNameNepali = "राप्ती सोनारी गाउँपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 734, VDCName = "Baijnath	 Rural Municipality", VDCNameNepali = "वैजनाथ गाउँपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 735, VDCName = "Khajura Rural Municipality", VDCNameNepali = "खजुरा गाउँपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 736, VDCName = "Janaki Rural Municipality", VDCNameNepali = "जानकी गाउँपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 737, VDCName = "Duduwa Rural Municipality", VDCNameNepali = "डुडुवा गाउँपालिका", DistrictId = 57 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 738, VDCName = "Narainapur Rural Municipality", VDCNameNepali = "नरैनापुर गाउँपालिका", DistrictId = 57 });

            //बर्दिया
            vdcmunlist.Add(new MunicipalityVDC() { Id = 281, VDCName = "	Barbardiya Municipality", VDCNameNepali = "बारबर्दिया नगरपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 282, VDCName = "	Gulariya Municipality", VDCNameNepali = "गुलरिया नगरपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 283, VDCName = "	Rajapur Municipality", VDCNameNepali = "राजापुर नगरपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 284, VDCName = "	Bansgadhi Municipality", VDCNameNepali = "बासगढी नगरपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 285, VDCName = "	Madhuwan Municipality", VDCNameNepali = "मधुवन नगरपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 286, VDCName = "	Thakurbaba Municipality", VDCNameNepali = "ठाकुरबाबा नगरपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 739, VDCName = "Badhaiyatal Rural Municipality", VDCNameNepali = "बढैयाताल	 गाउँपालिका", DistrictId = 58 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 740, VDCName = "Geruwa Rural Municipality", VDCNameNepali = "गेरुवा गाउँपालिका", DistrictId = 58 });




            //पश्चिमी रूकुम
            vdcmunlist.Add(new MunicipalityVDC() { Id = 287, VDCName = "	Aathabiskot Municipality", VDCNameNepali = "आठबिसकोट नगरपालिका", DistrictId = 59 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 288, VDCName = "	Chaurjahari Municipality", VDCNameNepali = "चौरजहारी नगरपालिका", DistrictId = 59 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 741, VDCName = "Sani Bheri Rural Municipality", VDCNameNepali = "सानीभेरी गाउँपालिका", DistrictId = 59 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 742, VDCName = "Triveni Rural Municipality", VDCNameNepali = "त्रिवेणी गाउँपालिका", DistrictId = 59 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 743, VDCName = "Banphikot Rural Municipality", VDCNameNepali = "बाँफिकोट गाउँपालिका", DistrictId = 59 });

            //सल्यान
            vdcmunlist.Add(new MunicipalityVDC() { Id = 289, VDCName = "	Bangad Kupinde Municipality", VDCNameNepali = "बनगाड कुपिण्डे नगरपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 290, VDCName = "	Bagchaur Municipality", VDCNameNepali = "बागचौर नगरपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 291, VDCName = "	Shaarda Municipality", VDCNameNepali = "शारदा नगरपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 744, VDCName = "Kumakh Rural Municipality", VDCNameNepali = "कुमाख गाउँपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 745, VDCName = "Kalimati Rural Municipality", VDCNameNepali = "कालीमाटी गाउँपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 746, VDCName = "Chhatreshwari Rural Municipality", VDCNameNepali = "छत्रेश्वरी गाउँपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 747, VDCName = "Darma Rural Municipality", VDCNameNepali = "दार्मा गाउँपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 748, VDCName = "Kapurkot Rural Municipality", VDCNameNepali = "कपुरकोट गाउँपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 749, VDCName = "Triveni Rural Municipality", VDCNameNepali = "त्रिवेणी गाउँपालिका", DistrictId = 60 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 750, VDCName = "Siddha Kumakh Rural Municipality", VDCNameNepali = "सिद्ध कुमाख गाउँपालिका", DistrictId = 60 });


            //डोल्पा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 293, VDCName = "Tripura Sundari Municipality", VDCNameNepali = "त्रिपुरासुन्दरी नगरपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 292, VDCName = "Thuli Bheri Municipality", VDCNameNepali = "ठूली भेरी नगरपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 752, VDCName = "Mudkechula Rural Municipality", VDCNameNepali = "मुड्केचुला गाउँपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 753, VDCName = "Kaike Rural Municipality", VDCNameNepali = "काईके गाउँपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 754, VDCName = "She Phoksundo Rural Municipality", VDCNameNepali = "शे फोक्सुन्डो गाउँपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 755, VDCName = "Jagadulla Rural Municipality", VDCNameNepali = "जगदुल्ला गाउँपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 756, VDCName = "Dolpo Buddha Rural Municipality", VDCNameNepali = "डोल्पो बुद्ध गाउँपालिका", DistrictId = 61 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 757, VDCName = "Chharka Tangsong Rural Municipality", VDCNameNepali = "छार्का ताङसोङ गाउँपालिका", DistrictId = 61 });


            //हुम्ला
            vdcmunlist.Add(new MunicipalityVDC() { Id = 758, VDCName = "Simkot Rural Municipality", VDCNameNepali = "सिमकोट गाउँपालिका", DistrictId = 62 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 759, VDCName = "Sarkegad Rural Municipality", VDCNameNepali = "सर्केगाड गाउँपालिका", DistrictId = 62 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 760, VDCName = "Adanchuli Rural Municipality", VDCNameNepali = "अदानचुली गाउँपालिका", DistrictId = 62 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 761, VDCName = "Kharpunath Rural Municipality", VDCNameNepali = "खार्पुनाथ गाउँपालिका", DistrictId = 62 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 762, VDCName = "Tanjakot Rural Municipality", VDCNameNepali = "ताँजाकोट गाउँपालिका", DistrictId = 62 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 763, VDCName = "Chankheli Rural Municipality", VDCNameNepali = "चंखेली गाउँपालिका", DistrictId = 62 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 764, VDCName = "Namkha Rural Municipality", VDCNameNepali = "नाम्खा गाउँपालिका", DistrictId = 62 });

            //जुम्ला
            vdcmunlist.Add(new MunicipalityVDC() { Id = 294, VDCName = "	Chandannath Municipality", VDCNameNepali = "चन्दननाथ नगरपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 765, VDCName = "Tatopani Rural Municipality", VDCNameNepali = "तातोपानी गाउँपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 766, VDCName = "Patarasi Rural Municipality", VDCNameNepali = "पातारासी गाउँपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 767, VDCName = "Tila Rural Municipality", VDCNameNepali = "तिला गाउँपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 768, VDCName = "Kanaka Sundari Rural Municipality", VDCNameNepali = "कनकासुन्दरी गाउँपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 769, VDCName = "Sinja Rural Municipality", VDCNameNepali = "सिंजा गाउँपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 770, VDCName = "Hima Rural Municipality", VDCNameNepali = "हिमा गाउँपालिका", DistrictId = 63 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 771, VDCName = "Guthichaur Rural Municipality", VDCNameNepali = "गुठिचौर गाउँपालिका", DistrictId = 63 });

            //कालिकोट
            vdcmunlist.Add(new MunicipalityVDC() { Id = 295, VDCName = "	Khandachakra Municipality", VDCNameNepali = "खाँडाचक्र नगरपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 296, VDCName = "	Raskot Municipality", VDCNameNepali = "रास्कोट नगरपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 297, VDCName = "	Tilagupha Municipality", VDCNameNepali = "तिलागुफा नगरपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 772, VDCName = "Narharinath Rural Municipality", VDCNameNepali = "नरहरिनाथ गाउँपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 773, VDCName = "Palata Rural Municipality", VDCNameNepali = "पलाता गाउँपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 774, VDCName = "Shubha Kalika Rural Municipality", VDCNameNepali = "	शुभ कालिका गाउँपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 775, VDCName = "Sanni Triveni Rural Municipality", VDCNameNepali = "सान्नी त्रिवेणी गाउँपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 776, VDCName = "Pachaljharana Rural Municipality", VDCNameNepali = "पचालझरना गाउँपालिका", DistrictId = 64 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 777, VDCName = "Mahawai Rural Municipality", VDCNameNepali = "महावै गाउँपालिका", DistrictId = 64 });

            //मुगु
            vdcmunlist.Add(new MunicipalityVDC() { Id = 298, VDCName = "	Chhayanath Rara", VDCNameNepali = "छायाँनाथ रारा नगरपालिका", DistrictId = 65 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 778, VDCName = "Khatyad Rural Municipality", VDCNameNepali = "खत्याड गाउँपालिका", DistrictId = 65 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 779, VDCName = "Soru Rural Municipality", VDCNameNepali = "सोरु गाउँपालिका", DistrictId = 65 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 780, VDCName = "Mugum Karmarong Rural Municipality", VDCNameNepali = "मुगुम कार्मारोंग गाउँपालिका", DistrictId = 65 });

            //सुर्खेत
            vdcmunlist.Add(new MunicipalityVDC() { Id = 299, VDCName = "	Birendranagar Municipality", VDCNameNepali = "बीरेन्द्रनगर नगरपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 300, VDCName = "	Gurbhakot Municipality", VDCNameNepali = "गुर्भाकोट नगरपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 301, VDCName = "	Panchapuri Municipality", VDCNameNepali = "पञ्चपुरी नगरपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 302, VDCName = "	Bheriganga Municipality", VDCNameNepali = "भेरीगंगा नगरपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 303, VDCName = "	Lekbeshi Municipality", VDCNameNepali = "लेकबेशी नगरपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 781, VDCName = "Barahatal Rural Municipality", VDCNameNepali = "बराहताल गाउँपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 782, VDCName = "Simta Rural Municipality", VDCNameNepali = "सिम्ता गाउँपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 783, VDCName = "Chaukune Rural Municipality", VDCNameNepali = "चौकुने गाउँपालिका", DistrictId = 66 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 784, VDCName = "Chingad Rural Municipality", VDCNameNepali = "चिङ्गाड गाउँपालिका", DistrictId = 66 });

            //दैलेख
            vdcmunlist.Add(new MunicipalityVDC() { Id = 304, VDCName = "	Dullu Municipality", VDCNameNepali = "दुल्लु नगरपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 305, VDCName = "	Aathabis Municipality", VDCNameNepali = "आठबीस नगरपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 306, VDCName = "	Narayan Municipality", VDCNameNepali = "नारायण नगरपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 307, VDCName = "	Chamunda Bindrasaini Municipality", VDCNameNepali = "चामुण्डा विन्द्रासैनी नगरपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 785, VDCName = "Gurans Rural Municipality", VDCNameNepali = "गुराँस गाउँपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 786, VDCName = "Bhairabi Rural Municipality", VDCNameNepali = "भैरवी गाउँपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 787, VDCName = "Naumule Rural Municipality", VDCNameNepali = "नौमुले गाउँपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 788, VDCName = "Mahabu Rural Municipality", VDCNameNepali = "महावु गाउँपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 789, VDCName = "Thantikandh Rural Municipality", VDCNameNepali = "ठाँटीकाँध गाउँपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 790, VDCName = "Bhagawatimai Rural Municipality", VDCNameNepali = "भगवतीमाई गाउँपालिका", DistrictId = 67 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 791, VDCName = "Dungeshwar Rural Municipality", VDCNameNepali = "डुंगेश्वर गाउँपालिका", DistrictId = 67 });

            //जाजरकोट
            vdcmunlist.Add(new MunicipalityVDC() { Id = 308, VDCName = "	Chhedagad Municipality", VDCNameNepali = "छेडागाड नगरपालिका", DistrictId = 68 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 309, VDCName = "	Bheri Municipality", VDCNameNepali = "भेरी नगरपालिका", DistrictId = 68 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 310, VDCName = "	Nalgad Municipality", VDCNameNepali = "नलगाड नगरपालिका", DistrictId = 68 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 792, VDCName = "Junichande Rural Municipality", VDCNameNepali = "जुनीचाँदे गाउँपालिका", DistrictId = 68 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 793, VDCName = "Kushe Rural Municipality", VDCNameNepali = "कुसे गाउँपालिका", DistrictId = 68 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 794, VDCName = "Barekot Rural Municipality", VDCNameNepali = "बारेकोट गाउँपालिका", DistrictId = 68 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 795, VDCName = "Shivalaya Rural Municipality", VDCNameNepali = "शिवालय गाउँपालिका", DistrictId = 68 });

            //कैलाली
            vdcmunlist.Add(new MunicipalityVDC() { Id = 311, VDCName = "	Dhangadhi Municipality", VDCNameNepali = "धनगढी उप-महानगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 312, VDCName = "	Tikapur Municipality", VDCNameNepali = "टिकापुर नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 313, VDCName = "	Godawari Municipality", VDCNameNepali = "गोदावरी नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 314, VDCName = "	Lamki Chuha Municipality", VDCNameNepali = "लम्की चुहा नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 315, VDCName = "	Tikapur Municipality", VDCNameNepali = "टिकापुर नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 316, VDCName = "	Ghodaghodi Municipality", VDCNameNepali = "घोडाघोडी नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 317, VDCName = "	Gauriganga Municipality", VDCNameNepali = "गौरीगंगा नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 318, VDCName = "	Bhajani Municipality", VDCNameNepali = "भजनी नगरपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 796, VDCName = "Janaki Rural Municipality", VDCNameNepali = "जानकी गाउँपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 797, VDCName = "Kailari Rural Municipality", VDCNameNepali = "कैलारी गाउँपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 798, VDCName = "Joshipur Rural Municipality", VDCNameNepali = "जोशीपुर गाउँपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 799, VDCName = "Bardagoriya Rural Municipality", VDCNameNepali = "बर्गगोरिया गाउँपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 800, VDCName = "Mohanyal Rural Municipality", VDCNameNepali = "मोहन्याल गाउँपालिका", DistrictId = 69 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 801, VDCName = "Chure Rural Municipality", VDCNameNepali = "चुरे	 गाउँपालिका", DistrictId = 69 });

            //अछाम
            vdcmunlist.Add(new MunicipalityVDC() { Id = 319, VDCName = "Kamalbazar Municipality", VDCNameNepali = "कमलबजार नगरपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 320, VDCName = "Sanphebagar Municipality", VDCNameNepali = "साफेबगर नगरपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 321, VDCName = "Mangalsen Municipality", VDCNameNepali = "मंगलसेन नगरपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 322, VDCName = "Panchadewal Binayak Municipality", VDCNameNepali = "पञ्चदेवल विनायक नगरपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 802, VDCName = "Ramaroshan Rural Municipality", VDCNameNepali = "रामारोशन गाउँपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 803, VDCName = "Chaurpati Rural Municipality", VDCNameNepali = "चौरपाटी गाउँपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 804, VDCName = "Turmakhand Rural Municipality", VDCNameNepali = "तुर्माखाँद गाउँपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 805, VDCName = "Mellekh Rural Municipality", VDCNameNepali = "मेल्लेख गाउँपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 806, VDCName = "Dhankari Rural Municipality", VDCNameNepali = "ढँकारी गाउँपालिका", DistrictId = 70 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 802344567, VDCName = "Bannigadi Jayagad Rural Municipality", VDCNameNepali = "बान्नीगडीजैगड गाउँपालिका", DistrictId = 70 });

            //डोटी
            vdcmunlist.Add(new MunicipalityVDC() { Id = 323, VDCName = "Dipayal Silgadhi Municipality", VDCNameNepali = "दिपायल सिलगढी नगरपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 324, VDCName = "Shikhar Municipality", VDCNameNepali = "शिखर नगरपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 807, VDCName = "Aadarsha Rural Municipality", VDCNameNepali = "आदर्श गाउँपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 808, VDCName = "Purbichauki Rural Municipality", VDCNameNepali = "पूर्वीचौकी गाउँपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 809, VDCName = "K.I. Singh Rural Municipality", VDCNameNepali = "केआईसिंह गाउँपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 810, VDCName = "Jorayal Rural Municipality", VDCNameNepali = "जोरायल गाउँपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 811, VDCName = "Sayal Rural Municipality", VDCNameNepali = "सायल गाउँपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 812, VDCName = "Bogatan-Phudsil Rural Municipality", VDCNameNepali = "वोगटान–फुड्सिल गाउँपालिका", DistrictId = 71 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 813, VDCName = "Badikedar Rural Municipality", VDCNameNepali = "बड्डी केदार गाउँपालिका", DistrictId = 71 });

            //बझाङ
            vdcmunlist.Add(new MunicipalityVDC() { Id = 325, VDCName = "Bungal Municipality", VDCNameNepali = "बुंगल नगरपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 326, VDCName = "Jaya Prithvi Municipality", VDCNameNepali = "जयपृथ्वी नगरपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 814, VDCName = "Kedarsyu Rural Municipality", VDCNameNepali = "केदारस्यु गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 815, VDCName = "Thalara Rural Municipality", VDCNameNepali = "थलारा गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 816, VDCName = "Bitthadchir Rural Municipality", VDCNameNepali = "बित्थडचिर गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 817, VDCName = "Chhabis Pathibhera Rural Municipality", VDCNameNepali = "छब्बीसपाथिभेरा गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 818, VDCName = "Chhanna Rural Municipality", VDCNameNepali = "छान्ना गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 819, VDCName = "Masta Rural Municipality", VDCNameNepali = "मष्टा गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 820, VDCName = "Durgathali Rural Municipality", VDCNameNepali = "दुर्गाथली गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 821, VDCName = "Talkot Rural Municipality", VDCNameNepali = "तलकोट गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 822, VDCName = "Surma Rural Municipality", VDCNameNepali = "सुर्मा गाउँपालिका", DistrictId = 72 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 823, VDCName = "Saipal Rural Municipality", VDCNameNepali = "सइपाल गाउँपालिका", DistrictId = 72 });

            //बाजुरा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 327, VDCName = "Budhiganga Municipality", VDCNameNepali = "बुढीगंगा नगरपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 328, VDCName = "Budhinanda Municipality", VDCNameNepali = "बुढीनन्दा नगरपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 329, VDCName = "Tribeni Municipality", VDCNameNepali = "त्रिवेणी नगरपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 330, VDCName = "Badimalika Municipality", VDCNameNepali = "बडीमालिका नगरपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 824, VDCName = "Khaptad Chhededaha Rural Municipality", VDCNameNepali = "खप्तड छेडेदह गाउँपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 825, VDCName = "Swami Kartik Khapar Rural Municipality", VDCNameNepali = "स्वामिकार्तिक खापर गाउँपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 826, VDCName = "Jagannath Rural Municipality", VDCNameNepali = "जगन्नाथ गाउँपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 827, VDCName = "Himali Rural Municipality", VDCNameNepali = "हिमाली गाउँपालिका", DistrictId = 73 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 828, VDCName = "Gaumul Rural Municipality", VDCNameNepali = "गौमुल गाउँपालिका", DistrictId = 73 });

            //कंचनपुर
            vdcmunlist.Add(new MunicipalityVDC() { Id = 331, VDCName = "	Bhimdatta", VDCNameNepali = "भीमदत्त नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 332, VDCName = "	Krishnapur", VDCNameNepali = "कृष्णपुर नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 333, VDCName = "	Punarbas", VDCNameNepali = "पुनर्वास नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 334, VDCName = "	Belauri", VDCNameNepali = "बेलौरी नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 335, VDCName = "	Bedkot", VDCNameNepali = "वेदकोट नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 336, VDCName = "	Shuklaphanta", VDCNameNepali = "शुक्लाफाँटा नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 337, VDCName = "	Mahakali", VDCNameNepali = "माहाकाली नगरपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 829, VDCName = "Laljhadi Rural Municipality", VDCNameNepali = "लालझाँडी गाउँपालिका", DistrictId = 74 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 830, VDCName = "Beldandi Rural Municipality", VDCNameNepali = "बेलडाँडी गाउँपालिका", DistrictId = 74 });

            //डडेलधुरा
            vdcmunlist.Add(new MunicipalityVDC() { Id = 338, VDCName = "	Parashuram Municipality", VDCNameNepali = "परशुराम नगरपालिका", DistrictId = 75 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 339, VDCName = "	Amargadhi Municipality", VDCNameNepali = "अमरगढी नगरपालिका", DistrictId = 75 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 831, VDCName = "Navadurga Rural Municipality", VDCNameNepali = "नवदुर्गा	 गाउँपालिका", DistrictId = 75 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 832, VDCName = "Aalitaal Rural Municipality", VDCNameNepali = "आलिताल गाउँपालिका", DistrictId = 75 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 833, VDCName = "Ganyapadhura Rural Municipality", VDCNameNepali = "गन्यापधुरा गाउँपालिका", DistrictId = 75 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 834, VDCName = "Bhageshwar Rural Municipality", VDCNameNepali = "भागेश्वर गाउँपालिका", DistrictId = 75 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 835, VDCName = "Ajaymeru Rural Municipality", VDCNameNepali = "अजयमेरु गाउँपालिका", DistrictId = 75 });

            //बैतडी
            vdcmunlist.Add(new MunicipalityVDC() { Id = 340, VDCName = "	Melauli Municipality", VDCNameNepali = "मेलौली नगरपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 341, VDCName = "	Purchaundi Municipality", VDCNameNepali = "पुर्चौडी नगरपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 342, VDCName = "	Dasharathchand Municipality", VDCNameNepali = "दशरथचन्द नगरपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 343, VDCName = "	Patan Municipality", VDCNameNepali = "पाटन नगरपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 836, VDCName = "Dogdakedar Rural Municipality", VDCNameNepali = "दोगडाकेदार गाउँपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 837, VDCName = "Dilashaini Rural Municipality", VDCNameNepali = "डिलाशैनी गाउँपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 838, VDCName = "Sigas Rural Municipality", VDCNameNepali = "सिगास गाउँपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 839, VDCName = "Pancheshwar Rural Municipality", VDCNameNepali = "पञ्चेश्वर गाउँपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 840, VDCName = "Surnaya Rural Municipality", VDCNameNepali = "सुर्नया गाउँपालिका", DistrictId = 76 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 841, VDCName = "Shivanath Rural Municipality", VDCNameNepali = "शिवनाथ गाउँपालिका", DistrictId = 76 });

            //दार्चुला
            vdcmunlist.Add(new MunicipalityVDC() { Id = 344, VDCName = "	Shailyashikhar Municipality", VDCNameNepali = "शैल्यशिखर नगरपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 345, VDCName = "	Mahakali Municipality", VDCNameNepali = "महाकाली नगरपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 842, VDCName = "Naugad Rural Municipality", VDCNameNepali = "नौगाड गाउँपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 843, VDCName = "Malikarjun Rural Municipality", VDCNameNepali = "मालिकार्जुन गाउँपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 844, VDCName = "Marma Rural Municipality", VDCNameNepali = "मार्मा गाउँपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 845, VDCName = "Lekam Rural Municipality", VDCNameNepali = "लेकम गाउँपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 846, VDCName = "Duhu Rural Municipality", VDCNameNepali = "दुहु गाउँपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 847, VDCName = "Vyans Rural Municipality", VDCNameNepali = "ब्याँस गाउँपालिका", DistrictId = 77 });
            vdcmunlist.Add(new MunicipalityVDC() { Id = 848, VDCName = "Api Himal Rural Municipality", VDCNameNepali = "अपि हिमाल गाउँपालिका", DistrictId = 77 });


            return vdcmunlist;
        }
    }
}
