﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
   public  class ShrediDto
    {
        public long Id { get; set; }
        public string SherdiEnglish { get; set; }
        public string SherdiNepali { get; set; }
    }
}
