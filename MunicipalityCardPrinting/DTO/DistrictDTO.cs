﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DistrictDTO
    {
        public long Id { get; set; }
        public long ProvinceId { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameNepali { get; set; }
        public string HeadQuarter { get; set; }
        public string Description { get; set; }
    }
}
