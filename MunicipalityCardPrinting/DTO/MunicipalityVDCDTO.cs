﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class MunicipalityVDCDTO
    {
        public long Id { get; set; }
        public long DistrictId { get; set; }
        public string VDCName { get; set; }
        public string VDCNameNepali { get; set; }
        public string Description { get; set; }
    }
}
