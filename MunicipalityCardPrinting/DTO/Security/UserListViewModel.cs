﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class UserListViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string FullNameNepali { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}
