﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class EditUserViewModel
    {
        public string Id { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "ई-मेल")]
        public string Email { get; set; }
        [Display(Name = "युजरनेम")]
        [Required]
        public string UserName { get; set; }
        [Display(Name = "फोन नम्बर ")]
        [MaxLength(10, ErrorMessage = "Phone number shouldn't be greater than 10 digit")]
        public string PhoneNumber { get; set; }
        
        [Display(Name = "नाम(English)")]
        public string FullName { get; set; }
        [Display(Name = "नाम")]
        public string NameNepali { get; set; }
        [Display(Name = "पद")]
        public long DesignationId { get; set; }
        [Display(Name = "रोल")]
        public string RoleId { get; set; }
    }
}
