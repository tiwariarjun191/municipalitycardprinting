﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ProgressReportTotalModel
    {
        public string TypeAMale { get; set; }
        public string TypeAFemale { get; set; }
        public string TypeAOther { get; set; }
        public string TypeATotal { get; set; }
        public string TypeBMale { get; set; }
        public string TypeBFemale { get; set; }
        public string TypeBOther { get; set; }
        public string TypeBTotal { get; set; }
        public string TypeCMale { get; set; }
        public string TypeCFemale { get; set; }
        public string TypeCOther { get; set; }
        public string TypeCTotal { get; set; }
        public string TypeDMale { get; set; }
        public string TypeDFemale { get; set; }
        public string TypeDOther { get; set; }
        public string TypeDTotal { get; set; }

        public string TotalMale { get; set; }
        public string TotalFemale { get; set; }
        public string TotalOther { get; set; }
        public string Total { get; set; }

    }
}
