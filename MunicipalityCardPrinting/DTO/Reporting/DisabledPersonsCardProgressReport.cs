﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DisabledPersonsCardProgressReport
    {
        public int DisabilityTypeEnumValue { get; set; }
        public string DisabilityTypeNepali { get; set; }
        public ProgressReportTypeWiseModel TypeA { get; set; }
        public ProgressReportTypeWiseModel TypeB { get; set; }
        public ProgressReportTypeWiseModel TypeC { get; set; }
        public ProgressReportTypeWiseModel TypeD { get; set; }
        public int TotalMale { get; set; }
        public int TotalFemale { get; set; }
        public int TotalOther { get; set; }
        public int Total { get; set; }
        public string TotalMaleNepali { get; set; }
        public string TotalFemaleNepali { get; set; }
        public string TotalOtherNepali { get; set; }
        public string TotalNepali { get; set; }
    }
}
