﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ProgressReportTypeWiseModel
    {
        public long Id { get; set; }
       
        public int MaleCount { get; set; }
        public int FemaleCount { get; set; }
        public int OtherCount { get; set; }
        public int TotalCount { get; set; }
        public string MaleCountNepali { get; set; }
        public string FemaleCountNepali { get; set; }
        public string OtherCountNepali { get; set; }
        public string TotalCountNepali { get; set; }
    }
}
