﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DisabledCardViewModel
    {
        public long CardId { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string CardHolder { get; set; }
        public string DisablityTypeByNature { get; set; }
        public string DisablityTypeBySeriousness { get; set; }
    }
}
