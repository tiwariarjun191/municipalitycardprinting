﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ElderCardViewModel
    {
        public long CardId { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string CategoryOfElder { get; set; }

    }
}
