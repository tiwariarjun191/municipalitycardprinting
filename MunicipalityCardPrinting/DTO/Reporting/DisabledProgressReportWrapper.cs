﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DisabledProgressReportWrapper
    {
        public string DistrictNepali { get; set; }
        public string Municipality { get; set; }
        public string DateNepali { get; set; }
        public List<DisabledPersonsCardProgressReport> reports { get; set; }
        public ProgressReportTotalModel Total { get; set; }

    }
}
