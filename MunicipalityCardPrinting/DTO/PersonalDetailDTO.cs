﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class PersonalDetailDTO
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string FullNameNepali { get; set; }
        public int Gender { get; set; }
        public long? TempAddressId { get; set; }
        public long? PermanentAddressId { get; set; }
        public string TempTole { get; set; }
        public string PermanentTole { get; set; }
        public string TempWardNumber { get; set; }
        public string PermanentWardNumber { get; set; }
        public DateTime? DOB { get; set; }
        public string DOBNepali { get; set; }
        public string CitizenshipNumber { get; set; }
        public string CitizenshipNumberNepali { get; set; }
        public int? BloodGroup { get; set; }
        public int? MarritalStatus { get; set; }
        public string SpouseName { get; set; }
        public string ContactNumber { get; set; }

        public bool IsDeleted { get; set; }
    }
}
