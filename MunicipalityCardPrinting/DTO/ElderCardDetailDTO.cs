﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class ElderCardDetailDTO
    {
        public long Id { get; set; }
        public long DisabledPersonId { get; set; }
        public long GuardianId { get; set; }
        public int CardCategory { get; set; }

        [Display(Name = "पुरा नाम")]
        public string FullNameNepali { get; set; }
        [Display(Name = "अस्थाई प्रदेश")]
        [Required]
        public long? TempProvinceId { get; set; }
        [Display(Name = "अस्थाई जिल्ला")]
        public long TempDistrictId { get; set; }
        [Display(Name = "अस्थाई नगर/गाउँपालिका")]
        public long TempAddressId { get; set; }
        [Display(Name = "टोल")]
        public string TempTole { get; set; }
        [Display(Name = "वार्ड")]
        [Required]
        public string TempWardNumber { get; set; }
        [Display(Name = "जन्म मिति")]
        public string DOBNepali { get; set; }
        [Display(Name = "नागरिकता नं")]
        public string CitizenshipNumberNepali { get; set; }
        [Display(Name = "लिङ्ग")]
        public int Gender { get; set; }
        [Display(Name = "रक्त समूह")]
        public int? BloodGroup { get; set; }
        [Display(Name = "वैवाहिक अवस्था")]
        public int? MarritalStatus { get; set; }
        [Display(Name = "संरक्षकको नाम")]
        public string GuardianFullNameNepali { get; set; }
        [Display(Name = "प्रदेश")]
        [Required]
        public long? GuardianTempProvinceId { get; set; }
        [Display(Name = "जिल्ला")]
        public long GuardianTempDistrictId { get; set; }
        [Display(Name = "नगर/गाउँपालिका")]
        public long GuardianTempAddressId { get; set; }
        [Display(Name = "टोल")]
        public string GuardianTempTole { get; set; }
        [Display(Name = "वार्ड")]
        public string GuardianTempWardNumber { get; set; }
        [Display(Name = "पति/पत्नीको नाम")]
        public string SpouseName { get; set; }
        [Display(Name = "उपलब्ध सेवा/सुबिधा")]
        public string OtherFacilities { get; set; }
        [Display(Name = "रोगको नाम(यदि भएमा)")]
        public string DiseaseName { get; set; }
        [Display(Name = "सेवन गर्ने दवाइको नाम")]
        public string MedicineName { get; set; }
        [Display(Name ="सम्पर्क नं")]
        public string ContactNumber { get; set; }
        [Display(Name = "जेष्ठताको वर्गीकरण")]
        public int? CategoryOfElderness { get; set; }
        [Display(Name = "प्रमाणित गर्ने ")]
        [Required]
        public string ApprovedBy { get; set; }
        [Display(Name = "फोटो")]
        public string Photo { get; set; }
    }
}
