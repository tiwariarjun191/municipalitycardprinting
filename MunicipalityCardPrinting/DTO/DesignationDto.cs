﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DesignationDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameNepali { get; set; }
    }
}
