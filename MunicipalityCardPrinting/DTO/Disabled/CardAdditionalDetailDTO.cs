﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class CardAdditionalDetailDTO
    {
        public long CardDetailId { get; set; }
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }
        public string Qualification { get; set; }
        [Display(Name = "Current School/College")]
        public string CurrentSchoolCollegeName { get; set; }
        [Display(Name = "Study status")]
        public string StudyingOrFinished { get; set; }
        [Display(Name = "What Kind Of Work can Do In Daily Life?")]
        public string WhatKindOfWorkcanDoInDailyLife { get; set; }
        [Display(Name = "What Kind Of Work can't do?")]
        public string WhatKindOfWorkCantDo { get; set; }
        [Display(Name = "Is needed any equipement?")]
        public int? IsNeededAnyEquipement { get; set; }
        [Display(Name = "Additional Equipements")]
        public string AdditionalEquipements { get; set; }
        [Display(Name = "Has Any Additional Equipement?")]
        public int? HasAnyAdditionalEquipement { get; set; }
        [Display(Name = "Othe Facilities")]
        public string OtherFacilities { get; set; }
        [Display(Name = "Other Facilities Needed?")]
        public string OtherFacilitiesNeeded { get; set; }
        [Display(Name = "Office To Provide Facility")]
        public string OfficeToProvideFacility { get; set; }
        public string Remarks { get; set; }
        [Display(Name = "Marrital Status")]
        public int? MarritalStatus { get; set; }
        [Display(Name = "Guardian Citizenship Number")]
        public string GuardianCitizenshipNumber { get; set; }
        [Display(Name = "Guardian Citizenship Number Nepali")]
        public string GuardianCitizenshipNumberNepali { get; set; }
        [Display(Name = "Guardian Blood Group")]
        public int? GuardianBloodGroup { get; set; }
        [Display(Name = "Guardian Marrital Status")]
        public int? GuardianMarritalStatus { get; set; }
    }
}
