﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class PrintedDisabledList
    {
        public long CardId { get; set; }
        public string CardNumber { get; set; }
        public string PrintedDate { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string CitizenshipNumber { get; set; }
    }
}
