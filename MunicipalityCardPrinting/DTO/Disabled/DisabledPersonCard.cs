﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DisabledPersonCard
    {
        public long Id { get; set; }
        public string CardTypeCSSClass { get; set; } 
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string DOB { get; set; }
        public string CitizenshipNumber { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string OnTheBasisOfNature { get; set; }
        public string OnTheBasisOfSeriousness { get; set; }
        public string GuardianName { get; set; }
        public string ApproverName { get; set; }
        public string Designation { get; set; }
        public string Date { get; set; }
        public string Photo { get; set; }
        public long PrintCount { get; set; }
        public string PrintDate { get; set; }

        //Nepali Field
        public string CardNumberNepali { get; set; }
        public string CardTypeNepali { get; set; }
        public string NameNepali { get; set; }
        public string AddressNepali { get; set; }
        public string DOBNepali { get; set; }
        public string CitizenshipNumberNepali { get; set; }
        public string GenderNepali { get; set; }
        public string BloodGroupNepali { get; set; }
        public string OnTheBasisOfNatureNepali { get; set; }
        public string OnTheBasisOfSeriousnessNepali { get; set; }
        public string GuardianNameNepali { get; set; }
        public string ApproverNameNepali { get; set; }
        public string DesignationNepali { get; set; }
        public string DateNepali { get; set; }
        public string PhotoNepali { get; set; }
        public string PrintCountNepali { get; set; }
        public string PrintDateNepali { get; set; }


        // For Missing Message
        public string Message { get; set; }
    }
}
