﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class DisabledIndividualReport
    {
        public long Id { get; set; }

        public int CardCategory { get; set; }
        public string CardNumberNepali { get; set; }
        public string CardTypeNepali { get; set; }
        public string NameNepali { get; set; }
        public string AddressNepali { get; set; }
        public string DOBNepali { get; set; }
        public string CitizenshipNumberNepali { get; set; }
        public string GenderNepali { get; set; }
        public string BloodGroupNepali { get; set; }
        public string OnTheBasisOfNatureNepali { get; set; }
        public string OnTheBasisOfSeriousnessNepali { get; set; }
        public string GuardianNameNepali { get; set; }
        public string MarritalStatusNepali { get; set; }
        public string PermanentAddress { get; set; }
        public string GuardianAddressNepali { get; set; }
        public string RelationWithCitizen { get; set; }
        public string ApproverNameNepali { get; set; }
        public string DesignationNepali { get; set; }
        public string DateNepali { get; set; }
        public string PhotoNepali { get; set; }
        public string PrintCountNepali { get; set; }
        public string PrintDateNepali { get; set; }

        public string Occupation { get; set; }
        public string Qualification { get; set; }
        public string CurrentSchoolCollegeName { get; set; }
        public string StudyingOrFinished { get; set; }
        public string WhatKindOfWorkcanDoInDailyLife { get; set; }
        public string WhatKindOfWorkCantDo { get; set; }
        public string IsNeededAnyEquipement { get; set; }
        public string AdditionalEquipements { get; set; }
        public string HasAnyAdditionalEquipement { get; set; }
        public string OtherFacilities { get; set; }
        public string OtherFacilitiesNeeded { get; set; }
        public string OfficeToProvideFacility { get; set; }
        public string Remarks { get; set; }


        // For Missing Message
        public string Message { get; set; }
    }
}
