﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class CardBasicDetailDTO
    {
        public long Id { get; set; }
        public long DisabledPersonId { get; set; }
        public long GuardianId { get; set; }
        public int CardCategory { get; set; }
        [Required]
        [Display(Name = "Card Type")]
        public int? IdCardType { get; set; }
        //[Display(Name = "Card Number")]
        //public string IdCardnumber { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        [Display(Name = "Full Name In Nepali")]
        public string FullNameNepali { get; set; }
        [Display(Name = "Municipality/Vdc")]
        public long? TempAddressId { get; set; }
        [Display(Name = "Vdc")]
        public long? PermanentAddressId { get; set; }
        [Display(Name = "Tole")]
        public string TempTole { get; set; }
        [Display(Name = "Tole")]
        public string PermanentTole { get; set; }
        [Display(Name = "Ward Number")]
        public string TempWardNumber { get; set; }
        [Display(Name = "Ward Number")]
        public string PermanentWardNumber { get; set; }
        [Display(Name = "DOB")]
        public DateTime? DOB { get; set; }
        public string DOBNepali { get; set; }
        [Display(Name = "Citizenship Number")]
        public string CitizenshipNumber { get; set; }
        [Display(Name = "Citizenship Number Nepali")]
        public string CitizenshipNumberNepali { get; set; }
        [Display(Name = "Gender")]
        public int Gender { get; set; }
        [Display(Name = "Marrital Status")]
        public int? MarritalStatus { get; set; }
        [Display(Name = "Blood Group")]
        public int? BloodGroup { get; set; }
        [Display(Name = "Disability Type By Nature")]
        public int DisabilityTypeByNature { get; set; }
        [Display(Name = "Disability Type By Sovernity")]
        public int DisabilityTypeBySeriousness { get; set; }
        [Display(Name = "Guardian Full Name")]
        public string GuardianFullName { get; set; }
        [Display(Name = "Guardian Full Name Nepali")]
        public string GuardianFullNameNepali { get; set; }
        [Display(Name = "Guardian Gender")]
        public int GuardianGender { get; set; }
        [Display(Name = "Municipality/VDC")]
        public long? GuardianTempAddressId { get; set; }
        [Display(Name = "Tole")]
        public string GuardianTempTole { get; set; }
        [Display(Name = "Ward Number")]
        public string GuardianTempWardNumber { get; set; }
        [Display(Name = "Guardian DOB")]
        public DateTime? GuardianDOB { get; set; }
        public string GuardianDOBNepali { get; set; }
        [Display(Name = "प्रमाणित गर्ने ")]
        [Required]
        public string ApprovedBy { get; set; }
        [Display(Name = "फोटो")]
        public string Photo { get; set; }
        [Display(Name = "अपाङ्ग व्यक्तिसंगको नाता")]
        public string GuardianRelationWithCitizen { get; set; }

        public IFormFile file { get; set; }

    }
}
