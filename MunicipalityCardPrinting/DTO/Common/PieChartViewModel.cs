﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class PieChartViewModel
    {
        public string DimensionOne { get; set; }
        public int Quantity { get; set; }
    }
}
