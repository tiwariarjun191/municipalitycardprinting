﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class DropdownModel
    {
        public long Id { get; set; }
        public string GUID { get; set; }
        public string DisplayName { get; set; }
    }
}
