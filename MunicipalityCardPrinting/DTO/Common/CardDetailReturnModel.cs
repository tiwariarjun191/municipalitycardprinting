﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class CardDetailReturnModel
    {
        public long CardId { get; set; }
        public long DisabledPersonId { get; set; }
        public long GuardianId { get; set; }
    }
}
