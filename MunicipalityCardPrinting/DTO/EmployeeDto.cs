﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class EmployeeDto
    {

        public long Id { get; set; }
        public string Employee_Name_English { get; set; }
        public string Employee_Name_Nepali { get; set; }
        public int Gender { get; set; }
        public string DOB { get; set; }

        public IFormFile PhotoFile { get; set; }
        public string Photo { get; set; }
        public int District_Id { get; set; }
        public long Vdc_Id { get; set; }
       
        public string Ward { get; set; }
        public string Office { get; set; }

        public long Shredi_Id { get; set; }
        public long Designation_Id { get; set; }
        public string Sanket_Number { get; set; }

        public string Office_Entry_Date { get; set; }

    }
}
