﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTO
{
    public class ChildBasicDetailDTO
    {
        public long Id { get; set; }
        public string CardNumber { get; set; }
        public string CardNumberNepali { get; set; }

        [Display(Name = "पुरा नाम")]
        public string NameNepali { get; set; }

        [Display(Name = "पुरा नाम (English)")]
        public string NameEnglish { get; set; }
        [Display(Name = "स्थाई प्रदेश")]
        [Required]
        public long? PermanentProvinceId { get; set; }
        [Display(Name = "स्थाई जिल्ला")]
        public long PermanentDistrictId { get; set; }
        [Display(Name = "स्थाई नगर/गाउँपालिका")]
        public long? PermanentVDCId { get; set; }
        [Display(Name = "स्थाई वार्ड")]
        public string PermanentWard { get; set; }

        [Display(Name = "अस्थाई प्रदेश")]
        [Required]
        public long? TemporaryProvinceId { get; set; }
        [Display(Name = "अस्थाई जिल्ला")]
        public long TemporaryDistrictId { get; set; }
        [Display(Name = "अस्थाई नगर/गाउँपालिका")]
        public long? TemporaryVDCId { get; set; }
        [Display(Name = "अस्थाई वार्ड")]
        public string TemporaryWard { get; set; }

      
        public string GenderNepali { get; set; }

        [Display(Name = "बाबुको नाम")]
        public string FatherNameNepali { get; set; }

        [Display(Name = "बाबुको नाम (English)")]
        public string FatherNameEnglish { get; set; }

        [Display(Name = "बाबुको ठेगाना")]
        public string FatherAddressNepali { get; set; }

        [Display(Name = "बाबुको ठेगाना (English)")]
        public string FatherAddressEnglish { get; set; }

        [Display(Name = "आमाको नाम")]
        public string MotherNameNepali { get; set; }

        [Display(Name = "आमाको नाम (English)")]
        public string MotherNameEnglish { get; set; }

        [Display(Name = "आमाको ठेगाना (English)")]
        public string MotherAddressEnglish { get; set; }

        [Display(Name = "आमाको ठेगाना")]
        public string MotherAddressNepali { get; set; }
        public string Photo { get; set; }
        public string PrintDate { get; set; }
        public int PrintCountNepali { get; set; }
        public string UserId { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedByNepali { get; set; }
        public string PositionEnglish { get; set; }
        public string PositionNepali { get; set; }
        public string FatherMotherCitizenship { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IsCardPrinted { get; set; }
        public DateTime? LastPrintDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int Gender { get; set; }
        public long Employee_Id { get; set; }
        public long ApprovedById { get; set; }
        public string Employee_Address { get; set; }
        public IFormFile file { get; set; }



        public IFormFile FatherCitizenFile { get; set; }
        public string FatherCitizen { get; set; }

        public IFormFile MotherCitizenFile { get; set; }
        public string MotherCitizen { get; set; }

        public IFormFile BirthCertificateFile { get; set; }
        public string BirthCertificate { get; set; }

        public IFormFile OtherFile { get; set; }

        public string other { get; set; }
        public virtual List<AddMoreSectionDto> AddMoreSections { get; set; } = new List<AddMoreSectionDto>();

        public class AddMoreSectionDto
        {
            public long id { get; set; }
            public long childDetailId { get; set; }

            public IFormFile fileinput { get; set; }
            public string File { get; set; }
            public string TextArea { get; set; }

            [ForeignKey(nameof(childDetailId))]
            public virtual ChildBasicDetailDTO ChildDetail { get; set; }
        }


    }
}
