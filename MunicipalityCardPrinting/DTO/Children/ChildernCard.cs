﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Children
{
    public class ChildernCard
    {

        public long Id { get; set; }
        public string CardNumber { get; set; }

        public string CardTypeCSSClass { get; set; }
        public string CardNumberNepali { get; set; }

        [Display(Name = "पुरा नाम")]
        public string NameNepali { get; set; }

        [Display(Name = "पुरा नाम (English)")]
        public string NameEnglish { get; set; }
        [Display(Name = "स्थाई प्रदेश")]
        [Required]
        public string PermanentProvinceId { get; set; }
        public string PermanentNepaliProvinceId { get; set; }
        [Display(Name = "स्थाई जिल्ला")]
        public string PermanentDistrictId { get; set; }
        public string PermanentNepaliDistrictId { get; set; }
        [Display(Name = "स्थाई नगर/गाउँपालिका")]
        public string PermanentVDCId { get; set; }
        public string PermanentNepaliVDCId { get; set; }
        [Display(Name = "स्थाई वार्ड")]
        public string PermanentWard { get; set; }

        [Display(Name = "Janma प्रदेश")]
        [Required]
        public String TemporaryProvinceId { get; set; }
        public String TemporaryNepaliProvinceId { get; set; }
        [Display(Name = "Janma जिल्ला")]
        public string TemporaryDistrictId { get; set; }
        public string TemporaryNepaliDistrictId { get; set; }
        [Display(Name = "Janma नगर/गाउँपालिका")]
        public string TemporaryVDCId { get; set; }
        public string TemporaryNepaliVDCId { get; set; }
        [Display(Name = "Janma वार्ड")]
        public string TemporaryWard { get; set; }


        public string GenderNepali { get; set; }

        [Display(Name = "बाबुको नाम")]
        public string FatherNameNepali { get; set; }

        [Display(Name = "बाबुको नाम (English)")]
        public string FatherNameEnglish { get; set; }

        [Display(Name = "बाबुको ठेगाना")]
        public string FatherAddressNepali { get; set; }

        [Display(Name = "बाबुको ठेगाना (English)")]
        public string FatherAddressEnglish { get; set; }

        [Display(Name = "आमाको नाम")]
        public string MotherNameNepali { get; set; }

        [Display(Name = "आमाको नाम (English)")]
        public string MotherNameEnglish { get; set; }

        [Display(Name = "आमाको ठेगाना (English)")]
        public string MotherAddressEnglish { get; set; }

        [Display(Name = "आमाको ठेगाना")]
        public string MotherAddressNepali { get; set; }
        public string Photo { get; set; }
        public string PrintDate { get; set; }
        public int PrintCountNepali { get; set; }
        public string PrintCountnpl { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedByNepali { get; set; }
        public string FatherMotherCitizenShip { get; set; }
        public string FatherMotherCitizenShipNepali { get; set; }
        public string PositionEnglish { get; set; }
        public string PositionNepali { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IsCardPrinted { get; set; }
        public DateTime? LastPrintDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Gender { get; set; }
        //public IFormFile file { get; set; }
    }
}
