﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class ElderCardSaveModel
    {
        public long Id { get; set; }
        public long DisabledPersonId { get; set; }
        public long GuardianId { get; set; }
        [Display(Name = "Card Number")]
        public string IdCardnumber { get; set; }
        [Display(Name = "Full Name In Nepali")]
        public string FullNameNepali { get; set; }
        [Display(Name = "Municipality/Vdc")]
        public long? TempAddressId { get; set; }
        [Display(Name = "Tole")]
        public string TempTole { get; set; }
        [Display(Name = "Ward Number")]
        public string TempWardNumber { get; set; }
        [Display(Name = "DOB")]
        public string DOBNepali { get; set; }
        [Display(Name = "Citizenship Number Nepali")]
        public string CitizenshipNumberNepali { get; set; }
        [Display(Name = "Marrital Status")]
        public int? MarritalStatus { get; set; }
        [Display(Name = "Gender")]
        public int Gender { get; set; }
        [Display(Name = "Blood Group")]
        public int? BloodGroup { get; set; }
        [Display(Name = "Guardian Full Name")]
        public string GuardianFullNameNepali { get; set; }
        [Display(Name = "Municipality/VDC")]
        public long? GuardianTempAddressId { get; set; }
        [Display(Name = "Tole")]
        public string GuardianTempTole { get; set; }
        [Display(Name = "Ward Number")]
        public string GuardianTempWardNumber { get; set; }
        [Display(Name = "Spouse Name")]
        public string SpouseName { get; set; }
        public string OtherFacilities { get; set; }
        public int CardCategory { get; set; }
        public int? CategoryOfElderness { get; set; }
        [Display(Name = "प्रमाणित गर्ने ")]
        public string ApprovedBy { get; set; }
        [Display(Name = "फोटो")]
        public string Photo { get; set; }
        public IFormFile file { get; set; }
        [Display(Name = "Disease Name")]
        public string DiseaseName { get; set; }
        [Display(Name = "Medicine Name")]
        public string MedicineName { get; set; }
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }
        
       
    }
}
