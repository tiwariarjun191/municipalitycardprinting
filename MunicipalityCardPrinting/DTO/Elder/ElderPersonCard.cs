﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ElderPersonCard
    {
        public long Id { get; set; }
        public string CardNumber { get; set; }
        public string CardNumberNepali { get; set; }
        public string NameNepali { get; set; }
        public string AddressNepali { get; set; }
        public string DOBNepali { get; set; }
        public string CitizenshipNumberNepali { get; set; }
        public string GenderNepali { get; set; }
        public string Age { get; set; }
        public string CategoryOfElderness { get; set; }
        public string Facilities { get; set; }
        public string SpouseName { get; set; }
        public string GuardianNameAddress { get; set; }
        public string BloodGroupDiseaseMedicine { get; set; }
        public string PhoneNumber { get; set; }
        public string ApproverNameNepali { get; set; }
        public string DesignationNepali { get; set; }
        public string Photo { get; set; }
        public string PrintDate { get; set; }

        public string PrintCountNepali { get; set; }
    }
}
