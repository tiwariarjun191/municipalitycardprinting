﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class HomeViewModel
    {
        public string TotalEntryOfDisabledCard;
        public string TotalPrintedDisabledCard;
        public string TotalYetToPrintDisabledCard;
        public string TotalElderPersonCard;
        public string TotalPrintedElderCard;
        public string TotalYetToPrintElderCard;
        public string TotalExtremeDisabled;
        public string TotalNormalDisabled;



        public string TotalBodyDisability;
        public string TotalVisionDisability;
        public string TotalHearingDisability;
        public string TotalVisionAndHearingDisability;
        public string TotalSpeakingDisability;
        public string TotalSocialDisability;
        public string TotalMentalDisability;
        public string TotalHemopholia;
        public string TotalOtism;
        public string TotalMultipleDisability;
        public string TotalChild;
        public string TotalChalanai;
        public string Totaldarta;
       
    }
}
