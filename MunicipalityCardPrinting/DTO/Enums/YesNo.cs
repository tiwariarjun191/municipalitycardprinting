﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Enums
{
    public enum YesNo
    {
        [Display(Name ="हो")]
        Yes=1,
        [Display(Name = "होइन")]
        No =2
    }
}
