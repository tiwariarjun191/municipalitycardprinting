﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Enums
{
    public enum OfficeSetupEnum
    {
        [Display (Name="District")]
        Name,
        [Display(Name = "District")]
        Province,
        [Display(Name = "District")]
        District,
        [Display(Name = "District")]
        VDC,
        [Display(Name = "District")]
        NameNepali,
        [Display(Name = "District")]
        ProvinceNepali,
        [Display(Name = "District")]
        DistrictNepali,
        [Display(Name = "District")]
        VDCNepali,
        [Display(Name = "District")]
        PhoneNumber,
        [Display(Name = "District")]
        PhoneNumberNepali,

    }
}
