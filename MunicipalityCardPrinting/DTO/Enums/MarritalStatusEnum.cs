﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Enums
{
    public enum MarritalStatusEnum
    {
        [Display(Name ="अविवाहित")]
        Unmarried = 1,
        [Display(Name = "विवाहित")]
        Married,
        [Display(Name = "अन्य")]
        Other
    }
   

}
