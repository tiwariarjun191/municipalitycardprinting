﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DTO.Enums
{
    public enum CardType
    {
        [Display(Name ="RedCard")]
        A=1,
        [Display(Name = "BlueCard")]
        B,
        [Display(Name = "YellowCard")]
        C,
        [Display(Name = "WhiteCard")]
        D
    }

    public class CardTypeEnumHandler
    {
        public static string ConvertToNepali(CardType type)
        {
            Dictionary<CardType, string> data = new Dictionary<CardType, string>();
            data.Add(CardType.A, "क");
            data.Add(CardType.B, "ख");
            data.Add(CardType.C, "ग");
            data.Add(CardType.D, "घ");
            if (data.ContainsKey(type))
            {
                var val = data.Where(x => x.Key == type).FirstOrDefault().Value;
                return val;
            }
            return "";
        }
    }
}
