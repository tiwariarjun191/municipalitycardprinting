﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Enums
{
    public enum DisabilityTypeOnTheBasisOfSeriousness
    {
        [Display(Name ="सामान्य")]
        Normal=1,
        [Display(Name = "चरम")]
        Extreme
    }
}
