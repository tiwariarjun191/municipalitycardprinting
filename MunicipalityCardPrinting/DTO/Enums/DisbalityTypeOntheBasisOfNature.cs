﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DTO.Enums
{
    public enum DisbalityTypeOntheBasisOfNature
    {
        [Display(Name = "Body Disability")]
        BodyDisability=1,
        [Display(Name = "Vision Disability")]
        VisionDisability,
        [Display(Name = "Hearing Disability")]
        HearingDisability,
        [Display(Name = "Vision And Hearing Disability")]
        VisionAndHearingDisability,
        [Display(Name = "Speaking Disability")]
        SpeakingDisability,
        [Display(Name = "Social Disability")]
        SocialDisability,
        [Display(Name = "Mental Disability")]
        MentalDisability,
        [Display(Name = "Hemopholia")]
        Hemopholia,
        [Display(Name = "Otism")]
        Otism,
        [Display(Name = "Multiple Disability")]
        MultipleDisability
            
    }
    public class DisabilityNatureEnumHandler
    {
        public static string ConvertToNepali(DisbalityTypeOntheBasisOfNature type)
        {
            Dictionary<DisbalityTypeOntheBasisOfNature, string> data = new Dictionary<DisbalityTypeOntheBasisOfNature, string>();
            data.Add(DisbalityTypeOntheBasisOfNature.BodyDisability, "शारीरिक अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.VisionDisability, "दृस्टी सम्बन्धि अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.HearingDisability, "सुनाइ सम्बन्धि अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.VisionAndHearingDisability, "श्रवण दृस्टी विहिन अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.SpeakingDisability, "स्वर बोलाइ सम्बन्धि अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.SocialDisability, "मानसिक वा मनोसामाजिक अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.MentalDisability, "बौद्धिक अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.Hemopholia, "अनुब्म्सीय रक्तश्राव हेमोफोलिया");
            data.Add(DisbalityTypeOntheBasisOfNature.Otism, "अटिज्म सम्बन्धि अपाङ्गता");
            data.Add(DisbalityTypeOntheBasisOfNature.MultipleDisability, "वहु अपाङ्गता");
            if (data.ContainsKey(type))
            {
                var val = data.Where(x => x.Key == type).FirstOrDefault().Value;
                return val;
            }
            return "";
        }
    }
}
