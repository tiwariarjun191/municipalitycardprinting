﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DTO.Enums
{
    public static class EnumExtension
    {
        public static string GetDisplayName(this System.Enum enumValue)
        {
            Type enumType = enumValue.GetType();

            FieldInfo fieldInfo = enumType.GetField(enumValue.ToString());

            if (fieldInfo != null)
            {
                object[] attrs = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false);
                if (attrs != null && attrs.Length > 0)
                {
                    return ((DisplayAttribute)attrs[0]).Name;
                }
            }

            return enumValue.ToString();
        }
        public static TAttribute GetAttribute<TAttribute>(this Enum value)
       where TAttribute : Attribute
        {
            var enumType = value.GetType();
            var name = Enum.GetName(enumType, value);
            return enumType.GetField(name).GetCustomAttributes(false).OfType<TAttribute>().SingleOrDefault();
        }
        public static Dictionary<object, string> GetDisplayNames(Type enumType)
        {
            Dictionary<object, string> displayNames = new Dictionary<object, string>();

            foreach (object value in System.Enum.GetValues(enumType))
            {
                FieldInfo fieldInfo = enumType.GetField(value.ToString());

                if (fieldInfo != null)
                {
                    object[] attrs = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false);
                    if (attrs != null && attrs.Length > 0)
                    {
                        displayNames[value] = ((DisplayAttribute)attrs[0]).Name;
                        continue;
                    }
                }

                displayNames[value] = value.ToString();
            }

            return displayNames;
        }

    }
}
