﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Enums
{
    public enum Gender
    {
        Male=1,
        Female,
        Other
    }
    public class GenderEnumHandler
    {
        public static string ConvertToNepali(Gender type)
        {
            Dictionary<Gender, string> data = new Dictionary<Gender, string>();
            data.Add(Gender.Male, "पुरुष");
            data.Add(Gender.Female, "महिला");
            data.Add(Gender.Other, "अन्य");
            if (data.ContainsKey(type))
            {
                var val = data.Where(x => x.Key == type).FirstOrDefault().Value;
                return val;
            }
            return "";
        }
    }
}
