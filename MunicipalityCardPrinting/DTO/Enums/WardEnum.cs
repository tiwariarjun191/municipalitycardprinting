﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Enums
{
    public enum WardEnum
    {
        [Display(Name = "१")]
        One = 1,
        [Display(Name = "२")]
        Two,
        [Display(Name = "३")]
        Three
        ,
        [Display(Name = "४")]
        Four,
        [Display(Name = "५")]
        Five
        ,
        [Display(Name = "६")]
        Six
        ,
        [Display(Name = "७")]
        Seven
        ,
        [Display(Name = "८")]
        Eight
        ,
        [Display(Name = "९")]
        Nine
        ,
        [Display(Name = "१०")]
        Ten,
        [Display(Name = "११")]
        Eleven
        ,
        [Display(Name = "१२")]
        Twelve
        ,
        [Display(Name = "१३")]
        Thirteen,

        [Display(Name = "१४")]
        Fourteen,
        [Display(Name = "१५")]
        Fifteen,

    }
}
