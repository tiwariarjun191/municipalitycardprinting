﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO.Enums
{
    public enum CategoryOfElderness
    {
        [Display (Name = "जेष्ठ नागरिक")]
        Elder,
        [Display(Name = "बरिष्ट जेष्ठ नागरिक")]
        RespectedElder,
        [Display(Name = "असहाय जेष्ठ नागरिक")]
        HelplessElder,
        [Display(Name = "अशक्त जेष्ठ नागरिक")]
        DisabledElder
    }
}
