﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DTO.Enums
{
    public enum BloodGroupEnum
    {
        [Display(Name ="A+")]
        APositive=1,
        [Display(Name = "A-")]
        ANegative,
        [Display(Name = "O+")]
        OPositive,
        [Display(Name = "O-")]
        ONegative,
        [Display(Name = "B+")]
        BPositive,
        [Display(Name = "B-")]
        BNegative,
        [Display(Name = "AB+")]
        ABPositive,
        [Display(Name = "AB-")]
        ABNegative

    }
    public class BloodGroupEnumHandler
    {
        public static string ConvertToNepali(BloodGroupEnum type)
        {
            Dictionary<BloodGroupEnum, string> data = new Dictionary<BloodGroupEnum, string>();
            data.Add(BloodGroupEnum.APositive, "ए पोजीटिभ");
            data.Add(BloodGroupEnum.ANegative, "ए नेगेटीभ");
            data.Add(BloodGroupEnum.BPositive, "बि पोजीटिभ");
            data.Add(BloodGroupEnum.BNegative, "बि नेगेटीभ");
            data.Add(BloodGroupEnum.ABPositive, "ओ पोजीटिभ");
            data.Add(BloodGroupEnum.ABNegative, "ओ नेगेटीभ");
            data.Add(BloodGroupEnum.OPositive, "एबि पोजीटिभ");
            data.Add(BloodGroupEnum.ONegative, "एबि नेगेटीभ");
            if (data.ContainsKey(type))
            {
                var val = data.Where(x => x.Key == type).FirstOrDefault().Value;
                return val;
            }
            return "";
        }
    }
}
