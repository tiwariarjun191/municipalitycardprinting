﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class CardDetailDTO
    {
        public long Id { get; set; }
        public long DisabledPersonId { get; set; }
        public long GuardianId { get; set; }
        public int CardCategory { get; set; }

        [Required]
        [Display(Name = "कार्डको प्रकार")]
        public int? IdCardType { get; set; }
        //[Display(Name = "Card Number")]
        //public string IdCardnumber { get; set; }
       
        [Display(Name = "पुरा नाम (English)")]
        public string FullName { get; set; }
        [Display(Name = "पति/पत्नीको नाम")]
        public string SpouseName { get; set; }
        [Display(Name = "पुरा नाम")]
        public string FullNameNepali { get; set; }
        [Display(Name = "अस्थाई प्रदेश")]
        [Required]
        public long? TempProvinceId { get; set; }
        [Display(Name = "अस्थाई जिल्ला")]
        public long TempDistrictId { get; set; }
        [Display(Name = "अस्थाई नगर/गाउँपालिका")]
        public long TempAddressId { get; set; }
        [Display(Name = "स्थाई प्रदेश")]
        [Required]
        public long? PermanentProvinceId { get; set; }
        [Display(Name = "स्थाई जिल्ला")]
        public long PermanentDistrictId { get; set; }
        [Display(Name = "स्थाई नगर/गाउँपालिका")]
        public long? PermanentAddressId { get; set; }
        [Display(Name = "अस्थाई टोल")]
        public string TempTole { get; set; }
        [Display(Name = "स्थाई टोल")]
        public string PermanentTole { get; set; }
        [Required]
        [Display(Name = "अस्थाई वार्ड")]
        public string TempWardNumber { get; set; }
        [Required]
        [Display(Name = "स्थाई वार्ड")]
        public string PermanentWardNumber { get; set; }
        [Display(Name = "जन्म मिति")]
        public string DOBNepali { get; set; }
        public DateTime? DOB { get; set; }
        [Display(Name = "नागरिकता नं (English)")]
        public string CitizenshipNumber { get; set; }
        [Display(Name = "नागरिकता नं")]
        public string CitizenshipNumberNepali { get; set; }
        [Display(Name = "लिङ्ग")]
        public int Gender { get; set; }
        [Display(Name = "रक्त समूह")]
        public int? BloodGroup { get; set; }
        [Display(Name = "अपाङ्गताको किसिम (प्रकृति)")]
        public int? DisabilityTypeByNature { get; set; }
        [Display(Name = "अपांगताको किसिम (गम्भीरता)")]
        public int? DisabilityTypeBySeriousness { get; set; }
        [Display(Name = "संरक्षकको नाम (English)")]
        public string GuardianFullName { get; set; }
        [Display(Name = "संरक्षकको नाम")]
        public string GuardianFullNameNepali { get; set; }
        [Display(Name = "संरक्षकको लिङ्ग")]
        public int GuardianGender { get; set; }
        [Display(Name = "प्रदेश")]
        [Required]
        public long? GuardianTempProvinceId { get; set; }
        [Display(Name = "जिल्ला")]
        public long GuardianTempDistrictId { get; set; }
        [Display(Name = "नगर/गाउँपालिका")]
        public long GuardianTempAddressId { get; set; }
        [Display(Name = "टोल")]
        public string GuardianTempTole { get; set; }
        [Display(Name = "वार्ड")]
        public string GuardianTempWardNumber { get; set; }
        [Display(Name = "अपाङ्ग व्यक्तिसंगको नाता")]
        public string GuardianRelationWithCitizen { get; set; }
        [Display(Name = "जन्म मिति")]
        public DateTime? GuardianDOB { get; set; }
        [Display(Name = "प्रमाणित गर्ने ")]
        [Required]
        public string ApprovedBy { get; set; }
        [Display(Name = "फोटो")]
        public string Photo { get; set; }



        //Additional Detail
        public long CardDetailId { get; set; }
        [Display(Name = "पेशा")]
        public string Occupation { get; set; }
        [Display(Name = "योग्यता")]
        public string Qualification { get; set; }
        [Display(Name = "स्कुल/कलेज")] 
        public string CurrentSchoolCollegeName { get; set; }
        [Display(Name = "पढाइ सकेको/पढाई नगरेको")]
        public string StudyingOrFinished { get; set; }
        [Display(Name = "कस्ता दैनिक क्रियाकलाप गर्न सकिन्छ?")]
        public string WhatKindOfWorkcanDoInDailyLife { get; set; }
        [Display(Name = "कस्ता दैनिक क्रियाकलाप गर्न सकिदैन ?")]
        public string WhatKindOfWorkCantDo { get; set; }
        [Display(Name = "सहायक सामग्री आवश्यक ?")]
        public int? IsNeededAnyEquipement { get; set; }
        [Display(Name = "आवश्यक पर्ने सामग्री")]
        public string AdditionalEquipements { get; set; }
        [Display(Name = "हाल सहायक सामग्री पाएको?")]
        public int? HasAnyAdditionalEquipement { get; set; }
        [Display(Name = "अन्य सेवा/सुबिधा")]
        public string OtherFacilities { get; set; }
        [Display(Name = "आवश्यक पर्ने अन्य सुबिधा")]
        public string OtherFacilitiesNeeded { get; set; }
        [Display(Name = "सेवा प्रदान गर्ने निकाय")]
        public string OfficeToProvideFacility { get; set; }
        [Display(Name = "अन्य")]
        public string Remarks { get; set; }
        [Display(Name = "वैवाहिक अवस्था")]
        public int? MarritalStatus { get; set; }
        [Display(Name = "Guardian Citizenship Number")]
        public string GuardianCitizenshipNumber { get; set; }
        [Display(Name = "Guardian Citizenship Number Nepali")]
        public string GuardianCitizenshipNumberNepali { get; set; }
        [Display(Name = "Guardian Blood Group")]
        public int? GuardianBloodGroup { get; set; }
        [Display(Name = "Guardian Marrital Status")]
        public int? GuardianMarritalStatus { get; set; }
       
    }
}
