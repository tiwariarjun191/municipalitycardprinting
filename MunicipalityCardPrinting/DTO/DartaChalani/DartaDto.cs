﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class DartaDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameNepali { get; set; }
        public string Subject { get; set; }
        public DateTime? DartadateEnglish { get; set; }
        public string Dartadate { get; set; }

        public DateTime? EnglishDateofReceivedLetter { get; set; }
        public string DateofReceivedLetter { get; set; }
        public string CompanyName { get; set; }

        [Display(Name = "प्रदेश")]
        [Required]
        public long? AddressId { get; set; }
        [Display(Name = "जिल्ला")]
        public long DistrictId { get; set; }
        [Display(Name = "स्थाई नगर/गाउँपालिका")]
        public long? VDCId { get; set; }
        [Display(Name = "वार्ड")]
        public string Ward { get; set; }
        public long? Faat { get; set; }
        public string CompanyAddress { get; set; }
        public string ReferenceNo { get; set; }
        public string ReceivedBy { get; set; }
        public string Remarks { get; set; }

        public string Photo { get; set; }
   
        public IFormFile file { get; set; }
    }
}
