﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Utils.Statics
{
    public static class NepaliNumberConverter
    {
        public static string ConvertToNepali(double number)
        {
            var nepaliNo = "";
            foreach (var character in number.ToString("r").ToCharArray())
            {
                nepaliNo = nepaliNo + charactermapping[character];
            }
            return nepaliNo;
        }
        public static string ConvertToNepaliWords(long number)
        {
            var converter = new NumberConverter(number, NumberConverter.ConvertStyle.Nepali);
            return converter.Convert();
        }
        public static string ConvertDigitsToNepaliDigits(string value)
        {
            if (value == null)
                return null;
            var nepaliNo = "";
            foreach (var character in value.ToString().ToCharArray())
            {
                try
                {

                    if (charactermapping.ContainsKey(character))
                    {
                        var equivalentCharacter = charactermapping[character];
                        nepaliNo = nepaliNo + equivalentCharacter;
                    }

                    else
                        nepaliNo = nepaliNo + character;
                }
                catch (Exception ex) { nepaliNo = nepaliNo + character; };
            }
            return nepaliNo;
        }

        public static string ToNepali(this string value)
        {
            if (value == null)
                return null;
            var nepaliNo = "";
            foreach (var character in value.ToString().ToCharArray())
            {
                try
                {

                    if (charactermapping.ContainsKey(character))
                    {
                        var equivalentCharacter = charactermapping[character];
                        nepaliNo = nepaliNo + equivalentCharacter;
                    }

                    else
                        nepaliNo = nepaliNo + character;
                }
                catch (Exception ex) { nepaliNo = nepaliNo + character; };
            }
            return nepaliNo;
        }
        public static string ConvertNepaliDigitsToEnglishDigits(string value)
        {
            if (value == null)
                return null;
            var englishNo = "";
            foreach (var character in value.ToString().ToCharArray())
            {
                try
                {
                    if (charactermappingNepali.ContainsKey(character))
                    {
                        var equivalentCharacter = charactermappingNepali[character];
                        englishNo = englishNo + equivalentCharacter;
                    }
                    else
                    {
                        englishNo = englishNo + character;
                    }
                }
                catch (Exception ex) { englishNo = englishNo + character; };
            }
            return englishNo;
        }
        static Dictionary<char, char> charactermapping = new Dictionary<char, char>() {
            {'0', '०'},
            {'1', '१'},
            {'2', '२'},
            {'3', '३'},
            {'4', '४'},
            {'5', '५'},
            {'6', '६'},
            {'7', '७'},
            {'8', '८'},
            {'9', '९' },
            {'.','.' },
            {',',',' },
            {'/','/' },
            {'-','-' }
            };
        static Dictionary<char, char> charactermappingNepali = new Dictionary<char, char>() {
            { '०','0'},
            { '१','1'},
            { '२','2'},
            { '३','3'},
            { '४','4'},
            { '५','5'},
            { '६','6'},
            { '७','7'},
            { '८','8'},
            { '९','9'},
            { '.','.'},
            { ',',','},
            { '/','/'},
            { '-','-'}
            };

        static Dictionary<char, char> NepaliNumberMappingToEnglish = new Dictionary<char, char>() {
            { '०','0'},
            { '१','1'},
            { '२','2'},
            { '३','3'},
            { '४','4'},
            { '५','5'},
            { '६','6'},
            { '७','7'},
            { '८','8'},
            { '९','9'},
        };

        static Dictionary<string, string> NepaliEquivalentOfEnglishCharacter = new Dictionary<string, string>() {
            {"ka", "क"},
            {"kha", "ख"},
            {"ga", "ग"},
            {"gha", "घ"}
        };

        public static string GetNepaliStringEquivalent(string EnglishValue)
        {
            string nepaliValue = EnglishValue;
            foreach (var key in NepaliEquivalentOfEnglishCharacter.Keys)
            {
                if (nepaliValue.ToLower().Contains(key.ToLower()))
                {
                    nepaliValue =
        Regex.Replace(nepaliValue, key, NepaliEquivalentOfEnglishCharacter[key], RegexOptions.IgnoreCase);
                }
            }
            return nepaliValue;

        }
        public static bool IsConvertableNepaliNumber(string NepaliNumber)
        {
            bool result = true;
            if (NepaliNumber == null)
                return false;

            foreach (var character in NepaliNumber.ToString().ToCharArray())
            {
                try
                {
                    var equivalentCharacter = NepaliNumberMappingToEnglish[character];
                    if (string.IsNullOrEmpty(equivalentCharacter.ToString()))
                        result = false;

                }
                catch (Exception ex)
                {
                    return false;
                };
            }
            return result;

        }
        public static string ConvertDigitsToNepaliCharacters(string value)
        {
            if (value == null)
                return null;
            var nepaliNo = "";
            foreach (var character in value.ToString().ToCharArray())
            {
                try
                {
                    if (kaKhaCharactermapping.ContainsKey(character))
                        nepaliNo = nepaliNo + kaKhaCharactermapping[character];
                    else
                        nepaliNo = nepaliNo + character;
                }
                catch (Exception ex) { nepaliNo = nepaliNo + character; };
            }
            return nepaliNo;
        }
        public static string ConvertDigitsToEnglishCharacters(string value)
        {
            if (value == null)
                return null;
            var EngNo = "";
            foreach (var character in value.ToString().ToCharArray())
            {
                try
                {
                    if (ABCharactermapping.ContainsKey(character))
                        EngNo = EngNo + ABCharactermapping[character];
                    else
                        EngNo = EngNo + character;
                }
                catch (Exception ex) { EngNo = EngNo + character; };
            }
            return EngNo;
        }
        public static string ConvertDigitsToKaKha(int value)
        {
            if (value == null)
                return null;
            var nepaliNo = "";
            try
            {
                nepaliNo = kaKhaCharactermapping[value].ToString();
            }
            catch (Exception ex) { return nepaliNo; };

            return nepaliNo;
        }

        public static string NumberToRoman(int number)
        {
            return
                new string('I', number)
                    .Replace(new string('I', 1000), "M")
                    .Replace(new string('I', 900), "CM")
                    .Replace(new string('I', 500), "D")
                    .Replace(new string('I', 400), "CD")
                    .Replace(new string('I', 100), "C")
                    .Replace(new string('I', 90), "XC")
                    .Replace(new string('I', 50), "L")
                    .Replace(new string('I', 40), "XL")
                    .Replace(new string('I', 10), "X")
                    .Replace(new string('I', 9), "IX")
                    .Replace(new string('I', 5), "V")
                    .Replace(new string('I', 4), "IV");
        }

        static Dictionary<int, char> kaKhaCharactermapping = new Dictionary<int, char>() {
            {1, 'क'},
            {2, 'ख'},
            {3, 'ग'},
            {4, 'घ'},
            {5, 'ङ'},
            {6, 'च'},
            {7, 'छ'},
            {8, 'ज'},
            {9, 'झ'},
            {10, 'ञ'},
            {11, 'ट'},
            {12, 'ठ'},
            {13, 'ड'},
            {14, 'ढ'},
            {15, 'ण'},
            {16, 'त'},
            {17, 'थ'},
            {18, 'द'},
            {19, 'ध'},
            {20, 'न'},
            {21, 'प'},
            {22, 'फ'},
            {23, 'ब'},
            {24, 'भ'},
            {25, 'म'},
            {26, 'य'},
            {27, 'र'},
            {28, 'ल'},
            {29, 'व'},
            {30, 'श'},
            {31, 'ष'},
            {32, 'स'},
            {33, 'ह'}
            //{34, 'क्ष'},
            //{35, 'त्र'},
            //{36, 'ज्ञ'}
        };
        static Dictionary<char, char> ABCharactermapping = new Dictionary<char, char>() {
            {'1', 'A'},
            {'2', 'B'},
            {'3', 'C'},
            {'4', 'D'},
            {'5', 'E'},
            {'6', 'F'},
            {'7', 'G'},
            {'8', 'H'},
        };
        public static Dictionary<int, string> NepaliLanguageVowels = new Dictionary<int, string>()
        {
            {1,"अ"},
            {2,"आ"},
            {3,"इ"},
            {4,"ई"},
            {5,"उ"},
            {6,"ऊ"},
            {7,"ए"},
            {8,"ऐ"},
            {9,"ओ"},
            {10,"औ"},
            {11,"अं"},
            {12,"अ:"}
        };

    }
}
