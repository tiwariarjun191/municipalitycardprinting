﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    public class NumberConverter
    {
        public NumberConverter(long amount, ConvertStyle style)
        {
            Amount = amount;
            Style1 = style;
        }

        public string Convert()
        {
            switch (Style)
            {
                case ConvertStyle.SouthAsian:
                    {
                        return SouthAsianStyle();
                    }
                case ConvertStyle.Nepali:
                    {
                        return NepaliStyle();
                    }
            }
            return string.Empty;
        }

        public string FormatNumber()
        {
            switch (this.Style)
            {
                case ConvertStyle.SouthAsian:
                    {
                        return this.FormatNumberPerLanguage("hi-IN");
                    }

                case ConvertStyle.Nepali:
                    {
                        return this.FormatNumberPerLanguage("hi-IN");
                    }
            }

            return null;
        }

        private string SouthAsianStyle()
        {
            string amountString = Amount.ToString();
            var decimalString = "";
            if (amountString.Contains("."))
            {
                decimalString = amountString.Split('.')[1];
                amountString = amountString.Split('.')[0];
                if (decimalString.Length > 2)
                {
                    decimalString = decimalString.Substring(0, 2);
                }
            }
            if (Amount == 0)
                return "Zero"; // Unique and exceptional case
            if (amountString.Length > 15)
                return "That's too long...";

            int[] amountArray = NumberToArray(amountString);

            int j = 0;
            int digit = 0;
            string result = "";
            string separator = "";
            string higherDigitAsianString = "";
            string codeIndex = "";
            for (int i = amountArray.Length; i >= 1; i += -1)
            {
                j = amountArray.Length - i;
                digit = amountArray[j];

                codeIndex = SouthAsianCodeArray[i - 1];
                higherDigitAsianString = HigherDigitSouthAsianStringArray[System.Convert.ToInt32(codeIndex.Substring(0, 1)) - 1];


                if (codeIndex == "1")
                    result = result + separator + SingleDigitStringArray[digit];
                else if (codeIndex.Length == 2 & digit != 0)
                {
                    int suffixDigit = amountArray[j + 1];

                    if (digit == 1)
                    {
                        result = result + separator + TenthDigitStringArray[suffixDigit] + " " + higherDigitAsianString;
                        i -= 1; // Skip the next round as we already looked at it
                    }
                    else if (suffixDigit == 0)
                        result = result + separator + DoubleDigitsStringArray[digit] + " " + higherDigitAsianString;
                    else
                        result = result + separator + DoubleDigitsStringArray[digit];
                }
                else if (digit != 0)
                    result = result + separator + SingleDigitStringArray[digit] + " " + higherDigitAsianString;

                separator = " ";
            }
            if (decimalString != "")
                result = result + " " + HundredNepaliDigitArray[System.Convert.ToInt32(decimalString)];
            return RemoveSpaces(result);
        }

        private string NepaliStyle()
        {
            string amountString = Amount.ToString();
            var decimalString = "";
            if (amountString.Contains("."))
            {
                decimalString = amountString.Split('.')[1];
                amountString = amountString.Split('.')[0];
                if (decimalString.Length > 2)
                {
                    decimalString = decimalString.Substring(0, 2);
                }
            }
            if (Amount == 0)
                return "शून्य"; // Unique and exceptional case
            if (amountString.Length > 15)
                return "That's too long...";

            int[] amountArray = NumberToArray(amountString);

            int j = 0;
            int digit = 0;
            string result = "";
            string separator = "";
            string higherDigitHindiString = "";
            string codeIndex = "";

            for (int i = amountArray.Length; i >= 1; i += -1)
            {
                j = amountArray.Length - i;
                digit = amountArray[j];
                codeIndex = SouthAsianCodeArray[i - 1];
                higherDigitHindiString = HigherDigitNepaliNumberArray[System.Convert.ToInt32(codeIndex.Substring(0, 1)) - 1];
                if (codeIndex == "1")
                    result = result + separator + HundredNepaliDigitArray[digit];
                else if (codeIndex.Length == 2 & digit != 0)
                {
                    int suffixDigit = amountArray[j + 1];
                    int wholeTenthPlaceDigit = digit * 10 + suffixDigit;
                    result = result + separator + HundredNepaliDigitArray[wholeTenthPlaceDigit] + " " + higherDigitHindiString;
                    i -= 1;
                }
                else if (digit != 0)
                    result = result + separator + HundredNepaliDigitArray[digit] + " " + higherDigitHindiString;

                separator = " ";
            }
            if (decimalString != "")
                result = result + " " + HundredNepaliDigitArray[System.Convert.ToInt32(decimalString)];
            return RemoveSpaces(result);
        }

        private string FormatNumberPerLanguage(string culterInfoName)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(culterInfoName);
            ci.NumberFormat.NumberDecimalDigits = 0;
            return Amount.ToString("N", ci);
        }

        private int[] NumberToArray(string amountString)
        {
            int[] amountArray = new int[amountString.Length - 1 + 1];
            for (int i = amountArray.Length; i >= 1; i += -1)
                amountArray[i - 1] = System.Convert.ToInt32(amountString.Substring(i - 1, 1));
            return amountArray;
        }

        private string RemoveSpaces(string word)
        {
            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("  ");
            return regEx.Replace(word, " ").Trim();
        }

        public long Amount { get; }

        public ConvertStyle Style
        {
            get
            {
                return Style1;
            }
        }

        public ConvertStyle Style1 { get => m_style; set => m_style = value; }

        private ConvertStyle m_style;

        private string[] SingleDigitStringArray = new[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten" };
        private string[] DoubleDigitsStringArray = new[] { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        private string[] TenthDigitStringArray = new[] { "Ten", "Eleven", "Tweleve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };

        private string[] HundredNepaliDigitArray = new[] { "", "एक", "दुई", "तीन", "चार", "पाँच", "छ", "सात", "आठ", "नौ", "दश", "एघार", "बाह्र", "तेह्र", "चौध", "पन्ध्र", "सोह्र", "सत्र", "अठार", "उन्नाईस", "बीस", "एक्काईस", "बाईस", "तेईस", "चौबीस", "पच्चीस", "छब्बीस", "सत्ताईस", "अठ्ठाईस", "उनन्तीस", "तीस", "एकतीस", "बत्तीस", "तेत्तीस", "चौंतीस", "पैंतीस", "छत्तीस", "सैंतीस", "अठ्तीस", "उनन्चालीस", "चालीस", "एकचालीस", "बयालीस", "त्रिचालीस", "चौवालीस", "पैंतालीस", "छयालीस", "सत्चालीस", "अठ्चालीस", "उनन्चास", "पचास", "एकाउन्न", "बाऊन्न", "त्रिपन्न", "चौवन्न", "पचपन्न", "छप्पन्न", "सन्त्तावन्न", "अन्ठावन्न", "उनन्साठी", "साठी", "एकसठ्ठी", "बैसठ्ठी", "त्रिसठ्ठी", "चौंसठ्ठी", "पैंसठ्ठी", "छैसठ्ठी", "सत्सठ्ठी", "अठ्सठ्ठी", "उनन्सत्तरी", "सत्तरी", "एकहत्तर", "बहत्तर", "त्रिहत्तर", "चौहत्तर", "पचहत्तर", "छैहत्तर", "सतहत्तर", "अठहत्तर", "उनान्असी", "असी", "एकासी", "बयासी", "त्रियासी", "चौरासी", "पचासी", "छयासी", "सत्तासी", "अठासी", "उनानब्बे", "नब्बे", "एकानब्बे", "बयानब्बे", "त्रियानब्बे", "चौरानब्बे", "पंचानब्बे", "छयानब्बे", "सन्त्तानब्बे", "अन्ठानब्बे", "उनान्सय" };
        private string[] HigherDigitNepaliNumberArray = new[] { "", "", "सय", "हजार", "लाख", "करोड़", "अरब", "खरब", "नील" };

        private string[] HigherDigitSouthAsianStringArray = new[] { "", "", "Hundred", "Thousand", "Lakh", "Karod", "Arab", "Kharab", "Neel" };
        private string[] SouthAsianCodeArray = new[] { "1", "22", "3", "4", "42", "5", "52", "6", "62", "7", "72", "8", "82", "9", "92" };
        private string[] EnglishCodeArray = new[] { "1", "22", "3" };

        public enum ConvertStyle
        {
            SouthAsian,
            Nepali
        }
    }
}
