﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Utils
{
    public interface IFileHelper1
    {
        string saveImageAndGetFileName(IFormFile file, string file_prefix = "");
        bool isImageValid(string file_name);
        bool isExcelFileValid(string file_name);
    }
}
