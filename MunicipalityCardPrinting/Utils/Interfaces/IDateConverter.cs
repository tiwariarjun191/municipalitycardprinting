﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MunicipalityCardPrinting.Utils
{
    public interface IDateConverter
    {
        DateTime ToAD(string gDate);
        string ToBS(System.DateTime gDate);
        int getLastDayOfMonthNep(int year, int month);
        string getCurrentNepaliDate();

        int getFiscalYear(System.DateTime givenDate);
        System.DateTime getStartDateOfFiscalYear(int fiscal_year);
    }
}
